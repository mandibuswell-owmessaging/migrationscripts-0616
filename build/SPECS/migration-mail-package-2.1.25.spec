Summary: Mail migration tool
Name: owm-migration-mail-package
Version: 2.1.25
Release: 1
License: Commercial
Group: Synchronoss/Migration
Source: http://home.openwave.com/~devbuild/repository/com/opwvmsg/migration/migration-package/2.1.25/migration-package-2.1.25.tar.gz
Requires: jdk > 1.7.0
prefix: /home/imail
Patch0: migration-executor-2.1.25-a41de7d5.tar.gz

%define __jar_repack %{nil}
%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

%description
Mail migration tool

%prep
rm -rf $RPM_BUILD_DIR/migration-package
rm -rf $RPM_BUILD_DIR/migration-package-%{version}
tar -zxf $RPM_SOURCE_DIR/migration-package-%{version}.tar.gz
rm $RPM_BUILD_DIR/migration-package-%{version}/batchmigrate.bat
rm $RPM_BUILD_DIR/migration-package-%{version}/bulkUpload.bat
rm $RPM_BUILD_DIR/migration-package-%{version}/migrate.bat
rm $RPM_BUILD_DIR/migration-package-%{version}/config/*
rmdir $RPM_BUILD_DIR/migration-package-%{version}/config
tar -C migration-package-%{version}/lib -zxf $RPM_SOURCE_DIR/migration-executor-2.1.25-a41de7d5.tar.gz

%build
if grep -q '^echo $CLASSPATH' $RPM_BUILD_DIR/migration-package-%{version}/migrate.sh; then
    sed -i '/^echo $CLASSPATH/a HEAP=__FINAL_HEAP__\necho Heap: ${HEAP} MB' $RPM_BUILD_DIR/migration-package-%{version}/migrate.sh
else
    echo 'ERROR: Cannot find "echo $CLASSPATH" in migrate.sh in order to add heap size'
    exit 1
fi
if grep -q '^java com.opwvmsg' $RPM_BUILD_DIR/migration-package-%{version}/migrate.sh; then
    sed -i 's/^java com.opwvmsg.migration.executor.MigratorApplication/java -Xms${HEAP}m -Xmx${HEAP}m com.opwvmsg.migration.executor.MigratorApplication/' $RPM_BUILD_DIR/migration-package-%{version}/migrate.sh
else
    echo 'ERROR: Cannot find "java com.opwvmsg" in batchmigrate.sh in order to add heap size'
    exit 1
fi
if grep -q '^HEAP=' $RPM_BUILD_DIR/migration-package-%{version}/batchmigrate.sh; then
    sed -i 's/^HEAP=.*$/HEAP=__SYNC_HEAP__/' $RPM_BUILD_DIR/migration-package-%{version}/batchmigrate.sh
else
    echo 'ERROR: Cannot find a line starting with "HEAP=" in batchmigrate.sh in order to change heap size'
    exit 1
fi

%install
for PKG in FINAL SYNC OWMFINAL OWMSYNC; do
  rm -rf $RPM_BUILD_ROOT/%{prefix}/migration-package-${PKG}
  mkdir -p $RPM_BUILD_ROOT/%{prefix}/migration-package-${PKG}
  tar --directory=$RPM_BUILD_DIR/migration-package-%{version} -cf - . | tar --directory=$RPM_BUILD_ROOT/%{prefix}/migration-package-${PKG} -xf -
  chmod -R o-rwx $RPM_BUILD_ROOT/%{prefix}/migration-package-${PKG}
done
rm -rf $RPM_BUILD_DIR/migration-package-%{version}
rm $RPM_BUILD_ROOT/%{prefix}/migration-package-FINAL/batchmigrate.sh
rm $RPM_BUILD_ROOT/%{prefix}/migration-package-OWMFINAL/batchmigrate.sh

%clean
for PKG in FINAL SYNC OWMFINAL OWMSYNC; do
  rm -rf $RPM_BUILD_ROOT/%{prefix}/migration-package-${PKG}
done
rmdir $RPM_BUILD_ROOT/%{prefix}
rmdir $RPM_BUILD_ROOT/home
rmdir $RPM_BUILD_ROOT

%files
/%{prefix}/migration-package-SYNC
/%{prefix}/migration-package-FINAL
/%{prefix}/migration-package-OWMSYNC
/%{prefix}/migration-package-OWMFINAL

%changelog

%pre
echo "Running 'pre-install'..."
usage()
{
    echo ""
    echo "Available environment variables:"
    echo "HELP=<0|1> default:0  print usage message if 1"
    echo "INSTALL_USER=<user> default: migration"
    echo "INSTALL_GROUP=<group> default: imail"
    echo "SYNC_HEAP=<size in mb for preload/sync> default: 4096, minimum: 4096"
    echo "FINAL_HEAP=<size in mb for finals> default: 1024, minimum: 1024"
    echo ""
    exit 1
}

echo_err()
{
    echo "######################################################################"
    echo ""
    echo "ERROR: $*"
    echo ""
    echo "######################################################################"
}

if [ "${HELP}" = "1" ]; then
    usage
fi

if [ -z "${INSTALL_USER}" ]; then
    INSTALL_USER=migration
fi

if ! id -u ${INSTALL_USER} >/dev/null 2>&1 ; then
    echo_err "The user '${INSTALL_USER}' does not exist. Please check the 'INSTALL_USER' environment variable."
    usage
fi

if [ -z "${INSTALL_GROUP}" ]; then
    INSTALL_GROUP=imail
fi

if ! id ${INSTALL_USER} | cut -d ' ' -f3 | cut -d '=' -f2 | grep -q -w "${INSTALL_GROUP}" ; then
    echo_err "The group '${INSTALL_GROUP}' does not exist. Please check the 'INSTALL_GROUP' environment variable."
    usage
fi

if [ ! -d "${RPM_INSTALL_PREFIX0}" ]; then
    echo_err "The directory '${RPM_INSTALL_PREFIX0}' does not exist. Did you specify a valid '--prefix'?"
    usage
fi

OWNER=`stat -c %U:%G ${RPM_INSTALL_PREFIX0}`
if [[ "${OWNER}" != "${INSTALL_USER}:${INSTALL_GROUP}" ]]; then
    echo_err "The directory '${RPM_INSTALL_PREFIX0}' is not owned by '${INSTALL_USER}:${INSTALL_GROUP}'."
    usage
fi

if [ -n "${SYNC_HEAP}" ]; then
    if [ ${SYNC_HEAP} -lt 4096 ]; then
        echo_err "The SYNC_HEAP setting cannot be less than 4096 megabytes"
        usage
    fi
fi

if [ -n "${FINAL_HEAP}" ]; then
    if [ ${FINAL_HEAP} -lt 1024 ]; then
        echo_err "The FINAL_HEAP setting cannot be less than 1024 megabytes"
        usage
    fi
fi

echo "Installing in '${RPM_INSTALL_PREFIX0}' as '${INSTALL_USER}:${INSTALL_GROUP}'."

%post

echo "Running 'post-install'..."
if [ -z "${INSTALL_USER}" ]; then
    INSTALL_USER=migration
fi
if [ -z "${INSTALL_GROUP}" ]; then
    INSTALL_GROUP=imail
fi

if [ -z "${SYNC_HEAP}" ]; then
	SYNC_HEAP=4096
fi

if [ -z "${FINAL_HEAP}" ]; then
	FINAL_HEAP=1024
fi

for PKG in FINAL SYNC OWMFINAL OWMSYNC; do
    chown -R ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/migration-package-${PKG}
done

echo "Setting heap size for preloads to be ${SYNC_HEAP} megabytes"
for FILE in ${RPM_INSTALL_PREFIX0}/migration-package-*/batchmigrate.sh; do
    sed -i "s/__SYNC_HEAP__/${SYNC_HEAP}/" ${FILE}
done

echo "Setting heap size for finals to be ${FINAL_HEAP} megabytes"
for FILE in ${RPM_INSTALL_PREFIX0}/migration-package-*/migrate.sh; do
    sed -i "s/__FINAL_HEAP__/${FINAL_HEAP}/" ${FILE}
done

echo "Post install complete"
