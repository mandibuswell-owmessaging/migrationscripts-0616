#!/bin/sh

cd SOURCES

OLDTAR=migration-package-${OLDVER}.tar.gz
OLDDIR=migration-package-${OLDVER}/config
NEWTAR=migration-package-${NEWVER}.tar.gz
NEWDIR=migration-package-${NEWVER}/config

if [ ! -f ${OLDTAR} ]; then
	echo "ERROR: \"${OLDTAR}\" does not exist"
	exit 1;
fi

if [ ! -f ${NEWTAR} ]; then
	echo "ERROR: \"${NEWTAR}\" does not exist"
	exit 1;
fi

tar -zxf ${OLDTAR} ${OLDDIR}
tar -zxf ${NEWTAR} ${NEWDIR}

CHANGES=0
for OLDFILE in ${OLDDIR}/*; do
	FILENAME=`echo "${OLDFILE}" | sed "s/migration-package-${OLDVER}//"`
	NEWFILE=`echo "${OLDFILE}" | sed "s/${OLDVER}/${NEWVER}/"`
	if [ ! -f ${NEWFILE} ]; then
		echo "File \"${FILENAME}\" from \"${OLDTAR}\" does not exist in \"${NEWTAR}\""
		CHANGES=1
	else
		diff -q ${OLDFILE} ${NEWFILE}
		if [ $? -gt 0 ]; then
			CHANGES=1
		fi
	fi
done

for NEWFILE in ${NEWDIR}/*; do
	FILENAME=`echo "${NEWFILE}" | sed "s/migration-package-${NEWVER}//"`
	OLDFILE=`echo "${NEWFILE}" | sed "s/${NEWVER}/${OLDVER}/"`
	if [ ! -f ${OLDFILE} ]; then
		echo "File \"${FILENAME}\" from \"${NEWTAR}\" does not exist in \"${OLDTAR}\""
		CHANGES=1
	fi
done

if [ ${CHANGES} -ne 0 ]; then
	echo "Differences detected in the migration packages, not removing extracted files"
else
	rm -rf "${OLDDIR}" "${NEWDIR}"
	echo "No differences detected.  Extracted files cleaned up."
fi