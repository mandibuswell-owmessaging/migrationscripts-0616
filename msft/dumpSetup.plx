#!/usr/bin/perl
###########################################################################
#
#  Copyright 2014 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     dumpSetup.plx
#  Description: Script to unpack a user profile dump file from Microsoft to
#               allow faster searching for data related to a PUID.
#  Input:       user profile dump file from Microsoft
#  Author:      Andrew Perkins <andrew.perkins@owmessaging.com>
#  Date:        1Q/2015
#  Version:     1.0.0 - Mar 3, 2015
#
#
#
#  Version History:
#  1.1 Jul 6 2015 : Mandi Buswell : Modify to use Log4perl
#
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use strict;
use warnings;
use Getopt::Long;
use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;
use Log::Dispatch::FileRotate;
use File::Path qw( rmtree );
use File::Pid::Quick qw( manual );
use URI::Escape;
#use Cwd;

# A package that provides an easy way to have a config file
use Config::Simple;

###########################################################################
# Global variables
###########################################################################

$| = 1;    ## Makes STDOUT flush immediately
my $SDEBUG;  #MCB: use SDEBUG so as not to interfer with imported log4perl scalars 

our $configPath = 'conf/migrator.conf';
our $configLog = 'conf/log.conf';

# Logging object.
Log::Log4perl->init($configLog);
our $logger = get_logger("MSFT.SETUP");

# Config object.
# And read the config file so all subs have access to params.
my $cfg  = new Config::Simple();
$cfg->read($configPath)
  or $logger->logdie("SCRIPT|FATAL: ".$cfg->error()."\nINFO: Please fix the issue and restart the migration program. Exiting.\n\n");

  
# Catch Terminate and interupt signals

$SIG{INT}  = sub { $logger->logdie("SCRIPT|SCRIPT INTERRUPT SIGNAL HAS BEEN CAUGHT... processing stopped before completion."); };
$SIG{TERM} = sub { $logger->logdie("SCRIPT|SCRIPT INTERRUPT SIGNAL HAS BEEN CAUGHT... processing stopped before completion."); };


my $LOGSTRING = "SCRIPT|";

# Write PID file
my $pidFile = $cfg->param('dump_pid');
File::Pid::Quick->check($pidFile);

######################################################
# USAGE
######################################################
sub usage {

	print STDERR qq(

  Utility to unpack the five user profile dump files from Microsoft to allow faster searching for data related to a given PUID.
  It also does some preprocessing on two files types and creates a '.ready' files.

  The user profile dump files are:
  Partner_Account.csv, Partner_MailForwarding.csv, Partner_Signatures.csv, Partner_UserLists.csv and Partner_VacationReplies.csv.

  Utility takes data in each of the five dump files and puts it in an easily searchable directory path / file structure. It does this 
  by taking the last three characters in the PUID, found in each line of the dump files, and uses it as a hash for the directory path.

  For example:

  In Partner_Account.csv dump file, the format of the file is:
  <email>,<PUID>,<language>,<timezone>

  If the first four lines in Partner_Account.csv were:

  franledamun\@openwave.net,0003BFFD9521AA36,EN,GMT
  annieandjeremy\@openwave.com,0003BFFD84A04C0C,EN,GMT
  desjardinsgaetan\@owm.net,0003BFFDB0522BAC,EN,
  amir1\@owm.net,00037FFE9ACA7C0C,EN,GMT
  ... etc ...

  These four lines would be written to the following files:

  <userProfileUnpack> / A36 / Partner_Account    <-- 1st data line written to this file.
                      / C0C / Partner_Account    <-- 2nd and 4th data lines written to this file.
                      / BAC / Partner_Account    <-- 3rd data line written to this file.

  Command line:

  # dumpSetup.plx -target <userProfileUnpack> [-d]
  where:
  <userProfileUnpack>    is the root directory under which all the unpack data will be written.

  Configuation setting:
  Script reads configuration file at ./conf/migration.conf. The user profile data file setting are 
  part of this confiuration:
  in_userlist, in_mailforward, in_account, in_vacation and in_signature

  Log file:
  Log events are written to ./log/dumpSetup.log 

);

	exit(0);
}

############################################################
# explodeFile
############################################################
sub explodeFile($$$)
{
  my $inFilePath = shift;
  my $targetDir = shift;
  my $expectedNumFields = shift;
  my $count = 0;
  my $logEventFreq = 100000;

  my(@filePathParts) = split('/',$inFilePath);
  my $filename = $filePathParts[-1];
  my($fileType,$suffix) = split(/\./,$filename);

  $logger->info("$LOGSTRING: Processing $filename and verifying there are $expectedNumFields fields in each entry.");

  open(my $FILE, "<", $inFilePath)
    || die("Couldn't open input file : $inFilePath");

  while(<$FILE>) {

    my $line = $_;

    my $commaCount = $line =~ tr/,//;
    my $fieldsCount = $commaCount + 1;

    if ( $expectedNumFields != $fieldsCount ) {
      $logger->warn("SCRIPT|Misformated entry in $filename : $line");
      print("WARNING:: Misformated entry in $filename. See log for details\n");
      next;
    }

    my($email,$puid,@otherData) = split(/,/,$line);
    my $subDir = substr($puid,-3);
    my $newFile = "$targetDir/$subDir/$fileType";

    mkdir("$targetDir/$subDir");
    open(my $NEWFILE, ">>", $newFile)
      || die("Couldn't open output file : $newFile");
    print $NEWFILE $line;

    $count++;
    if( ($count % $logEventFreq) == 0 ) {
      $logger->info("$LOGSTRING: Processing line $count in $filename.");
      print("$LOGSTRING: Processing line $count in $filename.\n");
    }
  }
}

############################################################
## - PreProcessUserlist -                                  #
##   Pre-process the user list file to group block / allow #
##   fiters                                                #
############################################################
sub PreProcessUserlist
{
  my $in_file = shift;
  my $tmp_file = shift;
  my $allow = "";
  my $block = "";
  my $currentEmail = "";
  my $userCount = 0;
  my $email;
  my $puid;
  my $listType;
  my $filterStr;

  # Delete any old tmp file.
  unlink $tmp_file;

  $logger->info("SCRIPT|Pre-processing $in_file and creating $tmp_file");

  open(my $fh_in_file, "<", $in_file)
    or die "\nFATAL: Failed to open input data file: $in_file\n\n";

  open(my $fh_tmp_file, ">>", $tmp_file)
    or die "\nFATAL: Failed to open tmp data file for writing / appending: $tmp_file\n\n";

  # Move pointer back to start of file
  seek($fh_in_file,0,0);
  while(<$fh_in_file>) {

    my $user_data_line = $_;
    chomp($user_data_line);
    $user_data_line =~ s/\r//g;    # Remove ^M charactors.

    ($email,$puid,$listType,$filterStr) = split(/,/,$user_data_line);

    # The file should be sorted so all entries are in groups of email addresses. If this is
    # NOT the case, this script will not perform as expected.
    #
    # If the email changes, perform final step on the last email allow / block string and initialize next email.
    if($currentEmail ne $email) {

      # For last email allow / block string, remove the ';' delimitor at the start and write to tmp file
      if($block =~ m/=:/) {
        $block =~ s/=:/=/;
        print $fh_tmp_file ($block."\n");
      }
      if($allow =~ m/=:/) { 
        $allow =~ s/=:/=/;
        print $fh_tmp_file ($allow."\n");
      }

      # For next email allow / block string, initialize it.
      $userCount++;
      $currentEmail = $email;
      $block = "$email,$puid,blocked=";
      $allow = "$email,$puid,allowed=";

    }
	$LOGSTRING = "$email|$puid";
	#some filtered email address strings include '=' or '+' sign that need URL encoding ie: noreply=mimco.com.au@mail298.us2.mcsv.net or update+z2zfyjac@facebookmail.com
	#add configurable options for how to handle these scenarios
	my $skipEntry = "false";
	$filterStr = uri_escape( $filterStr );
	$filterStr =~ s/%40/@/;
    #do not include the users own email address in any lists 
    if ( $filterStr eq $currentEmail ) {
        $skipEntry = "true";
    }

	unless ($skipEntry eq "true") {
		if ($listType == 0) {
		  $block .= ":" . $filterStr;
		} elsif ($listType == 1) {
		  $block .= ":" . $filterStr;
		} elsif ($listType == 2) {
		  $allow .= ":" . $filterStr;
		} elsif ($listType == 3) {
		  $allow .= ":" . $filterStr;
		}
	}
  }

  # For last email allow / block string, remove the ';' delimitor at the start and write to tmp file
  if($block =~ m/=:/) {
    $block =~ s/=:/=/;
    print $fh_tmp_file ($block."\n");
  }
  if($allow =~ m/=:/) {
    $allow =~ s/=:/=/;
    print $fh_tmp_file ($allow."\n");
  }

  close($fh_tmp_file);
  close($fh_in_file);

}

############################################################
## - PreProcessMailForward -                               #
############################################################
sub PreProcessMailForward
{
  my $in_file = shift;
  my $tmp_file = shift;
  my %puidAlreadyUsed;

  # Delete any old tmp file.
  unlink $tmp_file;

  $logger->info("$LOGSTRING: Pre-processing $in_file and creating $tmp_file");

  open(my $fh_in_file, "<", $in_file)
    or die "\nFATAL: Failed to open input data file: $in_file\n\n";

  open(my $fh_tmp_file, ">>", $tmp_file)
    or die "\nFATAL: Failed to open tmp data file for writing / appending: $tmp_file\n\n";

  # Move pointer back to start of file
  seek($fh_in_file,0,0);
  while(<$fh_in_file>) {

    my $user_data_line = $_;
    chomp($user_data_line);
    $user_data_line =~ s/\r//g;    # Remove ^M charactors.

    my ($email,$puid,@other) = split(/,/,$user_data_line);

    if (exists $puidAlreadyUsed{$puid}) {
      # print("$email,$puid:Data line already found, so skipping : $user_data_line\n");
    } else {
      $puidAlreadyUsed{$puid} = "found";
      print $fh_tmp_file "$user_data_line\n";
    }

  }

  close($fh_tmp_file);
  close($fh_in_file);

}


############################################################
# Start Main
############################################################

$SDEBUG = $cfg->param('log_debug');

my @inData;
my $targetDir;

GetOptions( '-d',\$SDEBUG,               # Override debug in config file.
            'target=s',\$targetDir,
            ) || usage();

if ( (! defined $targetDir) || ($targetDir eq '') ) {
  usage();
}

if ($SDEBUG) {
  print("Running in DEBUG MODE\n");
  $logger->level($DEBUG);               #Override debug in log file when -d or debug set in config file
}

# Get location of log file.
$logger->debug("$LOGSTRING: Running in DEBUG MODE");


# Check configuration file is set up to use input files that exist.
# These are the MSFT user profile dump files.
my %fileInfo1 = ( fileLocation => $cfg->param('in_account'), fileNumFields => '4' );
push(@inData,\%fileInfo1);

my %fileInfo2 = ( fileLocation => $cfg->param('in_mailforward'), fileNumFields => '6' );
push(@inData,\%fileInfo2);

my %fileInfo3 = ( fileLocation => $cfg->param('in_vacation'), fileNumFields => '8' );
push(@inData,\%fileInfo3);

my %fileInfo4 = ( fileLocation => $cfg->param('in_signature'), fileNumFields => '4' );
push(@inData,\%fileInfo4);

my %fileInfo5 = ( fileLocation => $cfg->param('in_userlist'), fileNumFields => '4' );
push(@inData,\%fileInfo5);

foreach my $thisInData (@inData) {
  my $thisInFile = $thisInData->{fileLocation};
  if (! -e $thisInFile) {
    $logger->error("$LOGSTRING: Input file doesn't exist : $thisInFile");
    $logger->error("$LOGSTRING: Please check configuration");
    die("$LOGSTRING: Input file doesn't exist : $thisInFile");
  } else {
    $logger->info("$LOGSTRING: Input file exist : $thisInFile");
  }
}

# If the target directory already exist, warn it will be deleted.
if (-e $targetDir) {
  print("\nWARNING :: This script will DELETE current $targetDir.");
  print("\n           If a migration is in progress this may have unintended results.");
  print("\n           Do you want to continue? (y/n) [n] : ");
  my $userinput =  <STDIN>;
  chomp $userinput;
  if($userinput ne 'y') {
    print("\n\nExiting.\n\n");
    exit;
  }
  print("\n\nContinuing...\n\n");
  $logger->info("$LOGSTRING: Deleting current $targetDir");
  rmtree($targetDir);
}

# Create the target directory where input files will be unpacked too.
$logger->info("SCRIPT|Create $targetDir");
mkdir($targetDir) || die ( "$LOGSTRING: Error creating dir $targetDir" );

# Start unpacking input files.
foreach my $thisInData (@inData) {
  my $thisInFile = $thisInData->{fileLocation};
  my $thisInFields = $thisInData->{fileNumFields};
  $logger->info("SCRIPT|Start unpacking : $thisInFile");
  print("Start unpacking : $thisInFile\n");
  explodeFile($thisInFile,$targetDir,$thisInFields);
}

# Now do some preprocessing so UserLists and MailForwarding
# file are in a format better suited for the migration script
# to work on. The result is a Partner_UserLists.ready and
# Partner_MailForwarding.ready file in each of the subdirs
# of $targetDir

# For subdirs under $targetDir...
my @dirs = split(/\n/,`find $targetDir/* -type d`);
foreach my $dir (@dirs) {

  print("Pre-processing Partner_UserLists and Partner_MailForwarding in $dir\n");
  my $inFile;
  my $inFileSorted;
  my $tmpFile;

  # Preprocessing UserLists
  $inFile = $dir . '/Partner_UserLists';
  if (-e $inFile) {
    $inFileSorted = $inFile . '.sort';
    $tmpFile = $inFile . '.ready';
    `sort $inFile > $inFileSorted`;
    PreProcessUserlist($inFileSorted,$tmpFile);
    unlink($inFileSorted);
  }

  # Preprocessing MailForwarding
  $inFile = $dir . '/Partner_MailForwarding';
  if (-e $inFile) {
    $inFileSorted = $inFile . '.sort';
    $tmpFile = $inFile . '.ready';
    `sort $inFile > $inFileSorted`;
    PreProcessMailForward($inFileSorted,$tmpFile);
    unlink($inFileSorted);
  }
}

$logger->info("$LOGSTRING: Dump file unpacking & preprocessing completed");
print("Dump file unpacking & preprocessing completed\n");


# Exit script gracefully.
print "\n";    # This just makes things look clean


######################################################
# End Main
######################################################

exit 0;
