#!/usr/bin/perl -w
# Include all libraries that all the scripts use as a precheck
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use ExtUtils::Installed;
my $inst    = ExtUtils::Installed->new();
my $module;
my @modules = $inst->modules();
print "Full install list (via cpan): \n";
 foreach $module (@modules){
      print $module . "\n";
}



print ("checking required modules .... \n\n");
#CORE modules
use Carp;
use Data::Dumper;
use Digest::MD5;
use File::Basename;
use File::Copy;
use File::Path qw( rmtree );
use FileHandle;
use Getopt::Long;
use POSIX qw(strftime);
use Sys::Hostname;
use Time::Local;

use strict;
use utf8;
use vars;
use warnings;
use Cwd;

#Application modules
use Config::Simple;
use Crypt::Mode::ECB; 
use Crypt::OpenSSL::RSA;
use Crypt::CBC;
use Crypt::Cipher::AES;
use Data::ICal;
use Date::Format;
use DateTime::TimeZone;
use DateTime;
use DBI;
use Email::MIME;
use Email::Valid;
use Encode;
use File::Pid::Quick qw( manual );
use File::Temp;
use HTML::Entities;
use HTML::Strip;
use HTTP::Request::Common;
use HTTP::Request;
use HTTP::Response;
use IO::Select; 
use IPC::Open3;
use JSON;
use Log::Dispatch::FileRotate;  
use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;
use LWP::Protocol::https;
use LWP::UserAgent;
use LWP;
use Mail::IMAPClient;
use MIME::Base64;
use Net::LDAP::Constant;
use Net::LDAP;
use Net::Telnet; 
use Parallel::Fork::BossWorker;
use SOAP::Lite;
use Symbol;
use Term::ProgressBar;
use Text::CSV;
use Time::HiRes;
use URI;
use UUID::Generator::PurePerl;
use UUID::Object;
use XML::LibXML;
use XML::Simple;

#Custom Modules
use com::owm::CALClient;
use com::owm::imapAppend;
use com::owm::Logger;       
use com::owm::MicrosoftPwd;
use com::owm::MicrosoftPwd_SOAP;
use com::owm::migBellCanada;
use com::owm::migMyInbox;
use com::owm::migTelstraEvents;
use com::owm::mosApi;
use com::owm::Mx8toMx9;
use com::owm::OlsonToVTimezone;
use com::owm::OxHttpApi;        
use com::owm::PABClient;

print "\n\nWhat does INC say \n \"@INC\"\n";


  print "\n";