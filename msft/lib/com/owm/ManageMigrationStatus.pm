#!/usr/bin/perl
###########################################################################
#
#  Copyright 2015 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <huanwen.jiao@owmessaging.com>
#  Date:        Sep/2015
#  Version:     1.0.0 - Sep 23 2015
#
###########################################################################

package com::owm::ManageMigrationStatus;
use SOAP::Lite;

use MIME::Base64;
use strict;
use Date::Format;
use DateTime;
use DateTime::Format::XSD;
use Sys::Hostname;

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

sub fault_($$) {

	my ($email, $log) = @_;
	$log->error("$email UpdateMailboxStatus soap call failed.");
	return;
}


sub ManageMigrationStatus($$$$$$$$$){
	my ($proxy, $wsdl, $email, $eventId ,$jobId, $jobDesc, $mailboxsequence, $mailboxCount, $log) = @_;
    my $server = hostname();
    my @h = split (/\./,$server);
    $server = $h[0];

    # silence a warning from SOAP::Lite about redefining
    # subroutines if the same process calls this routine
    # for different users
    local $SIG{'__WARN__'} = sub { 
        if ($_[0] =~ /Subroutine.*redefined/) 
        {
            return;
        }
        print $_[0];
        return;
    };

    if ($eventId == 197) {
    	$eventId = 101;
    } elsif ($eventId >= 150) {
        $eventId = 151;
    } elsif ($eventId == 102) {
        $eventId = 100;
    } elsif ($eventId == 103) {
        $eventId = 101;
    } elsif (($eventId != 100) && ($eventId != 101)) {
        $log->error($email.'|ManageMigrationStatus passed unknown event ID "'.$eventId.'".  Only event ID 100, 101 and greater than 149 (errors) are recognised.  SOAP call will likely fail.');
    }

	my $uri = 'http://telstra.com/sdfcore/bsc/mailboxMigration/mailboxStatusManagement/v1';
	my $soap = SOAP::Lite ->readable(1)  ->uri($uri)  ->service($wsdl) ->proxy($proxy);

	# my $ug = UUID::Generator::PurePerl->new();
	# my $uuid = $ug->generate_v3(uuid_ns_dns, 'www.telstra.com')->as_string();
	my $sessionId = "migration_jobid_$jobId";
    my $timeStamp = DateTime::Format::XSD->format_datetime( DateTime->now);
	
	my $requestApplicationLabel =	SOAP::Header->name('requestApplicationLabel' => 'openwave_migration_01') -> prefix("v1");
	my $requestSessionID =	SOAP::Header->name('requestSessionID' => $sessionId)-> prefix("v1");
	my $requestTransactionID = SOAP::Header->name('requestTransactionID' => $email)-> prefix("v1");
	my $requestTimestamp =	SOAP::Header->name('requestTimestamp' => $timeStamp)-> prefix("v1");
	my $requestorId =	SOAP::Header->name('requestorId' => $server)-> prefix("v1");
	my $SDFConsumerRequestHeader = SOAP::Header->name('SDFConsumerRequestHeader')-> value (\SOAP::Header->value($requestApplicationLabel,$requestSessionID,$requestTransactionID,$requestTimestamp,$requestorId));
	$SDFConsumerRequestHeader ->  prefix("v1");
	$soap->headerattr({'xmlns:v1' => 'http://telstra.com/sdfcore/SDFConsumerRequestHeader/v1_0'});
	$soap->bodyattr({ 'xmlns:v11' => 'http://telstra.com/sdfcore/bsc/mailboxMigration/mailboxStatusManagement/v1' });
	
	my $status = SOAP::Data->name('Mailbox' => \SOAP::Data->value(
        SOAP::Data->name('MailboxEmailAddress' => $email) -> prefix("v11"),
		SOAP::Data->name('EventLog' => \SOAP::Data->value(
			 SOAP::Data->name('Event' => \SOAP::Data->value(
				SOAP::Data->name('EventId' => $eventId)-> prefix("v11"))-> prefix("v11"))-> prefix("v11"))-> prefix("v11"),
		SOAP::Data->name('Job' => \SOAP::Data->value(
			SOAP::Data->name('JobId' => $jobId)-> prefix("v11"),
			SOAP::Data->name('JobDescription' => $jobDesc)-> prefix("v11")-> prefix("v11"),
            SOAP::Data->name('Server' => $server) -> prefix("v11"),
            SOAP::Data->name('MailboxSequence' => $mailboxsequence) -> prefix("v11"),
            SOAP::Data->name('MailboxCount' => $mailboxCount) -> prefix("v11"))
		)-> prefix("v11"))-> prefix("v11"))-> prefix("v11"))->prefix("v11");

	
	$soap->on_fault(sub { fault_($email,$log) });
	 
	my $som = $soap->call('v11:UpdateMailboxStatusRequest', $status, $SDFConsumerRequestHeader);

    if (! defined $som) {
     $log->error("$email|UpdateMailboxStatusRequest Request returned undefined");
     return ('',1,$jobId);
   }
	if ($som->fault) {
        #my $faultdetail = $som->faultdetail;
        $log->error("$email|ManageMigrationStatus SOAP call failed: $sessionId, $timeStamp, $email, $jobId, eventID=$eventId, $mailboxsequence/$mailboxCount,".$som->faultcode .",". $som->faultstring.", code=".$som->valueof('//Fault/detail/InvalidParameterFault/code'));
		return ('',1,$jobId);
	 } else {
	 	my $requestApplicationLabel = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestApplicationLabel');
		my $requestTransactionID = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestTransactionID');
		my $requestSessionID = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestSessionID');
		my $requestTimestamp = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestTimestamp');

		my $status = $som->valueof('//UpdateMailboxStatusResponse/Status');
		my $JobId =  $som->valueof('//UpdateMailboxStatusResponse/Job/JobId');
		#my $jobDesc =  $som->valueof('//UpdateMailboxStatusResponse/Job/JobDescription');
		
        if ( $status ne "OK"){
                $log->warn("$email|ManageMigrationStatus SOAP call failed: $requestTransactionID, $requestSessionID, $requestApplicationLabel, $requestTimestamp, $email, $JobId, $mailboxsequence/$mailboxCount, eventID=$eventId, status=$status");
                return ($status,1,$JobId);
        } else {
                $log->info("$email|ManageMigrationStatus SOAP call OK: $requestTransactionID, $requestSessionID, $requestApplicationLabel, $requestTimestamp, $email, $JobId, $mailboxsequence/$mailboxCount, eventID=$eventId, status=$status");
                return ($status,0, $JobId);
        }
	 }

}

1;
