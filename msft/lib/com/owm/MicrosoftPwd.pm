package com::owm::MicrosoftPwd;

#####################################################################
# Used modules
#####################################################################

use MIME::Base64;
use Crypt::OpenSSL::RSA;
use Cwd;

#####################################################################
# Variables
#####################################################################

my $DEBUG = 0; # 0=ff, 1=on
my $DIR = getcwd() . '/lib/com/owm';

my $cryptedbase64;

# Decryption key (for decrypting the encrypted password from Microsoft).
my $privatekey64 = "-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDFy2umMSZ4ZrLS
oU5JonJfDt6tL3tBjuI8YqU6frKs4y+rP14SFiq27+TphjZ3Z3gLb5hFANChGcJe
oV95lMM8av6KAP5W+jAuapWS5WxWR+jOZpTBhHY8utz1x3+igXMti7JH9fvqha9f
RVPsGBVMal7XLKBJUreaNPEzBj1+4xXmHrYOGNvr6KtXRLkViD/b3SYXxx661xJO
HmzbsUfQWcvL3N2v8LaEYpLk05vBgb6zKBOQYjhW03qFTFzPYxJNlIxKNYrKeT09
GNXEx4+lYTFGBhScIyYteqWi50vke0XXkkm8cKF/iofU/yuQA5VL2iWJeiuFI3cZ
XTTXkp2FAgMBAAECggEAPkryfES+eOzfJLbokQg4PPfR6XkquFrnpVzr7KA6ZXin
aqdqWQfiK/shfOpn/PL5O6DJs/Gtww75yBPKnd/cHQsVXyEw+POSeQvYT4sxs3qy
N07XSpsNIdM4A6/w4p0iSRXV8BPoYtP3h2muJIzxKD/X3qyPToheipJh5E/yziLo
xIULg7tg0CEyWGT8zWF378+VcjDh7pTGleF3yiY/LWPXCvuOcX+D6wkJ/nQQ6B6e
feT708fXcsTRKj0SimkZi9tA7wTkqG6tTFSvx+MwtOutOtdbXPHDjCGbD9d4STPK
6BIs1zjHmdEoYd4sGvmlQ5vrDfXh7I1dYpT5I4HTgQKBgQDq2vHe7sLkjQGrgHyg
zyiO17ZcV2p12IK1mxBXN14BEWdzxtgzUvkn3Z6bX+0guTBPACU6h1rN9N2C8Mvh
1sPEYPjGeVnUS6xDW/GLMvdpJsVXRqposbLLm+/xN0XLPQ5vfQ8yNguFVB0Cmok6
cRVkC/jJD0E4f9XEwtsuBYmvJwKBgQDXmkjSQDFlK0fP5MfU+Jpd2ZcLnVs4K98D
qW3278KweXOtkOSZInMfySKQoRquR6EhjajUXLqsu084fB1LpblzXqhQmmkpbkm6
1Z5oxsMlATvGd/Vknnfjhl1eeUrgsf3aX7U9HSWe6m0Yo47F8ZzTN+17tb7nev4x
i3b3b0T5cwKBgCGe8l09xPy/CuwoWq8SuPWp/9s8UGtKcXvaFP7HWFfCzBKQ9sZy
LGT+nwQTOXkjybylgq7cTa2KTmO+YRGVkzT7xOF04d04cBK8iNmqlCyjdkDGRwLx
3Wy0iCb6CK3NsdFYD+LaRc/yeKy0vsx0/+Dwrc7qtiWGd23Rj1sJQ+qlAoGBAJt6
gntEmZpojL/qLwVxRfjZ+igBERA/6vJ8E3XZTx/uZbJL495ezoCkjo2X8GKv+w30
V0TVnjUEWsvUDVhJKaR+JYkC4UVkaD0Y4cSTWZTs/8tPJKKr+K8WbWy8sXe6Q+Nf
05Y/16pzSbtT75jldP+IvHTTCtlpg7f86Azlq+rrAoGBANLmx+dDo/RCBJIxAJ7d
XjCEP2d4I79NFnb8ukYk4Y75DJm2hL3DbGa6EOOh9cQJ3xZ7XeJxnXNFuFd6xq2Q
m0mDkVvI8XB0GmV7wcl11vv7Vw7YwJTzwZb/bQF6aHfuCsSqoNRosLWJNZqrMa90
odKjo/2yh3kNxPbHfGm+fENp
-----END PRIVATE KEY-----
";

#####################################################################
# decryptPwd
#####################################################################
sub decryptPwd($)
{
  my $cryptedbase64 = shift;

  my $rsa_key = Crypt::OpenSSL::RSA->new_private_key($privatekey64);
  $rsa_key->use_pkcs1_oaep_padding();

  my $crypted = decode_base64($cryptedbase64);            # base64 decode
  $crypted = reverse $crypted;                            # reverse the key big-endian

  my $decrypted = $rsa_key->decrypt($crypted);            # decryption

  chop $decrypted;                                        # strip chars
  chop $decrypted;
  substr $decrypted, 0, 3, "";

  return($decrypted);
}

#####################################################################
# getMicrosoftPwd
#
# This subroutine take a PUID and email address. It uses the PUID
# to retreive a password from MSFT and the email address that MSFT
# thinks belones to this PUID.
# 
# The script will check the email address passed in when called and
# one returned by MSFT match and if they do, the script returns the 
# clear text password.
#####################################################################
sub getMicrosoftPwd($$)
{
  my $puid = shift;
  my $email = shift;

  # bellnet.crt and bellnet.key are expected to be in the same dir as this perl module.
  my $certFile = $DIR . '/bellnet.crt';
  my $certKey = $DIR . '/bellnet.key';

  if (! -e $certFile) {
    die("$certFile does not exist");
  }

  if (! -e $certKey) {
    die("$certKey does not exist");
  }

  my $dataPrePUID = qq|'<?xml version="1.0" encoding="UTF-8"?><soap:Envelope soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:namesp1="http://namespaces.soaplite.com/perl" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soap:Header><WSSecurityHeader xmlns="http://schemas.microsoft.com/Passport/SoapServices/CredentialServiceAPI/V1"><version>eshHeader25</version><ppSoapHeader25><s:ppSoapHeader version="1.0" xmlns:s="http://schemas.microsoft.com/Passport/SoapServices/SoapHeader"><s:credentials /><s:clientIP>10.90.35.10</s:clientIP><s:lcid>1033</s:lcid><s:authorizationLicence /><s:auditInfo /><s:delegate /><s:sitetoken><t:siteheader id="261683" xmlns:t="http://schemas.microsoft.com/Passport/SiteToken" /></s:sitetoken></s:ppSoapHeader></ppSoapHeader25></WSSecurityHeader></soap:Header><soap:Body><GetEDUMigrationData xmlns="http://schemas.microsoft.com/Passport/SoapServices/CredentialServiceAPI/V1"><PassID soapenc:arrayType="xsd:anyType[2]" xsi:type="soapenc:Array"><bstrID>|;
  my $dataPostPUID = qq|</bstrID><pit>PASSID_PUID</pit></PassID></GetEDUMigrationData></soap:Body></soap:Envelope>'|;

  my $data = $dataPrePUID . $puid . $dataPostPUID;

  # Build CURL SOAP request.
  # CURL is used instead of the perl SOAP::LITE module as a 60 sec delay was seen using the  perl SOAP::LITE module.
  # Using CURL is a work around.

  my $cmd = qq|curl -H "Accept:text/xml" -H "Accept:multipart/*" -H "Accept:application/soap" -H "Content-Type:text/xml" -H 'SOAPAction:"http://schemas.microsoft.com/Passport/SoapServices/CredentialServiceAPI/V1#GetEDUMigrationData"' --data |;
  $cmd .= $data . " --cert $certFile --key $certKey https://api.login.live.com/pksecure/PPSACredentialPK.srf -s";

  my $res = `$cmd`;
  print ("\n---\nPassword retrieval command:\n$cmd\n") if ($DEBUG);
  print ("\n---\nPassword retrieval result:\n$res\n") if ($DEBUG);

  # $res is expected to have an email address in it for the PUID made in the call. The email address and delimiters are expected
  # to look like this:
  # <emailDelimiterStart><email><emailDelimiterEnd>
  # eg.
  # &#x000A;opentest105000@bell.net&lt;

  my $emailDelimiterStart = '&#x000A;';
  my $emailDelimiterEnd = '&lt;';
  if ($res =~ /$emailDelimiterStart$email$emailDelimiterEnd/i) {   # Note - Search is case insensitive. This is because MSFT sometimes return email in mix cases.
    # Email matches, so conitune.
  } else {
    return('',$res,2); # Error code 2 - For email mismatch
  }

  if ($res =~ /CipherValue&gt;&#x000D;&#x000A;(.+)&lt;\/p:CipherValue/) {
    my $clearPwd = decryptPwd($1);
    return($clearPwd,'',0);
  } else {
    return('','',1); # Error code 1 - For general error
  }
}

1;   # Indicate successful package loading.

