package com::owm::OlsonToVTimezone;
##############################################################################################################
#
#  Copyright 2015 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 OlsonToVTimezone.pm
#  Description:             Perl module to return VTIMEZONE based on Olson TZ name
#  Author:                  Gary Palmer - Openwave Messaging
#  Date:                    Nov, 2015
#  Customer:                Telstra
#  Version:                 1.0.0 - Nov 2015
#
#############################################################################################################
#
#  Version Control
#
#  Version: 1.0.0
#  Changes:
#
##############################################################################################################

# Some TZ names are actually just aliases for another zone.  The %aliasMap
# hash is a map of alias to concrete zone.
my %aliasMap = (
	"Africa/Asmera" => "Africa/Nairobi",
	"Africa/Timbuktu" => "Africa/Abidjan",
	"America/Argentina/ComodRivadavia" => "America/Argentina/Catamarca",
	"America/Atka" => "America/Adak",
	"America/Buenos_Aires" => "America/Argentina/Buenos_Aires",
	"America/Catamarca" => "America/Argentina/Catamarca",
	"America/Coral_Harbour" => "America/Atikokan",
	"America/Cordoba" => "America/Argentina/Cordoba",
	"America/Ensenada" => "America/Tijuana",
	"America/Fort_Wayne" => "America/Indiana/Indianapolis",
	"America/Indianapolis" => "America/Indiana/Indianapolis",
	"America/Jujuy" => "America/Argentina/Jujuy",
	"America/Knox_IN" => "America/Indiana/Knox",
	"America/Louisville" => "America/Kentucky/Louisville",
	"America/Mendoza" => "America/Argentina/Mendoza",
	"America/Montreal" => "America/Toronto",
	"America/Porto_Acre" => "America/Rio_Branco",
	"America/Rosario" => "America/Argentina/Cordoba",
	"America/Shiprock" => "America/Denver",
	"America/Virgin" => "America/Port_of_Spain",
	"Antarctica/South_Pole" => "Pacific/Auckland",
	"Asia/Ashkhabad" => "Asia/Ashgabat",
	"Asia/Calcutta" => "Asia/Kolkata",
	"Asia/Chongqing" => "Asia/Shanghai",
	"Asia/Chungking" => "Asia/Shanghai",
	"Asia/Dacca" => "Asia/Dhaka",
	"Asia/Harbin" => "Asia/Shanghai",
	"Asia/Kashgar" => "Asia/Urumqi",
	"Asia/Katmandu" => "Asia/Kathmandu",
	"Asia/Macao" => "Asia/Macau",
	"Asia/Saigon" => "Asia/Ho_Chi_Minh",
	"Asia/Tel_Aviv" => "Asia/Jerusalem",
	"Asia/Thimbu" => "Asia/Thimphu",
	"Asia/Ujung_Pandang" => "Asia/Makassar",
	"Asia/Ulan_Bator" => "Asia/Ulaanbaatar",
	"Atlantic/Faeroe" => "Atlantic/Faroe",
	"Atlantic/Jan_Mayen" => "Europe/Oslo",
	"Australia/ACT" => "Australia/Sydney",
	"Australia/Canberra" => "Australia/Sydney",
	"Australia/LHI" => "Australia/Lord_Howe",
	"Australia/NSW" => "Australia/Sydney",
	"Australia/North" => "Australia/Darwin",
	"Australia/Queensland" => "Australia/Brisbane",
	"Australia/South" => "Australia/Adelaide",
	"Australia/Tasmania" => "Australia/Hobart",
	"Australia/Victoria" => "Australia/Melbourne",
	"Australia/West" => "Australia/Perth",
	"Australia/Yancowinna" => "Australia/Broken_Hill",
	"Australia/Broken_Hil" => "Australia/Broken_Hill",
	"Brazil/Acre" => "America/Rio_Branco",
	"Brazil/DeNoronha" => "America/Noronha",
	"Brazil/East" => "America/Sao_Paulo",
	"Brazil/West" => "America/Manaus",
	"Canada/Atlantic" => "America/Halifax",
	"Canada/Central" => "America/Winnipeg",
	"Canada/East-Saskatchewan" => "America/Regina",
	"Canada/Eastern" => "America/Toronto",
	"Canada/Mountain" => "America/Edmonton",
	"Canada/Newfoundland" => "America/St_Johns",
	"Canada/Pacific" => "America/Vancouver",
	"Canada/Saskatchewan" => "America/Regina",
	"Canada/Yukon" => "America/Whitehorse",
	"Chile/Continental" => "America/Santiago",
	"Chile/EasterIsland" => "Pacific/Easter",
	"Cuba" => "America/Havana",
	"Egypt" => "Africa/Cairo",
	"Eire" => "Europe/Dublin",
	"Europe/Belfast" => "Europe/London",
	"Europe/Tiraspol" => "Europe/Chisinau",
	"GB" => "Europe/London",
	"GB-Eire" => "Europe/London",
	"GMT+0" => "Etc/GMT",
	"GMT-0" => "Etc/GMT",
	"GMT" => "Etc/GMT",
	"Greenwich" => "Etc/GMT",
	"Hongkong" => "Asia/Hong_Kong",
	"Iceland" => "Atlantic/Reykjavik",
	"Iran" => "Asia/Tehran",
	"Israel" => "Asia/Jerusalem",
	"Jamaica" => "America/Jamaica",
	"Japan" => "Asia/Tokyo",
	"Kwajalein" => "Pacific/Kwajalein",
	"Libya" => "Africa/Tripoli",
	"Mexico/BajaNorte" => "America/Tijuana",
	"Mexico/BajaSur" => "America/Mazatlan",
	"Mexico/General" => "America/Mexico_City",
	"NZ" => "Pacific/Auckland",
	"NZ-CHAT" => "Pacific/Chatham",
	"Navajo" => "America/Denver",
	"PRC" => "Asia/Shanghai",
	"Pacific/Ponape" => "Pacific/Pohnpei",
	"Pacific/Samoa" => "Pacific/Pago_Pago",
	"Pacific/Truk" => "Pacific/Chuuk",
	"Pacific/Yap" => "Pacific/Chuuk",
	"Poland" => "Europe/Warsaw",
	"Portugal" => "Europe/Lisbon",
	"ROC" => "Asia/Taipei",
	"ROK" => "Asia/Seoul",
	"Singapore" => "Asia/Singapore",
	"Turkey" => "Europe/Istanbul",
	"UCT" => "Etc/UCT",
	"US/Alaska" => "America/Anchorage",
	"US/Aleutian" => "America/Adak",
	"US/Arizona" => "America/Phoenix",
	"US/Central" => "America/Chicago",
	"US/East-Indiana" => "America/Indiana/Indianapolis",
	"US/Eastern" => "America/New_York",
	"US/Hawaii" => "Pacific/Honolulu",
	"US/Indiana-Starke" => "America/Indiana/Knox",
	"US/Michigan" => "America/Detroit",
	"US/Mountain" => "America/Denver",
	"US/Pacific" => "America/Los_Angeles",
	"US/Samoa" => "Pacific/Pago_Pago",
	"UTC" => "Etc/UTC",
	"Universal" => "Etc/UTC",
	"W-SU" => "Europe/Moscow",
	"Zulu" => "Etc/UTC",
);
#UTC Offset Mapping Hashes. Only consider the Australian timezones.
my %st_chart = (
"Australia/Adelaide" => "+0930",
"Australia/Brisbane" => "+1000",
"Australia/Broken_Hill" => "+0930",	
"Australia/Broken_Hil" => "+0930",	
"Australia/Currie" => "+1000",
"Australia/Darwin" => "+0930",
"Australia/Eucla" => "+0845",
"Australia/Hobart" => "+1000",
"Australia/Lindeman" => "+1000",
"Australia/Lord_Howe" => "+1030",
"Australia/Melbourne" => "+1000",
"Australia/Perth" => "+0800",
"Australia/Sydney" => "+1000" );

my %dst_chart = (
"Australia/Adelaide" => "+1030",
"Australia/Brisbane" => "+1000",
"Australia/Broken_Hill" => "+1030",	
"Australia/Broken_Hil" => "+1030",	
"Australia/Currie" => "+1100",
"Australia/Darwin" => "+0930",
"Australia/Eucla" => "+0845",
"Australia/Hobart" => "+1100",
"Australia/Lindeman" => "+1000",
"Australia/Lord_Howe" => "+1100",
"Australia/Melbourne" => "+1100",
"Australia/Perth" => "+0800",
"Australia/Sydney" => "+1100" );

sub getLocalOlson {
#Taking the UTC offset determine the best matching TZID for this event
	my ($offset, $profileTz, $eventStartTime, $debug, $cfg) = @_;
	
	my $defaultAustralia = DateTime::TimeZone->new( name => 'Australia/Sydney' );
	if (defined($aliasMap{$profileTz})) {
		$profileTz = $aliasMap{$profileTz};
	}
	#Ajdust for perth, sydney, adelaide (DST), eucla  as these can only be one possible option.
	print "\n User timezone offsets dst|st ".$dst_chart{$profileTz}."|". $st_chart{$profileTz} if ($debug);
	if ( $defaultAustralia->is_dst_for_datetime( $eventStartTime ) ) {
		#if DST is ON then lookup the DST hash
		if ( $offset eq $dst_chart{$profileTz} ) {
			print "\nDST user timezone matches".$dst_chart{$profileTz} ."\n"  if ($debug);
		} else {
			print "\nDST user timezone does not match ".$dst_chart{$profileTz}." - $offset  $profileTz , checking known zones \n " if ($debug);
			$profileTz = getKnownTimezone($offset);
		}
   } else {
	#daylight savings is OFF then look up ST hash
	if ( $offset eq $st_chart{$profileTz} ) {
		print "\nST user timezone matches ".$st_chart{$profileTz} ."\n" if ($debug);
	} else {
		print "\nST user timezone does not match ".$st_chart{$profileTz}." - $offset  $profileTz , checking known zones \n" if ($debug);
		$profileTz = getKnownTimezone($offset);
	}
   }
   print "Final user profile TZID $profileTz \n"  if ($debug);
   return ($profileTz);
}

sub getKnownTimezone {
# when the profile and even timezones don't match, check for known timezones and set these.
	my $offset = shift;

	if ($offset eq "+0800") {
		return("Australia/Perth");
	} elsif ($offset eq "+0845")   {
		return("Australia/Eucla");
	} elsif ($offset eq "+1100")   {
		return("Australia/Sydney");
	} else {
		#return unknown if still no matches
		return ("unknown");
	}
}

sub getVtimezone($$$)
{
	my ($origTz, $cfg, $log) = @_;

	my $realTz = $origTz;
	if (defined($aliasMap{$realTz}))
	{
		$realTz = $aliasMap{$realTz};
	}

	my $tzDir = $cfg->param('vtimezone_path');
	my $tzPath = $tzDir.'/'.$realTz.'.ics';
	if (-f $tzPath && -r $tzPath)
	{
		unless (open(TZIN, '<', $tzPath))
		{
			return(undef); #indicate failure
		}
		my $old_slash = $/; # remember the old value for line separator
		$/ = undef; # read the entire file at once
		my $vtimezone = <TZIN>;
		$/ = $old_slash; # restore line separator
		close(TZIN);
		# vtic produces complete VCALENDAR files, so strip everything until
		# the beginning of the VTIMEZONE definition
		$vtimezone =~ s/^.*BEGIN:VTIMEZONE/BEGIN:VTIMEZONE/s;
		# also strip the VCALENDAR end statement
		$vtimezone =~ s/END:VCALENDAR//;
		# sanitize to make sure there is only one \n at the very end
		$vtimezone =~ s/[\r\n]+$//;
		$vtimezone .= "\n";
		# Change TZID to match the requested TZ.  Else OWM CAL server
		# gets grumpy
		if ($realTz ne $origTz)
		{
			$vtimezone =~ s/^TZID:.*$/TZID:$origTz/m;
		}
		return($vtimezone);
	}
	$log->warn("SCRIPT| Unable to find timezone file details $tzPath ");
	return(undef); #indicate failure
}
