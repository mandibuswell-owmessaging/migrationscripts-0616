package OxHttpApi;
##############################################################################################################
#
# This is a perl module to implement parts of the OX HTTP API (see http://oxpedia.org/index.php?title=HTTP_API)
#
##############################################################################################################
=head1 OxHttpApi

Toolkit to implement a subset of calls to the OX http API (http://oxpedia.org/index.php?title=HTTP_API).

The goal of OxHttpApi.pm is to lower the bar for interfacing to Richamil to getting / setting user data like contacts data, appointments data, configuration data, etc.
OxHttpApi.pm is a perl module that implements a subset of the calls to the OX http API (http://oxpedia.org/index.php?title=HTTP_API). It also provide a set of helper function to easy processing responses from Richmail.

Attached to this article at support.openwave.com (http://support.openwave.com/display/IKB/OxHttpApi.pm+-+toolkit+for+OX+http+API), are some additional perl script that give example of how OxHttpApi.pm may be use. They have been well commented, to help the reader to be able to understand and reuse the code.

=head2 Methods

=over 16

=cut

use strict;
use JSON;
use URI;
#use HTTP::Request;
use HTTP::Request::Common;
use HTTP::Response;
use LWP::UserAgent;

##############################################################################################################
# Macros
##############################################################################################################

# In OX HTTP API, when requesting contact data, to request must contain the field IDs to be fetch. $allContactDetailsFields is set
# to all the fields that a contact can have (this is 500 - 615, with the exception of 593, 600, 603, 604, 607, 609.
our $allContactDetails = "500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,594,595,596,597,598,599,601,602,605,606,608,610,611,612,613,614,615";

# Common object data.
our $allCommonObject = "1,2,3,4,5,20,100,101,102,103,104,105";

##############################################################################################################
# new
# The object constructor
# Aurgment: host, port (optional)
=item C<new>

Constructs a new OxHttpApi object that can then be used to interface to Richmail.

=over

=item B<< $ox = OxHttpApi->new($richmailHost) >>

=item B<< $ox = OxHttpApi->new($richmailHost,$port) >>

=item

=back
=cut
##############################################################################################################

sub new
{
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self  = {};
  if (@_ == 1) {
    $self->{HOST}      = shift;
    $self->{PORT}      = undef;
    $self->{BASE_URI}  = "http://".$self->{HOST}."/ajax/";
  } elsif (@_ == 2) {
    $self->{HOST}      = shift;
    $self->{PORT}      = shift;
    $self->{BASE_URI}  = "http://".$self->{HOST}.":".$self->{PORT}."/ajax/";
  } else {
    die "ERROR :: OxHttpApi->new called with invalid number of arguments\n";
  }

  $self->{UA}        = LWP::UserAgent->new;
  $self->{UA}->cookie_jar({});  # Set in memory cookie jar.

  $self->{SESSION}   = undef;
  $self->{RANDOM}    = undef;
  $self->{ERROR}     = undef;
  $self->{RESPONSE}  = HTTP::Response->new;
  $self->{CALL_URI}  = undef;

  bless($self,$class);
  return $self;
}

##############################################################################################################
# host
# Set or return host value
# Aurgment: host (optional)
##############################################################################################################
sub host
{
  my $self = shift;
  if (@_) { $self->{HOST} = shift }
  return $self->{HOST};
}

##############################################################################################################
# port 
# Set or return port value
# Aurgment: port (optional)
##############################################################################################################
sub port
{
  my $self = shift;
  if (@_) { $self->{PORT} = shift }
  return $self->{PORT};
}

##############################################################################################################
# session
# Return session value
# Aurgment: <NONE>
##############################################################################################################
sub session
{
  my $self = shift;
  return $self->{SESSION};
}

##############################################################################################################
# random
# Return random value
# Aurgment: <NONE>
##############################################################################################################
sub random
{
  my $self = shift;
  return $self->{RANDOM};
}

##############################################################################################################
# error
# Return error value
# Aurgment: <NONE>
##############################################################################################################
sub error
{
  my $self = shift;
  return $self->{ERROR};
}

##############################################################################################################
# base_uri
# Return base_uri value
# Aurgment: <NONE>
##############################################################################################################
sub base_uri
{
  my $self = shift;
  return $self->{BASE_URI};
}

##############################################################################################################
# is_success
# Return 0 (zero) if last request was unsuccessfully processed, otherwise non-zero.
# Aurgment: <NONE>
=item C<is_success>

Indicates to the last HTTP call to Richmail was successful. Returns '0' if last request was unsuccessfully processed, otherwise non-zero.

B<< $ox->is_success >>
=cut
##############################################################################################################

sub is_success
{
  my $self = shift;
  if ($self->{ERROR} eq "") {
    return 1;
  } else {
    return 0;
  }
}

##############################################################################################################
# print_error
# Aurgment: <NONE>
=item C<print_error>

Print the error message associated with the last HTTP call to Richmail.

B<< $ox->print_error >>
=cut
##############################################################################################################
sub print_error
{
  my $self = shift;
  print STDERR $self->{ERROR};
}

##############################################################################################################
# Login
# Login to RM via OX HTTP API.
# Aurgment: username, password, origin ip.
=item C<Login>

Implements POST /ajax/login?action=login in the OX http API.
Log a user into Richmail. A user must be logged in before their user data can be accessed or set.

B<< $ox->Login($username,$password) >>
=cut
##############################################################################################################
sub Login
{
  my $self = shift;
  my $user = shift;
  my $password = shift;
  my $Origin_Ip = shift; # If the originIp is in the AppSuite's white list, login to an account in 'M' will work.
  if (@_ != 0) { die "ERROR :: OxHttpApi->Login called with invalid number of arguments\n"; }

  $Origin_Ip = '10.90.35.42';

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."login");
  $self->{CALL_URI}->query_form( 'action' => 'login',
                                 'name' => $user,
                                 'password' => $password );

  # Do HTTP request.
  my $req = GET( $self->{CALL_URI});
  #$req->header(qw' :Origin_Ip '.$Origin_Ip.' ');
  $req->header(qw' :Origin_Ip 10.90.35.42 ');  # Works
  $self->{RESPONSE} = $self->{UA}->request($req);

  my $DEBUG = 0;
  if ($DEBUG) {
    print("\naction = login, name = $user, password = $password, Origin_Ip = $Origin_Ip\n");
    print("\nDEBUG REQUEST  ::\n".$req->as_string."\n");
    print("\nDEBUG RESPONSE ::\n".$self->{RESPONSE}->as_string."\n");
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    my $decoded_json = decode_json($content);
    $self->{RANDOM}  = $decoded_json->{"random"};
    $self->{SESSION} = $decoded_json->{"session"};
    $self->{ERROR}   = $decoded_json->{"error"};    # If login is successful, this will be set to "".
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }

  if ($self->is_success) {
    $self->{LOGGEDIN} = 1;   # If login was successful, set LOGGEDIN to true.
    
  }
}

##############################################################################################################
# Logout
# Logout of RM via OX HTTP API.
# Aurgment: <NONE>
=item C<Logout>

Implements GET /ajax/login?action=logout in the OX http API.
Log a user out of Richmail. After which a new login can be preformed using the same OxHttpApi object ($ox).

B<< $ox->Logout >>
=cut
##############################################################################################################
sub Logout
{
  my $self = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->Logout called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."login");     # Note: 'login' was correct here, but 'action' is 'logout. See next line.
  $self->{CALL_URI}->query_form( 'action' => 'logout',
                                 'session' => $self->{SESSION} );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    if ($self->{RESPONSE}->header('Set-Cookie') ne "") {
      # Logout cookie returned, logout was successful.
      $self->{ERROR} = "";
    } else {
      $self->{ERROR} = "Logout appears to have failed. No logout cookie was returned.";
    }
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# GetConfData
# Get configuration data from RM via OX HTTP API.
# Aurgment: Path (Namespace) of required config data.
# Return:   If successful, a JSON encoded object-data, containing value of the node specified by path.
#           Note - The JSON fetch by GetConfData() is encapsulated in a 'data' object. This 'date' object needs
#                  to be stripped off if the JSON is being used to set a user's config data with SetConfData()
=item C<GetConfData>

Implements GET /ajax/config/path in the OX http API.
Gets the user's configuration data based on the 'path'. For the 'path', the prefix of /ajax/config/ should be omitted. So to get configuration data from 'path' '/ajax/config/gui', just enter 'gui'.

B<< $ox->GetConfData($path) >>
=cut
##############################################################################################################
sub GetConfData
{
  my $self = shift;
  my $path = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetConfData called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."config/".$path);
  $self->{CALL_URI}->query_form( 'session' => $self->{SESSION} );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $decoded_json = decode_json($content);
    $self->{ERROR} = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
    return ($decoded_json);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# SetConfData
# Set configuration data from RM via OX HTTP API.
# Aurgment: Path (Namespace) of required config data.
#           Config JSON object.
#           Note - The JSON fetch by GetConfData() is encapsulated in a 'data' object. This 'date' object needs
#                  to be stripped off if the JSON is being used to set a user's config data with SetConfData()
=item C<SetConfData>

Implements PUT /ajax/config/path in the OX http API.
Sets the user's configuration data based on the 'path'. For the 'path', the prefix of /ajax/config/ should be omitted. So to get configuration data from 'path' '/ajax/config/gui', just enter 'gui'. The 'encode_content' is the new value of the node specified by path, in the form of a JSON object.

B<< $ox->SetConfData($path,$encode_content) >>
=cut
##############################################################################################################
sub SetConfData
{
  my $self = shift;
  my $path = shift;
  my $config = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->SetConfData called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."config/".$path);
  $self->{CALL_URI}->query_form( 'session' => $self->{SESSION} );

  my $req = new HTTP::Request 'PUT',$self->{CALL_URI};
  $req->content_type('text/javascript; charset=UTF-8');
  $req->content($config);

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->request($req);

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    $self->{ERROR} = "";
    return ("");
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# GetRootFolders
# Get all folders at the root level of the folder structure.
# Aurgment: columns - A comma-separated list of columns to return. Each column is specified by a numeric column identifier.
# Return:   If successful, a JSON encoded object-data, containing data for all folders at the root level.
=item C<GetRootFolders>

Implements GET /ajax/folders?action=root in the OX http API.
Gets all folders at the root level of the folder structure. 'columns' is a comma-separated list of columns to return. Each column is specified by a numeric column identifier.

B<< $ox->GetRootFolders($columns) >>
=cut
##############################################################################################################
sub GetRootFolders
{
  my $self = shift;
  my $columns = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetRootFolders called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."folders");
  $self->{CALL_URI}->query_form( 'action' => 'root',
                                 'session' => $self->{SESSION},
                                 'columns' => $columns,
                                 'allowed_modules' => 'contacts' );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $decoded_json = decode_json($content);
    $self->{ERROR}   = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
    return ($decoded_json);
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# GetSubFolders
# Get sub folders data
# Aurgment: 
#   parent  - Object ID of a folder, which is the parent folder of the requested folders.
#   columns - A comma-separated list of columns to return. Each column is specified by a numeric column identifier.
# Return:   If successful, a JSON encoded object-data, containing data for all folders, which have the folder with the requested object ID as parent.
=item C<GetSubFolders>

Implements GET /ajax/folders?action=list in the OX http API.
Get sub folders data. 'parent' is the folder ID of the parent folder. 'columns' is a comma-separated list of columns to return. Each column is specified by a numeric column identifier.

B<< $ox->GetSubFolders($parent,$columns) >>
=cut
##############################################################################################################
sub GetSubFolders
{ 
  my $self = shift;
  my $parent = shift;
  my $columns = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetSubFolders called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."folders");
  $self->{CALL_URI}->query_form( 'action' => 'list',
                                 'session' => $self->{SESSION}, 
                                 'parent'  => $parent, 
                                 'columns' => $columns );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # print("\n\nDEBUG REQUET ::\n".$self->{CALL_URI}->as_string."\n\n");

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $decoded_json = decode_json($content);
    $self->{ERROR}   = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
    return ($decoded_json);
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# GetFolderId
# Aurgment:   
#     folderName : A string of the folder to which the ID is needed.
# Return:
#     The ID of the contact folder (or empty string is none exists or error occured)
=item C<GetFolderId>

Gets folder ID of a named folder.

B<< $ox->GetFolderId($folderName) >>

=cut
##############################################################################################################
sub GetFolderId
{
  my $self = shift;
  my $folderName = shift;
  my $folderId = "";
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetFolderId called with invalid number of arguments\n"; }

  $self->GetSubFolders("1","1,300");

  if ($self->is_success) {
    my $content = $self->{RESPONSE}->content;
    my $decoded_json = decode_json($content);
    foreach (@{$decoded_json->{"data"}}) {
      if ( ${$_}[1] eq $folderName ) {
        $folderId = ${$_}[0];
        last; #Exit the loop
      }
    }
  }
  return($folderId);
}

##############################################################################################################
# GetContactFolderId
# Aurgment:   <NONE>
# Return:     The ID of the contact folder (or empty string is none exists or error occured)
##############################################################################################################
sub GetContactFolderId
{
  my $self = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetContactFolderId called with invalid number of arguments\n"; }
  return( $self->GetFolderId("Contacts") );
}

##############################################################################################################
# GetCalendarFolderId
# Aurgment:   <NONE>
# Return:     The ID of the calendar folder (or empty string is none exists or error occured)
##############################################################################################################
sub GetCalendarFolderId
{
  my $self = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetCalendarFolderId called with invalid number of arguments\n"; }
  return( $self->GetFolderId("Calendar") );
}

##############################################################################################################
# GetEmailFolderId
# Aurgment:   <NONE>
# Return:     The ID of the email folder (or empty string is none exists or error occured)
##############################################################################################################
sub GetEmailFolderId
{
  my $self = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetEmailFolderId called with invalid number of arguments\n"; }
  return( $self->GetFolderId("E-Mail") );
}

##############################################################################################################
# GetTasksFolderId
# Aurgment:   <NONE>
# Return:     The ID of the tasks folder (or empty string is none exists or error occured)
=item C<GetContactFolderId>

=item C<GetCalendarFolderId>

=item C<GetEmailFolderId>

=item C<GetTasksFolderId>

A set of methods to get the folder ID of commonly used folders.
  
=over

=item B<< $id = $ox->GetContactFolderId >>

=item B<< $id = $ox->GetCalendarFolderId >>

=item B<< $id = $ox->GetEmailFolderId >>

=item B<< $id = $ox->GetTasksFolderId >>

=item

=back

=cut
##############################################################################################################
sub GetTasksFolderId
{
  my $self = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetTasksFolderId called with invalid number of arguments\n"; }
  return( $self->GetFolderId("Tasks") );
}

##############################################################################################################
# GetAllAppointments
# Get all appointments for a user in a given time range.
# Aurgment:
#   folder  - The folder ID for the user's calendar folder to be searched.
#   columns - A comma-separated list of columns to return. Each column is specified by a numeric column identifier.
#   start   - Search start epoch time (lapsed time since 00:00:00 UTC on 1 January 1970) in milliSeconds.
#   end     - Search end epoch time (lapsed time since 00:00:00 UTC on 1 January 1970) in milliSeconds.
# Return:   If successful, 
=item C<GetAllAppointments>

Implements GET /ajax/calendar?action=all in the OX http API.
  
B<< $json = $ox->GetAllAppointments($folderId,$columns,$startTime,$endTime) >>
=cut
##############################################################################################################
sub GetAllAppointments
{
  my $self = shift;
  my $folder = shift;
  my $columns = shift;
  my $start = shift;
  my $end = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetAllAppointments called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."calendar");
  $self->{CALL_URI}->query_form( 'action' => 'all',
                                 'session' => $self->{SESSION},
                                 'folder'  => $folder,
                                 'columns' => $columns,
                                 'start'   => $start,
                                 'end'     => $end );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $decoded_json = decode_json($content);
    $self->{ERROR}   = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
    return ($decoded_json);
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# CreateAppointment
# 
# Aurgment:
#   content - Appointment object as described in  Common object data, Detailed task and appointment data and
#             Detailed appointment data. The field id is not included.
# Return:
#   A json object with attribute id of the newly created contact.
=item C<CreateAppointment>

Implements PUT /ajax/calendar?action=new in the OX http API.
  
B<< $appointmentId = $ox->CreateAppointment($newAppointmentData) >>
=cut
##############################################################################################################
sub CreateAppointment
{
  my $self = shift;
  my $content = shift;
  my $req;
  if (@_ != 0) { die "ERROR :: OxHttpApi->CreateAppointment called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."calendar");
  $self->{CALL_URI}->query_form( 'action' => 'new',
                                 'session' => $self->{SESSION} );

  # Create PUT request...
  $req = new HTTP::Request 'PUT',$self->{CALL_URI};
  $req->content_type('text/javascript; charset=UTF-8');
  $req->content($content);

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->request($req);

  # Evaluate the response...
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    # If response code is 200, the contect should but the JSON object with either error or appointment id.
    my $content = $self->{RESPONSE}->content;
    $decoded_json = decode_json($content);
    $self->{ERROR}   = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
    if ($self->is_success) {
      return ($decoded_json->{"data"}->{"id"});
    } else {
      return ("");
    }
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# PutMultiple
# The multiple module allows to bundle multiple requests to most other modules in a single request.
=item C<PutMultiple>

Implements PUT /ajax/multiple in the OX http API.
The multiple module allows to bundle multiple requests to most other modules in a single request.
  
B<< $ox->PutMultiple($content) >>
=cut
##############################################################################################################
sub PutMultiple
{
  my $self = shift;
  my $content = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->PutMultiple called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."multiple");
  $self->{CALL_URI}->query_form( 'session' => $self->{SESSION},
                                 'continue'  => 'true' );

  my $req = new HTTP::Request 'PUT',$self->{CALL_URI};
  $req->content_type('text/javascript; charset=UTF-8');
  $req->content($content);

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->request($req);

}

##############################################################################################################
# GetAllContacts
# Get all contacts
# Aurgment:
#   folder  - Object ID of the folder, whose contents are queried.
#   columns - A comma-separated list of columns to return. Each column is specified by a numeric column identifier.
# Return:   
=item C<GetAllContacts>

Implements GET /ajax/contacts?action=all in the OX http API.
  
B<< $json = $ox->GetAllContacts($contactFolderId,$columns) >>
=cut
##############################################################################################################
sub GetAllContacts
{
  my $self = shift;
  my $folder = shift;
  my $columns = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetAllContacts called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."contacts");
  $self->{CALL_URI}->query_form( 'action' => 'all',
                                 'session' => $self->{SESSION},
                                 'folder'  => $folder,         
                                 'columns' => $columns );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $decoded_json = decode_json($content);
    $self->{ERROR}   = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
    return ($decoded_json);
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# GetListContacts
# Get a list of contacts (PUT /ajax/contacts?action=list)
# Aurgment:
#   columns - A comma-separated list of columns to return. Each column is specified by a numeric column identifier.
#   content - Body of the request, an array with objects, each object contains fields "id" and "folder" of requested contacts. 
# Return:
=item C<GetListContacts>

Implements PUT /ajax/contacts?action=list in the OX http API.
  
B<< $ox->GetListContacts($columns,$content) >>
=cut
##############################################################################################################
sub GetListContacts
{
  my $self = shift;
  my $columns = shift;
  my $content = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetListContacts called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."contacts");
  $self->{CALL_URI}->query_form( 'action' => 'list',
                                 'session' => $self->{SESSION},
                                 'columns' => $columns );

  my $req = new HTTP::Request 'PUT',$self->{CALL_URI};
  $req->content_type('text/javascript; charset=UTF-8');
  $req->content($content);

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->request($req);
}

##############################################################################################################
# CreateContact
# 
# Aurgment:
#   content - Contact object as described in Common object data and Detailed contact data. The field id is not included.
#   image   - (OPTIONAL) Path to image to be uploaded with contact details.
# Return:
#   The ID of the newly created contact.
=item C<CreateContact>

Implements PUT /ajax/contacts?action=new in the OX http API.
Method can be used to create a new contact (with an image, if required). It can also be used to create contact distribution lists.

=over

=item B<< $contactId = $ox->CreateContact($newContactData) >>

=item B<< $contactId = $ox->CreateContact($newContactData,$imageFilePath) >>

=item B<< $contactId = $ox->CreateContact($newDistrListData) >>

=item

=back
=cut
##############################################################################################################
sub CreateContact
{
  my $self = shift;
  my $content = shift;
  my $imageFilePath;
  my $req;

  # Check if subroutine was called with optional image arg.
  if (@_ == 0) {
    $imageFilePath = "";
  } elsif (@_ == 1) {
    $imageFilePath = shift;
  } else {
    die "ERROR :: OxHttpApi->CreateContact called with invalid number of arguments\n";
  }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."contacts");
  $self->{CALL_URI}->query_form( 'action' => 'new',
                                 'session' => $self->{SESSION} );

  # Create PUT request based on if contact has an image or not...
  if ($imageFilePath eq "") {
    # Create a PUT request with no image.
    $req = new HTTP::Request 'PUT',$self->{CALL_URI};
    $req->content_type('text/javascript; charset=UTF-8');
    $req->content($content);
  } else {
    # Check image file exists.
    if (! -e $imageFilePath) {
      $self->{ERROR} = "Create contact failed. Image file does not exist : $imageFilePath.";
      return;
    }
    # Create a POST multipart/form-data request, adding image and json elements.
    $req = POST $self->{CALL_URI},
         Content_Type => 'form-data',
         Content      => [ file   => [$imageFilePath],
                           json   => $content,
                         ];
  }

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->request($req);

  # Evaluate the response.
  if ($imageFilePath eq "") {
    # Evaluate the response for contact without image...

    my $decoded_json;
    if ($self->{RESPONSE}->code == 200) {
      # If response code is 200, the contect should but the JSON object with either error or contact id.
      my $content = $self->{RESPONSE}->content;
      $decoded_json = decode_json($content);
      $self->{ERROR}   = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
      if ($self->is_success) {
        return ($decoded_json->{"data"}->{"id"});
      } else {
        return ("");
      }
    } else {
      $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
      return ("");
    }

  } else {
    # Evaluate the response for contact with image...

    my $decoded_json;
    if ($self->{RESPONSE}->code == 200) {
      # If response code is 200, the contect is a string of html with JSON object (with either error or contact id) embedded.
      my $content = $self->{RESPONSE}->content;
      # Extract the JSON object.
      (my $junk1, my $jsonWithoutBrackets, my $junk2) = ($content =~ /(.*)\({(.*)}\)(.*)/);
      $decoded_json = decode_json("{".$jsonWithoutBrackets."}");
      $self->{ERROR}   = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
      if ($self->is_success) {
        return ($decoded_json->{"data"}->{"id"});
      } else {
        return ("");
      }
    } else {
      $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
      return ("");
    }
  }
}

##############################################################################################################
# SearchContacts
# Aurgment:
#   columns - comma seperated list of requested field.
#   sort    - identifier of a column which determines the sort order of the response.
#   order   - "asc" / "desc" to sort in ascending or descending order.
#   content - An Object as described in Search contacts. 
# Return:
#   An array with contact data. Each array element describes one contact and is itself an array. The elements 
#   of each array contain the information specified by the corresponding identifiers in the columns parameter. 
=item C<SearchContacts>

Implements PUT /ajax/contacts?action=search in the OX http API.
  
B<< $json = $ox->SearchContacts($columns,$sortColumn,$order,$searchContact) >>
=cut
##############################################################################################################
sub SearchContacts
{
# my $json = $ox->SearchContacts("1,20,500,555,602,524,556,557,501,502",500,asc,$contact);
  my $self = shift;
  my $columns = shift;
  my $sort = shift;
  my $order = shift;
  my $content = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->SearchContacts called with invalid number of arguments\n"; }

print("columns = $columns\n");
print("sort = $sort\n");
print("order = $order\n");
print("content = $content\n");

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."contacts");
  $self->{CALL_URI}->query_form( 'action' => 'search',
                                 'session' => $self->{SESSION},
                                 'columns' => $columns,
                                 'sort'    => $sort,
                                 'order'   => $order );

  my $req = new HTTP::Request 'PUT',$self->{CALL_URI};
  $req->content_type('text/javascript; charset=UTF-8');
  $req->content($content);

  # print("\n\nDEBUG REQUET ::\n".$req->as_string."\n\n");

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->request($req);

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $decoded_json = decode_json($content);
    $self->{ERROR}   = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
    return ($decoded_json);
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# Export
# Aurgment:
#   type   - Type of data to export: "CSV", "ICAL" or "VCARD".
#   folder - ObjectID of the folder that the data is to export from.
# Return:
=item C<Export>

Implements GET /ajax/export?action=CSV, GET /ajax/export?action=ICAL and GET /ajax/export?action=VCARD in the OX http API.
Method to export user data in one of three format (CSV, vCard, iCAL).

=over

=item B<< $CSVdata = $ox->Export("CSV",$folderId) >>

=item B<< $VCARDdata = $ox->Export("VCARD",$folderId) >>

=item B<< $ICALdata = $ox->Export("ICAL",$folderId) >>

=item 

=back
=cut
##############################################################################################################
sub Export
{
  my $self = shift;
  my $type = shift;
  my $folder = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->Export called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."export");
  $self->{CALL_URI}->query_form( 'action' => $type,
                                 'session' => $self->{SESSION},
                                 'folder'  => $folder );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    $self->{ERROR} = "";
    return ($self->{RESPONSE}->content);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# Import
# Aurgment:
#   type   - Type of data to export: "CSV", "OUTLOOK_CSV", "ICAL" or "VCARD".
#   folder - ObjectID of the folder that the data is to import to.
# Return:
=item C<Import>

Implements POST /ajax/import?action=CSV, POST /ajax/import?action=OUTLOOK_CSV, http://oxpedia.org/index.php?title=HTTP_API#Import_Outlook_CSV and http://oxpedia.org/index.php?title=HTTP_API#Import_iCAL in the OX http API.

=over

=item B<< $json = $ox->Import("CSV",$folderId,$CSVdata) >>

=item B<< $json = $ox->Import("OUTLOOK_CSV",$folderId,$OUTLOOK_CSVdata) >>

=item B<< $json = $ox->Import("ICAL",$folderId,$ICALdata) >>

=item B<< $json = $ox->Import("VCARD",$folderId,$VCARDdata) >>

=item

=back
=cut
##############################################################################################################
sub Import
{
  my $self = shift;
  my $type = shift;
  my $folder = shift;
  my $filePath = shift;
  my $ignoreUIDs;
  if (@_ == 0) {
    $ignoreUIDs = ""; # Only use for type ICAL.
  } elsif (@_ == 1) { 
    $ignoreUIDs = shift; # Only use for type ICAL.
  } else {
    die "ERROR :: OxHttpApi->Import called with invalid number of arguments\n";
  }

  # Set default folder if $folder is empty.
  if ($folder eq '') {
    if ( ($type eq 'CSV') || ($type eq 'OUTLOOK_CSV') || ($type eq 'VCARD') ) {
      $folder = 22;
    }
    if ($type eq 'ICAL') {
      $folder = 21;
    }
  }

  my %queryFormHash = ('action', $type,
                       'session', $self->{SESSION},
                       'folder', $folder);

  if (($type eq "ICAL") && ($ignoreUIDs ne "")) {
    $queryFormHash{'ignoreUIDs'} = $ignoreUIDs;
  }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."import");
  $self->{CALL_URI}->query_form(\%queryFormHash);
  # $self->{CALL_URI}->query_form( 'action' => $type,
  #                                'session' => $self->{SESSION},
  #                                'folder'  => $folder );

  # Create a POST multipart/form-data request, adding image and json elements.
  my $req = POST $self->{CALL_URI},
       Content_Type => 'form-data',
       Content      => [ file   => [$filePath],
                       ];

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->request($req);

  # print("\n\nDEBUG REQUET ::\n".$req->as_string."\n\n");

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    $self->{ERROR} = "";
    return ($self->{RESPONSE}->content);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# GetAllSnippet
# Get all snippet for user.
# Aurgment:
#   type  - Optional CSV of types to filter by; e.g. "signature"
# Return:
=item C<GetAllSnippet>

Implements GET /ajax/snippet?action=all in the OX http API.

B<< $json = $ox->GetAllSnippet($type) >>
=cut
##############################################################################################################
sub GetAllSnippet
{
  my $self = shift;
  my $type = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->GetAllSnippet called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."snippet");
  $self->{CALL_URI}->query_form( 'action' => 'all',
                                 'session' => $self->{SESSION},
                                 'type' => $type );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $decoded_json = decode_json($content);
    $self->{ERROR}   = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
    return ($decoded_json);
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# PutAllSnippet
##############################################################################################################
sub PutAllSnippet
{
  my $self = shift;
  my $content = shift;
  my $type = shift;
  if (@_ != 0) { die "ERROR :: OxHttpApi->PutAllSnippet called with invalid number of arguments\n"; }

  if ( ($type ne 'new') && ($type ne 'update') ) {
    die "ERROR :: OxHttpApi->PutAllSnippet type is not supported. Should be \'new\' or \'update\', not $type";
  }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."snippet");
  if ($type eq 'new') {
    $self->{CALL_URI}->query_form( 'action' => 'new',
                                   'session' => $self->{SESSION} );
  } else {
    $self->{CALL_URI}->query_form( 'action' => 'update',
                                   'id' => '1',
                                   'session' => $self->{SESSION} );
  }

  my $req = new HTTP::Request 'PUT',$self->{CALL_URI};
  $req->content_type('text/javascript; charset=UTF-8');
  $req->content($content);

  # print("\nDEBUG REQUET ::\n".$req->as_string."\n");

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->request($req);

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $decoded_json = decode_json($content);
    $self->{ERROR}   = $decoded_json->{"error"};    # If operation is successful, this will be set to "".
    return ($decoded_json);
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}


##############################################################################################################
# PrintResponse
# Print the response to a HTTP Request
# Aurgment: <NONE>
=item C<PrintResponse>

Method which prints details of the last http transaction (request and response) with Richmail to STDOUT.

B<< $ox->PrintResponse >>
=cut
##############################################################################################################
sub PrintResponse
{
  my $self = shift;
  print STDOUT "\nREQUEST ::\n$self->{CALL_URI}\n\nRESPONSE ::\n".$self->{RESPONSE}->as_string."\n";
}

##############################################################################################################
# Exit perl module
##############################################################################################################
1;  # so the require or use succeeds
