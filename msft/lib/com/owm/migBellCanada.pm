package com::owm::migBellCanada;
##############################################################################################################
#
#  Copyright 2014 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 migBellCanada.pm
#  Description:             Perl module to migrate data from MSFT into MX / AppSuite via mOS.
#  Author:                  Andrew Perkins - Openwave Messaging

#  Version: 	            1.0.0 - Sept 2014
#
#  Additional info...
#  Perl Modules:            This module uses mosApi.pm, another perl module for making calls to mOS API.
#  mOS Version supported:   There has not been any firmal testing with mOS version, but has been used with mOS 2.1.30-2 and
#                           earlier versions of mOS.
#                           
#############################################################################################################
#
#  Version Control
# 
#  Version: 1.0.0
#  Changes:
#
##############################################################################################################

# Standard perl modules
use strict;
use Net::LDAP;
use Config::Simple;
use HTTP::Request::Common;
use HTTP::Response;
use JSON;
use XML::Simple;
use DateTime;
use Time::Local;
use POSIX qw(strftime);
use DBI;
use Cwd;
use vars qw ($sec $min $hour $mday $mon $year $wday $yday $isdst);
use Sys::Hostname;
use HTML::Strip;
use HTML::Entities;
use Email::MIME;
use Encode;

use Data::Dumper;

# OWM modules
use com::owm::mosApi;           # Module to enable mOS API calls
use com::owm::OxHttpApi;        # Module to allow OX HTTP API calls
use com::owm::MicrosoftPwd;     # Module to retrieve and decode password
use com::owm::OlsonToVTimezone;
use com::owm::MSFTPeopleAPI;

##############################################################################################################
# Global variables
##############################################################################################################
$| = 1; # Makes STDOUT flush immediately
my $DEBUG = 0; # 0=off, 1=on.
my $MOS;
my $USERTZ;

our $STATUS_SUCCESS = 0;
#Align codes to Telstra Event Id Assignments 100 - 149 INFO CODES, 150 - 199 ERROR CODES
our $STATUS_LOUD_SUCCESS = 100;
our $STATUS_MIG_NOTSTARTED = 150;
our $STATUS_FAIL_INVALID_INPUT = 151;
our $STATUS_FAIL_MOS_CONNECT = 152;
our $STATUS_FAIL_MOS_SETTING = 153;
our $STATUS_FAIL_INVALID_LIST_TYPE = 154;
our $STATUS_FAIL_PUID_LOOKUP = 155;
our $STATUS_FAIL_ACCESS_TOKEN = 156;
our $STATUS_FAIL_NO_CONSENT = 157;
our $STATUS_FAIL_PASSWORD_RETRIEVAL = 158;
our $STATUS_FAIL_MSFT_CALENDAR = 159;
our $STATUS_FAIL_MSFT_NAME = 160;
our $STATUS_FAIL_AS_NAME = 161;
our $STATUS_FAIL_NOT_ALLOWED = 162;
our $STATUS_FAIL_AS_CONNECT = 163;
our $STATUS_FAIL_AS_LOGIN = 164;
our $STATUS_FAIL_AS_SET_SIGN = 165;
our $STATUS_FAIL_PASSWORD_SETTING = 166;
our $STATUS_FAIL_MAILBOX_MIGRATION = 177;
our $STATUS_FAIL_MSFT_CONTACTS = 168;
our $STATUS_FAIL_AS_SET_CALENDAR = 169;
our $STATUS_FAIL_AS_SET_CONTACTS = 170;
our $STATUS_FAIL_MAX_LIMIT_EXCEEDED = 171;
our $STATUS_FAIL_PUID_EMAIL_MISMATCH = 172;
our $STATUS_FAIL_NOT_PROCESSED = 173;
our $STATUS_FAIL_MIME = 174;
our $STATUS_FAIL_LAST_MSG_APPEND = 175;
our $STATUS_FAIL_WELCOME_MSG_APPEND = 176;
our $STATUS_FAIL_AS_CONTEXT_DELETE = 177;
our $STATUS_FAIL_MAILLOGIN_SETTING = 178;
our $STATUS_FAIL_MAILBOX_IN_MAINTENANCE = 179;
our $STATUS_FAIL_SIGNATURE = 180;
our $STATUS_FAIL_MAILBOX_UPDATE = 181;
our $STATUS_FAIL_MAILPROXY_SETTING = 182;
our $STATUS_FAIL_MAILBOX_CREATE = 183;
our $STATUS_FAIL_FORWARDING = 184;
our $STATUS_FAILED_CONSENT = 185;
our $EVENT_REALM_FAILED = 186;
our $STATUS_MIG_INTERRUPTED = 187;
our $STATUS_FAIL_VACATION = 188;
our $STATUS_FAIL_AUTOREPLY = 189;
our $STATUS_MIGRATION_IN_PROGRESS = 190;

our $STATUS_COMPLETION_ERRORS = 197;
our $STATUS_ABORT_ERROR = 198;
our $STATUS_GENERAL_ERROR = 199;

our $EVENT_MIGRATION_START = 100;
our $EVENT_MIGRATION_END = 101;
our $EVENT_MIGRATION_RUN_START = 102;
our $EVENT_MIGRATION_RUN_END = 103;
our $EVENT_WATCHER_START = 110;
our $EVENT_REALM_PROCESS = 111;
our $EVENT_BATCHFILE_START = 112;
our $EVENT_PROCESS_CONSENT = 113;
our $EVENT_PREPARE_PUID = 114;
our $EVENT_FINAL_TIMES = 115;
our $EVENT_UPDATEMX_START = 116;
our $EVENT_FINAL_COUNTS = 117;
our $EVENT_MIGRATION_BATCH_END = 118;
our $EVENT_CONFLICT_IMAGE = 140;
our $EVENT_CONFLICT_PUID = 141;

our %EVENT_NAMES = ($EVENT_MIGRATION_START => 'MigrationStart',
					$EVENT_MIGRATION_END => 'MigrationSuccess',
					$EVENT_MIGRATION_RUN_START => 'MigrationRunStart',
					$EVENT_MIGRATION_RUN_END => 'MigrationRunEnd',
					$EVENT_WATCHER_START => 'Watcher',
					$EVENT_REALM_PROCESS => 'UpdateRealm',
					$EVENT_BATCHFILE_START => 'BatchFileStart',
					$EVENT_PROCESS_CONSENT => 'ProcessConsent',
					$EVENT_PREPARE_PUID => 'UpdatePUID',
					$STATUS_GENERAL_ERROR => 'MigrationRunError',
					$STATUS_ABORT_ERROR => 'MigrationAborted',
					$STATUS_MIG_INTERRUPTED => 'MigrationInterrupted',
					$STATUS_MIG_NOTSTARTED => 'MigrationNotStarted',
					$STATUS_ABORT_ERROR => 'MigrationAborted',
					$STATUS_COMPLETION_ERRORS => 'MigrationSuccessError',
					$EVENT_FINAL_TIMES => 'FinalMigrationTimes',
					$EVENT_FINAL_COUNTS => 'FinalMigrationCounters',
					$STATUS_FAIL_NOT_ALLOWED => 'MigrationNotAllowed',
					$STATUS_MIGRATION_IN_PROGRESS => 'MigrationInProgress',
					$EVENT_MIGRATION_BATCH_END => 'MigrationBatchEnd',
					$EVENT_UPDATEMX_START => 'UpdateMXStart'
	);

our $DEFAULT_JOBID = 'UNK9999';

##############################################################################################################
# processUserLists
#
# Route to take a the user's block / allow data and import is into MX.
# 
# Args:
# - String of data containing block / allow info.
#   String of the form:
#     <email>,<puid>,<allowed|blocked>=<list1>,<list2>,...
#   Example 1 for blocked info:
#     opentest105000@bell.net,00034001CAEEB3D9,blocked=block@this.com:blockthis.com:block01@this.com
#   Example 2 for allowed info:
#     opentest105000@bell.net,00034001CAEEB3D9,allowed=allow01@this.com:allow02@this.com:allow03@this.com:allow04@this.com
# - Debug setting (0=off, 1=on)
# - log object
# - mOS object
# - config object
#
# Return:
# IF successful
#  - Return STATUS_SUCCESS and email address
# ELSE
#  - Return error code and email
#
##############################################################################################################
sub processUserLists($$$$$$$)
{
  my $email = shift;
  my $puid = shift;
  my $user_data_line = shift;
  $DEBUG = shift;
  my $log = shift;
  my $mos = shift;
  my $cfg = shift;
  my $blockOrAllow;

  my @user_data_fields = split(/=/,$user_data_line);

  # Check data line has the correct number of fields.
  my $num_user_data_fields = $#user_data_fields + 1;
  if($num_user_data_fields != 2) {
    $log->error("$email|$puid:In processUserLists, user data has wrong number of fields ($num_user_data_fields) - $user_data_line");
    return($STATUS_FAIL_INVALID_INPUT,$email);
  }

  my @user_fields = split(/,/,$user_data_fields[0]);

  my $num_user_fields = $#user_fields + 1;
  if($num_user_fields != 3) {
    $log->error("$email|$puid:In processUserLists, user data has wrong number of fields ($num_user_fields) - $user_data_line");
    return($STATUS_FAIL_INVALID_INPUT,$email);
  }

  my $list_type = $user_fields[2];
  my $filter_string =  $user_data_fields[1];

print "list_type=$list_type" if ($DEBUG);
  if ( ($list_type ne 'allowed') && ($list_type ne 'blocked') ) {
    $log->warn("$email|$puid:List type not allowed or blocked. Actual list type was $list_type");
    return($STATUS_FAIL_INVALID_LIST_TYPE,$email);
  }

  # Check if number of filters is greater than max filters.
  my @filters = split(/:/,$filter_string);
  my $maxFilters = $cfg->param('max_num_filters');
  my $migFilterStr = "";
  my $notMigFilterStr = "";
  my $count = 0;
  foreach my $thisFilte (@filters) {
    $count++;
    if ($count <= $maxFilters) {
      $migFilterStr .= $list_type.'Sender='.$thisFilte.'&';
    } else {
      $notMigFilterStr .= "$thisFilte:";
    }
  }
  chop($migFilterStr); # Chop the last '&' off.

  $log->debug("$email|$puid:filter to be set = $migFilterStr");
  if ( $notMigFilterStr ne '' ) {
    $log->error("$email|$puid:filter that exceed max (not set) = $notMigFilterStr");
    return($STATUS_FAIL_MAX_LIMIT_EXCEEDED,$email);
  }

  $log->info("$email|$puid:Creating sender rule with $count filters");
  # Set sender blocking filter rule
  $mos->user($email);
  my $rtn = $mos->CreateSenderBlockAllow($migFilterStr,$list_type);
  if (!$mos->is_success) {
    my $decoded_json = decode_json($rtn);
    $log->error("$email|$puid:Failed create sender rule : $rtn");
    return($STATUS_FAIL_MOS_SETTING,$email,$decoded_json->{"shortMessage"});
  }

  return($STATUS_SUCCESS,$email);

}

##############################################################################################################
# processAccount
#
# Route to take a string of account data and import is into MX.
#
# Args:
# - String of data containing language and timezone info.
#   String of the form:
#     <email>,<puid>,<language>,<timezone>
#   Example:
#     opentest105000@bell.net,00034001CAEEB3D9,EN,America/Montreal
# - Debug setting (0=off, 1=on)
# - log object
# - mOS object
# - config object
#
# Return:
# IF successful
#  - Return STATUS_SUCCESS and email address 
# ELSE
#  - Return error code and email
#
##############################################################################################################
sub processAccount($$$$$$$)
{
  my $email = shift;
  my $puid = shift;
  my $user_data_line = shift;
  $DEBUG = shift;
  my $log = shift;
  my $mos = shift;
  my $cfg = shift;

  my @user_data_fields = split(/,/,$user_data_line,-1);

  # Check data line has the correct number of fields.
  my $num_user_data_fields = $#user_data_fields + 1;
  if($num_user_data_fields != 4) {
    $log->error("$email|$puid:In processAccount, user data has wrong number of fields ($num_user_data_fields) - $user_data_line");
    return($STATUS_FAIL_INVALID_INPUT,$email);
  }

  my $locale = $user_data_fields[2];          # Language .
  my $tz =  $user_data_fields[3];             # Time zone string (eg, GMT or America/Toronto).
  $tz =~ s/\r|\n//g;
  my $defaultTimezone = $cfg->param("default_tz");
  if ( (! defined $tz) || ( $tz eq "" ) || ( ! $tz )) {
	$tz = $defaultTimezone;
	$log->debug("$email|$puid: Setting default timezone ($tz)");
  }
  $log->debug("$email|$puid: Setting  timezone ($tz)");

  # Set timezone via mOS
  $mos->user($email);
  my $rtn = $mos->GeneralPreference("timezone=".$tz);
  if (!$mos->is_success) {
	chomp($rtn);
	$log->error("$email|$puid:Failed set timezone : tz = $tz : ".$rtn);
	#if unsuccessful default to default timezone unless already set as default
	if ($tz ne $defaultTimezone) {
		$tz = "$defaultTimezone";
		$log->error("$email|$puid:Defaulting to : defaultTimezone = $defaultTimezone ");
		my $rtn2 = $mos->GeneralPreference("timezone=".$tz);
		chomp($rtn2);
		if (!$mos->is_success) {
			$log->error("$email|$puid:Failed set default timezone : tz = $defaultTimezone : ".$rtn2);
			# $mos->PrintResponse;
			return($STATUS_FAIL_MOS_SETTING,$email);
		 }
	} else {
		# $mos->PrintResponse;
		return($STATUS_FAIL_MOS_SETTING,$email);
	}
  }  else {
    $log->info("$email|$puid:Set timezone to $tz");
  } 

  # Only support one language EN so default all to the default locale
  my $localeAccount = $cfg->param('default_locale');
  if ( $locale ne "EN" ) {
	$log->error("$email|$puid: non-english locale setting.");
  }

  # Set locale via mOS
  $mos->user($email);
  $localeAccount = "locale=$localeAccount";
  $rtn = $mos->GeneralPreference($localeAccount);

  if (!$mos->is_success) {
    chomp($rtn);
    $log->error("$email|$puid:Failed set locale : locale = $localeAccount : ".$rtn); 
    return($STATUS_FAIL_MOS_SETTING,$email);
  } else {
    $log->info("$email|$puid:Set locale to $localeAccount");
  }
  return($STATUS_SUCCESS,$email,$tz);
}

##############################################################################################################
# processMailForward
#
# Route to take a string of mail forwarding data and import is into MX.
#
# Args:
# - String of data containing mail forwarding info.
#   String of the form:
#     <email>,<puid>,<lastWrite>,<enabled>,<keepCopy>,<emails>
#     where - 
#     <lastWrite> can be ignored.
#     <enabled> 0 if disables, 1 if enabled
#     <keepCopy> 1 if the user desires to keep a copy of the message
#     <emails> list of e-mail addresses to forward messages to in hex format. E-mail addresses are separated by a semicolon after decoding.
#   Example:
#     opentest105000@bell.net,00034001CAEEB3D9,2013-10-02 21:47:27.587,1,0,0x6972656E65406C697070657267726F75702E636F6D
# - Debug setting (0=off, 1=on)
# - log object
# - mOS object
# - config object
# 
# Return:
# IF successful
#  - Return STATUS_SUCCESS and email address
# ELSE
#  - Return error code and email
#
##############################################################################################################
sub processMailForward($$$$$$$)
{
  my $email = shift;
  my $puid = shift;
  my $user_data_line = shift;
  $DEBUG = shift;
  my $log = shift;
  my $mos = shift;
  my $cfg = shift;
  my $overallSuccess = 1;

  my @user_data_fields = split(/,/,$user_data_line);

  # Check data line has the correct number of fields.
  my $num_user_data_fields = $#user_data_fields + 1;
  if($num_user_data_fields != 6) {
    $log->("$email|$puid:In processMailForward, user data has wrong number of fields ($num_user_data_fields) - $user_data_line");
     return($STATUS_FAIL_INVALID_INPUT,$email);
  }

  my $changeTime = $user_data_fields[2];       # Not used.
  my $enabled = $user_data_fields[3];          # 0 = Forwarding disabled, 1 = forwarding enabled.
  my $keepCopy = $user_data_fields[4];         # 0 = Don't keep copy, 1 = keep copy.
  my $hexForwardingStr = $user_data_fields[5]; # Forwarding string in HEX.
  $hexForwardingStr =~ s/\r|\n//g;             # Remove ^M at end if it is there.

  # HERE ARE THE STEPS NEEDED FOR ALL USER FORWARDING:
  # 1) Set forwardingEnabled=yes, this allow another forwarding API calls to work
  # 2) Set copyOnForward=yes/no
  # 3) Add forwarding list of addresses
  # 4) If forwarding is disabled, set forwardingEnabled=no

  # Step 1) Set forwardingEnabled=yes
  $mos->user($email);
  my $rtn = $mos->SetForwardingSettings("forwardingEnabled=yes");
  if (!$mos->is_success) {
    chomp($rtn);
    $log->error("$email|$puid:forwarding data on 1 of 4 failed : ".$rtn);
	$overallSuccess = 0;
    # $mos->PrintResponse;
  }  else {
    $log->info("$email|$puid:forwarding data 1 of 4 complete");
  }

  # Step 2) Set copyOnForward=yes/no
  $mos->user($email);
  if ($keepCopy == 1) {
    $rtn = $mos->SetForwardingSettings("copyOnForward=yes");
  } else {
    $rtn = $mos->SetForwardingSettings("copyOnForward=no");
  }
  if (!$mos->is_success) {
    chomp($rtn);
    $log->error("$email|$puid:forwarding data on 2 of 4 failed : ".$rtn);
	#set forwarding to disabled when copyOnForward fails
    $enabled = 0;
	$overallSuccess = 0;
    # $mos->PrintResponse;
  }  else {
    $log->info("$email|$puid:forwarding data 2 of 4 complete");
  }

  # Step 3) Add forwarding list of addresses
  # Step 3a) Convert HEX string to ascii
  $hexForwardingStr =~ s/^0x//;
  $hexForwardingStr =~ s/([a-fA-F0-9][a-fA-F0-9])/chr(hex($1))/eg;
  my $asciiForwardingStr = $hexForwardingStr;

  # Step 3b) Convert ASCII string to format mOS will take
  $asciiForwardingStr =~ s/;/&forwardingAddress=/g;
  $asciiForwardingStr = "forwardingAddress=".$asciiForwardingStr;
  $log->debug("$email|$puid:asciiForwardingStr = $asciiForwardingStr") if ($DEBUG);

  # Step 3c) Set forwarding addresses
  $mos->user($email);
  $rtn = $mos->SetForwardingAddresses($asciiForwardingStr);
  if (!$mos->is_success) {
    chomp($rtn);
    $log->warn("$email|$puid:forwarding data on 3 of 4 failed : ".$rtn);
	#set forwarding to disabled when setting email address fails
    $enabled = 0;
	$overallSuccess = 0;
    # $mos->PrintResponse;
  }  else {
    $log->info("$email|$puid:forwarding data 3 of 4 complete");
  }

  # 4) If forwarding is disabled, set forwardingEnabled=no
  if ($enabled == 0) {
    $mos->user($email);

    # Note: There is a bug in the mOS API and disabling forwarding by trying
    #       to set "forwardingEnabled=no", fails. See JIRA SERVICES-2204 and LEAPFROG-2775.
    #       There is a workaround using "forwardingEnabled=no&copyOnForward=yes" to disable forwarding.

    # $rtn = $mos->SetForwardingSettings("forwardingEnabled=no");
    $rtn = $mos->SetForwardingSettings("forwardingEnabled=no&copyOnForward=yes");

    if (!$mos->is_success) {
      chomp($rtn);
      $log->warn("$email|$puid:forwarding data on 4 of 4 failed : ".$rtn);
	  $overallSuccess = 0;
      $mos->PrintResponse  if ($DEBUG);
    }  else {
      $log->info("$email|$puid:forwarding data 4 of 4 complete");
    }
  } else {
    # Nothing to do.
    $log->info("$email|$puid:forwarding data 4 of 4 complete");
  }

  if ( $overallSuccess == 1) { 
	return($STATUS_SUCCESS,$email);
  } else {
	return($STATUS_FAIL_FORWARDING,$email);
  }

}

##############################################################################################################
# processVacation
#
# Route to take a string of vacation data and import is into MX.
#
# Args:
# - String of data containing vacation info.
#   String of the form:
#     <email>,<puid>,<lastWrite>,<autoReplyStartTime>,<autoReplyEndTime>,<enabled>,<onlyContacts>,<vacationReply>
#     where -
#     <lastWrite> can be ignored.
#     <autoReplyStartTime> and <autoReplyEndTime> are the dates for which the automatic reply will take effect and end on. Not supported by script.
#     <enabled> 0 if disables, 1 if enabled.
#     <onlyContact> 1 if reply-to-contacts-only is enabled. Not supported by script.
#     <vacationReply> reply in hex format.
#   Example:
#     opentest105000@bell.net,00034001CAEEB3D9,2014-08-21 12:54:14.880,2014-08-21 12:54:00,2079-06-06 00:00:00,0,0,0x436F6E74656E742D547970653A20746578742F68746D6C3B20636861727365743D2269736F2D383835392D31220D0A46726F6D3A203C737461727A794073796D70617469636F2E63613E0D0A5375626A6563743A0D0A436F6E74656E742D5472616E736665722D456E636F64696E673A2071756F7465642D7072696E7461626C650D0A4D494D452D56657273696F6E3A20312E300D0A0D0A4920616D206177617920756E74696C204D6F6E6461793D32432041756775737420323574682E2049206861766520736F6D652061636365737320746F20656D61696C20696620492063616E3D0D0A20666967757265206F757420686F7720746F20757365206D79207461626C6574207768656E20492074726176656C20284C756464697465292E3C6469763E3C6469763E3C62723E3C2F64693D0D0A763E3C6469763E506C61792042616C6C21213C2F6469763E3C6469763E3C62723E3C2F6469763E3C6469763E4A6F686E3C2F6469763E3C2F6469763E3D0D0A
# - Debug setting (0=off, 1=on)
# - log object
# - mOS object
# - config object
#
# Return:
# IF successful
#  - Return STATUS_SUCCESS and email address
# ELSE
#  - Return error code and email
#
##############################################################################################################
sub processVacation($$$$$$$)
{
  my $email = shift;
  my $puid = shift;
  my $user_data_line = shift;
  $DEBUG = shift;
  my $log = shift;
  my $mos = shift;
  my $cfg = shift;

  my @user_data_fields = split(/,/,$user_data_line);

  # Check data line has the correct number of fields.
  my $num_user_data_fields = $#user_data_fields + 1;
  if($num_user_data_fields != 8) {
    $log->error("$email|$puid:In processVacation, user data has wrong number of fields ($num_user_data_fields) - $user_data_line");
    return($STATUS_FAIL_INVALID_INPUT,$email);
  }

  my $timestampWrite = $user_data_fields[2];  # Date of when rule was created (not used).
  my $timestampStart = $user_data_fields[3];  # Date of when to start sending vacation msg.
  my $timestampEnd   = $user_data_fields[4];  # Date of when to stop sending vacation msg.
  my $timestampEndYear;
  my $timestampEndMonth;
  my $timestampEndDay;
  my $enabled =  $user_data_fields[5];        # 1 = enables, 0 = disabled
  my $onlyContacts = $user_data_fields[6];    # 1 = enables, 0 = disabled. 'onlyContacts' is not supported in AppSuite. If enable, don't enable the vacation msg.
  my $hexVacationStr = $user_data_fields[7];  # Vacation string in HEX.
  $hexVacationStr =~ s/\r|\n//g;

  # Convert HEX string to ascii
  $hexVacationStr =~ s/^0x//;
  $hexVacationStr =~ s/([a-fA-F0-9][a-fA-F0-9])/chr(hex($1))/eg;
  my $asciiVacationStr = $hexVacationStr;
  # The ascii is actually an email with MIME body data being the actual vacation message.
  # Now extract MIME body from ascii.
  my $parsed = Email::MIME->new($asciiVacationStr);
  #$parsed->encoding_set( 'quoted-printable' );
  #$parsed->charset_set( 'UTF-8' );
  my $mimeBody = $parsed->body;
  $mimeBody =~ s/\x0D\n|\x0D\r//g; #get rid of ^M line feeds
   print("$email|$puid:Vacation MIME body BEFORE :\n$mimeBody\n") if ($DEBUG);
  #substitue and remove HTML characters as UX Suite doesnt support html in Vacation Message
  $mimeBody =~ s/<div><br><\/div>/\n/g; #substitute BR withing a div with one line feed
  $mimeBody =~ s/<br>\s+/\n/g; #substitute BR with line feed
  $mimeBody =~ s/<br>/\n/g; #substitute BR with line feed
  $mimeBody =~ s/<div>//g; #remove start div blocks.
  $mimeBody =~ s/<\/div>/\n/g; #substitute end div blocks with a line feed as this would be how it would look in HTML. 
  $mimeBody =~ s/&#160;/ /g; # replace non-breaking space with a regular space, as UX renders it as a question mark in a black diamond
  #Strip out any additional HTML characters remaining.
  my $hs = HTML::Strip->new();
  $mimeBody = $hs->parse( $mimeBody );
  $hs->eof;
  $mimeBody =~ s/^\s+//g; #remove any spaces at the beginning of the text
  if ($parsed->content_type =~ /charset="([^"]+)"/i) {
    my $charset = $1;
    $log->info($email.'|'.$puid.':Vacation message charset is "'.$charset.'"');
    if (lc($charset) ne "utf-8") {
      my $mimeBodyUtf8 = decode($charset, $mimeBody);
      $mimeBody = $mimeBodyUtf8;
    }
  }

  print("$email|$puid:Vacation MIME body:\n$mimeBody\n") if ($DEBUG);

  # Check if the vacation end time has passed. If it has, disable the vacation message when importing.
  $log->debug("$email|$puid:Vacation timestampEnd:$timestampEnd");

  if ( $timestampEnd =~ /(\d{4})-(\d{2})-(\d{2}) \d+:\d+:\d+/ ) {

    my $dateEnd = $1.$2.$3;
    $log->debug("$email|$puid:timestampEnd in expected timestamp format, (dateEnd = $dateEnd)") if ($DEBUG);

    # Now get current date...
    my $dt = DateTime->now;
    my $dateNow = $dt->ymd;
    $dateNow =~ s/-//g;
    $log->debug("$email|$puid:Vacation dateNow:$dateNow") if ($DEBUG);

    # Compare current date to vacation end time.
    if ( $dateNow > $dateEnd ) {
      $enabled = 0;
      $log->info("$email|$puid:Vacation date has passed, so disable vacation message.");
    } 
 
  } else {
    $log->error("$email|$puid:timestampEnd not in expected timestamp format (YYYY-MM-DD hh:mm:ss). timestampEnd = $timestampEnd.");
  }
 
  # Check if the vacation onlyContacts is enabled. If it has, disable the vacation message when importing.
  # onlyContacts is a MSFT option to send the vacation message only to senders who are in the users addressbook. we do not have that feature so the vacation message is migrated but disabled.
  if ( $onlyContacts eq 1 ) {
    $enabled = 0;
    $log->info("$email|$puid:Vacation onlyContacts is enable, so disable vacation message.");
  }

  my @parts = $parsed->parts;
  if ((scalar @parts) gt 1) {
    $log->error("$email|$puid:More than one MIME parts were found in the vacation message.");
    return($STATUS_FAIL_MIME,$email);
  } elsif ($mimeBody eq '') {
    $log->info("$email|$puid:No MIME parts of message.");
    $mimeBody = ' ';
  }

  my $autoReplyMode;
  if ($enabled == 0) {
    $autoReplyMode = "none";
  } else {
    # $autoReplyMode = "vacation";  # Reply once.
    $autoReplyMode = "reply";  # Reply everytime.
  }

  # Set Vacation message
  $mos->user($email);
  my $rtn = $mos->SetVacation($autoReplyMode,$mimeBody);
  if (!$mos->is_success) {
    chomp($rtn);
    $log->error("$email|$puid:Setting vacation failed : ".$rtn);
    # $mos->PrintResponse;
	return($STATUS_FAIL_VACATION,$email);
  }  else {
    $log->info("$email|$puid:Setting vacation complete");
    return($STATUS_SUCCESS,$email);
  }

}

##############################################################################################################
# processSignature
#
# Route to take a string of signature data and import is into MX.
#
# Args:
# - String of data containing signature info.
#   String of the form:
#     <email>,<puid>,<lastWrite>,<signature>
#     where -
#     <lastWrite> can be ignored.
#     <signature> signature text in hex format.
#   Example:
#     opentest105000@bell.net,00034001CAEEB3D9,2010-11-30 03:10:23.787,0x3C666F6E742073697A653D333E0D0A3C7374726F6E673E3C666F6E742073697A653D3320666163653D476172616D6F6E643E3C2F666F6E743E3C2F7374726F6E673EC2A03C62723E0D0A3C7374726F6E673E3C666F6E742073697A653D3320666163653D476172616D6F6E643E4469616E652026616D703B204D696368656C6C652048696C626F726E3C2F666F6E743E3C2F7374726F6E673E3C62723E0D0A3C666F6E742073697A653D3320666163653D476172616D6F6E643E5374616D70696E27205570212044656D6F6E73747261746F72733C2F666F6E743E3C62723E0D0A3C6120687265663D226D61696C746F262335383B64657369676E696E676D696E64734062656C6C2E6E6574223E3C666F6E742073697A653D3320666163653D476172616D6F6E643E64657369676E696E676D696E64734062656C6C2E6E65743C2F666F6E743E3C2F613E3C666F6E742073697A653D3320666163653D476172616D6F6E643E207C202835313929203736362D393832333C2F666F6E743E3C62723E0D0A3C666F6E742073697A653D3320666163653D476172616D6F6E643E436F6E7461637420757320746F2067657420796F7572204652454520636F7079206F662074686520323031302D32303131204964656120426F6F6B2026616D703B20436174616C6F6775653C2F666F6E743E3C62723E3C2F666F6E743E
# - Debug setting (0=off, 1=on)
# - log object
# - mOS object
# - config object
#
# Return:
# IF successful
#  - Return STATUS_SUCCESS and email address
# ELSE
#  - Return error code and email
#
##############################################################################################################
sub processSignature($$$$$$$$$)
{
  my $email = shift;
  my $puid = shift;
  my $user_data_line = shift;
  $DEBUG = shift;
  my $log = shift;
  my $mos = shift;
  my $cfg = shift;
  my $userPwd = shift;
  my $conflictLog = shift;
  my $img_rplc_data = $cfg->param("img_replace_data");
  my $endImgTag;
  my $end_ind;
  my $fragment;

  my @user_data_fields = split(/,/,$user_data_line);

  # Check data line has the correct number of fields.
  my $num_user_data_fields = $#user_data_fields + 1;
  print "\n-------num_user_data_fields=$num_user_data_fields--------\n"  if ($DEBUG);
  if($num_user_data_fields != 4) {
    $log->error("$email|$puid:In processSignature, user data has wrong number of fields ($num_user_data_fields) - $user_data_line");
    return($STATUS_FAIL_INVALID_INPUT,$email);
  }

  my $timestamp1 = $user_data_fields[2];      # Not used
  my $hexSignatureStr = $user_data_fields[3];  # Signature string in HEX.
  $hexSignatureStr =~ s/\r|\n//g;

  # Convert HEX string to ascii
  $hexSignatureStr =~ s/^0x//;
  $hexSignatureStr =~ s/([a-fA-F0-9][a-fA-F0-9])/chr(hex($1))/eg;
  $hexSignatureStr =~ s/\x0D\n|\x0D\r//g; #get rid of ^M line feeds
  ##substitue and remove HTML characters as UX Suite doesnt support html in Vacation Message
  #$hexSignatureStr =~ s/<br>/\n/g; #substitute BR with line feed
  #$hexSignatureStr =~ s/<div>//g; #substitute div blocks with a line feed as this would be how it would look in HTML.
  #$hexSignatureStr =~ s/<\/div>/\n/g; #substitute end div blocks with a line feed as this would be how it would look in HTML. 
  ##Strip out any additional HTML characters remaining.
  #my $hs = HTML::Strip->new();
  #my $asciiSignatureStr = $hs->parse( $hexSignatureStr );
  #$hs->eof;
  #prepend a content type to force rich text signature
  my $asciiSignatureStr;
  #Remove Inline image from signature
  my $startImgTag = index($hexSignatureStr, "<img ");#Find out if we have any images in the signature
  if ( $startImgTag > -1 ) {  # image tag is found, log it and replace it.
	  my $errorImgTag = index($hexSignatureStr, "GetInline.aspx",$startImgTag); #Find out if the image is a suspected unmigratable image 
	  if ($errorImgTag > -1 ) {
		  $conflictLog->warn("$email|$puid: $EVENT_CONFLICT_IMAGE Suspect Image tag found in signature.");
		  $startImgTag +=4;# adds up characters of search string given in above sentence
		  $endImgTag = index($hexSignatureStr,"</img>",$startImgTag); #Finds out index of end Image Tag
		  if ($endImgTag != -1){
			 $end_ind = $endImgTag + 5; #adds up characters of end search string
		  } else {
			  $endImgTag = index($hexSignatureStr,">",$startImgTag); #if <img> tag is closed with >, searches end of <img tag by looking for the next > 
			  $end_ind = $endImgTag + 1;# adds up characters of end search string
		  }
		  my $start_ind = $startImgTag - 4;
		  my $len_img = $end_ind-$start_ind;# finds out length of image tag
		  
		  if (length $img_rplc_data){
			$fragment =  substr $hexSignatureStr, $start_ind, $len_img, "<div>$img_rplc_data</div>";
			$log->debug("$email|$puid:Replaced image tag in signature. ");
			print ("IMAGE TAG Fragment:Replaced image tag in signature \nEntire Image Tag :------- $fragment\n") if ($DEBUG);
		} else {
			$log->debug("$email|$puid:Suspect image tag NOT REPLACED as per configuration. ");
		}
	} else {
		$log->debug("$email|$puid:Signature contains an non-suspect image");
	}
  }
 
  if ( $hexSignatureStr =~ "<.+>" ) {  #if any end tags are found asume rich text
	$asciiSignatureStr = "Content-Type: text/html\n".$hexSignatureStr;
  } else {
	$asciiSignatureStr = $hexSignatureStr;
  }
  
  print("$email|$puid:asciiSignatureStr :\n$asciiSignatureStr") if ($DEBUG);
  # Set Signature
  my $signature_set_via = $cfg->param('signature_set_via');
  if ($signature_set_via eq 'mos') {

    #
    #              *** IMPORTANT ***
    #
    # Originally, the script was gotting to use mOS to set signation in AppSuite,
    # but could not get it working at the time the script was written. Looked like
    # a bug in mOS. JIRA SERVICES-2211.
    #

    $log->debug("$email|$puid:Set signature via mOS.") if ($DEBUG);

    # Signature will be set via mOS. 
    $mos->user($email);

    my $target = $cfg->param('signature_target');  # target should be 'mss' or 'appsuite'

    my $rtn = $mos->signatureCreate($target,$asciiSignatureStr,'signature','yes'); 
    if (!$mos->is_success) {
      chomp($rtn);
      $log->error("$email|$puid:Setting signature failed : ".$rtn);
      $mos->PrintResponse  if ($DEBUG);
	  return($STATUS_FAIL_SIGNATURE,$email);
    }  else {
      $log->info("$email|$puid:Setting signature complete");
    }

  } elsif ($signature_set_via eq 'appsuite') {

    $log->debug("$email|$puid:Set signature via AppSuite.") if ($DEBUG);

    # Signature will be set directly using OX (AppSuite)
    my $as_host = $cfg->param('appSuiteHost');
    my $as_port = $cfg->param('appSuitePort');
    my $ox = OxHttpApi->new($as_host,$as_port);
    if (!$ox->is_success) {
      $log->error("$email|$puid:Failed to connect to AppSuite. as_host = $as_host, as_port = $as_port");
      return($STATUS_FAIL_AS_CONNECT,$email);
    }

    # Set debug on for OxHttpApi object if needed.
    $ox->debugOn if $DEBUG;

    # Login...
    my $wait = $cfg->param('allowForReplication');
    my $originIp =  $cfg->param('appSuiteOriginIp'); # If the originIp is in the AppSuite's white list, login to an account in 'M' will work.
    #$ox->Login($email,$userPwd,$originIp);
    $ox->LoginWithRetries($email,$userPwd,$originIp,3,5);
    if (!$ox->is_success) {
      $log->error("$email|$puid:Failed to login and get session ID for user (AS host = $as_host). Sleep for $wait sec and try AS alternate host.");
      sleep($wait);
      
      # Retry login...
      $as_host = $cfg->param('appSuiteHostAlt');
      $ox->host($as_host);
      $ox->Login($email,$userPwd,$originIp);
      if (!$ox->is_success) {
        $log->error("$email|$puid:Failed to login and get session ID for user on 2nd attempt (AS host = $as_host).");
        return($STATUS_FAIL_AS_LOGIN,$email);
      } else {
        $log->info("$email|$puid:Logged in and got session ID on 2nd attempt (AS host = $as_host).");
      }

    } else {
      $log->debug("$email|$puid:Logged in and got session ID.") if $DEBUG;
    }

    # Sleep for a second, in case a bit if time is needed after Login (and context creation).
    sleep($wait);

    # The OX snippetObj build just below, need some characters to be escaped with a backslash.
    # For details, see: http://oxpedia.org/wiki/index.php?title=AppSuite:HTTP_API#Low_level_protocol
    $asciiSignatureStr =~ s/\\/\\\\/g;  # Replace backslash (\) with backslash backslash (\\)
    $asciiSignatureStr =~ s/"/\\"/g;    # Replace double quote (") with backslash double quote (\")
    $asciiSignatureStr =~ s/\r/\\\r/g;  # Replace newline (\r) with backslash newline (\\r)

    $log->debug("$email|$puid:asciiSignatureStr after char replacement:\n$asciiSignatureStr") if $DEBUG;

    my $snippetObj = '{"content":"'.$asciiSignatureStr.'", "type":"signature", "displayname":"Signature", "module":"io.ox/mail"}';
    my $newId = $ox->PutAllSnippet($snippetObj,'new');
    if (!$ox->is_success) {
      $ox->PrintResponse  if ($DEBUG);
      $log->error("$email|$puid:Failed to set signature.");
      return($STATUS_FAIL_AS_SET_SIGN,$email);
    } else {
      $log->debug("$email|$puid:Set signature.") if $DEBUG;
    }

    # Logout...
    $ox->Logout;
    if (!$ox->is_success) {
      $log->debug("$email|$puid:Logout failed.") if $DEBUG;
    }

  } else {

    $log->error("$email|$puid:signature_set_via not valid. signature_set_via = $signature_set_via");
    die;
    
  }

  $log->info("$email|$puid:Signature set successfully.");
  return($STATUS_SUCCESS,$email);

}

##############################################################################################################
# lookupPUID
#
# Routine to lookup the ldap account using PUID.
#
# Args:
# - puid
# - config object
# - log object
#
# Return:
# - status
# - email address
# - mailboxstatus
# - migstatus
# - consentstatus
# - maillogin
# - pabstorehost
# - calstorehost
# - adminuserpolicy
# - mailfrom
#
##############################################################################################################
sub lookupPUID($$$)
{
  my $puid = shift;
  my $cfg = shift;
  my $log = shift;
  my $searchString;
  my $mode;
  my $email;
  my $logString;
  my $mx_dircache_host = $cfg->param('mx_dircache_host');
  my $mx_dircache_port = $cfg->param('mx_dircache_port');
  my $mx_dircache_pwd  = $cfg->param('mx_dircache_pwd');

  my $ldap = Net::LDAP->new ( $mx_dircache_host, port => $mx_dircache_port );
  if (! $ldap) {
    $log->error("PUID|$puid:connect to $mx_dircache_host:$mx_dircache_port failed");
    return($STATUS_FAIL_AS_CONNECT);
  }

  my $mesg = $ldap->bind ( "cn=root", password => $mx_dircache_pwd );
  if ( $mesg->code ne '0' ) {
    $log->error("PUID|$puid:bind to $mx_dircache_host:$mx_dircache_port failed. Check LDAP password.");
    return($STATUS_FAIL_AS_CONNECT);
  } 

  if ( index($puid, "@") != -1  ){
	#An email address was passed in the PUID attribute, so search by email
	$mode="owm";
	$searchString = "mail=$puid";
	$email=$puid;
	$logString = "$email|";
  } else {
	$searchString = "puid=$puid";
	$logString = "PUID|$puid:";
  }


	$log->debug("$logString searchString = $searchString");

  my $result = $ldap->search ( base   => "", filter  => "$searchString")
    || $log->error("$logString search failed");

  if ($result->code) {
    $log->error("$logString Lookup failed. $result->error");
    return($STATUS_FAIL_PUID_LOOKUP);
  } elsif ($result->count == 0) {
    $log->error("$logString 0 accounts were found.");
    return($STATUS_FAIL_PUID_LOOKUP);
  } elsif ($result->count > 1) {
    $log->error("$logString 2 or more accounts were found.");
    return($STATUS_FAIL_PUID_EMAIL_MISMATCH);
  }

  my @entries = $result->entries;

  my $dn = $entries[0]->dn;
  (my $jumk,$email,my @jumk) = split(/[=,]/,$dn);

  my $mailboxstatus = $entries[0]->get_value("mailboxstatus");
  my $migstatus = $entries[0]->get_value("migstatus");
  my $consentstatus = $entries[0]->get_value("consentstatus");
  my $maillogin = $entries[0]->get_value("maillogin");
  my $userpabstorehost = $entries[0]->get_value("pabstorehost");
  my $usercalstorehost = $entries[0]->get_value("calendarstorehost");
  my $useradminpolicy = $entries[0]->get_value("adminpolicydn");
  my $USERTZ = $entries[0]->get_value("msgtimezone");
  my $mailfrom = $entries[0]->get_value("mailfrom");
  
  my $mssvip = $entries[0]->get_value("mailmessagestore");
  my @adminpolicy = split(',',$useradminpolicy);
  my @adminpolicy1 = split('=',$adminpolicy[0]);
  $useradminpolicy = $adminpolicy1[1];

  return($STATUS_SUCCESS,$email,$mailboxstatus,$migstatus,$consentstatus,$maillogin,$userpabstorehost,$usercalstorehost,$useradminpolicy,$USERTZ,$mailfrom, $mssvip);
}

##############################################################################
# lookupExternalMailAccounts
# 
# Routine to lookup external mail accounts in MX8 using EMAIL address.
#
# Args:
# - email address
# - config object
# - log object
#
# Return:
# - external mail accounts as list
#
##############################################################################
sub lookupExternalMailAccounts
{
    my $puid = shift;
    my $cfg = shift;
    my $log = shift;
    my $searchString;
    my $mode;
    my $email;
    my $logString;
    my $mx_dircache_host = $cfg->param('mx_dircache_host_src');
    my $mx_dircache_port = $cfg->param('mx_dircache_port_src');
    my $mx_dircache_pwd  = $cfg->param('mx_dircache_pwd_src');

    my $ldap = Net::LDAP->new ( $mx_dircache_host, port => $mx_dircache_port );
    if (! $ldap) {
      $log->error("PUID|$puid:connect to $mx_dircache_host:$mx_dircache_port failed");
      return($STATUS_FAIL_AS_CONNECT);
    }
    my $mesg = $ldap->bind ( "cn=root", password => $mx_dircache_pwd );
    if ( $mesg->code ne '0' ) {
      $log->error("PUID|$puid:bind to $mx_dircache_host:$mx_dircache_port failed. Check LDAP password.");
      return($STATUS_FAIL_AS_CONNECT);
    } 

    if ( index($puid, "@") != -1  ) {
      	#An email address was passed in the PUID attribute, so search by email
        $mode="owm";
        $searchString = "mail=$puid";
        $email=$puid;
        $logString = "$email|";
    }
    else {
        $searchString = "puid=$puid";
        $logString = "PUID|$puid:";
    }

    $log->debug("$logString searchString = $searchString");
    
    
    my $result = $ldap->search ( base   => "", filter  => "$searchString")
      || $log->error("$logString search failed");

    if ($result->code) {
      $log->error("$logString Lookup failed. $result->error");
      return($STATUS_FAIL_PUID_LOOKUP);
    } elsif ($result->count == 0) {
      $log->error("$logString 0 accounts were found.");
      return($STATUS_FAIL_PUID_LOOKUP);
    } elsif ($result->count > 1) {
      $log->error("$logString 2 or more accounts were found.");
      return($STATUS_FAIL_PUID_EMAIL_MISMATCH);
    }

    my @entries = $result->entries;

    my $dn = $entries[0]->dn;
    (my $jumk,$email,my @jumk) = split(/[=,]/,$dn);

    my @external_accounts = $entries[0]->get_value("netmailpopacctdetails");

    return (@external_accounts);
}

##############################################################################################################
# lookupPuidFromEmail
# 
# Routine to lookup account using EMAIL address.
#
# Args:
# - email address
# - config object
# - log object
#
# Return:
# - puid
# - mailboxid
#
##############################################################################################################
sub lookupPuidFromEmail($$$)
{
  my $email = shift;
  my $cfg = shift;
  my $log = shift;
  my $mx_dircache_host = $cfg->param('mx_dircache_host');
  my $mx_dircache_port = $cfg->param('mx_dircache_port');
  my $mx_dircache_pwd  = $cfg->param('mx_dircache_pwd');

  my $ldap = Net::LDAP->new ( $mx_dircache_host, port => $mx_dircache_port ) or die "$@";

  my $mesg = $ldap->bind ( "cn=root",
                        password => $mx_dircache_pwd );

  my $searchString = "mail=$email";
  $log->debug("$email|searchString = $searchString") if ($DEBUG);

  my $result = $ldap->search ( base   => "",
                               filter  => "$searchString");

  if ($result->code) {
    $log->error("$email|PUID lookup for EMAIL failed. $result->error");
    return;
  } elsif ($result->count != 1) {
    $log->error("$email|Either 0 or 2+ accounts had matching EMAIL ($email). Can't determine which email address to use.");
    return("many") if ($result->count > 1);
    return("") if ($result->count == 0);
  }

  my @entries = $result->entries;

  my $puid = $entries[0]->get_value("puid");
  $log->debug("$email|EMAIL ($email) --> PUID ($puid)");
  return($puid,$entries[0]->get_value("mailboxid"));
}

##############################################################################################################
# processCalendarContacts
#
# Route to migration a user's calendar, contact and name data from Microsoft platform to AppSuite.
#
# Args:
# - puid
# - email
# - Debug setting (0=off, 1=on)
# - log object
# - mOS object
# - config object
# - do_cal (0=no, 1=yes)   WARNING - Only call sub for one at a time! Sub should be split in three (will do if I have time).
# - do_cont (0=no, 1=yes)  WARNING - Only call sub for one at a time! Sub should be split in three (will do if I have time).
# - do_name (0=no, 1=yes)  WARNING - Only call sub for one at a time! Sub should be split in three (will do if I have time).
# - password
# - access_token
#
# Return:
# IF successful
#  - Return STATUS_SUCCESS and email address
# ELSE
#  - Return error code and email
#
##############################################################################################################
sub processCalendarContacts
{
  my $puid = shift;
  my $email = shift;
  $DEBUG = shift;
  my $log = shift;
  my $conflictLog = shift;
  my $mos = shift;
  my $cfg = shift;
  my $do_cal = shift;
  my $do_cont = shift;
  my $do_name = shift;
  my $password = shift;
  my $access_token = shift;
  my $usercalstorehost = shift;
  my $userpabstorehost = shift;
  $USERTZ = shift;
  my $start_date_range = shift;
  my $end_date_range = shift;
  my $preload = shift;
  my $refreshToken;
  my $calendar;
  my $events;
  my $contacts;
  my $iCalendar = "BEGIN:VCALENDAR\nVERSION:2.0\nCALSCALE:GREGORIAN\nMETHOD:PUBLISH\n";
  #the default return status will be success and be overridden in case of failure.
  my $status=$STATUS_SUCCESS;
  
  #Record counts for reporting		
  my %counters;	
  #initialise counters to -1 so that in the case none are found we dont get an unitialised value
  $counters{'retrieved'}=-1;
  $counters{'eventsRetrieved'}=-1 if ( $do_cal );
  $log->debug("$email|$puid:processCalendarContacts") if ($DEBUG);
  if ( $cfg->get_param('SERVICES-3182_Workaround') eq "true") {
	  #update user status to ACTIVE for migration of PAB CAl. work around for SERVICES-3182
	  $log->debug("$email| Setting account status to active for PABCAL migration");
	  $mos->userUpdateMailbox("update",$email,"active",$cfg->param('defaultCos'),"none",$cfg,$log) ;
		  if (! $mos->is_success) {
			$log->error("$email|Could not set user to active. skipping migration of PAB/CAL data.");
			return($STATUS_GENERAL_ERROR,$email);
		  }
  }

  ###
  ### Do first and last name
  ###

  if ( $do_name ) {
    my $retryCount = 0;
    my $successful = 0;
    $log->info("$email|$puid:Get first and last name info.");
    while ((!$successful) && ($retryCount < 5)) {
      $retryCount ++;
      $contacts = getFirstLastName($email, $cfg, $log, $conflictLog, $access_token);
      if (defined($contacts)) {
        $successful = 1;
      } else {
        sleep($retryCount);
      }
    }
    if ( ! defined $contacts ) {
      $log->error("$email|$puid:Failed to retrieve names");
	  #update user status to ACTIVE for migration of PAB CAl. work around for SERVICES-3182
	   if ( $cfg->get_param('SERVICES-3182_Workaround')  eq "true") {
		  $mos->userUpdateMailbox("update",$email,"proxy",$cfg->param('defaultCos'),"none",$cfg,$log);
		  if (! $mos->is_success) {
			$log->error("$email|Could not set user to proxy. MAIL WILL NOT MIGRATE.");
		  }
	  }
      return($STATUS_FAIL_MSFT_NAME,$email);
    }

    print "\n\ncontacts:\n$contacts\n\n" if $DEBUG;
    my $decoded = decode_json($contacts);
    my $firstName = $decoded->{'first_name'};
    my $lastName = $decoded->{'last_name'};

    my $status = com::owm::migBellCanada::setNames($email,$puid,$cfg,$log,$firstName,$lastName);
	#update user status to ACTIVE for migration of PAB CAl. work around for SERVICES-3182
	if ( $cfg->get_param('SERVICES-3182_Workaround')  eq "true") {
		  $mos->userUpdateMailbox("update",$email,"proxy",$cfg->param('defaultCos'),"none",$cfg,$log);
		  if (! $mos->is_success) {
			$log->error("$email|Could not set user to proxy. MAIL WILL NOT MIGRATE.");
		  }
	}
    return($status,$email);

  }

  # If doing calendar or contact, a direct login to OX (appSuite) is needed.
  
  my $ox; # OX context object.
  my $cal_pab_target = $cfg->param('cal_pab_target');
  if (( $do_cal || $do_cont ) && ($cal_pab_target eq "appsuite")) {

    ###
    ### Connect, login to AppSuite (ox) and get calendar folder ID.
    ###

    my $appSuiteHost = $cfg->param('appSuiteHost');
    my $appSuitePort = $cfg->param('appSuitePort');

    $ox = OxHttpApi->new($appSuiteHost);
    if (!$ox->is_success) {
      $log->error("$email|$puid:Fail to connect to AppSuite.");
      return($STATUS_FAIL_AS_CONNECT,$email);
    }

    # Login...
    my $originIp =  $cfg->param('appSuiteOriginIp'); # If the originIp is in the AppSuite's white list, login to an account in 'M' will work.
    $ox->Login($email,$password,$originIp);
    if (!$ox->is_success) {
      $log->error("$email|$puid:Login failed to AppSuite.");
      return($STATUS_FAIL_AS_LOGIN,$email);
    }

  }

  ###
  ### Do calendar
  ###

  if ( $do_cal ) {
	my $cal_base_url =  $cfg->param('cal_base_url'); 
    my ($port, $error) = getPortNumber('http','cal',$usercalstorehost,$log,$cfg);
	$cal_base_url =~ s/<host>/$usercalstorehost/;
	$cal_base_url =~ s/<port>/$port/;
	$log->debug("$email|: User calendar url is $cal_base_url");
	
	my $cal_data = getCalendars($email, $cfg, $log, $conflictLog, $access_token);

	  if ( ! defined $cal_data ) {
		$log->info("$email|$puid: No Calendar");
	  } else {
	    my $cal_data_decoded = decode_json($cal_data);
		print Dumper($cal_data_decoded) if ($DEBUG); # test
        my @calenders = @{ $cal_data_decoded->{'data'} };
		foreach my $cal (@calenders){
			$counters{'retrieved'}++;
			my $cal_url = undef;
			my $calendar_id = $cal->{"id"};
			if ($DEBUG) {
			  $log->debug("$email|---------calendar-------\n\n");
			  $log->debug("$email|ID " . $cal->{"id"}."\n");
			  $log->debug("$email|name " . $cal->{"name"} . "\n");
			  $log->debug("$email|is_default " . $cal->{"is_default"} . "\n");
			  $log->debug("$email|description " . $cal->{"description"} . "\n");
			  $log->debug("$email|subscription_location " . $cal->{"subscription_location"} . "\n");
			  $log->debug("$email|preload " . $preload . "\n");
			}
			#check if this is an external calendar and migrate it that way
			if ( defined $cal->{"subscription_location"} ) {
				if ( $cal->{"subscription_location"} =~ "sharing" ) {
					#do not migration internal calendar subscriptions
					$log->info("$email|$puid: skipping internal shared calendar ");
				}elsif ($preload eq "true") {
					#do not migrate subscriptions in preloadmode
					$log->debug("$email|$puid: skipping external calendar subscription in preload mode ");
				}else {
					#create external subscription
					$log->debug("$email|$puid: external calendar subscription ");
					my $result = addSubscription($email,$puid,$cal->{"subscription_location"},$cal->{"name"},$cfg,$log);
					$counters{'externalSubscription'}++;
				}
				next;
			}
			
			
			if ( $cal->{"is_default"} eq "1" ) {
				my $t_user=$email; #"u1\@d1.com";
				my $url_pri_cal = $cal_base_url."/".$t_user."/";
				my $href = com::owm::CALClient::findPrimaryCal($url_pri_cal,$email,$password,$DEBUG);
				if (defined $href){
                	$log->debug("$email|$puid: primary calendar, href=$href\n") if ($DEBUG); #print "href=$href\n";
					$cal_url = $cfg->param('cal_host').$href;
					$cal_url =~ s/<host>/$usercalstorehost/;
					$cal_url =~ s/<port>/$port/;
				} else {
					$log->error("$email|$puid: primary calendar does not exist, migration failed"); 
					#update user status to ACTIVE for migration of PAB CAl. work around for SERVICES-3182
					 if ( $cfg->get_param('SERVICES-3182_Workaround')  eq "true") {
						$mos->userUpdateMailbox("update",$email,"proxy",$cfg->param('defaultCos'),"none",$cfg,$log);
						if (! $mos->is_success) {
						  $log->error("$email|Could not set user to proxy. MAIL WILL NOT MIGRATE.");
						}
					}
					return($STATUS_FAIL_AS_SET_CALENDAR,$email);
				}
			} else {
			  #mkcalendar
				my $calDisplayName = $cal->{"name"};
				my $calName = $cal->{"name"} ;
				my $t_user = $email;
				my $url_cal = $cal_base_url."/".$t_user."/".$calendar_id."/";
				$cal_url = com::owm::CALClient::mkcalendar($url_cal, $calDisplayName, $calName, $email,  $password,$DEBUG);
				$log->debug("$email|$puid: cal_url=$cal_url\n") if ($DEBUG); ##debug
				if ( ! defined $cal_url ) {
					$log->error("$email|$puid: mkcalendar failed "); 
					my $resp = com::owm::CALClient::propfind($url_cal, $email,  $password, $DEBUG);
					if ( $resp->code == 404) {
						$log->error("$email|$puid: calendar ($calName) does not exist, calendar migration failed");
						$counters{'makeCalendarsFailed'}++;
						#work around for SERVICES-3182
						if ( $cfg->get_param('SERVICES-3182_Workaround')  eq "true"){
							$mos->userUpdateMailbox("update",$email,"proxy",$cfg->param('defaultCos'),"none",$cfg,$log);
							if (! $mos->is_success) {
							  $log->error("$email|Could not set user to proxy. MAIL WILL NOT MIGRATE.");
							}	
						}
						return($STATUS_FAIL_AS_SET_CALENDAR,$email);
					} else {
						$log->info("$email|$puid: calendar ($calName) already exists, skip create and continue to process records. Duplicates might appear.");
						$cal_url = $url_cal;
					}
				} else {
					$log->info("$email|$puid: mkcalendar succeed");
				}
			}

			$log->info("$email|$puid:Get calendar info.");

			# Set a limit of 100 events for each response. 
			my $offset = 0;
			my $limit = 100;
			my $next = '';
			my %idAlreadyUsed;

			# Read setting from config file.
			if (! defined $start_date_range ) {
				$start_date_range = $cfg->param('calendar_start_date');
			}
			if (! defined $end_date_range ) {
				$end_date_range = $cfg->param('calendar_end_date');
			}
			my $daysInRequestWindow = 24 * 3600 * ( $cfg->param('calendar_request_window') );

			my($year,$month,$day) = split('-',$start_date_range);
			my $start_date_range_epoch = timelocal('0','0','0',$day,$month-1,$year);

		   ($year,$month,$day) = split('-',$end_date_range);
			my $end_date_range_epoch = timelocal('0','0','0',$day,$month-1,$year);
			my $time = time();
			# print "\n\nstart_date_range_epoch = $start_date_range_epoch\nend_date_range_epoch = $end_date_range_epoch\ntime = $time\n\n";

			my $epoch_start_time = $start_date_range_epoch;
			my $epoch_end_time = $start_date_range_epoch + $daysInRequestWindow;
			if ( $epoch_end_time > $end_date_range_epoch ) {
               $epoch_end_time = $end_date_range_epoch;
             }

			my $inDateRange = 'N';
			my $iCalendar = "BEGIN:VCALENDAR\nVERSION:2.0\nCALSCALE:GREGORIAN\nMETHOD:PUBLISH\n";

            my $try_counter = 0;
            my $calendar_max_try = $cfg->param('max_number_of_msft_api_tries');
            
			do {
			  do {
				# Use $epoch_start_time and $epoch_end_time to set 31 day window to retreive calendar events for.
				my $start_time = strftime '%Y-%m-%dT00:00:00Z', localtime $epoch_start_time;
				my $end_time = strftime '%Y-%m-%dT00:00:00Z', localtime $epoch_end_time;
				# print "start_time = $start_time\nend_time = $end_time\n\n";
				# print"\n\nstart_time = $start_time\nend_time = $end_time\n\n";

        $events = getCalendarEvents($calendar_id, $email, $puid, $cfg, $log, 
                                    $conflictLog, $access_token, $offset, 
                                    $limit, $start_time, $end_time);
				if ( ! defined $events ) {
				  $log->error("$email|$puid:API call to get calendar events returned no results. Calendar is likely uninitialised.");
				  	#update user status to ACTIVE for migration of PAB CAl. work around for SERVICES-3182
					if ( $cfg->get_param('SERVICES-3182_Workaround')  eq "true"){
						$mos->userUpdateMailbox("update",$email,"proxy",$cfg->param('defaultCos'),"none",$cfg,$log);
						if (! $mos->is_success) {
						  $log->error("$email|Could not set user to proxy. MAIL WILL NOT MIGRATE.");
						}
					}
                    $try_counter ++ ;
                    $log->debug("$email|$puid:API call to get calendar events failed, tried $calendar_id ..");
                    if ($try_counter >= $calendar_max_try) {
                        $log->debug("$email|$puid:API call to get calendar events failed, tried $try_counter，exceeded $calendar_max_try");
                        $try_counter = 0;
                        #$epoch_start_time = $end_date_range_epoch ;
                        $epoch_end_time = $end_date_range_epoch;
                    } else {
                        $log->debug("$email|$puid:API call to get calendar events failed, tried $try_counter ..");
                    }
				  #return($STATUS_FAIL_MSFT_CALENDAR,$email); #TODO - this line causes empty/uninitialised calendars to show as failed
				} else {
                    $try_counter = 0;
					print $events if $DEBUG;
				   # print "\n\nevents:\n$events\n\n";
					my $decoded = decode_json($events);
					my @event = @{ $decoded->{'data'} };
					$next = $decoded->{'paging'}{'next'};
					if ($next =~ /offset=(\d+)/) {
					  $offset = $1;
					} 

					NEXTEVENT: foreach my $e (@event) {
					  $counters{'eventsRetrieved'}++;
					  my $eventID = $e->{"id"};
					  if (exists $idAlreadyUsed{$eventID}) {
						$log->debug("$email|$puid:Event already found and processed, so skipping : $eventID") if ($DEBUG);
						$counters{'eventExists'}++;
						next NEXTEVENT;
					  } else {
						$log->info("$email|$puid:New event : $eventID");
						$idAlreadyUsed{$eventID} = "found";
					  }

					  if ($DEBUG) {
						print "name : " . $e->{"name"} . "\n";
						print "id : " . $e->{"id"} . "\n";
						print "created_time : " . $e->{"created_time"} . "\n";
						print "updated_time : " . $e->{"updated_time"} . "\n";
						print "description : " . $e->{"description"} . "\n";
						print "calendar_id : " . $e->{"calendar_id"} . "\n";
						print "fromName : " . $e->{"from"}{"name"} . "\n";
						print "fromId : " . $e->{"from"}{"id"} . "\n";
						print "start_time : " . $e->{"start_time"} . "\n";
						print "end_time : " . $e->{"end_time"} . "\n";
						print "location : " . $e->{"location"} . "\n";
						print "is_all_day_event : " . $e->{"is_all_day_event"} . "\n";
						print "is_recurrent : " . $e->{"is_recurrent"} . "\n";
						print "recurrence : " . $e->{"recurrence"} . "\n";
						print "reminder_time : " . $e->{"reminder_time"} . "\n";
						print "availability : " . $e->{"availability"} . "\n";
						print "visibility : " . $e->{"visibility"} . "\n";
						print "---\n";
					  }

					  # Retrieve the details of event organizer (if from ID not null).
					  my $fromDetail;
					  my $fromEmail = '';

					  if ( $e->{"from"}{"id"} ne '' ) {

						# User events have a from/id that is not ''.
						# So this is a valid event, so process it.

						$log->info("$email|$puid:User event, '".$e->{"name"}."'. Processing.");
						$fromDetail = getFromDetail($email, $cfg, $log, $conflictLog,
                                        $access_token, $e->{"from"}{"id"});
						if (defined $fromDetail) {
							my $decoded = decode_json($fromDetail);
							$fromEmail = $decoded->{"emails"}{"preferred"};
							print("\nGet fromDetail\n") if $DEBUG;
						}else{
							$log->debug("$email|$puid:From detail was undefined.");
						}
						my $thisVEVENT = createVEVENT($DEBUG,$log,$email,$e,$fromEmail,$cfg,$USERTZ);
						$counters{'eventsProcessed'}++;
						if ( $thisVEVENT =~ /RRULE.*FREQ=.*/ ) {
								#If this is a reoccuring event, migrate it immediately
								print "\nRecurring event ...\n" if ($DEBUG);
								my $t_user = $email;
								my $body = "BEGIN:VCALENDAR\nVERSION:2.0\nCALSCALE:GREGORIAN\nMETHOD:PUBLISH\n" . $thisVEVENT . "END:VCALENDAR\n";
								if (defined $cal_url) { ##has checked above
										my $event_url = $cal_url.$e->{"id"};
										my $resp_succ = com::owm::CALClient::putString($event_url, $t_user, $password,$body,$DEBUG);
										if ( $resp_succ->code == 200 || $resp_succ->code == 201){
												$log->info ("$email|$puid: ".$e->{"id"}." putString ok");
												$counters{'eventsMigrated'}++;
										} elsif ( $resp_succ->code == 412 || $resp_succ->code == 403 ){
												#This error indicates the an instance for this reoccuring event has already been provisioned
												#It is a valid error.
												$log->info ("$email|$puid: Precondition failed for recurring event " . $resp_succ->code);
												$counters{'calendarsEventExists'}++;
										} else {
												$log->error( "$email|$puid: putString failed");
												$counters{'calendarsFailed'}++;
										}
								}

						} else {
						$iCalendar .= $thisVEVENT;
						}

					  } else {

						# System events have a '' from/id
						$log->info("$email|$puid:System event, '".$e->{"name"}."'. Skipping.");
						$counters{'eventsSystemSkipped'}++;
					  }

					} # End of foreach @event loop.
				} # end if events returned
			  } while ($next ne `` || ($try_counter != 0));
			  $epoch_start_time = $epoch_end_time + 1;
			  $epoch_end_time += $daysInRequestWindow;
			  if ( $epoch_end_time > $end_date_range_epoch ) {
				$epoch_end_time = $end_date_range_epoch;
			  }

			} while ($epoch_start_time < $end_date_range_epoch);

			$iCalendar .= "END:VCALENDAR\n";
			print $iCalendar if $DEBUG;

			if ($cal_pab_target eq "appsuite") {
				my $icalfile = "tmp/$email.$calendar_id.ical";
				open(my $fh, '>', $icalfile) or die "Could not open file '$icalfile' $!";
				print $fh "$iCalendar";
				close $fh;
				# To set calendar data, we need to know the user's calendar folder Id...
				my $calendarFolderId = $ox->GetCalendarFolderId();
				$ox->PrintResponse if $DEBUG;

				my $json = $ox->Import("ICAL",$calendarFolderId,$icalfile,'true');
				$ox->PrintResponse if $DEBUG;
				if (!$ox->is_success) {
				  $log->error("$email|$puid:Import calendar to AppSuite failed.");
				  $ox->Logout;
				  return($STATUS_FAIL_AS_SET_CALENDAR,$email);
				}
#			unlink($icalfile);
			} elsif ($cal_pab_target eq "ux") {
				my $t_user = $email;
				print "cal_url=".$cal_url."\n"  if ($DEBUG);

				if (defined $cal_url) { ##has checked above
					my $resp_succ = com::owm::CALClient::postString($cal_url, $t_user, $password,$iCalendar,$DEBUG);
					if ( $resp_succ->code == 200 ){
						$log->info ("$email|$puid: post ok");
						$counters{'calendarsMigrated'}++;
					} else {
						$log->error( "$email|$puid: post failed");
						$counters{'calendarsFailed'}++;
					}
				}
			}
			$counters{'eventsRetrieved'}++; #add one as we initialised it to -1 to start.
			$counters{'calendarErrors'} = $counters{'eventsRetrieved'} - $counters{'eventExists'} - $counters{'eventsProcessed'} - $counters{'eventsSystemSkipped'};
		}
		$counters{'retrieved'}++; #add one as we initialised it to -1 to start.
	}

  }

  ###
  ### Do contacts
  ###

  my $allVCARDs = '';
  if ( $do_cont ) {
    $log->info("$email|$puid:Get contact info.");
    $counters{'retrieved'}=-1;

    my $people_api_endpoint = $cfg->param('microsoft_people_api_endpoint');
    if (defined($people_api_endpoint) && ($people_api_endpoint =~ /https/)) {
      #MSFT People API
      if ($cal_pab_target eq "ux") {
          %counters = com::owm::MSFTPeopleAPI::migrateContacts($puid,$email,$password,$userpabstorehost,$cfg,$log,$DEBUG);
          #$counters{'retrieved'}++; #add one as we initialised it to -1 to start.
          if (defined($counters{'fakePuid'})) {
            if ($counters{'fakePuid'} > 0) {
              $log->error($email.'|'.$puid.':Fake PUID detected, address book migration using people API failed');
              delete($counters{'fakePuid'});
              return($STATUS_FAIL_MSFT_CONTACTS,$email);
            }
          }
          if (defined($counters{'fatalerror'})) {
            if ($counters{'fatalerror'} > 0) {
              $log->error($email.'|'.$puid.':Non-recoverable error while migrating contacts.');
              delete($counters{'fatalerror'});
              return($STATUS_FAIL_MSFT_CONTACTS,$email);
            }
          }
          #tally up the counts if the total number of errors matches the total number process, then report contact migration as a failure.
          if ($counters{'retrieved'} > 0) {
              my $totalErrorCount = $counters{'error424'} + $counters{'error'};
              if ( $counters{'processed'} == $totalErrorCount ) {
                  $log->error("$email|Total error count ($totalErrorCount) equals total records processed (".$counters{'processed'}."), count this as an error");
                  return($STATUS_FAIL_MSFT_CONTACTS,$email);
              }
          }
      }
    } else {
    # Retreive contact data from MSFT. If call times out, retry.
    my $tries = 0; # Count number of reties.
    my $maxTries = $cfg->param('max_number_of_msft_api_tries');
    while () {
      $tries++;
      $contacts = getContacts($email, $cfg, $log, $conflictLog, $access_token);

      if ( ! defined $contacts ) {
        if ($tries >= $maxTries) {
          $log->error("$email|$puid:Fail retrieve data from MSFT at try $tries of $maxTries");
          if ( $cfg->get_param('SERVICES-3182_Workaround')  eq "true"){
                $mos->userUpdateMailbox("update",$email,"proxy",$cfg->param('defaultCos'),"none",$cfg,$log);
                if (! $mos->is_success) {
                  $log->error("$email|Could not set user to proxy. MAIL WILL NOT MIGRATE.");
                }
            }
          return($STATUS_FAIL_MSFT_CONTACTS,$email);
        } else {
          $log->info("$email|$puid:Fail retrieve data from MSFT at try $tries of $maxTries");
          sleep $cfg->param('sleep_between_msft_api_tries');
        }
      } else {
        $log->info("$puid:Successfully retrieve data from MSFT");
        last; # Break out of loop.
      }
    }

    print "\n\ncontacts:\n$contacts\n\n" if $DEBUG;
    my $decoded = decode_json($contacts);
    my @contact = @{ $decoded->{'data'} };

    foreach my $c (@contact) {
		$counters{'retrieved'}++;
        my $contact_reference = "put contact=[ " . $c->{"id"} . " - " . $c->{"last_name"} . ", " . $c->{"first_name"} . " ]";
      if ($DEBUG) {
        print "id : " . $c->{"id"} . "\n";
        print "first_name : " . $c->{"first_name"} . "\n";
        print "last_name : " . $c->{"last_name"} . "\n";
        print "name : " . $c->{"name"} . "\n";
        print "is_friend : " . $c->{"is_friend"} . "\n";
        print "is_favorite : " . $c->{"is_favorite"} . "\n";
        print "user_id : " . $c->{"user_id"} . "\n";
        print "emails.preferred : " . $c->{"emails"}{"preferred"} . "\n";
        print "emails.account : " . $c->{"emails"}{"account"} . "\n";
        print "emails.personal : " . $c->{"emails"}{"personal"} . "\n";
        print "emails.business : " . $c->{"emails"}{"business"} . "\n";
        print "emails.other : " . $c->{"emails"}{"other"} . "\n";
        print "updated_time : " . $c->{"updated_time"} . "\n";
        print "birth_day : " . $c->{"birth_day"} . "\n";
        print "birth_month : " . $c->{"birth_month"} . "\n";
        print "---\n";
      }

      my $thisVCARD = createVCARD($DEBUG,$log,$email,$c);
      print "\n--- thisVCARD ---\n$thisVCARD---\n" if $DEBUG;
	  
	  ###
    if ($cal_pab_target eq "ux") {
      my $pab_url =  $cfg->param('pab_base_url');
          my ($port, $error) = getPortNumber('http','pab',$userpabstorehost,$log,$cfg);
      $pab_url =~ s/<host>/$userpabstorehost/;
      $pab_url =~ s/<port>/$port/;
      my $t_user = $email; #"u1\@d1.com";
      $pab_url = $pab_url."/".$t_user."/"."Main";
      $counters{'processed'}++;
      my $resp = com::owm::PABClient::put($pab_url,$t_user,$password,$thisVCARD,$log);
      if ($resp->code == 201) {
      print "put ok\n" if ($DEBUG);
      $log->info("$email|$puid: $contact_reference successful.");
      $counters{'success'}++;
      } elsif ($resp->code == 412) {
      $log->info("$email|$puid: $contact_reference failed. " . $resp->code . "Suggestion: check syntax, check contact does not already exist.");
      $counters{'duplicate'}++;
      } elsif ($resp->code == 424) {
      $log->error("$email|$puid: $contact_reference precondition failed, check file path location on PAB/CAL, or the number of contacts limit on PAB server " . $resp->code);
      $counters{'error424'}++;
      } else {
      $log->error("$email|$puid: put contact failed " . $resp->code);
      $counters{'error'}++;
      }
    }
	  
      $allVCARDs .= $thisVCARD;
    }

    print "--- allVCARDs ---\n$allVCARDs---\n" if ($DEBUG);

    if ($cal_pab_target eq "appsuite") {
	    my $tmpFile = "tmp/$email.vcard";
		open(my $fh, '>', $tmpFile) or die "Could not open file '$tmpFile' $!";
		binmode($fh, ':encoding(utf8)');
		print $fh "$allVCARDs";
		close $fh;
		  # To retrieve contact data, we need to know the user's contact folder Id...
      my $contactFolderId = $ox->GetContactFolderId();
		my $json = $ox->Import('VCARD',$contactFolderId,$tmpFile);
		$ox->PrintResponse if $DEBUG;
		if (!$ox->is_success) {
		  $log->error("$email|$puid: Import contacts to AppSuite failed.");
		  $ox->Logout;
		  return($STATUS_FAIL_AS_SET_CONTACTS,$email);
		}
		unlink($allVCARDs);
	}
  $counters{'retrieved'}++; #add one as we initialised it to -1 to start.
  #tally up the counts if the total number of errors matches the total number process, then report contact migration as a failure.
  if ($cal_pab_target eq "ux" && $counters{'retrieved'} > 0) {
    my $totalErrorCount = $counters{'error424'} + $counters{'error'};
    if ( $counters{'processed'} == $totalErrorCount ) {
      $log->error("$email|Total error count ($totalErrorCount) equals total records processed (".$counters{'processed'}."), count this as an error");
      return($STATUS_FAIL_MSFT_CONTACTS,$email);
    }
  }
  }
  }
  
  if ($cal_pab_target eq "appsuite") {
    # Logout of OX if needed.
    if ( $do_cal || $do_cont ) {
  	$ox->Logout;
    }
  }
  #update user status to ACTIVE for migration of PAB CAL. work around for SERVICES-3182
  if ( $cfg->get_param('SERVICES-3182_Workaround')  eq "true") {
    $mos->userUpdateMailbox("update",$email,"proxy",$cfg->param('defaultCos'),"none",$cfg,$log);
    if (! $mos->is_success) {
  	$log->error("$email|Could not set user to proxy. MAIL WILL NOT MIGRATE.");
    }
  }
 return($status,$email,%counters);
}

##############################################################################################################
# createVEVENT
#
# Route to create an ICAL VEVENT from a Microsoft event.
# For details of Microsoft event, see: https://msdn.microsoft.com/en-us/library/windows/apps/hh243648.aspx#event
# For details of ICAL VEVENT, see: http://www.ietf.org/rfc/rfc2445.txt (or you may find this helpful: http://www.kanzaki.com/docs/ical/vevent.html)
#
# Args:
# - DEBUG
# - log object
# - email address
# - Microsoft source event
# - From email (who create the Microsoft source event)
#
# Return:
# - A string containing a VEVENT
# 
##############################################################################################################
sub createVEVENT
{
  $DEBUG = shift;
  my $log = shift;
  my $email = shift;
  my $sourceEvent = shift;
  my $fromEmail = shift;
  my $cfg = shift;
  $USERTZ = shift;
  my $targetEvent;

  my %mon2num = qw(
    jan 01  feb 02  mar 03  apr 04  may 05  jun 06
    jul 07  aug 08  sep 09  oct 10 nov 11 dec 12
  );

  my %month2num = qw(
    January 1 February 2 March 3 April 4 May 5 June 6 July 7 August 8 September 9 October 10 November 11 December 12
  );

  my %pos2num = qw(
    first 1 second 2 third 3 fourth 4 last -1
  );

  my %day2dayshort = qw(
    Sunday SU Monday MO Tuesday TU Wednesday WE Thursday TH Friday FR Saturday SA
  );

  my %daysets2days = (
    'day' => 'SU,MO,TU,WE,TH,FR,SA',
    'weekday' => 'MO,TU,WE,TH,FR',
    'weekend day' => 'SU,SA'
  );
  # The localtime is determined by the 
  if ( ! defined $USERTZ ) {
    # If not timezone has been migrated then derive it.
	$USERTZ = $cfg->param("default_tz"); #temporarily set to default TZ
  }

  $targetEvent .= "BEGIN:VEVENT\n";
  $targetEvent .= "DTSTAMP:" . iso8601timeToUtcTime( $sourceEvent->{"created_time"}, "UTC", $USERTZ ) . "\n";
  $sourceEvent->{"name"} =~ s/\t/\ /g;
  $targetEvent .= "SUMMARY:" . $sourceEvent->{"name"} . "\n";
  $sourceEvent->{"description"} =~ s/\t/\ /g;
  $sourceEvent->{"description"} =~ s/\r\n/abcrnrnabc/g;
  $sourceEvent->{"description"} =~ s/\n/\\n/g;
  $sourceEvent->{"description"} =~ s/abcrnrnabc/\\n/g;
  $targetEvent .= "DESCRIPTION:" . $sourceEvent->{"description"} . "\n";  
  #$targetEvent .= "DTSTART:" . iso8601timeToUtcTime( $sourceEvent->{"start_time"}, "UTC", $USERTZ ) . "\n";
  #$targetEvent .= "DTEND:" . iso8601timeToUtcTime( $sourceEvent->{"end_time"}, "UTC", $USERTZ ) . "\n";
   my ($dtstart,$startProfileTz) = iso8601timeToUtcTime( $sourceEvent->{"start_time"}, "LT", $USERTZ, $sourceEvent->{'is_all_day_event'} );
   $targetEvent .= "DTSTART" . $dtstart. "\n";
   my ($dtend,$endProfileTz) = iso8601timeToUtcTime( $sourceEvent->{"end_time"}, "LT_END", $USERTZ, $sourceEvent->{'is_all_day_event'} );
   $targetEvent .= "DTEND" . $dtend . "\n";
  if ( ($sourceEvent->{"visibility"} eq 'public') || ($sourceEvent->{"visibility"} eq '') ) {
    $targetEvent .= "CLASS:PUBLIC\n";
  } elsif ( $sourceEvent->{"visibility"} eq 'private' ) {
    $targetEvent .= "CLASS:PRIVATE\n";
  } else {
    # visibility setting not recognised, so set to PRIVATE.
    $log->warn("$email|Event visibility unexpected value : " . $sourceEvent->{"visibility"}
                . ". Setting VEVENT's CLASS PRIVATE.");
    $targetEvent .= "CLASS:PRIVATE\n";
  }

  $targetEvent .= "LOCATION:" . $sourceEvent->{"location"} . "\n";

  if ( ($sourceEvent->{"availability"} eq 'free') || ($sourceEvent->{"availability"} eq '') ) {
    $targetEvent .= "TRANSP:TRANSPARENT\n";
  } elsif ( $sourceEvent->{"availability"} eq 'busy' ) {
    $targetEvent .= "TRANSP:OPAQUE\n";
  } elsif ( $sourceEvent->{"availability"} eq 'tentative' ) {
    $targetEvent .= "TRANSP:OPAQUE\n";
  } elsif ( $sourceEvent->{"availability"} eq 'out_of_office' ) {
    $targetEvent .= "TRANSP:OPAQUE\n";
  } else {
    $log->warn("$email|Event availability unexpected value : " . $sourceEvent->{"availability"}
                . ". The VEVENT's TRANSP to OPAQUE.");
    $targetEvent .= "TRANSP:OPAQUE\n";
  }

  $targetEvent .= "UID:" . $sourceEvent->{"id"} . "\n";

  if ( $sourceEvent->{"updated_time"} ne '' ) {
    $targetEvent .= "LAST-MODIFIED:" . iso8601timeToUtcTime( $sourceEvent->{"updated_time"}, "UTC", $USERTZ ) . "\n";
  }

  if ( $fromEmail ne '' ) {
    $targetEvent .= "ORGANIZER:mailto:" . $fromEmail . "\n";
  } else {
    $targetEvent .= "ORGANIZER:mailto:\n";
  }

  $targetEvent .= "SEQUENCE:0\n";

  # Is there a reminder? If so, process it here...
  if ( $sourceEvent->{"reminder_time"} ne '') {
    $targetEvent .= "BEGIN:VALARM\n";
    $targetEvent .= "TRIGGER:-PT".$sourceEvent->{"reminder_time"}."M\n";
    $targetEvent .= "ACTION:EMAIL\n";
    $targetEvent .= "DESCRIPTION:Reminder notification\n";
    $targetEvent .= "SUMMARY:Reminder notification\n";
    $targetEvent .= "ATTENDEE:MAILTO:$email\n";
    $targetEvent .= "END:VALARM\n";
  }

  # Is this a recurrent appointment (eg, daily or weekly)? If so, process it here...
  if ( $sourceEvent->{"is_recurrent"} eq '1') {
    my @targetRules = ();
    my $sourceRule = $sourceEvent->{"recurrence"};
	my $until = "";
	$sourceEvent->{"end_time"} =~ m/(T.*$)/ ;
	my $end_time= $1;
	my $source_tzoffset;
	if ( index($end_time,"+") ne -1 ) {
		$source_tzoffset = substr($end_time,index($end_time,"+"),length($end_time));
	} else {
		$source_tzoffset = substr($end_time,index($end_time,"-"),length($end_time));
	}
	# REGEX has been checked with http://regexr.com/
    # The 'until' part of rule is handle the same in all cases (daily, weekly...)
    # Find if the rule has an 'until' statement and date.
    # Until / Date format is like: 'until Sunday, January 25, 2015.'
    # or like this               : 'until July 25, 2015.'
	# or like                    : 'until Sunday, 25 January 2015.'
	
	# in the form of  : 'until Sunday, 25 January, 2015.'
	
	# the end_time on the until statement needs to be one second before midnight UTC
    if ( $sourceRule =~ m/ until \w+, (\w+)\s(\d+),\s(\d+)./ ) {
      my $month = $mon2num{ lc substr($1, 0, 3) };
      my $day = $2;
      my $year = $3;
      $until = $year.'-'.$month.'-'.$day;
      my $untilTzOffset = untilDateTimeOffset($sourceEvent->{'start_time'},
                                              $USERTZ, $source_tzoffset,
                                              $DEBUG, $year, $month, $day);
      # $targetRule .= "UNTIL=$year$month$dayT045959Z;";
      #$until .= $end_time;
      my $end_time_until = $year.'-'.$month.'-'.$day.'T23:59:59'.$untilTzOffset;
      print "\nThe end time for until statements if required, is $end_time_until \n\n" if ($DEBUG);
      push(@targetRules, 'UNTIL='. iso8601timeToUtcTime($end_time_until, 'UTC', $USERTZ));
  # in the form of  : 'until Sunday, 25 January 2015.'
     } elsif ( $sourceRule =~ m/ until \w+, (\d+)\s(\w+)\s(\d+)./ ) {
      my $month = $mon2num{ lc substr($2, 0, 3) };
      my $day = $1;
      my $year = $3;
      # $targetRule .= "UNTIL=$year$month$dayT045959Z;";
      $until = $year.'-'.$month.'-'.$day;
      my $untilTzOffset = untilDateTimeOffset($sourceEvent->{'start_time'},
                                              $USERTZ, $source_tzoffset,
                                              $DEBUG, $year, $month, $day);
      #$until .= $end_time;
      my $end_time_until = $year.'-'.$month.'-'.$day.'T23:59:59'.$untilTzOffset;
      print "\nThe end time for until statements if required, is $end_time_until \n\n" if ($DEBUG);
      push(@targetRules, 'UNTIL='. iso8601timeToUtcTime($end_time_until, 'UTC', $USERTZ));
      # For until of form: 'until January 25, 2015.'
    } elsif ( $sourceRule =~ m/ until (\w+)\s(\d+),\s(\d+)./ ) {
      my $month = $mon2num{ lc substr($1, 0, 3) };
      my $day = $2;
      my $year = $3;
      # $targetRule .= "UNTIL=$year$month$dayT045959Z;";
      $until = $year.'-'.$month.'-'.$day;
      my $untilTzOffset = untilDateTimeOffset($sourceEvent->{'start_time'},
                                              $USERTZ, $source_tzoffset,
                                              $DEBUG, $year, $month, $day);
      #$until .= $end_time;
      my $end_time_until = $year.'-'.$month.'-'.$day.'T23:59:59'.$untilTzOffset;
      print "\nThe end time for until statements if required, is $end_time_until \n\n" if ($DEBUG);
      push(@targetRules, 'UNTIL=' . iso8601timeToUtcTime($end_time_until, 'UTC', $USERTZ));
    }
    # For daily appointments.
	#Occurs every day
	#Occurs every day until {enddate} 
	#Occurs every {N} days
	#Occurs every {N} days until {enddate}
    if ( $sourceRule =~ /Occurs every (\d+ )?day/ ) {

      my $interval = "INTERVAL=$1";
      $interval =~ s/ //; # Remove the space after the number.
      if ($interval eq "INTERVAL=") { # Default interval to 1.
        $interval .= "1";
      }

      push(@targetRules, 'FREQ=DAILY');
      push(@targetRules, $interval);
      
    # For weekday appointments (daily apart from Sat and Sun).
	#Occurs every weekday
	#Occurs every weekday until {enddate}
    } elsif ( $sourceRule =~ /Occurs every weekday[.\s]/ ) {

      # For weekday appointments from Microsoft, appointments are always on MO,TU,WE,TH,FR.
      push(@targetRules, 'FREQ=WEEKLY');
      push(@targetRules, 'INTERVAL=1');
      push(@targetRules, 'BYDAY=MO,TU,WE,TH,FR');

    # For weekly appointments.
	#Occurs every week on {weekdays}
	#Occurs every week on {weekdays} until {enddate}
	#Occurs every {N} weeks on {weekdays}
	#Occurs every {N} weeks on {weekdays} until {enddate}
    } elsif ( $sourceRule =~ /Occurs every (\d+ )?week[s\s]/ ) {

      my $interval = "INTERVAL=$1";
      $interval =~ s/ //; # Remove the space after the number.
      if ($interval eq "INTERVAL=") { # Default interval to 1.
        $interval .= "1";
      }

      push(@targetRules, 'FREQ=WEEKLY');
      push(@targetRules, $interval);

      my $byday = "BYDAY=";

      # As the sourceRule has a day in the 'until' part of the rule, a simple regex could not be easily
      # found to handle this. For this reason this IF/ELSE is used (IF to handle rules with 'until' and
      # ELSE to handle rules without 'until').
      if ( $sourceRule =~ / until / ) {
        if ( $sourceRule =~ / on .*Saturday.*until/ ) {
          $byday .= "SA,";
        }
        if ( $sourceRule =~ / on .*Sunday.*until/ ) {
          $byday .= "SU,";
        }
        if ( $sourceRule =~ / on .*Monday.*until/ ) {
          $byday .= "MO,";
        }
        if ( $sourceRule =~ / on .*Tuesday.*until/ ) {
          $byday .= "TU,";
        }
        if ( $sourceRule =~ / on .*Wednesday.*until/ ) {
          $byday .= "WE,";
        }
        if ( $sourceRule =~ / on .*Thursday.*until/ ) {
          $byday .= "TH,";
        }
        if ( $sourceRule =~ / on .*Friday.*until/ ) {
          $byday .= "FR,";
        }
      } else {
        if ( $sourceRule =~ / on .*Saturday/ ) {
          $byday .= "SA,";
        }
        if ( $sourceRule =~ / on .*Sunday/ ) {
          $byday .= "SU,";
        }
        if ( $sourceRule =~ / on .*Monday/ ) {
          $byday .= "MO,";
        }
        if ( $sourceRule =~ / on .*Tuesday/ ) {
          $byday .= "TU,";
        }
        if ( $sourceRule =~ / on .*Wednesday/ ) {
          $byday .= "WE,";
        }
        if ( $sourceRule =~ / on .*Thursday/ ) {
          $byday .= "TH,";
        }
        if ( $sourceRule =~ / on .*Friday/ ) {
          $byday .= "FR,";
        }
      }
      if ($byday ne "BYDAY=") {  # IE if day(s) have been added to the string
        $byday =~ s/,$//; # Remove the comma at the end of string.
        push(@targetRules, $byday);
      }

    # For monthly appointments.
	#	Occurs every month on the {position} {weekday} of the month
	#	Occurs every month on the {position} {weekday} of the month until {enddate}
	#	Occurs every month on day {N} of the month
	#	Occurs every month on day {N} of the month until {enddate}
    #   Occurs every month on the (first|second|third|last) day of the month until Sunday, 31 January 2016."  
	#	Occurs every {N} months on the {position} {weekday} of the month
	#	Occurs every {N} months on the {position} {weekday} of the month until {enddate}
	#	Occurs every {N} months on day {M} of the month
	#	Occurs every {N} months on day {M} of the month until {enddate}
    } elsif ( $sourceRule =~ /Occurs every (\d+ )?month[s\s]/ ||  $sourceRule =~ /Occurs every month on day (\d+ )?of the/) {
      my $interval = "INTERVAL=$1";
      $interval =~ s/ //; # Remove the space after the number.
      if ($interval eq "INTERVAL=") { # Default interval to 1.
        $interval .= "1";
      }

      push(@targetRules, 'FREQ=MONTHLY');
      push(@targetRules, $interval);

      if ( $sourceRule =~ / on day (\d+) of the month/ ) {
        push(@targetRules, 'BYMONTHDAY='.$1);
        #As per TELSTRA-395 last call is to have on day x of the month to be that day and if none exist then it is simply skipped
        #However leaving the code in, in case Telstra change their minds
        #if ( $1 == 29 ) {
        #    $targetRule .= "BYMONTHDAY=28,29;BYSETPOS=-1";
        #}elsif ( $1 == 30 ) {
        #    $targetRule .= "BYMONTHDAY=28,29,30;BYSETPOS=-1";
        #}elsif ( $1 == 31 ) {
        #    $targetRule .= "BYMONTHDAY=-1"; # day 31 assume last day of the month
        #} else {
        #    $targetRule .= "BYMONTHDAY=".$1;
        #}
      }

      if ( $sourceRule =~ / on the (first|second|third|fourth|last) (Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday) of the month/ ) {
        my $byday = $pos2num{$1}.$day2dayshort{$2};
        push(@targetRules, 'BYDAY='.$byday);
      }
      
      if ( $sourceRule =~ / on the last day of the month/ ) {
        push(@targetRules, 'BYMONTHDAY=-1');
      }
      
      if ( $sourceRule =~
        / on the (first|second|third|fourth) (day|weekday|weekend day) of the month/ ) {
        push(@targetRules, 'BYDAY='.$daysets2days{$2});
        push(@targetRules, 'BYSETPOS='.$pos2num{$1});
      }

	  print "\nMatching monthly interval - $interval\n" if ($DEBUG);
    # For yearly appointments.
	# Occurs every year on the {position} {weekday} of {month}
	# Occurs every year on the {position} {weekday} of {month} until {enddate}
	# Occurs every year on day {N} of {month}
	# Occurs every year on day {N} of {month} until {enddate}
    } elsif ( $sourceRule =~ /Occurs every year/ ) {
      push(@targetRules, 'FREQ=YEARLY');
      push(@targetRules, 'INTERVAL=1');

      # INTERVAL=1;BYMONTH=1;BYDAY=FR;BYSETPOS=4

      if ( $sourceRule =~ / on day (\d+) of (January|February|March|April|May|June|July|August|September|October|November|December)/ ) {
        push(@targetRules, 'BYMONTH='.$month2num{$2});
        push(@targetRules, 'BYMONTHDAY='.$1);
		#As per Telstra-395 removing this logic, keeping here in case Telstra change their minds
		# if ( $1 == 29 && $2 eq "February" ) {
        #    $targetRule .= ";BYMONTHDAY=28,29;BYSETPOS=-1";
        #} else {
		#	$targetRule .= ";BYMONTHDAY=".$1;
		#}
      }

      if ( $sourceRule =~ / on the (first|second|third|fourth|last) (Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday) of (January|February|March|April|May|June|July|August|September|October|November|December)/ ) {
        my $byday = $pos2num{$1}.$day2dayshort{$2};
        push(@targetRules, 'BYMONTH='.$month2num{$3});
        push(@targetRules, 'BYDAY='.$byday);
      }

      if ( $sourceRule =~
        / on the last day of (January|February|March|April|May|June|July|August|September|October|November|December)/ ) {
        push(@targetRules, 'BYMONTH='.$month2num{$1});
        push(@targetRules, 'BYMONTHDAY=-1');
      } elsif ( $sourceRule =~
        / on the (first|second|third|fourth|last) (day|weekday|weekend day) of (January|February|March|April|May|June|July|August|September|October|November|December)/ ) {
        push(@targetRules, 'BYMONTH='.$month2num{$3});
        push(@targetRules, 'BYDAY='.$daysets2days{$2});
        push(@targetRules, 'BYSETPOS='.$pos2num{$1});
      }
      

    }
    my $targetRule = join(';', @targetRules);
    $targetEvent .= "RRULE:$targetRule\n";
  }

  # Waiting be Microsoft to tell us how to get ATTENDEE information.
  # $targetEvent .= "ATTENDEE:";

  $targetEvent .= "END:VEVENT\n";
  #$vTimezone =~ s/\r|\n//g;
  #Add both the vtimezone information for the user profile timezone and the event timezone if they are different.
  $targetEvent .= com::owm::OlsonToVTimezone::getVtimezone($USERTZ, $cfg, $log);
  if ( $startProfileTz ne $USERTZ ) {
	$targetEvent .= com::owm::OlsonToVTimezone::getVtimezone($startProfileTz, $cfg, $log); 
   }

  return($targetEvent);
}

##############################################################################################################
# untilDateTimeOffset
#
# Take the start date, use that to work out the time zone, and then figure out what the offset from GMT is
# for that time zone when on the UNTIL date as the offset might have changed due to DST boundaries
#
##############################################################################################################
sub untilDateTimeOffset($$$$$$$) {
  my $iso8601StartTime = shift;
  my $USERTZ = shift;
  my $tzOffset = shift;
  my $DEBUG = shift;
  my $year = shift;
  my $month = shift;
  my $day = shift;

  my ($date, $timeAndTZ) = split(/T/,$iso8601StartTime);
  my ($time, $tz) = split(/[-+]/,$timeAndTZ);
  my ($startyear, $startmon, $startday) = split(/-/,$date);
  my $startDt = DateTime->new(
      year      => $startyear,
      month     => $startmon,
      day       => $startday,
      hour      => '23',
      minute    => '59',
      second    => '59',
  );
  print 'UNTIL: startDt: '.$startDt->iso8601()."\n" if ($DEBUG);
  my $localOlson = com::owm::OlsonToVTimezone::getLocalOlson($tzOffset, $USERTZ, $startDt, $DEBUG, "");
  print 'UNTIL: localOlson: '.$localOlson."\n" if ($DEBUG);
  if ($localOlson ne "unknown") {
    my $localUntilTime = DateTime->new(year => $year, month => $month, day => $day, hour => 23, minute => 59, second => 59, time_zone => $localOlson);
    my $untilTzOffset = $localUntilTime->offset();
    print 'UNTIL: untilTzOffset: '.$untilTzOffset."\n" if ($DEBUG);
    $tzOffset = int($untilTzOffset / 3600) * 100;
    $tzOffset = $tzOffset + int(($untilTzOffset % 3600) / 60);
    if ($tzOffset > 0) {
      $tzOffset = '+'.$tzOffset;
    }
    print 'UNTIL: tzOffset: '.$tzOffset."\n" if ($DEBUG);
  }
  return($tzOffset);
}

##############################################################################################################
# createVCARD
#
# Route to create an VCARD from a Microsoft contact.
#
# For details of Microsoft contact, see: https://msdn.microsoft.com/en-us/library/windows/apps/hh243648.aspx#contact
# For details of VCARD, see: https://www.ietf.org/rfc/rfc2426.txt
#
# Args:
# - DEBUG
# - log object
# - email address
# - Microsoft source contact
#
# Return:
# - A string containing a VCARD
#
##############################################################################################################
sub createVCARD
{
  $DEBUG = shift;
  my $log = shift;
  my $email = shift;
  my $sourceContact = shift;
  my $targetContact;
  my $notes = 'NOTE:';

  $targetContact = "BEGIN:VCARD\n";
  $targetContact .= "VERSION:3.0\n";
  $targetContact .= "FN:".$sourceContact->{'last_name'}."\, ".$sourceContact->{'first_name'}."\n";
  $targetContact .= "N:".$sourceContact->{'last_name'}.";".$sourceContact->{'first_name'}.";;;\n";
  $targetContact .= "X-OPEN-XCHANGE-CTYPE:contact\n";

  # The OX API used to import VCARDs into AppSuite will not allow malformed emails and will reject the whole
  # imported file any any email is not ok. The regex test on the emails below is an attempt to make sure 
  # emails are ok, or not added to VCARD for import.

  if ($sourceContact->{'emails'}{'business'} =~ /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/ ) {
    $targetContact .= "EMAIL;TYPE=INTERNET,work:".$sourceContact->{'emails'}{'business'}."\n";
  } elsif ($sourceContact->{'emails'}{'business'} eq '' ) {
    print "emails.business is empty\n" if $DEBUG;
  } else {
    print "emails.business does not match allow pattern.\n" if $DEBUG;
    $log->info("$email|Source email (business) not valid format. Will added to the 'Comments'. Email : ".$sourceContact->{'emails'}{'business'});
    $targetContact .= "EMAIL;TYPE=INTERNET,work:".$sourceContact->{'emails'}{'business'}."\n";
    $notes .= "Work email : ".$sourceContact->{'emails'}{'personal'}.'\n';
  }

  if ($sourceContact->{'emails'}{'personal'} =~ /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/ ) {
    $targetContact .= "EMAIL;TYPE=INTERNET,home:".$sourceContact->{'emails'}{'personal'}."\n";
  } elsif ($sourceContact->{'emails'}{'personal'} eq '' ) {
    print "emails.personal is empty\n" if $DEBUG;
  } else {
    print "emails.personal does not match allow pattern.\n" if $DEBUG;
    $log->info("$email|Source email (personal) not valid format. Will added to the 'Comments'. Email : ".$sourceContact->{'emails'}{'personal'});
    $targetContact .= "EMAIL;TYPE=INTERNET,home:".$sourceContact->{'emails'}{'personal'}."\n";
    $notes .= "Home email : ".$sourceContact->{'emails'}{'personal'}.'\n';
  }

  if ($sourceContact->{'emails'}{'other'} =~ /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/ ) {
    $targetContact .= "EMAIL;TYPE=INTERNET,other:".$sourceContact->{'emails'}{'other'}."\n";
  } elsif ($sourceContact->{'emails'}{'other'} eq '' ) {
    print "emails.other is empty\n" if $DEBUG;
  } else {
    print "emails.other does not match allow pattern.\n" if $DEBUG;
    $log->info("$email|Source email (other) not valid format. Will added to the 'Comments'. Email : ".$sourceContact->{'emails'}{'other'});
    $targetContact .= "EMAIL;TYPE=INTERNET,other:".$sourceContact->{'emails'}{'other'}."\n";
    $notes .= "Other email : ".$sourceContact->{'emails'}{'personal'}.'\n';
  }

  # Add notes
  $targetContact .= $notes."\n";

  $targetContact .= "UID:".$sourceContact->{'id'}."\n";
  $targetContact .= "END:VCARD\n";

  return($targetContact);
}

##############################################################################################################
# iso8601timeToUtcTime
#
# Route to convert ISO 8601 time to UTC time
# For example: ISO 8601 time, 2014-11-19T18:58:21+0000, is converted to UTC time, 20141119T185821Z.
#
# Args:
# - String in ISO 8601 time format
#
# Return:
#  - String in UTC time format
#
##############################################################################################################
sub iso8601timeToUtcTime
{
  my $iso8601time = shift;
  my $dateType = shift;
  my $eventTz = shift;  
  my $allDay = shift;

  # $iso8601time = "2014-11-03T03:13:23+0130"; # Uncomment to test a specific time.

  print "created_time = ".$iso8601time."\n" if $DEBUG;
  my ($date, $timeAndTZ) = split(/T/,$iso8601time);
  my ($time, $tz) = split(/[-+]/,$timeAndTZ);
  my ($hour, $min, $sec) = (0, 0, 0);
  if ($allDay ne '1')
  {
    ($hour, $min, $sec) = split(/:/, $time);
  }
  my ($year, $mon, $mday) = split(/-/,$date);
  my $localTime = $year . $mon . $mday .'T'. $time;
  $localTime =~ s/://g;  #the original time of the appointment
  my $localOffset; #the original offset of the appointment
  my $localStartTime = DateTime->new(
      year      => $year,
      month     => $mon,
      day       => $mday,
      hour      => $hour,
      minute    => $min,
      second    => $sec,
	);
	
  #If this is an all day event only the date is required
  if ( $allDay eq "1")
  {
    my $eventDay ;
    if ($dateType eq 'LT_END')
    {
        my $dtend = $localStartTime;
        $dtend->set_time_zone($eventTz);
		#MSFT is already passing the endtime as the next day, no need to add one day as is done for myInbox
        #$dtend->add(days => 1);
        $eventDay = ";VALUE=DATE:".$dtend->strftime('%Y%m%d');
    }
    else
    {
        $eventDay = ";VALUE=DATE:$year$mon$mday";
    }
    return ($eventDay, $eventTz);
  }
  $year -= 1900;
  $mon -= 1;
  my $tzSign;

  $tz = sprintf("%04d",$tz); # Make sure $tz has 4 digits.
  my $tzhh = substr($tz, 0, 2);
  my $tzmm = substr($tz, 2, 4);

  my $epoch = timegm($sec, $min, $hour, $mday, $mon, $year);
  print ("epoch = $epoch\n") if ($DEBUG);
  my $outset = (3600 * $tzhh) + (60 * $tzmm);
  print ("outset = $outset\n") if $DEBUG;
  if ($timeAndTZ =~ /\+/) {
    # The timezone has a '+' offset, so time needs to be SUBTRACTED.
    $epoch = $epoch - $outset;
	$localOffset = "+$tz";
  } else {
    # The timezone has a '-' offset, so time needs to be ADDED.
    $epoch = $epoch + $outset;
	$localOffset = "-$tz";
  }

  my  $dt = DateTime->from_epoch( epoch => $epoch );
  my $utcTime = $dt->ymd('') . 'T' . sprintf("%02d%02d%02d",$dt->hour,$dt->min,$dt->sec) . 'Z';
  print "utc_time = ".$utcTime."\n" if $DEBUG;
  
  if ($dateType eq "LT" || $dateType eq 'LT_END') {
    #Determine the Olson timezone
	my $localOlson = com::owm::OlsonToVTimezone::getLocalOlson($localOffset,$eventTz,$localStartTime,$DEBUG,"");
	if ($localOlson eq "unknown") {
		#$log->warn("$email|$puid: local timezone could not be determined. using UTCtime. this may cause incompatibilities in the UI display");
		return(":$utcTime",$eventTz); 
	} else {
		return(";TZID=$localOlson:$localTime",$localOlson); 
	}
    #Return the derived event timezone so the correct VTIMEZONE data can be added to VCAL
  } else {
	return($utcTime); 
  }
}

##############################################################################################################
# getRefreshToken
#
# Lookup a refresh token for a user from consent DB. The refresh token is needed to get an access token for Microsoft.
#
# Args:
# - email address of user whose refresh token is needed
# - config object
# - log object
#
# Return:
# IF successful
#  - Return a refresh token
# ELSE
#  - Return undef
#
##############################################################################################################
sub getRefreshToken($$$$)
{
  my $puid = shift;
  my $email = shift;
  my $cfg = shift;
  my $log = shift;

my $mysql_user =  $cfg->param('mysql_user');
my $mysql_pass = $cfg->param('mysql_pass');
my $mysql_db = $cfg->param('mysql_db');
my $mysql_host = $cfg->param('mysql_host');

  $log->debug("$email|$puid: getRefreshToken mysql_user=$mysql_user, mysql_pass=$mysql_pass, mysql_db=$mysql_db, mysql_host=$mysql_host") if ($DEBUG);

  my $table = "flow";
  my $dbi_str = "DBI:mysql:" . $mysql_db . ";host=" . $mysql_host;
  my $dbh = DBI->connect($dbi_str,$mysql_user,$mysql_pass)
    or return (undef);

  my $sql = "select refresh_token from $table where email='$email'";
  my $sth = $dbh->prepare($sql);
  $sth->execute or return ("");
  my $rowsReturnCount = $sth->rows;
  my @row = $sth->fetchrow_array();
  my ($refresh_token) = @row;     

  $sth->finish();
  $dbh->disconnect();
  if ($rowsReturnCount) {
    $log->debug("$email|$puid:refresh_token = $refresh_token") if ($DEBUG);
    return($refresh_token);
  } else {
    $log->info("$email|$puid:Account has no refresh_token");
    return(undef);
  }
}

##############################################################################################################
# getAccessToken
#
# Retrieve an access token using Microsoft API. 
#
# Args:
# - puid
# - email address of user whose access token is needed
# - config object
# - log object
# - refresh token
#
# Return:
# IF successful
#  - Return an access token
# ELSE
#  - Return undef
#
##############################################################################################################
sub getAccessToken($$$$$)
{
  my $puid = shift;
  my $email = shift;
  my $cfg = shift;
  my $log = shift;
  my $refreshToken = shift;
  my $ms_access_token_url  = $cfg->param('ms_access_token_url');
  my $ms_access_client_id = $cfg->param('ms_access_client_id');
  my $ms_access_client_secret = $cfg->param('ms_access_client_secret');

  $log->debug("$email|$puid:ms_access_token_url = $ms_access_token_url") if ($DEBUG);
  $log->debug("$email|$puid:refreshToken = $refreshToken") if ($DEBUG);

  # Create a POST for updating contact
  my $req = POST( $ms_access_token_url,
                  Content => [ 'client_id' => $ms_access_client_id,
                               'client_secret' => $ms_access_client_secret,
                               'refresh_token' => $refreshToken,
                               'grant_type' => 'refresh_token' ] );
  $req->content_type('application/x-www-form-urlencoded');

#   $DEBUG = 1;
  print "-------------------------\n".$req->as_string."\n-------------------------\n" if $DEBUG;

  # Do HTTP request.
  my $ua = LWP::UserAgent->new;
  my $response;
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $response = $ua->request($req);
  }

  print "-------------------------\n".$response->as_string."\n-------------------------\n" if $DEBUG;

  if ( $response->code == 200 ) {
    my $content = $response->content;
    my $decoded_json = decode_json($content);
    my $access_token = $decoded_json->{"access_token"};
    $log->debug("$email|$puid:response = ".$response->content) if ($DEBUG);
    $log->debug("$email|$puid:access_token = $access_token") if ($DEBUG);
    return($access_token);
  } else {
    $log->error("$email|$puid:Failed to get access_token. ".$response->code." - ".$response->message);
    return(undef);
  }
}

##############################################################################################################
# getCalendar
#
# Retrieve calendar object using Microsoft API.
# For detail of requesting calendar object, see https://msdn.microsoft.com/en-us/library/windows/apps/hh243648.aspx#calendar
#
# Args:
# - email address of user whose access token is needed
# - config object
# - log object
# - access token
#
# Return:
# IF successful
#  - Return contect containing the calendar object
# ELSE
#  - Return undef
#
##############################################################################################################
sub getCalendars($$$$$)
{
  my $email = shift;
  my $cfg = shift;
  my $log = shift;
  my $conflictLog = shift;
  my $access_token = shift;
  my $ms_calendar_url  = $cfg->param('ms_calendar_url');

  my $url = URI->new($ms_calendar_url);
  $url->query_form( 'access_token' => $access_token );

  my $req = GET( $url );

  print "-------------------------\n".$req->as_string."\n-------------------------\n" if $DEBUG;

  # Do HTTP request.
  my $ua = LWP::UserAgent->new;
  my $response;
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $response = $ua->request($req);
  }

  if ( $response->code == 200 ) {
    my $content = $response->content;
    $content =~ s/\r|\n//g;
    $log->debug("$email|content = ".$content) if ($DEBUG);
    return($content);
  } elsif ( $response->code == 401 ) {
    my $content = $response->content;
    if ($content =~ /"error":/) {
      my $decoded_json = decode_json($content);
      if (defined($decoded_json->{'error'}{'message'})) {
        $log->error($email.'|Unauthorized error from MSFT calendar call: '.$decoded_json->{'error'}{'message'});
        $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call: '.$decoded_json->{'error'}{'message'});
      } else {
        $log->error($email.'|Unauthorized error from MSFT calendar call');
        $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call');
      }
    } else {
      $log->error($email.'|Unauthorized error from MSFT calendar call');
      $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call');
    }
    return(undef);
  } else {
    $log->error("$email|Failed to get calendar. ".$response->code." - ".$response->message);
    return(undef);
  }
}

##############################################################################################################
# getCalendarEvents
#
# Retrieve calendar event object(s) using Microsoft API.
# For detail of requesting calendar event object, see https://msdn.microsoft.com/en-us/library/windows/apps/hh243648.aspx#event
# and https://msdn.microsoft.com/en-us/library/windows/apps/hh243648.aspx#http_verbs
# and https://msdn.microsoft.com/en-us/library/hh826523.aspx#cal_rest
# https://apis.live.net/v5.0/$calendar_id/events?access_token=
#
# Args:
# - email address of user whose access token is needed
# - config object
# - log object
# - access token
# - offset, position in calendar events to start at.
# - limit, max number of calendar events to return.
# - start_time
# - end_time
#
# Return:
# IF successful
#  - Return contect containing the calendar event object(s)
# ELSE
#  - Return undef
#
##############################################################################################################
sub getCalendarEvents($$$$$$$$$$)
{
  my $calendar_id = shift;
  my $email = shift;
  my $puid = shift;
  my $cfg = shift;
  my $log = shift;
  my $conflictLog = shift;
  my $access_token = shift;
  my $offset = shift;
  my $limit = shift;
  my $start_time = shift;
  my $end_time = shift;
#  my $ms_calendar_events_url  = $cfg->param('ms_calendar_events_url');
  my $ms_calendar_events_url = "https://apis.live.net/v5.0/$calendar_id/events";

  $log->info("$email|$puid:	 from $start_time to $end_time, offset = $offset");

  my $url = URI->new($ms_calendar_events_url);
  $url->query_form( 'start_time' => $start_time,
                    'end_time' => $end_time,
                    # 'offset' => $offset, 
                    # 'limit' => $limit,
                    'access_token' => $access_token );

  my $req = GET( $url );

  print "-------------------------\n".$req->as_string."\n-------------------------\n" if $DEBUG;

  # Do HTTP request.
  my $ua = LWP::UserAgent->new;
  my $response;
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $response = $ua->request($req);
  }

  if ( $response->code == 200 ) {
    my $content = $response->content;
    $content =~ s/\r|\n//g;
    $log->debug("$email|content = ".$content) if ($DEBUG);
    return($content);
  } elsif ( $response->code == 401 ) {
    my $content = $response->content;
    if ($content =~ /"error":/) {
      my $decoded_json = decode_json($content);
      if (defined($decoded_json->{'error'}{'message'})) {
        $log->error($email.'|Unauthorized error from MSFT calendar call: '.$decoded_json->{'error'}{'message'});
        $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call: '.$decoded_json->{'error'}{'message'});
      } else {
        $log->error($email.'|Unauthorized error from MSFT calendar call');
        $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call');
      }
    } else {
      $log->error($email.'|Unauthorized error from MSFT calendar call');
      $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call');
    }
    return(undef);
  } else {
    $log->error("$email|Failed to get calendar events. ".$response->code." - ".$response->message);
    return(undef);
  }
}

##############################################################################################################
# getFromDetail
#
# Retrieve details about who created the calendar event object, using Microsoft API.
# For detail of requesting calendar event object, see https://msdn.microsoft.com/en-us/library/windows/apps/hh243648.aspx#top
#
# Args:
# - email address of user whose access token is needed
# - config object
# - log object
# - access token
# - fromId, ID of who created the calendar event.
#
# Return:
# IF successful
#  - Return contect containing who the calendar event was from 
# ELSE
#  - Return undef
#
##############################################################################################################
sub getFromDetail($$$$$$)
{
  my $email = shift;
  my $cfg = shift;
  my $log = shift;
  my $conflictLog = shift;
  my $access_token = shift;
  my $fromId = shift;
  my $ms_calendar_from_details_url  = $cfg->param('ms_calendar_from_details_url');

  my $url = URI->new($ms_calendar_from_details_url . '/' . $fromId);
  $url->query_form( 'access_token' => $access_token );

  my $req = GET( $url );

  print "-------------------------\n".$req->as_string."\n-------------------------\n" if $DEBUG;

  # Do HTTP request.
  my $ua = LWP::UserAgent->new;
  my $response;
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $response = $ua->request($req);
  }

  if ( $response->code == 200 ) {
    my $content = $response->content;
    $content =~ s/\r|\n//g;
    $log->debug("$email|content = ".$content) if ($DEBUG);
    return($content);
  } elsif ( $response->code == 401 ) {
    my $content = $response->content;
    if ($content =~ /"error":/) {
      my $decoded_json = decode_json($content);
      if (defined($decoded_json->{'error'}{'message'})) {
        $log->error($email.'|Unauthorized error from MSFT calendar call: '.$decoded_json->{'error'}{'message'});
        $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call: '.$decoded_json->{'error'}{'message'});
      } else {
        $log->error($email.'|Unauthorized error from MSFT calendar call');
        $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call');
      }
    } else {
      $log->error($email.'|Unauthorized error from MSFT calendar call');
      $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call');
    }
    return(undef);
  } else {
    $log->error("$email|Failed to get from details. ".$response->code." - ".$response->message);
    return(undef);
  }
}


##############################################################################################################
# getContacts
#
# Retrieve contacts using Microsoft API.
# For detail of requesting contact objects, see https://msdn.microsoft.com/en-us/library/hh826527.aspx#readwrite_rest
# and https://msdn.microsoft.com/en-us/library/windows/apps/hh243648.aspx#contact
#
# Args:
# - email address of user whose access token is needed
# - config object
# - log object
# - access token
#
# Return:
# IF successful
#  - Return contect containing contacts for user
# ELSE
#  - Return undef
#
##############################################################################################################
sub getContacts($$$$$)
{
  my $email = shift;
  my $cfg = shift;
  my $log = shift;
  my $conflictLog = shift;
  my $access_token = shift;
  my $ms_contacts_url  = $cfg->param('ms_contacts_url');

  my $url = URI->new($ms_contacts_url);
  $url->query_form( 'access_token' => $access_token );

  my $req = GET( $url );

  # $DEBUG = 1;
  print "-------------------------\n".$req->as_string."\n-------------------------\n" if $DEBUG;

  # Do HTTP request.
  my $ua = LWP::UserAgent->new;
  my $response;
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $response = $ua->request($req);
  }

  if ( $response->code == 200 ) {
    my $content = $response->content;
    $content =~ s/\r|\n//g;
    $log->debug("$email|content = ".$content) if ($DEBUG);
    return($content);
  } elsif ( $response->code == 401 ) {
    my $content = $response->content;
    if ($content =~ /"error":/) {
      my $decoded_json = decode_json($content);
      if (defined($decoded_json->{'error'}{'message'})) {
        $log->error($email.'|Unauthorized error from MSFT calendar call: '.$decoded_json->{'error'}{'message'});
        $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call: '.$decoded_json->{'error'}{'message'});
      } else {
        $log->error($email.'|Unauthorized error from MSFT calendar call');
        $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call');
      }
    } else {
      $log->error($email.'|Unauthorized error from MSFT calendar call');
      $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call');
    }
    return(undef);
  } else {
    $log->warn("$email|Failed to get contacts. ".$response->code." - ".$response->message);
    return(undef);
  }
}

##############################################################################################################
# getFirstLastName
#
# Retrieve first and last name for user, using Microsoft API.
# For detail of requesting object containing first and last name, see https://msdn.microsoft.com/en-us/library/windows/apps/hh243648.aspx#user
# and https://msdn.microsoft.com/en-us/library/hh826527.aspx
#
# Args:
# - email address of user whose access token is needed
# - config object
# - log object
# - access token
#
# Return:
# IF successful
#  - Return contect containing first and last name
# ELSE
#  - Return undef
#
##############################################################################################################
sub getFirstLastName($$$$$)
{
  my $email = shift;
  my $cfg = shift;
  my $log = shift;
  my $conflictLog = shift;
  my $access_token = shift;
  my $ms_firstlastname_url  = $cfg->param('ms_firstlastname_url');

  my $url = URI->new($ms_firstlastname_url);
  $url->query_form( 'access_token' => $access_token );

  my $req = GET( $url );

  print "-------------------------\n".$req->as_string."\n-------------------------\n" if $DEBUG;

  # Do HTTP request.
  my $ua = LWP::UserAgent->new;
  my $response;
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $response = $ua->request($req);
  }

  if ( $response->code == 200 ) {
    my $content = $response->content;
    $content =~ s/\r|\n//g;
    $log->debug("$email|content = ".$content) if ($DEBUG);
    return($content);
  } elsif ( $response->code == 401 ) {
    my $content = $response->content;
    if ($content =~ /"error":/) {
      my $decoded_json = decode_json($content);
      if (defined($decoded_json->{'error'}{'message'})) {
        $log->error($email.'|Unauthorized error from MSFT calendar call: '.$decoded_json->{'error'}{'message'});
        $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call: '.$decoded_json->{'error'}{'message'});
      } else {
        $log->error($email.'|Unauthorized error from MSFT calendar call');
        $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call');
      }
    } else {
      $log->error($email.'|Unauthorized error from MSFT calendar call');
      $conflictLog->error($email.'|'.$STATUS_FAIL_ACCESS_TOKEN.':Unauthorized error from MSFT calendar call');
    }
    return(undef);
  } else {
    $log->error("$email|Failed to get first / last name. ".$response->code." - ".$response->message);
    return(undef);
  }
}

##############################################################################################################
# updatePassword
#
# Retrieve and decrypt password from Microsoft. Then update LDAP with that password in SHAA1 encryption.
# There is no Microsoft API documentation on this API.
#
# Args:
# - email of user whose password is being updated
# - puid of user whose password is being updated
# - log object
# - mos object
# - config object
#
# Return:
#  - Return clear text password
#
##############################################################################################################
sub updatePassword($$$$$$$)
{
  my $jobID = shift;
  my $email = shift;
  my $puid = shift;
  $DEBUG = shift;
  my $log = shift;
  my $mos = shift;
  my $cfg = shift;

  # Retrieve decrypted password from MSFT
  my $password_endpoint = $cfg->param('password_endpoint');
  my ($clearPwd,$msftEmail,$error) = ('','','1');
  if ($password_endpoint){
    my $keyFile = $cfg->param('key_file');  
    ($clearPwd, $error) = com::owm::RetrievePassword::retrievePassword($jobID, $password_endpoint,$email,$email,"OWMP",$keyFile, $log);
  #($clearPwd, $error) = com::owm::RetrievePassword::retrievePassword($password_endpoint,$email,$email,"OWMP",$log);
  } else {
    ($clearPwd,$msftEmail,$error) = com::owm::MicrosoftPwd::getMicrosoftPwd($puid,$email);
  }

  if ($error eq 1) {

    $log->warn("$email|$puid:Error retrieving password.");
    return($STATUS_FAIL_PASSWORD_RETRIEVAL,'');

  } elsif ($error eq 2) {

    $log->warn("$email|$puid:Error during password retrieval, mismatch in email address (MSFT return string: $msftEmail).");
    return($STATUS_FAIL_PUID_EMAIL_MISMATCH,'');

  } else {

    $log->debug("$email|$puid:Successfully retrieved password.") if $DEBUG;

    # To debug out a partical password.
    my $particalClearPwd = $clearPwd;
    $particalClearPwd =~ s/(.)(.)(.)(.)/XXXX/;

    # This is commented out to make sure password is not written to log file.
    # $log->debug("$email|$puid:clearPwd = $particalClearPwd") if $DEBUG;

    # Set password in LDAP using mOS.
    ### $error = $mos->setCredentials($email,'ssha1',$clearPwd); # Setting pwd via mOS is cause LDAP issue when run at load. DO NOT USE!!!
    $error = setSsha1Pwd($email,$puid,$cfg,$log,$clearPwd);

    if ($error) {
      $log->warn("$email|$puid:Failed to set password via mOS.");
      return($STATUS_FAIL_PASSWORD_SETTING,'');
    } else {
      $log->info("$email|$puid:Password retrieved and set in LDAP.");
    }
  }

  return($STATUS_SUCCESS,$clearPwd);

}

##############################################################################################################
# callMigrationTool
#
# Subroutine that calls the Migration Tools to migrate one user's mailbox. The Migration Tool needs to be on the
# local host to the script and run by 'imail' account.
#
# Args:
# - email of user whose password is being updated
# - puid of user whose password is being updated
# - log object
# - config object
# - pwd of user
#
# Return:
#  - Return null
#
##############################################################################################################
sub callMigrationTool($$$$$$$$$)
{
  my $mode = shift;
  my $email = shift;
  my $puid = shift;
  $DEBUG = shift;
  my $log = shift;
  my $cfg = shift;
  my $pwd = shift;
  my $preload = shift;
  my $migrToolCmd;
  my $migrToolDir;
  my $mssvip = shift;

  if ($mode eq "MX8" || $mode eq "MYI") {
	  $migrToolCmd = $cfg->param('owm_migration_tool_cmd');
     if ($preload eq "true") {
	     $migrToolDir = $cfg->param('owm_migration_tool_dir_preload');
	  } else {
		  $migrToolDir = $cfg->param('owm_migration_tool_dir');
	  }
  } else {
	  $migrToolCmd = $cfg->param('migration_tool_cmd');
      if ($preload eq "true") {
		$migrToolDir = $cfg->param('migration_tool_dir_preload');	  
	  } else {
		$migrToolDir = $cfg->param('migration_tool_dir');
	  }
  }
  my $pwdOrig = cwd();

  $log->debug("$email|$puid: Calling Migration Tool ... from $migrToolDir ");
  $log->debug("$email|$puid:cd to ".$migrToolDir) if $DEBUG;
  $migrToolCmd =~ s/<account>/$email/;
  $migrToolCmd =~ s/<mssvip>/$mssvip/;
  $log->debug("$email|$puid:migrToolCmd = ".$migrToolCmd) if $DEBUG; #print out this command before putting in the real password for security reasons
  #Get encrypted password
  my $desPwd=com::owm::RetrievePassword::encryptDESPassword($pwd);
  $desPwd = "{DES}$desPwd";
  $migrToolCmd =~ s/<pwd>/'$desPwd'/;

  chdir($migrToolDir);
  my $res = `$migrToolCmd`;
  $log->debug("$email|$puid:migrToolCmd res = ".$res) if $DEBUG;

  chdir($pwdOrig);
  my $successStr = 'Migration completed';
  if ( index($res, $successStr) != -1  ) {
	return($STATUS_SUCCESS);
  } else {
	return($STATUS_FAIL_MAILBOX_MIGRATION);
  }
  
}

######################################################################
# setMigstatus
#
# Subroutine to update migstatus for an account.
#
# Args:
# - email of user whose migstatus is being updated
# - puid of user whose migstatus is being updated
# - config object
# - log object
# - current migstatus
# - update to the migstatus (one character, eg 'C' for successfully migrated calendar).
#
######################################################################
sub setMigstatus($$$$$$)
{
  my $email = shift;
  my $puid = shift;
  my $cfg = shift;
  my $log = shift;
  my $oldCurrentMigstatus = shift;  # This is the migstatus from a previous search (it could be out of date).
  my $updateToMigstatus = shift;    # This is the change to the migstatus (ie. the one component of the migstatus that been updated)
  my $lcUpdateToMigstatus = lc $updateToMigstatus;       # Lower case of $updateToMigstatus, AKA failed version.
  my $ucUpdateToMigstatus = uc $updateToMigstatus;       # Upper case of $updateToMigstatus, AKA passed version.
  my $mx_dirserv_host = $cfg->param('mx_dirserv_host');
  my $mx_dirserv_port = $cfg->param('mx_dirserv_port');
  my $mx_dirserv_pwd  = $cfg->param('mx_dirserv_pwd');
  my $currentMigstatus;
  my $result;

  # Set up connection to dirserver.
  my $ldap = Net::LDAP->new($mx_dirserv_host, port => $mx_dirserv_port)
    or die "Unable to connect to LDAP server $mx_dirserv_host: $@\n";

  my $mesg = $ldap->bind ( "cn=root",
                           password => $mx_dirserv_pwd,
                           version => 3 );          # use for changes/edits

  $result = $ldap->search( filter => "mail=$email" );


  my @entries = $result->entries;
  my $numEntries = $#entries + 1;
  print "numEntries: $numEntries  returned \n" if ($DEBUG);
  # Check no error occured and only one account was returned.
  if ($result->code) {
    $log->error("$email|$puid:Lookup failed. " . $result->error . " - " . $result->code);
    $log->error("$email|$puid:migstatus not updated from '$oldCurrentMigstatus' to '$updateToMigstatus'");
    $ldap->unbind;
    $ldap->disconnect();
    return;
  } elsif ($numEntries == 0) {
    $log->error("$email|$puid: 0 accounts matching found. Can't determine which account to update. ");
    $log->error("$email|$puid:migstatus not updated from '$oldCurrentMigstatus' to '$updateToMigstatus'");
    $ldap->unbind;
    $ldap->disconnect();
    return;
  } elsif ($numEntries > 1) {
    $log->error("$email|$puid: $numEntries accounts matching puid ($puid). Can't determine which account to update.");
    $log->error("$email|$puid:migstatus not updated from '$oldCurrentMigstatus' to '$updateToMigstatus'");
    $ldap->unbind;
    $ldap->disconnect();
    return;
  }

  # If migstatus is not defined yet, set it to $updateToMigstatus.
  if ( ! defined $entries[0]->get('migstatus') ) {
    $log->debug("$email|$puid:migstatus = undefined, update to migstatus = $updateToMigstatus") if $DEBUG;
    $entries[0]->add(migstatus => $updateToMigstatus);
    $currentMigstatus = $updateToMigstatus; 

  # Else update the current migstatus.
  } else {
    $currentMigstatus = ${$entries[0]->get('migstatus')}[0];
    $log->debug("$email|$puid:migstatus = $currentMigstatus, update to migstatus = $updateToMigstatus, lc = $lcUpdateToMigstatus, uc = $ucUpdateToMigstatus") if $DEBUG;

    # If $updateToMigstatus is lower case...
    if ( $updateToMigstatus eq $lcUpdateToMigstatus ) {
      # The update to be made is lower case.
      $log->trace("$email|$puid:The update to be made is lower case. '$updateToMigstatus'") if $DEBUG;

      if ( $currentMigstatus =~ /$ucUpdateToMigstatus/ ) {
        # The current migstatus already has upper case setting, so translate to lower case.
        $log->trace("$email|$puid:BEFORE - The current migstatus already has upper case setting ('$currentMigstatus'), so translate to lower case.") if $DEBUG;

        if ($updateToMigstatus eq 'p') {
          $currentMigstatus =~ tr/P/p/;
        } elsif ($updateToMigstatus eq 'u') {
          $currentMigstatus =~ tr/U/u/;
        } elsif ($updateToMigstatus eq 's') {
          $currentMigstatus =~ tr/S/s/;
        } elsif ($updateToMigstatus eq 'f') {
          $currentMigstatus =~ tr/F/f/;
        } elsif ($updateToMigstatus eq 'v') {
          $currentMigstatus =~ tr/V/v/;
        } elsif ($updateToMigstatus eq 'c') {
          $currentMigstatus =~ tr/C/c/;
        } elsif ($updateToMigstatus eq 'a') {
          $currentMigstatus =~ tr/A/a/;
        } elsif ($updateToMigstatus eq 'n') {
          $currentMigstatus =~ tr/N/n/;
        } elsif ($updateToMigstatus eq 'x') {
          $currentMigstatus =~ tr/X/x/;
        } elsif ($updateToMigstatus eq 'm') {
          $currentMigstatus =~ tr/M/m/;
        } elsif ($updateToMigstatus eq 'z') {
          $currentMigstatus =~ tr/Z/z/;        
		} elsif ($updateToMigstatus eq 'd') {
          $currentMigstatus =~ tr/D/d/;
        } else {
          $log->warn("$email|$puid:migstatus update failed.");
        }
        $log->trace("$email|$puid:AFTER - The current migstatus already has upper case setting ('$currentMigstatus'), so translate to lower case.") if $DEBUG;

      } elsif ( $currentMigstatus =~ /$lcUpdateToMigstatus/ ) {
        # The current migstatus already has lower case setting, so no need to update.
        $log->trace("$email|$puid:The current migstatus already has lower case setting, so no need to update. '$currentMigstatus'") if $DEBUG;
        
      } else {
        # The current migstatus has no upper or lower case setting, so add it.
        $log->trace("$email|$puid:The current migstatus has no upper or lower case setting, so add it. '$currentMigstatus' -> '" .$currentMigstatus.$updateToMigstatus. "'") if $DEBUG;
        $currentMigstatus .= $updateToMigstatus;
      }

    # If $updateToMigstatus is upper case...
    } elsif ( $updateToMigstatus eq $ucUpdateToMigstatus ) {
      # The update to be made is upper case.
      $log->trace("$email|$puid:The update to be made is upper case. '$updateToMigstatus'") if $DEBUG;

      if ( $currentMigstatus =~ /$lcUpdateToMigstatus/ ) {
        # The current migstatus already has lower case setting, so translate to upper case.
        $log->trace("$email|$puid:BEFORE - The current migstatus already has lower case setting ('$currentMigstatus'), so translate to upper case.") if $DEBUG;

        if ($updateToMigstatus eq 'P') {
          $currentMigstatus =~ tr/p/P/;
        } elsif ($updateToMigstatus eq 'U') {
          $currentMigstatus =~ tr/u/U/;
        } elsif ($updateToMigstatus eq 'S') {
          $currentMigstatus =~ tr/s/S/;
        } elsif ($updateToMigstatus eq 'F') {
          $currentMigstatus =~ tr/f/F/;
        } elsif ($updateToMigstatus eq 'V') {
          $currentMigstatus =~ tr/v/V/;
        } elsif ($updateToMigstatus eq 'C') {
          $currentMigstatus =~ tr/c/C/;
        } elsif ($updateToMigstatus eq 'A') {
          $currentMigstatus =~ tr/a/A/;
        } elsif ($updateToMigstatus eq 'N') {
          $currentMigstatus =~ tr/n/N/;
        } elsif ($updateToMigstatus eq 'X') {
          $currentMigstatus =~ tr/x/X/;
        } elsif ($updateToMigstatus eq 'M') {
          $currentMigstatus =~ tr/m/M/;
        } elsif ($updateToMigstatus eq 'Z') {
          $currentMigstatus =~ tr/z/Z/;
        } elsif ($updateToMigstatus eq 'D') {
          $currentMigstatus =~ tr/d/D/;
        } else {
          $log->error("$email|$puid:migstatus update failed.");
        }
        $log->trace("$email|$puid:AFTER - The current migstatus already has lower case setting ('$currentMigstatus'), so translate to upper case.") if $DEBUG;

      } elsif ( $currentMigstatus =~ /$ucUpdateToMigstatus/ ) {
        # The current migstatus already has upper case setting, so no need to update.
        $log->trace("$email|$puid:The current migstatus already has upper case setting, so no need to update. '$currentMigstatus'") if $DEBUG;
         
      } else {
        # The current migstatus has no upper or lower case setting, so add it.
        $log->trace("$email|$puid:The current migstatus has no upper or lower case setting, so add it. '$currentMigstatus' -> '" .$currentMigstatus.$updateToMigstatus. "'") if $DEBUG;
        $currentMigstatus .= $updateToMigstatus;
      }

    }

    # If currentMigstatus was interrupted ('i'), remove the interrupt.
	#Commented out till understand why this was removed for Bell CA migration
 #  if ($currentMigstatus =~ 'i') {
 #    $currentMigstatus =~ s/i//;
 #  }

    $log->debug("$email|$puid:replace migstatus => $currentMigstatus") if $DEBUG;
    $entries[0]->replace(migstatus => $currentMigstatus);

  }

  $entries[0]->update($ldap);
  $log->info("$email|$puid:migstatus updated with '$updateToMigstatus'. Changing migstatus from '$oldCurrentMigstatus' to '$currentMigstatus'");

  $ldap->unbind;
 $ldap->disconnect();
  return($currentMigstatus);
}

######################################################################
# setNames
#
# Subroutine to update cn and sn for an account.
#
# Args:
# - email of user whose migstatus is being updated
# - puid of user whose migstatus is being updated
# - config object
# - log object
# - cn (first name)
# - sn (last name)
#
######################################################################
sub setNames($$$$$$)
{
  my $email = shift;
  my $puid = shift;
  my $cfg = shift;
  my $log = shift;
  my $cn = shift;
  my $sn = shift;
  my $mx_dirserv_host = $cfg->param('mx_dirserv_host');
  my $mx_dirserv_port = $cfg->param('mx_dirserv_port');
  my $mx_dirserv_pwd  = $cfg->param('mx_dirserv_pwd');

  # Set up connection to dirserver.
  my $ldap = Net::LDAP->new($mx_dirserv_host, port => $mx_dirserv_port)
    or die "Unable to connect to LDAP server $mx_dirserv_host: $@\n";

  my $mesg = $ldap->bind ( "cn=root",
                           password => $mx_dirserv_pwd,
                           version => 3 );          # use for changes/edits

  # Search for the account using the puid as the key.
  my $result = $ldap->search( filter => "puid=$puid" );
  my @entries = $result->entries;
  my $numEntries = $#entries + 1;

  # Check no error occured and only one account was returned.
  if ($result->code) {
    $log->error("$email|$puid:Lookup failed. $result->error");
    $log->warn("$email|$puid:cn ($cn) and sn ($sn) not updated : ldap error code ".$result->code);
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_AS_NAME);
  } elsif ($numEntries != 1) {
    $log->error("$email|$puid:Either 0 or 2+ accounts matching puid ($puid). Can't determine which account to update.");
    $log->warn("$email|$puid:cn ($cn) and sn ($sn) not updated");
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_AS_NAME);
  }

  my $ldapCn = ${$entries[0]->get('cn')}[0];
  my $ldapSn = ${$entries[0]->get('sn')}[0];

  # If cn & sn is the default (email address)...
  # MCB: Comment this out as this is Bell logic that we may not need for Telstra.
 # if ( ($ldapCn eq $email) && ($ldapSn eq $email) ) {
    $log->debug("$email|$puid:The current cn '$ldapCn' -> '" .$cn. "'") if $DEBUG;
    $log->debug("$email|$puid:The current sn '$ldapSn' -> '" .$sn. "'") if $DEBUG;

    if ( $cn ne "" ) {
      $entries[0]->replace(cn => $cn);
    } else {
      $log->info("$email|$puid:cn from MSFT is null");
    }

    if ( $sn ne "" ) {
      $entries[0]->replace(sn => $sn);
    } else {
      $log->info("$email|$puid:sn from MSFT is null");
    }

    if ( ($cn eq "") && ($sn eq "") ) {
      $log->info("$email|$puid:both cn and sn from MSFT are null, so don't update cn and sn in LDAP");
      return($STATUS_SUCCESS);
    }

  # Else don't update.
#  } else {
#
#    $log->info("$email|$puid:cn ($ldapCn) or sn ($ldapSn) has been set to a non-default settings in LDAP (ie. email address), so don't update");
#    $ldap->unbind;
#     $ldap->disconnect();
#    return($STATUS_SUCCESS);
#  }

  $result = $entries[0]->update($ldap);
  if ($result->code) {
    $log->warn("$email|$puid:cn ($cn) and sn ($sn) not updated : ldap error code ".$result->code);
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_AS_NAME);
  }

  $ldap->unbind;
  $ldap->disconnect();
  return($STATUS_SUCCESS);

}

######################################################################
# setMaillogin
#
# Subroutine to update maillogin for an account.
#
# Args:
# - email of user whose migstatus is being updated
# - puid of user whose migstatus is being updated
# - config object
# - log object
# - New maillogin
#
######################################################################
sub setMaillogin($$$$$)
{
  my $email = shift;
  my $puid = shift;
  my $cfg = shift;
  my $log = shift;
  my $maillogin = shift;
  my $mx_dirserv_host = $cfg->param('mx_dirserv_host');
  my $mx_dirserv_port = $cfg->param('mx_dirserv_port');
  my $mx_dirserv_pwd  = $cfg->param('mx_dirserv_pwd');

  # Set up connection to dirserver.
  my $ldap = Net::LDAP->new($mx_dirserv_host, port => $mx_dirserv_port)
    or die "Unable to connect to LDAP server $mx_dirserv_host: $@\n";

  my $mesg = $ldap->bind ( "cn=root",
                           password => $mx_dirserv_pwd,
                           version => 3 );          # use for changes/edits

  # Search for the account using the puid as the key.
  my $result = $ldap->search( filter => "puid=$puid" );
  my @entries = $result->entries;
  my $numEntries = $#entries + 1;

  # Check no error occured and only one account was returned.
  if ($result->code) {
    $log->error("$email|$puid:Lookup failed. $result->error");
    $log->warn("$email|$puid:maillogin ($maillogin) not updated : ldap error code ".$result->code);
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_MAILLOGIN_SETTING);
  } elsif ($numEntries != 1) {
    $log->error("$email|$puid:Either 0 or 2+ accounts matching puid ($puid). Can't determine which account to update.");
    $log->warn("$email|$puid:maillogin ($maillogin) not updated");
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_MAILLOGIN_SETTING);
  }

  my $ldapMaillogin = ${$entries[0]->get('maillogin')}[0];

  $log->debug("$email|$puid:The current maillogin '$ldapMaillogin' -> '" .$maillogin. "'") if $DEBUG;
  $entries[0]->replace(maillogin => $maillogin);

  $result = $entries[0]->update($ldap);
  if ($result->code) {
    $log->warn("$email|$puid:maillogin ($maillogin) not updated : ldap error code ".$result->code);
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_MAILLOGIN_SETTING);
  }

  $ldap->unbind;
  $ldap->disconnect();
  return($STATUS_SUCCESS);

}

######################################################################
# setPwd
#
# Take clear text password, do ssha1 encryption and set in account LDAP.
#######################################################################
sub setSsha1Pwd($$$$$)
{
  my $email = shift;
  my $puid = shift;
  my $cfg = shift;
  my $log = shift;
  my $password = shift;

  (my $user, my $domain) = split('@',$email);

  # As imdbcontrol called in the shell, need to escape single-quote.
  $password =~ s/\'/\'\\\'\'/;

  # my $getHashCmd = qq~imdbcontrol sp opentest100011 bell.net '$password' -convert ssha1~;
  my $getHashCmd = qq~imdbcontrol sp $user $domain '$password' -convert ssha1~;
  $log->debug("$email|$puid:set password cmd:: $getHashCmd");

  my $cmdRes = `$getHashCmd`;
  if ( $cmdRes ne '' ) {
    $log->warn("$email|$puid:imdbcontrol failed to set password:: $cmdRes");
    return($STATUS_FAIL_PASSWORD_SETTING);
  } else {
    $log->debug("$email|$puid:imdbcontrol set password") if $DEBUG;
    return();
  }
}


######################################################################
# setProxyHosts
#
# Subroutine to add/update the proxy hosts on an account.
# Setting proxy hosts after account creation is not supported by mOS
# ldapModify must be used instead
######################################################################
sub setProxyHosts($$$$$$$$)
{
  my $mode = shift;
  my $email = shift;
  my $hostOne = shift;
  my $hostTwo = shift;
  my $hostThree = shift;
  my $cfg = shift;
  my $log = shift;
  $DEBUG = shift;
  my $mx_dirserv_host = $cfg->param('mx_dirserv_host');
  my $mx_dirserv_port = $cfg->param('mx_dirserv_port');
  my $mx_dirserv_pwd  = $cfg->param('mx_dirserv_pwd');

  # Set up connection to dirserver.
  my $ldap = Net::LDAP->new($mx_dirserv_host, port => $mx_dirserv_port)
    or die "Unable to connect to LDAP server $mx_dirserv_host: $@\n";

  my $mesg = $ldap->bind ( "cn=root",
                           password => $mx_dirserv_pwd,
                           version => 3 );          # use for changes/edits

  # Search for the account using the puid as the key.
  my $result = $ldap->search( filter => "mail=$email" );
  my @entries = $result->entries;
  my $numEntries = $#entries + 1;

  # Check no error occured and only one account was returned.
  if ($result->code) {
    $log->error("$email|Lookup failed. $result->error");
    $log->warn("$email|account not found : ldap error code ".$result->code);
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_MAILPROXY_SETTING);
  } elsif ($numEntries != 1) {
    $log->error("$email|Either 0 or 2+ accounts matching email ($email). Can't determine which account to update.");
    $log->warn("$email|account not updated");
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_MAILPROXY_SETTING);
  }

 # my $dn = ${$entries[0]->get('mail')}[0];
  my $dn = $entries[0]->dn;
  my @addArray;
  my @replaceArray;
  my @changesArray;
  my @deleteArray;

  
  if ( $mode eq "proxy" ) {
	#update the proxy settings for the users
	#Check to see if proxy host details exist, if not add else replace
	  if ( ! $entries[0]->get('mailIMAPProxyHost') ) {
			$log->debug("$email|Adding IMAP Proxy $hostOne" ) if $DEBUG;
			push @addArray, 'mailIMAPProxyHost', $hostOne;
	  } else {
			$log->debug("$email|Replacing IMAP Proxy $hostOne" ) if $DEBUG;
			push @replaceArray, 'mailIMAPProxyHost', $hostOne;
	  }
	  if ( ! $entries[0]->get('mailPOPProxyHost') ) {
			$log->debug("$email|Adding POP Proxy $hostTwo" ) if $DEBUG;
			push @addArray, 'mailPOPProxyHost', $hostTwo;
	  } else {
			$log->debug("$email|Replacing POP Proxy $hostTwo" ) if $DEBUG;
			push @replaceArray, 'mailPOPProxyHost', $hostTwo;
	  }
	  if ( ! $entries[0]->get('mailSMTPRelayHost') ) {
			$log->debug("$email|Adding SMTP Proxy $hostThree" ) if $DEBUG;
			push @addArray, 'mailSMTPRelayHost', $hostThree;
	  } else {
			$log->debug("$email|Replacing SMTP Proxy $hostThree" ) if $DEBUG;
			push @replaceArray, 'mailSMTPRelayHost', $hostThree;
	  }
  } elsif ( $mode eq "proxyrm" ) {
	#remove the user proxy entries 
	my $r = $ldap->modify( $dn, delete => [ 'mailsmtprelayhost' ] )  if $entries[0]->exists('mailsmtprelayhost');
	my $r1 = $ldap->modify( $dn, delete => [ 'mailpopproxyhost' ] )  if $entries[0]->exists('mailpopproxyhost');
	my $r2 = $ldap->modify( $dn, delete => [ 'mailimapproxyhost' ] )  if $entries[0]->exists('mailimapproxyhost');
	$log->debug("$email|Removing proxy entries for user if they existed ");
  } elsif ( $mode eq "realm" ) {
	#update the realm settings for this user
	$log->debug("$email: updating realm is $hostThree");
	  if ( ! $entries[0]->get('mailrealm') ) {
			$log->debug("$email|Adding mail realm $hostThree" ) if $DEBUG;
			push @addArray, 'mailrealm', $hostThree;
	  } else {
			$log->debug("$email|Replacing mail realm $hostThree" ) if $DEBUG;
			push @replaceArray, 'mailrealm', $hostThree;
	  }
  } elsif ( $mode eq "mss" ) {
		$log->debug("$email|Replacing mailmessagestore $hostOne" ) if $DEBUG;
		push @replaceArray, 'mailmessagestore', $hostOne;
		$log->debug("$email|Replacing mailautoreplyhost $hostTwo" ) if $DEBUG;
		push @replaceArray, 'mailautoreplyhost', $hostTwo;
  } else {
	$log->error("$email|No mode set $mode" );
	$ldap->unbind;
    $ldap->disconnect();
	return($STATUS_FAIL_MAILPROXY_SETTING);
  }
 
 ### Remove specific attributes. 
 #Remove the netmaildefaultvar if it exists TELSTRA-567
 #my $r4 = $ldap->modify( $dn, delete => [ 'netmailcurrentver' ] )  if $entries[0]->exists('netmailcurrentver');
 
 push @deleteArray, 'netmailcurrentver', ''  if $entries[0]->exists('netmailcurrentver');
 
 #Remove Mx8 attributes for blocking specific access TELSTRA-597
 push @deleteArray, 'mailpopaccess', ''  if $entries[0]->exists('mailpopaccess');
 push @deleteArray, 'mailpopsslaccess', ''  if $entries[0]->exists('mailpopsslaccess');
 push @deleteArray, 'mailwebmailaccess', ''  if $entries[0]->exists('mailwebmailaccess');
 push @deleteArray, 'pophost', ''  if $entries[0]->exists('pophost');
 push @deleteArray, 'popport', ''  if $entries[0]->exists('popport');
 push @deleteArray, 'popusername', ''  if $entries[0]->exists('popusername');
 push @deleteArray, 'mailquotatotkb', ''  if $entries[0]->exists('mailquotatotkb');

push @deleteArray, 'puremessagevirusactionverb', ''  if $entries[0]->exists('puremessagevirusactionverb');
push @deleteArray, 'puremessagespamactionverb', ''  if $entries[0]->exists('puremessagespamactionverb');
 #Remove all puremessage attributes if they exist TELSTRA-555
 #$r4 = $ldap->modify( $dn, delete => [ 'puremessagevirusactionverb' ] )  if $entries[0]->exists('puremessagevirusactionverb');
 #$r4 = $ldap->modify( $dn, delete => [ 'puremessagepamactionverb' ] )  if $entries[0]->exists('puremessagespamactionverb');
   
  
  if ( $#addArray > 0 ) {
        $log->debug("$email|Adding attributes");
	push @changesArray, 'add';
	push @changesArray, \@addArray;
 }
  if ( $#replaceArray > 0 ) {
        $log->debug("$email|Replacing attributes");
   push @changesArray, 'replace';
   push @changesArray, \@replaceArray;
 }
 
  if ( $#deleteArray > 0 ) {
        $log->debug("$email|Deleting attributes");
        push @changesArray, 'delete';
        push @changesArray, \@deleteArray;
  }

 
  $result = $ldap->modify ( $dn, changes => [ @changesArray ] );
  $log->debug("$email|Updated $mode hosts. Search result: " . $result->code ) if $DEBUG;
  if ($result->code) {
    $log->warn("$email|account not updated : ldap error code ".$result->code);
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_MAILPROXY_SETTING);
  }

  $ldap->unbind;
  $ldap->disconnect();
  return($STATUS_SUCCESS);

}

	######################################################
	# ESTABLISH THE EVENT LOG DATABASE CONNECTION
	######################################################
sub connectEventDB($$$){
	my $eventLog=shift;
	my $cfg = shift;
	my $log = shift;
	my $driver = $cfg->param('mysql_driver');
	my $database = $cfg->param('mysql_db1');
	my $host = $cfg->param('mysql_host');
	my $dsn = "DBI:$driver:database=$database:host=$host";
	my $userid = $cfg->param('mysql_user');
	my $password = $cfg->param('mysql_pass');
	my $exitOnConnectFail = $cfg->param("exitOnConnectFail");
	
	$log->debug("SCRIPT|Connecting to event log database ... ");

	my $dbh = DBI->connect($dsn, $userid, $password ) or $log->error("SCRIPT|Could not connect to event log database ". $DBI::errstr);
	if (! $dbh ) {
		if ($exitOnConnectFail eq "true" ) {
			$log->error("SCRIPT|Exiting migration process now.");
			return($STATUS_GENERAL_ERROR);
		}else{
			$log->error("SCRIPT|Continuing migration processes, using fallback event log.");
			$dbh = "log";
		}
	}
	return($dbh);
}
#######################################################
# EVENTS LOG TO DATABASE
#######################################################
sub logEventsToDatabase($$$$$$$$$$$$$$){
	my $dbh=shift;
	my $cfg = shift;
	my $log = shift;
	my $dateTime=getCurrentDateTime();
	my $processID=shift;
	my $scriptName=shift;
	my $line_number=shift;
	my $jobID=shift;
	my $eventID=shift;
	my $eventName=shift;
	my $migType=shift;
	my $primaryID=shift;
	my $secondaryID=shift;
	my $status=shift;
	my $message=shift;
    my $hostname = hostname;
	if ( my $database_version  = $dbh->get_info(  17 ) ) {
		my $sth = $dbh->prepare("INSERT INTO events
        	               (create_time, process_id,migration_host,migration_scriptname,line_number,migration_job_id,event_id,event_name,migtype,primary_id,secondary_id,status,message)
                	        values
                      		 (?,?,?,?,?,?,?,?,?,?,?,?,?)");
		$sth->execute($dateTime,$processID,$hostname,$scriptName,$line_number,$jobID,$eventID,$eventName,$migType,$primaryID,$secondaryID,$status,$message) or $log->logdie("SCRIPT|".$DBI::errstr);
	}
	
	
	return($STATUS_SUCCESS);
}

sub disconnectDB($$$){
	my $dbh=shift;
	my $cfg = shift;
	my $log = shift;	
	$log->debug("SCRIPT|disconnecting dbh");
	$dbh->disconnect;
}

#GET CURRENT DATETIME
sub getCurrentDateTime{
	my @months = qw( 1 2 3 4 5 6 7 8 9 10 11 12 );
	my @days = qw(Sun Mon Tue Wed Thu Fri Sat Sun);

	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
	$year = $year+1900;
	return "$year-$months[$mon]-$mday $hour:$min:$sec\n";
}

######################################################################
# setPUID
#
# Subroutine to add the PUID for an account.
#
# Args:
# - email of user whose migstatus is being updated
# - puid of user whose migstatus is being updated
#
######################################################################
sub setPUID($$$$)
{
  my $email = shift;
  my $puid = shift;
  my $cfg = shift;
  my $log = shift;
  my $mx_dirserv_host = $cfg->param('mx_dirserv_host');
  my $mx_dirserv_port = $cfg->param('mx_dirserv_port');
  my $mx_dirserv_pwd  = $cfg->param('mx_dirserv_pwd');
  


  # Set up connection to dirserver.
  my $ldap = Net::LDAP->new($mx_dirserv_host, port => $mx_dirserv_port)
    or logdie("Unable to connect to LDAP server $mx_dirserv_host: $@\n");

  my $mesg = $ldap->bind ( "cn=root",
                           password => $mx_dirserv_pwd,
                           version => 3 );          # use for changes/edits

  # Search for the account using the puid as the key.
  my $result = $ldap->search( filter => "maillogin=$email" );
  my @entries = $result->entries;
  my $numEntries = $#entries + 1;

  # Recheck search result.
  if ($result->code) {
    $log->error("$email|$puid setPUID: LDAP Lookup failed. " . $result->code );
    $ldap->unbind;
    $ldap->disconnect();
    return($com::owm::migBellCanada::STATUS_FAIL_MAILLOGIN_SETTING);
  } elsif ($numEntries != 1) {
    $log->error("$email|$puid setPUID: Either 0 or 2+ accounts matching puid. Can't determine which account to update.");
    $ldap->unbind;
    $ldap->disconnect();
    return($com::owm::migBellCanada::STATUS_FAIL_MAILLOGIN_SETTING);
  }

 # my $ldapMaillogin = ${$entries[0]->get('maillogin')}[0];

  $entries[0]->replace(puid => $puid);

  $result = $entries[0]->update($ldap);
  if ($result->code) {
    $log->warn("$email|$puid setPUID: Account not updated [inPUID: $puid inEMAIL: $email ] ".$result->code);
    $ldap->unbind;
    $ldap->disconnect();
    return($com::owm::migBellCanada::STATUS_FAIL_MAILLOGIN_SETTING);
  }

  $ldap->unbind;
  $ldap->disconnect();
  return($com::owm::migBellCanada::STATUS_SUCCESS);

}

######################################################################
# addSubscription
#
# add the calendar subscriptions to LDAP.
#
######################################################################
sub addSubscription($$$$$$)
{
  my $email = shift;
  my $puid = shift;
  my $location = shift;
  my $name = shift;
  my $cfg = shift;
  my $log = shift;
  my $mx_dirserv_host = $cfg->param('mx_dirserv_host');
  my $mx_dirserv_port = $cfg->param('mx_dirserv_port');
  my $mx_dirserv_pwd  = $cfg->param('mx_dirserv_pwd');
  


  # Set up connection to dirserver.
  my $ldap = Net::LDAP->new($mx_dirserv_host, port => $mx_dirserv_port)
    or logdie("Unable to connect to LDAP server $mx_dirserv_host: $@\n");

  my $mesg = $ldap->bind ( "cn=root",
                           password => $mx_dirserv_pwd,
                           version => 3 );          # use for changes/edits

  # Search for the account using the puid as the key.
  my $result = $ldap->search( filter => "maillogin=$email" );
  my @entries = $result->entries;
  my $numEntries = $#entries + 1;

  # Recheck search result.
  if ($result->code) {
    $log->error("$email|$puid addSubscription: LDAP Lookup failed. " . $result->code );
    $ldap->unbind;
    $ldap->disconnect();
    return($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
  } elsif ($numEntries != 1) {
    $log->error("$email|$puid addSubscription: Either 0 or 2+ accounts matching puid. Can't determine which account to update.");
    $ldap->unbind;
    $ldap->disconnect();
    return($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
  }
  my $location_url = $name.';'.int(rand(14)).';yes;'.$location;
  $entries[0]->add(calexternaldetails => $location_url);

  $result = $entries[0]->update($ldap);
  if ($result->code) {
    $log->warn("$email|$puid addSubscription: Account not updated [$location_url] ".$result->code);
    $ldap->unbind;
    $ldap->disconnect();
    return($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
  }

  $ldap->unbind;
  $ldap->disconnect();
  return($com::owm::migBellCanada::STATUS_SUCCESS);

}
######################################################################
# getPortNumber
# returns the port number for PAB/CAL services 
# Options are a fixed port defined by config, or derived based on hostname
# importantly assumes the hostname in the format of pabhaXX-vip
# optionally will take a fixed port if they are defined in migrator.conf
######################################################################
sub getPortNumber
{
    my $service = shift;
    my $serviceType = shift;
    my $hostname = shift;
    my $log = shift;
    my $cfg = shift;
    my $portNumber;
    
    my $hostnumber = substr($hostname, 5,2);
    if ($service eq "telnet") {
        if ($serviceType eq "pab"){
            if (defined $cfg->param('pabFixedTelnetPort')) {
                $portNumber = $cfg->param('pabFixedTelnetPort');
            }else{
                $portNumber = '52'.$hostnumber;
            }
        } elsif ($serviceType eq "cal")  {
            if (defined $cfg->param('calFixedTelnetPort')) {
                $portNumber = $cfg->param('calFixedTelnetPort');
            }else{
                $portNumber = '62'.$hostnumber;
             }
        } else {
           return("Invalid service type $serviceType",1);
        }
    } elsif ($service eq "http") {
        if ($serviceType eq "pab"){
            if (defined $cfg->param('pabFixedHttpPort')) {
                $portNumber = $cfg->param('pabFixedHttpPort');
            }else{
                $portNumber = '72'.$hostnumber;
             }
        } elsif ($serviceType eq "cal") {
            if (defined $cfg->param('calFixedHttpPort')) {
                $portNumber = $cfg->param('calFixedHttpPort');
            }else{
                $portNumber = '82'.$hostnumber;
            }
        } else {
           return("Invalid service type $service",1);
        }
    } else {
        return('Invalid service',1);
    }
    return($portNumber,0);
}
######################################################################
# setMx8Name
# sets the CN and SN using the Mx8 Mail From field 
# for MX8 subscribers only.
# Telstra-510
######################################################################
sub setMx8Name{
  my $email = shift;
  my $fromname = shift;
  my $cfg = shift;
  my $log = shift;
  my $mx_dirserv_host =  $cfg->param('mx_dirserv_host');
  my $mx_dirserv_port =  $cfg->param('mx_dirserv_port');
  my $mx_dirserv_pwd  =  $cfg->param('mx_dirserv_pwd');

  # Set up connection to dirserver.
  my $ldap = Net::LDAP->new($mx_dirserv_host, port => $mx_dirserv_port)
    or die "Unable to connect to LDAP server $mx_dirserv_host: $@\n";

  my $mesg = $ldap->bind ( "cn=root",
                           password => $mx_dirserv_pwd,
                           version => 3 );          # use for changes/edits

  # Search for the account using the puid as the key.
  my $result = $ldap->search( filter => "mail=$email" );
  my @entries = $result->entries;
  my $numEntries = $#entries + 1;

  # Check no error occured.
  if ($result->code) {
    $log->error("$email|setMX8Name Mx8 Lookup failed. $result->error"); 
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_AS_NAME);
  }
    if ( defined $fromname ) {
        $entries[0]->replace(cn => $fromname);
        $entries[0]->replace(sn => " ");
    }
  $result = $entries[0]->update($ldap);
  if ($result->code) {
    $log->warn("$email|setMX8Name cn and sn not updated : ldap error code ".$result->code);
    $ldap->unbind;
    $ldap->disconnect();
    return($STATUS_FAIL_AS_NAME);
  }

  $ldap->unbind;
  $ldap->disconnect();
  return($STATUS_SUCCESS);

}

##############################################################################################################
# Exit perl module
##############################################################################################################
1;  # so the require or use succeeds
