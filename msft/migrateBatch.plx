#!/usr/bin/perl
###########################################################################
#
#  Copyright 2014 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Andrew Perkins <andrew.perkins@owmessaging.com>
#  Date:        4Q/2014
#  Version:     1.0.0 - Sept 17, 2014
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                        # Locate this script
use lib "$FindBin::Bin/lib";        # Tell script where to find libraries
use lib "$FindBin::Bin/migperl";    # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use strict;
use warnings;
use Getopt::Long;
use JSON;
use XML::Simple;
use DateTime;
use Time::Local;
use POSIX qw(strftime);
use DBI;
use HTTP::Request::Common;
use HTTP::Response;
use File::Copy;
use DateTime;

use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;
use Log::Dispatch::FileRotate;

# A package that provides an easy way to have a config file
use Config::Simple;

# A package that provides an easy way to launch multiple threads, with returns.
use Parallel::Fork::BossWorker;

# A package that provides some status for our program
use Term::ProgressBar;

# Needed for LDAP search.
use Net::LDAP;
use Net::LDAP::Constant (qw/LDAP_SUCCESS/);

use com::owm::mosApi;
use com::owm::migBellCanada;
use com::owm::migMyInbox;
use com::owm::Mx8toMx9;
use com::owm::imapAppend;
use com::owm::CALClient;
use com::owm::PABClient;
use com::owm::RetrievePassword;
use com::owm::ManageMigrationStatus;
use com::owm::mgrConnection;

###########################################################################
# Global variables
###########################################################################

$| = 1;    ## Makes STDOUT flush immediately
my $SDEBUG;
my $MODE;
my $LOGMODE;
my $JOBID;
my $PROCESSID  = $$;
my $SCRIPTNAME = "migrateBatch";
my $LOGSTRING  = "SCRIPT|";

our $configPath = 'conf/migrator.conf';
our $configLog  = 'conf/log.conf';

# Varables for progress bar
my $progress;
my $count      = 0;
my $startcount = 0;

# Logging object.
#our $log;
Log::Log4perl->init($configLog);
our $log      = get_logger("MIGRATE");
our $mssLog   = get_logger("MSSLOG");
our $eventLog = get_logger("EVENT");
my $conflictLog = get_logger("PREP.CNFL");

# Boss Working object.
my $bw;

# Config object.
# And read the config file so all subs have access to params.
my $cfg = new Config::Simple();
$cfg->read($configPath)
  or $log->logdie( "SCRIPT|FATAL: " . $cfg->error() . ". Please fix the issue and restart the migration program. Exiting." );

# Flags for data to be migrated.
my $MAILBOX;
my $CONTACTS;
my $CALENDAR;
my $NAME;
my $USERLISTS;
my $FORWARDING;
my $VACATION;
my $SIGNATURE;
my $ACCOUNT;
my $DELETEUSER;
my $dataToBeMigrated;
my $firstUserStartSent;
my $dieKey = "SCRIPT";

my $INPUTFILE = '';

my $defaultCos = $cfg->param('defaultCos');
my $defaultPwd = $cfg->param('defaultPwd');

my $PRELOADMODE = $cfg->param('preloadMode');

my $mailtraceArchiveDir = $cfg->param('mailtraceArchiveDir');
my $archiveDir;
my $parseCounters_tool_cmd = $cfg->param('parseCounters_tool_cmd');

my $exitOnAccountFailure    = $cfg->param('exitOnAccountFailure');
my $exitOnSignatureFailure  = $cfg->param('exitOnSignatureFailure');
my $exitOnVacationFailure   = $cfg->param('exitOnVacationFailure');
my $exitOnForwardingFailure = $cfg->param('exitOnForwardingFailure');
my $exitOnUserListFailure   = $cfg->param('exitOnUserListFailure');
my $exitOnNameFailure       = $cfg->param('exitOnNameFailure');
my $exitOnContactsFailure   = $cfg->param('exitOnContactsFailure');
my $exitOnCalendarFailure   = $cfg->param('exitOnCalendarFailure');
my $exitOnDeleteUserFailure = $cfg->param('exitOnDeleteUserFailure');
my $exitOnConnectFail       = $cfg->param('exitOnConnectFail');

our $preloadMailBatchInParallel = lc( $cfg->param('preloadMailBatchInParallel') ) eq 'true';

# Signal flags
my $SIGNAL_CAUGHT = 0;
$SIG{__DIE__} = sub {
    $SIGNAL_CAUGHT = 1;
    my $message = shift;
    $log->warn( "$dieKey|Migration process has terminated, see trace log for more ..." . ( split /\n/, $message )[0] );

    #TODO: Add logic to allow continue to next record rather than exit out of the script.
    die $message;
};

$SIG{INT} = sub {
    $SIGNAL_CAUGHT = 1;
    $log->info( "SCRIPT|SCRIPT INTERRUPT SIGNAL HAS BEEN CAUGHT... script shutting down gracefully. Migration will complete for current users." );
};
$SIG{TERM} = sub {
    $SIGNAL_CAUGHT = 1;
    $log->info( "SCRIPT|SCRIPT INTERRUPT SIGNAL HAS BEEN CAUGHT... script shutting down gracefully. Migration will complete for current users." );
};

$log->info("SCRIPT|SCRIPT STARTING **********************");    #Log a starting entry for easier log file tracking

######################################################
# USAGE
######################################################
sub usage {

    print STDERR qq(

    Utility to migrated user's to Email Mx platform.

The script reads a list of primary keys, which identify a user to be migrated, and can migrate the user's data.
    The primary keys can be either PUID (for Microsoft migration) or EMAIL ADDRESS (for Mx8 migration)
    Mx8 Migration Elements
    - Mailbox
    - User Profile Data
    - Vacation Reply
    - Signature
    - Contacts

    Microsoft Only Migration Elements:
    - Mailbox
    - User Profile Data
    - User Lists (aka allow / block lists)
    - Mail Forwarding
    - Vacation Reply
    - Signature
    - Account (aka language and timezone)
    - Contacts (if consent is given)
    - Calendar (if consent is given)
    - First and Last Name (if consent is given)
    
    By default all the about data will be migrated if that data exists for the user and, where applicable,
    consent is given. Consent information is held in a consent DB, which this script queries to determine
    if a user has given consent.

    Command line to run script:

    migrateBatch.plx -INPUTFILE <INPUTFILE> [-mailbox] [-contacts] [-calendar] [-name]
    [-userlists] [-mailforwarding] [-vacation] [-signature] [-account] [-context]
    [-mode <mode>] [-jobid <jobid>]
    [-preload <true|false>]
    [-d]

where:
    <INPUTFILE>     is the path to a file containing the primary keys of accounts to be migrated.
    <mode>			is either MSFT or MX8
    <jobid>			is the unique identifier for each batch or migration job 

Switches:
    By default, the user is deleted (if it exists) and all data for an account is migrated. 
    Switches can by used to only migrate the data chosen.
    Common switches:
    -mailbox        migrate mailbox.
    -contacts       migrate contacts.
    -vacation       migrate vacation reply.
    -signature      migrate signature.
    -context        delete the user before migrating data.
    -preload		run this migration as preload migration.
    -d              turn debug on.
    Microsoft only switches: 
    -calendar       migrate calendar.
    -name           migrate name.
    -userlists      migrate userlists.
    -mailforwarding migrated mailforwarding.
    -account        migrate account.
    

    );

    exit(0);
}

############################################################
## - Progress Init -                                       #
##   Initialize our progress bar based on addrs read in    #
############################################################
sub progress_init($) {
    my $size = shift;
    $count = 0;
    $progress = Term::ProgressBar->new( { count => $size, term_width => '100', } );
    $progress->minor(0);
}

#############################################################################
# Create database connection to the preload table                           #
#############################################################################
sub connect_to_preload_state($$$$) {
    my ( $email, $puid, $cfg, $log ) = @_;

    my $driver   = $cfg->param('mysql_driver');
    my $database = $cfg->param('mysql_db1');
    my $host     = $cfg->param('mysql_host');
    my $dsn      = 'DBI:' . $driver . ':database=' . $database . ':host=' . $host;
    my $userid   = $cfg->param('mysql_user');
    my $password = $cfg->param('mysql_pass');

    $log->debug( $email . '|' . $puid . ':Connecting to preload_state database ... ' );

    my $dbh = DBI->connect( $dsn, $userid, $password );
    if ( !$dbh ) {
        $log->error( $email . '|' . $puid . ':Could not connect to preload_state ' . 'database ' . $DBI::errstr );
        return ( $com::owm::migBellCanada::STATUS_GENERAL_ERROR, undef );
    }
    return ( $com::owm::migBellCanada::STATUS_SUCCESS, $dbh );

}

#############################################################################
# Insert item into preload_state table                                      #
#############################################################################

sub query_preload_state($$$$$$) {
    my ( $dbh, $cfg, $log, $email, $puid, $item ) = @_;

    my ( $localpart, $domain ) = split( /@/, $email, 2 );
    my $sth = $dbh->prepare( 'SELECT `last_sync_epoch` FROM `preload_state` WHERE ' . '`domain`=? AND `localpart`=? AND `item`=?' );
    unless ( $sth->execute( $domain, $localpart, $item ) ) {
        $log->error( $email . '|' . $puid . ':Cannot query preload_state table: ' . $DBI::errstr );
        return ( $com::owm::migBellCanada::STATUS_GENERAL_ERROR, undef );
    }
    my $result = $sth->fetchrow_arrayref();

    if ($result) {
        $log->info( $email . '|' . $puid . ':last_sync value from preload_state ' . 'table for item "' . $item . '": ' . $result->[0] );
        return ( $com::owm::migBellCanada::STATUS_SUCCESS, $result->[0], );
    } else {
        $log->info( $email . '|' . $puid . ':No data found in preload_state table ' . 'for user and item "' . $item . '"' );
        return ( $com::owm::migBellCanada::STATUS_SUCCESS, undef );
    }
}

#############################################################################
# Using the config file and the information from the db, work out the start #
# and end dates and return them, or error out if there is a problem         #
#############################################################################

sub get_cal_start_end_dates($$$$$$$) {
    my ( $dbh, $cfg, $log, $email, $puid, $item, $PRELOADMODE ) = @_;

    # do this here to avoid too much duplicate error handling in the caller
    # (call connect_to_preload_state, handle error, call query_preload_state,
    #  handle error, etc)
    unless ( defined($dbh) ) {
        my $status;
        ( $status, $dbh ) = connect_to_preload_state( $email, $puid, $cfg, $log );
        unless ( $status == $com::owm::migBellCanada::STATUS_SUCCESS ) {
            return ( $status, undef, undef, undef );
        }
    }

    my ( $startDate, $status );
    my $endDate = $cfg->param('calendar_end_date');
    ( $status, $startDate ) = query_preload_state( $dbh, $cfg, $log, $email, $puid, $item );

    unless ( $status == $com::owm::migBellCanada::STATUS_SUCCESS ) {
        return ( $status, undef, undef, $dbh );
    }

    if ( $PRELOADMODE eq "true" ) {

        # Yesterday
        my $endDateDt = DateTime->now->subtract( days => 1 )->truncate( to => 'day' );
        $endDate = $endDateDt->strftime('%Y-%m-%d');

        $log->info( $email . '|' . $puid . ':Calendar preload mode.  Over-riding ' . 'configured end date and using yesterday (' . $endDate . ') instead' );

        if ( defined($startDate) ) {
            my $startDateDt = DateTime->from_epoch( epoch => $startDate )->truncate( to => 'day' );

            # Is the start date the same as or after the end date?
            # if it is, make it start one day before the end date
            # rather than  confusing things
            my $comparison = DateTime->compare( $startDateDt, $endDateDt );
            if ( ( $comparison == 0 ) || ( $comparison == 1 ) ) {
                $log->info( $email . '|' . $puid . ':Start date for ' . $item . ' migration is equal to or after the end date ' . '(' . $endDate . ').  Shifting to one day before ' . 'end date' );
                $startDateDt = $endDateDt->subtract( days => 1 );
            }

            $startDate = $startDateDt->strftime('%Y-%m-%d');
        }
    } elsif ( defined($startDate) ) {
        my $startDateDt = DateTime->from_epoch( epoch => $startDate )->truncate( to => 'day' );
        $startDate = $startDateDt->strftime('%Y-%m-%d');
    }

    # There was no previous attempt for this user, so use the
    # configured start date
    unless ( defined($startDate) ) {
        $startDate = $cfg->param('calendar_start_date');
        $log->info( $email . '|' . $puid . ':No previous ' . $item . ' preload migration' . ' for user, using configured start date of "' . $startDate . '"' );
    }

    # sanity check that start date <= end date
    my ( $year, $month, $day ) = split( /-/, $startDate, 3 );
    my $startDateDt = DateTime->new(
        year  => $year,
        month => $month,
        day   => $day
    )->truncate( to => 'day' );
    ( $year, $month, $day ) = split( /-/, $endDate, 3 );
    my $endDateDt = DateTime->new(
        year  => $year,
        month => $month,
        day   => $day
    )->truncate( to => 'day' );

    my $comparison = DateTime->compare( $startDateDt, $endDateDt );
    if ( $comparison == 1 ) {
        $log->error( $email . '|'
              . $puid
              . ':Calendar start/end date values do not '
              . 'make sense.  Start date: '
              . $startDate
              . ', end date: '
              . $endDate . '. '
              . 'Check configuration file values make sense for the current run '
              . 'mode for the migration and that the time on the migration server '
              . 'is correct' );
        return ( $com::owm::migBellCanada::STATUS_GENERAL_ERROR, undef, undef, $dbh );
    }

    return ( $com::owm::migBellCanada::STATUS_SUCCESS, $startDate, $endDate, $dbh );
}

#############################################################################
# Update a value in the preload_state table                                 #
#############################################################################

sub update_preload_state($$$$$$) {
    my ( $dbh, $log, $email, $puid, $item, $value ) = @_;

    my ( $localpart, $domain ) = split( /@/, $email, 2 );

    # cunning short cut that either inserts a new row if the
    # domain/localpart/item unique key doesn't exist yet, or if it does then
    # run the "ON DUPLICATE KEY" stanza which just updates the last_sync
    # column
    my $sth = $dbh->prepare( 'INSERT INTO `preload_state` (`domain`, ' . '`localpart`, `item`, `last_sync_epoch`) VALUES (?, ?, ?, ?) ON ' . 'DUPLICATE KEY UPDATE `last_sync_epoch`=?' );
    unless ( $sth->execute( $domain, $localpart, $item, $value, $value ) ) {
        $log->error( $email . '|' . $puid . ':Cannot update preload_state table: ' . $DBI::errstr );
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    $log->info( $email . '|' . $puid . ':Updated preload_state table for item "' . $item . '" with last_sync value "' . $value . '"' );

    return ($com::owm::migBellCanada::STATUS_SUCCESS);
}

#############################################################################
# Set a user on MX8 to proxy status if enabled in configuration             #
#############################################################################
sub set_mx8_proxy($$$$) {
    my ( $email, $puid, $cfg, $log ) = @_;

    my $mx8_proxy_user_host_smtp = $cfg->param('mx8_proxy_user_host_smtp');
    my $mx8_proxy_user_host_pop  = $cfg->param('mx8_proxy_user_host_pop');
    my $mx8_proxy_user_host_imap = $cfg->param('mx8_proxy_user_host_imap');
    my $mx_dircache_host_src     = $cfg->param('mx_dircache_host_src');
    my $mx_dircache_port_src     = $cfg->param('mx_dircache_port_src');
    my $mx_dircache_pwd_src      = $cfg->param('mx_dircache_pwd_src');

    unless ( defined($mx8_proxy_user_host_smtp)
        && ( length($mx8_proxy_user_host_smtp) > 0 ) )
    {
        $log->error( $email . '|' . $puid . ':mx8_proxy_user_host_smtp is not set ' . 'in configuration file - cannot set MX8 proxy status for user' );
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    unless ( defined($mx8_proxy_user_host_pop)
        && ( length($mx8_proxy_user_host_pop) > 0 ) )
    {
        $log->error( $email . '|' . $puid . ':mx8_proxy_user_host_pop is not set ' . 'in configuration file - cannot set MX8 proxy status for user' );
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    unless ( defined($mx8_proxy_user_host_imap)
        && ( length($mx8_proxy_user_host_imap) > 0 ) )
    {
        $log->error( $email . '|' . $puid . ':mx8_proxy_user_host_imap is not set ' . 'in configuration file - cannot set MX8 proxy status for user' );
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    unless ( defined($mx_dircache_host_src)
        && ( length($mx_dircache_host_src) > 0 ) )
    {
        $log->error( $email . '|' . $puid . ':mx_dircache_host_src is not set ' . 'in configuration file - cannot set MX8 proxy status for user' );
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    unless ( defined($mx_dircache_port_src)
        && ( length($mx_dircache_port_src) > 0 ) )
    {
        $log->error( $email . '|' . $puid . ':mx_dircache_port_src is not set ' . 'in configuration file - cannot set MX8 proxy status for user' );
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    unless ( defined($mx_dircache_pwd_src)
        && ( length($mx_dircache_pwd_src) > 0 ) )
    {
        $log->error( $email . '|' . $puid . ':mx_dircache_pwd_src is not set ' . 'in configuration file - cannot set MX8 proxy status for user' );
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    my $ldap = Net::LDAP->new( $mx_dircache_host_src, port => $mx_dircache_port_src );
    unless ($ldap) {
        $log->error( $email . '|' . $puid . ':connect to ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src . ' failed' );
        return ($com::owm::migBellCanada::STATUS_FAIL_PUID_LOOKUP);
    }

    my $mesg = $ldap->bind( 'cn=root', password => $mx_dircache_pwd_src );
    if ( $mesg->code != LDAP_SUCCESS ) {
        $log->error( $email . '|' . $puid . ':bind to ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src . ' failed. Check LDAP password .' );
        return ($com::owm::migBellCanada::STATUS_FAIL_PUID_LOOKUP);
    }

    my $searchString = '(mail=' . $email . ')';
    my $result = $ldap->search( base => '', filter => $searchString );

    if ( $result->code != LDAP_SUCCESS ) {
        $log->error( $email . '|' . $puid . ':Failure searching for user in ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src );
        return ($com::owm::migBellCanada::STATUS_FAIL_PUID_LOOKUP);
    }

    if ( $result->count == 0 ) {
        $log->error( $email . '|' . $puid . ':No matches searching for user in ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src );
        return ($com::owm::migBellCanada::STATUS_FAIL_PUID_LOOKUP);
    }

    if ( $result->count > 1 ) {
        $log->error( $email . '|' . $puid . ':Multiple matches searching for user ' . 'in ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src );
        return ($com::owm::migBellCanada::STATUS_FAIL_PUID_LOOKUP);
    }

    my @entries = $result->entries();
    my $entry   = $entries[0];

    if ( $entry->exists('mailboxstatus') ) {
        $entry->replace( 'mailboxstatus' => 'P' );
    } else {
        $entry->add( 'mailboxstatus' => 'P' );
    }

    if ( $entry->exists('mailsmtprelayhost') ) {
        $entry->replace( 'mailsmtprelayhost' => $mx8_proxy_user_host_smtp );
    } else {
        $entry->add( 'mailsmtprelayhost' => $mx8_proxy_user_host_smtp );
    }

    if ( $entry->exists('mailimapproxyhost') ) {
        $entry->replace( 'mailimapproxyhost' => $mx8_proxy_user_host_imap );
    } else {
        $entry->add( 'mailimapproxyhost' => $mx8_proxy_user_host_imap );
    }

    if ( $entry->exists('mailpopproxyhost') ) {
        $entry->replace( 'mailpopproxyhost' => $mx8_proxy_user_host_pop );
    } else {
        $entry->add( 'mailpopproxyhost' => $mx8_proxy_user_host_pop );
    }

    $mesg = $entry->update($ldap);

    if ( $result->code != LDAP_SUCCESS ) {
        $log->error( $email . '|' . $puid . ':Failure updating user with proxy ' . 'settings in ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src );

        # probably need a new status code here
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    return ($com::owm::migBellCanada::STATUS_SUCCESS);
}

###########################################################################
# setMx8MaintMode
#
# Set the mailboxstatus attribute on MX8 to the supplied value and
# return the current value
#
###########################################################################
sub setMx8MaintMode($$$$$) {
    my ( $email, $puid, $cfg, $log, $mailboxstatus ) = @_;
    my $mx_dircache_host_src = $cfg->param('mx_dircache_host_src');
    my $mx_dircache_port_src = $cfg->param('mx_dircache_port_src');
    my $mx_dircache_pwd_src  = $cfg->param('mx_dircache_pwd_src');

    unless ( defined($mx_dircache_host_src)
        && ( length($mx_dircache_host_src) > 0 ) )
    {
        $log->error( $email . '|' . $puid . ':mx_dircache_host_src is not set ' . 'in configuration file - cannot set MX8 proxy status for user' );
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    unless ( defined($mx_dircache_port_src)
        && ( length($mx_dircache_port_src) > 0 ) )
    {
        $log->error( $email . '|' . $puid . ':mx_dircache_port_src is not set ' . 'in configuration file - cannot set MX8 proxy status for user' );
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    unless ( defined($mx_dircache_pwd_src)
        && ( length($mx_dircache_pwd_src) > 0 ) )
    {
        $log->error( $email . '|' . $puid . ':mx_dircache_pwd_src is not set ' . 'in configuration file - cannot set MX8 proxy status for user' );
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    my $ldap = Net::LDAP->new( $mx_dircache_host_src, port => $mx_dircache_port_src );
    unless ($ldap) {
        $log->error( $email . '|' . $puid . ':connect to ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src . ' failed' );
        return ($com::owm::migBellCanada::STATUS_FAIL_PUID_LOOKUP);
    }

    my $mesg = $ldap->bind( 'cn=root', password => $mx_dircache_pwd_src );
    if ( $mesg->code != LDAP_SUCCESS ) {
        $log->error( $email . '|' . $puid . ':bind to ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src . ' failed. Check LDAP password .' );
        return ($com::owm::migBellCanada::STATUS_FAIL_PUID_LOOKUP);
    }

    my $searchString = '(mail=' . $email . ')';
    my $result = $ldap->search( base => '', filter => $searchString );

    if ( $result->code != LDAP_SUCCESS ) {
        $log->error( $email . '|' . $puid . ':Failure searching for user in ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src );
        return ($com::owm::migBellCanada::STATUS_FAIL_PUID_LOOKUP);
    }

    if ( $result->count == 0 ) {
        $log->error( $email . '|' . $puid . ':No matches searching for user in ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src );
        return ($com::owm::migBellCanada::STATUS_FAIL_PUID_LOOKUP);
    }

    if ( $result->count > 1 ) {
        $log->error( $email . '|' . $puid . ':Multiple matches searching for user ' . 'in ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src );
        return ($com::owm::migBellCanada::STATUS_FAIL_PUID_LOOKUP);
    }

    my @entries = $result->entries();
    my $entry   = $entries[0];

    my $oldMailboxstatus;
    if ( $entry->exists('mailboxstatus') ) {
        $oldMailboxstatus = $entry->get_value('mailboxstatus');
        $entry->replace( 'mailboxstatus' => $mailboxstatus );
    } else {
        # this should never happen, but the code should not pass an undef
        # value back up the stack.  "A"ctive is probably the best chance
        # of being the right status for the account.
        $oldMailboxstatus = 'A';
        $entry->add( 'mailboxstatus' => $mailboxstatus );
    }

    $mesg = $entry->update($ldap);

    if ( $result->code != LDAP_SUCCESS ) {
        $log->error( $email . '|' . $puid . ':Failure updating user with mailboxstatus="'.$mailboxstatus.'"' .
            'settings in ' . $mx_dircache_host_src . ':' . $mx_dircache_port_src );

        # probably need a new status code here
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR, $oldMailboxstatus);
    }

    return ($com::owm::migBellCanada::STATUS_SUCCESS, $oldMailboxstatus);
}

###########################################################################
# resetPabUser
###########################################################################
sub resetPabUser($$$$$$$$) {
    my $host                     = shift;
    my $port                     = shift;
    my $email                    = shift;
    my $puid                     = shift;
    my $log                      = shift;
    my $defaultCos               = shift;
    my $pabStorageLocation       = shift;
    my $cfg                      = shift;
    my $pab_telnetMasterPassword = $cfg->param('pab_telnetManagementPassword');
    my $result                   = '';
    ( my $user, my $domain ) = split( /\@/, $email );

    #
    # Telnet to host / port.
    #

    my $mgr = com::owm::mgrConnection->new(host => $host, port => $port, pass => $pab_telnetMasterPassword);
    if ($mgr->okError() !~ /^OK/) {
        $log->error( $email . '|' . $puid . ':Cannot connect/log in to PAB ' . 'server at ' . $host . ':' . $port . ' to delete the user: '.$mgr->okError() );
        undef($mgr);
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    #
    # Issue commands to erase and clear cache.
    #
    my $cmd = 'cache clear user ' . $email;
    $mgr->command($cmd);
    if ($mgr->okError() !~ /^OK/) {
        $log->error( $email . '|' . $puid . ':Cannot clear the PAB ' . 'user cache for the user on ' . $host . ':' . $port.': '.$mgr->okError() );
        undef($mgr);
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    $cmd = 'user ' . $domain . ' delete ' . $user . ' erase';
    $mgr->command($cmd);
    if ($mgr->okError() !~ /^OK/) {
        $log->error( $email . '|' . $puid . ':Cannot erase the PAB ' . 'user on ' . $host . ':' . $port.': '.$mgr->okError() );
        undef($mgr);
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    #
    # Recreate the user and on-disk storage location
    #

    $cmd = 'user ' . $domain . ' ADD ' . $user . ' PABLOCATION=' . $pabStorageLocation . ' CLASS=' . $defaultCos;
    $mgr->command($cmd);
    if ($mgr->okError() !~ /^OK/) {
        $log->error( $email . '|' . $puid . ':Cannot create the PAB ' . 'user on ' . $host . ':' . $port.': '.$mgr->okError()  );
        undef($mgr);
        return ($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }

    undef($mgr);
    return ($com::owm::migBellCanada::STATUS_SUCCESS);

}

######################################################################
## - worker -                                                       ##
######################################################################

sub worker {
    my $work = shift;
    my $dbh;
    my $status;
    my $mailboxstatus;
    my $migstatus;
    my $consentstatus;
    my $maillogin;
    my $email;
    my $usertz;
    my $mboxid;
    my $puid;
    my $errorCode;
    my $pwd;
    my $msftEmail;
    my $keyCommaDelimited;
    my $error;
    my $userpabstorehost;
    my $usercalstorehost;
    my $useradminpolicy;
    my $mailfrom;
    my $lookupKey;
    my $datafile;
    my %autoReplydata;
    my $mx8data;
    my $first_line = 0;
    my ( $subDir, $userProfileRoot, $subDirPath, $userProfileSignatureFile, $userProfileUserListsFile, $userProfileAccountFile, $userProfileMailForwardingFile, $userProfileVacationRepliesFile );
    my $myInboxObj;
    $dieKey = "SCRIPT";

    my $finalMigStatus   = '';
    my $finalProcessList = '';
    my $start            = time;

    my $time_ul;
    my $time_mb;
    my $time_ac;
    my $time_fw;
    my $time_va;
    my $time_si;
    my $time_ca;
    my $time_ab;
    my $time_na;
    my %run_times;

    my %pabCounters;
    my %calCounters;
    my %nameCounters;

    my $dbEmail;
    
    my $folders_json;
    my $folders_decoded;

    #initialise counters to -1 so that in the case none are found we don't get an uninitialised warning.
    #the calls to migBellCanada will return a fresh counter, so there is no required to do a +1 to these later for the count to be accurate.
    $pabCounters{'retrieved'}       = -1;
    $calCounters{'retrieved'}       = -1;
    $calCounters{'eventsRetrieved'} = -1;
    $nameCounters{'retrieved'}      = -1;    #this count is unused but needed as call returns three parameters

    #Strings for event logging
    my $finalEventId   = $com::owm::migBellCanada::EVENT_MIGRATION_RUN_END;
    my $finalEventName = 'MigrationRunEnd';
    my $finalEventMsg  = "";

    $dbh = com::owm::migBellCanada::connectEventDB( $eventLog, $cfg, $log );
    if ( $dbh eq $com::owm::migBellCanada::STATUS_GENERAL_ERROR ) {
        $log->error('SCRIPT|Database error, exiting');
        exit 0;
    } else {
        $log->debug('SCRIPT|Reconnected to event log database');
    }

    # Input file is list of primary keys (one per line) (PUID for MSFT, EMAIL for Mx8 and MyInbox)
    if ( $MODE eq "WL" ) {
        ( $puid, $email ) = split( /,/, $work->{data_line} );
        $dbEmail = $email;    #save the original email provided in the input file

        #Set up Microsoft required fields.
        $LOGSTRING         = "$email|$puid";
        $lookupKey         = $puid;
        $keyCommaDelimited = ",$puid,";
        chomp $puid;

# Set up where to file the user profile data files for this PUID.
# The user profile data files are pre-process by dumpSetup.plx, this script organizes the
# user profile data files base on the last 3 characters of the PUID.

        $subDir                         = substr( $puid, -3 );
        $userProfileRoot                = $cfg->param('userProfileRoot');
        $subDirPath                     = $userProfileRoot . $subDir . '/';
        $userProfileSignatureFile       = $subDirPath . 'Partner_Signatures';
        $userProfileUserListsFile       = $subDirPath . 'Partner_UserLists.ready';
        $userProfileAccountFile         = $subDirPath . 'Partner_Account';
        $userProfileMailForwardingFile  = $subDirPath . 'Partner_MailForwarding.ready';
        $userProfileVacationRepliesFile = $subDirPath . 'Partner_VacationReplies';

        #Do a check the userProfileRoot directory exists
        if ( -e $subDirPath ) {
            $log->debug("PUID|$puid: user profile directory exists. $subDirPath");
        } else {
            $log->debug("PUID|$puid: user profile directory does not exists. $subDirPath");
        }
    } elsif ( $MODE eq 'MYI' ) {
        $email = $work->{data_line};
        chomp $email;
        $LOGSTRING         = $email . '|MYI';
        $keyCommaDelimited = ',' . $email . ',';
        $lookupKey         = $email;

        # hardcoded PUID, won't have a real PUID for MyInbox users
        $puid = 'MYI';
        $myInboxObj = com::owm::migMyInbox->new( $email, $cfg, $log, $SDEBUG );

        # Only load the userid if it is necessary.  It is needed for most
        # settings, but not for calendar or address book.
        if (   ( $VACATION == 1 )
            || ( $FORWARDING == 1 )
            || ( $SIGNATURE == 1 )
            || ( $NAME == 1 )
            || ( $ACCOUNT == 1 ) )
        {
            my $status = $myInboxObj->getUserByUsername( $email, $cfg, $log );    # $email, $cfg, $log
            unless ( $status == $com::owm::migBellCanada::STATUS_SUCCESS ) {
                $finalEventId   = $com::owm::migBellCanada::STATUS_MIG_NOTSTARTED;
                $finalEventName = 'MigNotStarted';
                $finalEventMsg  = 'Failed to get numeric userid for MyInbox ' . 'user as getUserByUsername call failed';
                $log->error( $LOGSTRING . ':' . $finalEventMsg );
                goto END;
            }
        }
    } else {
        ( $email, $mboxid ) = split( /,/, $work->{data_line} );
        $LOGSTRING         = "$email|Mx8";
        $keyCommaDelimited = ",$email,";
        chomp $email;
        $userProfileRoot = $cfg->param('userProfileRootMx8');    #autoreplyexport files will be saved here
                                                                 #hardcoded PUID, won't have a real PUID for mx8 users
        $puid            = 'MX8';

        # get the autoReplyData from MX8
        # autoReplyData must have already been extracted and saved in the userProfileRoot directory
        $datafile  = $userProfileRoot . "/" . $email . ".txt";
        $lookupKey = $email;
        $dieKey    = $email;
    }

    if (defined($work->{first_line})) {
        $first_line = $work->{first_line};
    }

##################################################################
    # Has interrupt signal been caught? If so, finish migrating accounts
    # that are already being migrated and don't start migrating new ones,
    # but mark each with migstatus of 'i'.
##################################################################
    if ( $SIGNAL_CAUGHT eq '1' ) {

        # Sleep between 0 - 4 sec and then update migstatus and move on to the next account.
        # This is to help prevent spikes in LDAP connections when script is interrupted.
        my $sleepPeriod = rand(4);
        select( undef, undef, undef, $sleepPeriod );
        $log->info("$LOGSTRING:Migration interrupted, skip migrating this puid...");
        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'i' );    # Set migstatus to 'i' (mark account as interrupted)
        $finalEventId  = $com::owm::migBellCanada::STATUS_MIG_INTERRUPTED;
        $finalEventMsg = "Migration interrupted. Migration for this user skipped.";
        goto END;
    }

##################################################################
    # General setup needed for migration
##################################################################

    my $status_endpoint = $cfg->param('status_endpoint');
    my $status_wsdl     = $cfg->param('status_wsdl');
    my $status_log      = $cfg->param('status_log');

    $log->info("$LOGSTRING: Start processing this account ...");

    # Create mOS object.
    my $mosHost = $cfg->param('mosHost');
    my $mosPort = $cfg->param('mosPort');
    my $mos     = com::owm::mosApi->new( $mosHost, $mosPort );
    if ( !$mos->is_success ) {
        $status = $com::owm::migBellCanada::STATUS_FAIL_MOS_CONNECT;
        $log->error("$LOGSTRING:$status:Failed to create mOS object");
        $finalEventId  = $com::owm::migBellCanada::STATUS_MIG_NOTSTARTED;
        $finalEventMsg = "Failed to create mOS object.";
        goto END;
    }

    # Lookup account info using PUID.
    ( $status, $email, $mailboxstatus, $migstatus, $consentstatus, $maillogin, $userpabstorehost, $usercalstorehost, $useradminpolicy, $usertz, $mailfrom, my $mssvip ) = com::owm::migBellCanada::lookupPUID( $lookupKey, $cfg, $log );
    $LOGSTRING =~ s/PUID/$email/;
    $LOGSTRING .= ":JOB-$JOBID";
    print "$email is after mos object \n" if ($SDEBUG);
    $mos->user($email);

    # Set debug level for mOS object.
    $mos->debugOn if $SDEBUG;
    print "logstring: $LOGSTRING \n" if ($SDEBUG);

    #TODO add code here to call Telstra API to update status
    $dieKey = $email;
    if ($SDEBUG) {
        print "\nPUID=$puid, ->email=";
        print "$email" if ($email);
        print ", mailboxstatus=";
        print "$mailboxstatus" if ($mailboxstatus);
        print ", migstatus=";
        print "$migstatus" if ($migstatus);
        print ", maillogin=";
        print "$maillogin" if ($maillogin);
        print ", userpabstorehost=";
        print "$userpabstorehost" if ($userpabstorehost);
        print ", usercalstorehost=";
        print "$usercalstorehost" if ($usercalstorehost);
        print ", useradminpolicy=";
        print "$useradminpolicy" if ($useradminpolicy);
        print ", usertz=";
        print "$usertz" if ($usertz);
        print "\n";

    }

    if ( $status eq $com::owm::migBellCanada::STATUS_SUCCESS ) {

        #Allow migration only if the user is in PROXY or MAINTENANCE mode.
        if ( $cfg->param('onlyMigrateProxy') eq "true" && $mailboxstatus ne "P" && $mailboxstatus ne "M") {
            $status = $com::owm::migBellCanada::STATUS_FAIL_NOT_ALLOWED;
            $log->error("$LOGSTRING:$status: Migration could not be started as user is not in proxy mode");
            $finalEventId  = $com::owm::migBellCanada::STATUS_FAIL_NOT_ALLOWED;
            $finalEventMsg = "User is not in Proxy mode. Migration is not allowed.";
            goto END;
        }

        #If user is currently being migrated do not start migration for this user.
        if ( defined $migstatus && index( $migstatus, "Z" ) != -1 ) {
            $status = $com::owm::migBellCanada::STATUS_MIGRATION_IN_PROGRESS;
            $log->error("$LOGSTRING:$status: Migration could not be started as user is currently being migrated by another process");
            $finalEventId  = $com::owm::migBellCanada::STATUS_MIGRATION_IN_PROGRESS;
            $finalEventMsg = "User is currently being migrated.";
            goto ENDX;
        }
        $log->info("$LOGSTRING:Email is found mailbox");
        if ( $PRELOADMODE eq "true" || !$MAILBOX ) {
            #if mailbox is not included in this migration run, the start and end events shoudl be 102 and 103 respectively
            com::owm::migBellCanada::logEventsToDatabase( $dbh, $cfg, $log, $PROCESSID, $SCRIPTNAME, __LINE__, $JOBID, $com::owm::migBellCanada::EVENT_MIGRATION_RUN_START,
            'MigrationDataRunStart', $LOGMODE, $email, $puid, '', 'Start data migration for this user ' . $INPUTFILE );        
        } else {
            #if mailbox is included in this migration run, the start and end events shoudl be 100 and 101 respectively
            com::owm::migBellCanada::logEventsToDatabase( $dbh, $cfg, $log, $PROCESSID, $SCRIPTNAME, __LINE__, $JOBID, $com::owm::migBellCanada::EVENT_MIGRATION_START,
            'MigrationStart', $LOGMODE, $email, $puid, '', 'Start migration for this user ' . $INPUTFILE );
        }

        # call MSSol for status only if we are migrating the MAILBOX
        # and it is not the first user in the batch when the 
        # -firstUserStartSent CLI flag has been used.
        if ( $MAILBOX && $PRELOADMODE eq "false") {
            if (!$first_line) {
                if ( defined $status_endpoint ) {
                    my ( $statusRes, $statusError, $jobID ) =
                      com::owm::ManageMigrationStatus::ManageMigrationStatus( $status_endpoint, $status_wsdl, $email, $com::owm::migBellCanada::EVENT_MIGRATION_START, $JOBID, 'MigrationStart', $count, $startcount, $log );
                      $JOBID = $jobID;
                }
                if ( $status_log eq "true" ) {
                    $mssLog->info( "$LOGSTRING: UPDATING MSSOL HERE: $email, $com::owm::migBellCanada::EVENT_MIGRATION_START, $JOBID, MigrationStart" );
                }
            } else {
                $mssLog->info( "$LOGSTRING: NOT updating MSSOL here as START was calling from Watcher for this user: $email, $com::owm::migBellCanada::EVENT_MIGRATION_START, $JOBID, MigrationStart" );
            }
        }
    } else {
        $log->error("$LOGSTRING:$status:Failed to get account info using from Mx DIR");
        print("\n$LOGSTRING:$status:Failed to get account info using from Mx DIR") if ($SDEBUG);
        $finalEventId   = $com::owm::migBellCanada::STATUS_MIG_NOTSTARTED;
        $finalEventName = "MigNotStarted";
        $finalEventMsg  = "Failed to get account info using puid from Mx DIR.";
        $email          = $dbEmail if ( defined $dbEmail );                       #set the email to the WL batch file
        goto END;
    }

    my $mailloginOrig = $maillogin;
    print "Setting migration status \n " if ($SDEBUG);
    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'Z' );    # Set migstatus to 'Z' (to indicate migration is in progress)
    if ( $PRELOADMODE eq "true" ) {
        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'D' );    # Set migstatus to 'D' (to indicate preload/delta migration is in progress)
    }

##################################################################
    # Get password
##################################################################
    my $password_endpoint          = $cfg->param('password_endpoint');
    my $password_file              = $cfg->param('password_file');
    my $password_retry_count       = $cfg->param('password_retry_count');
    my $password_retrieval_by_file = $cfg->param('password_retrieval_by_file');
    my $keyFile                    = $cfg->param('key_file');
    my $http_username              = $cfg->param('http_username');
    my $http_password              = $cfg->param('http_password');
    ( $pwd, $msftEmail, my $pwdError ) = ( '', '', '1' );

    unless (defined($password_retry_count)) {
        $password_retry_count = 5;
    }
    if ( defined $password_endpoint || defined $password_file ) {
        if ( $password_retrieval_by_file eq "true" ) {
            ( $pwd, $pwdError ) = com::owm::RetrievePassword::retrievePasswordFromFile($password_file, $email, $log );    #clear text stub version
            $log->debug("$email|$puid: Getting password via flat file");
        } else {
            $log->debug("$email|$puid: Getting password via api");
            my $retryCount = 0;
            while ($pwdError != 0 and $retryCount < $password_retry_count) {
                eval {
                    local $SIG{__DIE__} = 'DEFAULT';
                    ( $pwd, $pwdError ) = com::owm::RetrievePassword::retrievePassword($JOBID, $password_endpoint, $email, $email, "OWMP", $log, $keyFile );
                };
                $retryCount ++;
                sleep($retryCount) if ($pwdError != 0);
            }
            if ( $pwdError eq 1 ) {

                #could not retrieve the password via the Telstra interface try the flat file
                if ( defined $password_file ) {
                    $log->debug("$email|$puid: API Failed, trying password via flat file");
                    ( $pwd, $pwdError ) = com::owm::RetrievePassword::retrievePasswordFromFile($password_file, $email, $log );
                }
            }
        }
    } else {
        # Retrieve decrypted password from MSFT
        ( $pwd, $msftEmail, $pwdError ) = com::owm::MicrosoftPwd::getMicrosoftPwd( $puid, $email );
        print "\ngetMicrosoftPwd\n" if ($SDEBUG);
    }

    if ( $pwdError eq 1 ) {
        $log->error("$LOGSTRING: $com::owm::migBellCanada::STATUS_FAIL_PASSWORD_RETRIEVAL:Error retrieving password.");
        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'x' );    # Set migstatus to 'x' (failed to get password)
        $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
        $finalEventMsg .= "Error retrieving password. ";
        goto END;

    } elsif ( $pwdError eq 2 ) {
        $log->error( "$LOGSTRING: $com::owm::migBellCanada::STATUS_FAIL_PUID_EMAIL_MISMATCH:Error during password retrieval, mismatch in email address (MSFT return string: $msftEmail)." );
        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'x' );    # Set migstatus to 'x' (failed to get password)
        $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
        $finalEventMsg .= "Error retrieving password, mismatch in email address. ";
        goto END;

    } else {

        # Continue.
        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'X' );    # Set migstatus to 'X' (successfully got password)
    }

##################################################################
    # Delete user before starting migration
##################################################################

    if ($DELETEUSER) {

        #this function should only be used to remove migrated data for assuming preload has been run previously.
        #Data should be removed just before it is migrated
        #If the user needs to be reset to their initial state, removing all migrated data including their mailbox - this must be done seperately using the resetUxUser proces


        my $result = deleteAccLdapAttributes( $email, $cfg, 'p', $MODE );

    }

##################################################################
    # Load user profile data.
##################################################################

    # Find and load user list data (if it exists)
    if ($USERLISTS) {
        $time_ul = time;
        if ($migstatus) {

            # For allow list...
            my $dataLine = `grep $keyCommaDelimited $userProfileUserListsFile 2>/dev/null | grep ",allowed="| head -1`;
            if ( $dataLine eq '' ) {
                $log->info("$LOGSTRING:No userlist allow data for $keyCommaDelimited in $userProfileUserListsFile");
            } else {
                chomp($dataLine);
                $log->debug("$LOGSTRING:userlist allow data:$dataLine") if ($SDEBUG);
                ( $status, $email ) = com::owm::migBellCanada::processUserLists( $email, $puid, $dataLine, $SDEBUG, $log, $mos, $cfg );
                $finalProcessList .= ",USERLISTS";
                if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                    $log->error("$LOGSTRING:$status:Failed to set allow userlist");
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'u' );    # Set migstatus to 'u' (failed userlist update)
                    $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                    if ( $exitOnUserListFailure eq "true" ) {
                        goto END;
                    } else {
                        $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                    }
                } else {
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'U' );    # Set migstatus to 'U' (successful userlist update)
                }
            }

            # For block list...
            $dataLine = `grep $keyCommaDelimited $userProfileUserListsFile 2>/dev/null | grep ",blocked="| head -1`;
            if ( $dataLine eq '' ) {
                $log->info("$LOGSTRING:No userlist block data for $keyCommaDelimited in $userProfileUserListsFile ");
            } else {
                chomp($dataLine);
                $log->debug("$LOGSTRING:userlist block data:$dataLine") if ($SDEBUG);
                ( $status, $email ) = com::owm::migBellCanada::processUserLists( $email, $puid, $dataLine, $SDEBUG, $log, $mos, $cfg );

                if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                    $log->error("$LOGSTRING:$status:Failed to set block userlist");
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'u' );    # Set migstatus to 'u' (failed userlist update)
                    $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                    if ( $exitOnUserListFailure eq "true" ) {
                        goto END;
                    } else {
                        $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                    }
                } else {
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'U' );    # Set migstatus to 'U' (success userlist update)
                }
            }
        }
        $time_ul = time - $time_ul;
        $run_times{"lists"} = $time_ul;
    }

    # Find and load account data (if it exists)
    if ($ACCOUNT) {
        $time_ac = time;
        if ( $MODE eq 'WL' ) {
            my $dataLine = `grep $keyCommaDelimited $userProfileAccountFile 2>/dev/null | head -1`;
            if ( $dataLine eq '' ) {
                $log->info("$LOGSTRING:No account data for $keyCommaDelimited in $userProfileAccountFile - using defaults");
                $dataLine = "$email,$puid,,";
            } else {
                chomp($dataLine);
            }
            $log->debug("$LOGSTRING:account:$dataLine") if ($SDEBUG);
            ( $status, $email, $usertz ) = com::owm::migBellCanada::processAccount( $email, $puid, $dataLine, $SDEBUG, $log, $mos, $cfg );
            print "\n  user usertz $usertz";
            $finalProcessList .= ",ACCOUNT";
            if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                $log->error("$LOGSTRING:$status:Failed to set account data");
                $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'p' );    # Set migstatus to 'p' (failed account update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                if ( $exitOnAccountFailure eq "true" ) {
                    goto END;
                } else {
                    $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                }
            } else {
                $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'P' );    # Set migstatus to 'P' (success account update)
            }
        } elsif ( $MODE eq 'MYI' ) {
            $finalProcessList .= ",ACCOUNT";
            $status = $myInboxObj->migrateUserConfiguration( $email, $cfg, $log, $mos );
            if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                $log->error( $LOGSTRING . $status . ':Failed to migrate account data.' );
                $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'p' );                 # Set migstatus to 'p' (failed account update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                $finalEventMsg .= 'Account data migration failed. ';
                if ( $exitOnAccountFailure eq 'true' ) {
                    goto END;
                } else {
                    $log->warn( $email . '|' . $puid . ':Migration continuing after failure as per configuration' );
                }
            } else {
                $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'P' );    # Set migstatus to 'P' (success account update)
            }
        } elsif ( $MODE eq 'MX8' ) {
            # remove existing external mail accounts from mos
            my $ext_acc_mos = $mos->readExternalAccounts($email);
            my @matches = ($ext_acc_mos =~ /accountId":"(\S+?)"/g);
            foreach (@matches) {
                $mos->deleteExternalAccount($email, $_);
            }
            # read external account data from MX8 and write to mos
            (my @external_mail_accounts) = com::owm::migBellCanada::lookupExternalMailAccounts($lookupKey, $cfg, $log);
            $mos->setExternalAccounts($email, @external_mail_accounts);
        } else {
            $log->error( $email . '|' . $puid . ':Unsupported migration mode "' . $MODE . '" in ACCOUNT code.  Skipping' );
        }

        if ( ( $MODE eq 'MX8' ) || ( $MODE eq 'WL' ) ) {
            my $disableClientTimezone = $cfg->param('disable_client_timezone');
            if ( defined($disableClientTimezone)
                && ( lc($disableClientTimezone) eq 'true' ) )
            {
                my @args = ( $FindBin::Bin . '/turn-off-client-tz.py', '--email=' . $email, '--mosHost=' . $cfg->param('mosHost'), '--mosPort=' . $cfg->param('mosPort') );
                my ( $ok, $statistics, $logStats ) = com::owm::migMyInbox::runPythonChild( $email, $puid, $log, $cfg, 'disable client TZ', \@args );
            }
        }

        $time_ac = time - $time_ac;
        $run_times{"acct"} = $time_ac;
    }

    # Find and load mail forwarding data (if it exists)
    if ($FORWARDING) {
        $time_fw = time;
        if ( $MODE eq 'WL' ) {
            my $dataLine = `grep $keyCommaDelimited $userProfileMailForwardingFile 2>/dev/null | head -1`;
            if ( $dataLine eq '' ) {
                $log->info("$LOGSTRING:No mail forwarding data for $keyCommaDelimited in $userProfileMailForwardingFile");
            } else {
                chomp($dataLine);
                $log->debug("$LOGSTRING:mail forwarding:$dataLine") if ($SDEBUG);
                ( $status, $email ) = com::owm::migBellCanada::processMailForward( $email, $puid, $dataLine, $SDEBUG, $log, $mos, $cfg );
                $finalProcessList .= ",FORWARDING";
                if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                    $log->error("$LOGSTRING:$status:Failed to set mail forwarding data");
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'f' );    # Set migstatus to 'f' (failed mail forwarding update)
                    $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                    if ( $exitOnForwardingFailure eq "true" ) {
                        goto END;
                    } else {
                        $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                    }
                } else {
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'F' );    # Set migstatus to 'F' (success mail forwarding update)
                }
            }
        } elsif ( $MODE eq 'MYI' ) {
            $status = $myInboxObj->migrateMailForward( $email, $cfg, $log, $mos );
            $finalProcessList .= ',FORWARDING';
            if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                $log->error( $LOGSTRING . $status . ':Failed to set mail forwarding data' );
                $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'f' );        # Set migstatus to 'f' (failed mail forwarding update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                if ( $exitOnForwardingFailure eq "true" ) {
                    goto END;
                } else {
                    $log->warn( $LOGSTRING . 'Migration continuing after failure as per configuration' );
                }
            } else {
                $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'F' );        # Set migstatus to 'F' (success mail forwarding update)
            }
        } else {
            $log->error( $email . '|' . $puid . ':Unsupported migration mode "' . $MODE . '" in FORWARDING code.  Skipping' );
        }
        $time_fw = time - $time_fw;
        $run_times{"fwd"} = $time_fw;
    }

    # Find and load vacation data (if it exists)
    if ( $MODE eq "MX8" ) {
        $mx8data = com::owm::Mx8toMx9::getAutoReplyData( $datafile, $email, $log );
        %autoReplydata = %$mx8data;
    }

    if ($VACATION) {
        $time_va = time;
        my $vacationRun = "false";
        if ( $MODE eq "MX8" ) {
            if ( $autoReplydata{'99'} ne "none" && defined $autoReplydata{'1'} ) {
                ( $status, $email ) = com::owm::Mx8toMx9::processVacationMsg( $email, $autoReplydata{'1'}, $mos, $cfg, $log );
                $vacationRun = "true";
            }
        } elsif ( $MODE eq 'MYI' ) {
            $status = $myInboxObj->migrateAutoreply( $email, $cfg, $log, $mos );
            $vacationRun = 'true';
        } else {
            my $dataLine = `grep $keyCommaDelimited $userProfileVacationRepliesFile 2>/dev/null | head -1`;
            if ( $dataLine eq '' ) {
                $log->info("$LOGSTRING:No vacation data for $keyCommaDelimited in $userProfileVacationRepliesFile");
            } else {
                chomp($dataLine);
                $log->debug("$LOGSTRING:vacation:$dataLine") if ($SDEBUG);
                ( $status, $email ) = com::owm::migBellCanada::processVacation( $email, $puid, $dataLine, $SDEBUG, $log, $mos, $cfg );
                $vacationRun = "true";
            }
        }

        if ( $vacationRun eq "true" ) {
            $finalProcessList .= ",VACATION";
            if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                $log->error("$LOGSTRING:$status:Failed to set vacation data");
                $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'v' );    # Set migstatus to 'v' (failed vacation update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                if ( $exitOnVacationFailure eq "true" ) {
                    goto END;
                } else {
                    $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                }
            } else {
                $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'V' );    # Set migstatus to 'V' (success vacation update)
            }
        }
        $time_va = time - $time_va;
        $run_times{"vac"} = $time_va;
    }

    # Find and load signature data (if it exists)
    if ($SIGNATURE) {
        $time_si = time;
        my $setVia    = $cfg->param('signature_set_via');
        my $mosTarget = $cfg->param('signature_target');                                                                               # target should be 'mss' or 'appsuite'
                                                                                                                                       # doesn't work for appsuite
        if ( ( $setVia eq 'mos' ) && ( $mosTarget eq 'mss' ) ) {
            my $sigIds    = $mos->signatureList($mosTarget);
            my $logprefix = $email . '|' . $puid . ':';
            unless ( $mos->is_success ) {
                my $error = $mos->{ERROR};
                chomp($error);
                $log->warn( $logprefix . 'Failed to list signatures, unable to delete any previous signatures if they exist: ' . $error );

                #return($com::owm::migTelstraEvents::STATUS_FAIL_MOS_SETTING); ## this error should not cause the migration to fail.
            }

            if ( defined($sigIds) ) {
                my @sigIds = @{$sigIds};
                if ( scalar(@sigIds) > 0 ) {
                    $log->info( $logprefix . 'Found ' . scalar(@sigIds) . ' old signatures to delete' );
                } else {
                    $log->info( $logprefix . 'Found no old signatures to delete' );
                }

                foreach my $sigId (@sigIds) {
                    my $retval = $mos->signatureDelete( $mosTarget, $sigId );
                    if ( $mos->is_success ) {
                        $log->info( $logprefix . 'Successfully deleted signature ID ' . $sigId );
                    } else {
                        my $error = $mos->{ERROR};
                        chomp($error);
                        $log->error( $logprefix . 'Failed to delete signature ' . $sigId . ': ' . $error );
                    }
                }
            } else {
                $log->info( $logprefix . 'Found no old signatures to delete' );
            }
        }
        my $signatureRun = "false";
        if ( $MODE eq "MX8" ) {
            if ( $autoReplydata{'99'} ne "none" && defined $autoReplydata{'2'} ) {
                ( $status, $email ) = com::owm::Mx8toMx9::processSignature( $email, $autoReplydata{'2'}, $mos, $cfg, $log );
                $signatureRun = "true";
            }
        } elsif ( $MODE eq 'MYI' ) {
            $status = $myInboxObj->migrateSignature( $email, $cfg, $log, $mos );
            $signatureRun = 'true';
        } else {
            my $dataLine = `grep $keyCommaDelimited $userProfileSignatureFile 2>/dev/null | head -1`;
            if ( $dataLine eq '' ) {
                $log->info("$LOGSTRING:No signature data for $keyCommaDelimited in $userProfileSignatureFile");
            } else {
                chomp($dataLine);
                $log->debug("$LOGSTRING:signature:$dataLine") if ($SDEBUG);
                chomp($dataLine);
                ( $status, $email ) = com::owm::migBellCanada::processSignature( $email, $puid, $dataLine, $SDEBUG, $log, $mos, $cfg, $pwd, $conflictLog );
                $signatureRun = "true";
            }
        }
        if ( $signatureRun eq "true" ) {
            $finalProcessList .= ",SIGNATURE";
            if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                $log->error("$LOGSTRING:$status:Failed to set signature data");
                $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 's' );    # Set migstatus to 's' (failed signature update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                if ( $cfg->param('exitOnSignatureFailure') eq "true" ) {
                    goto END;
                } else {
                    $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                }
            } else {
                $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'S' );    # Set migstatus to 'S' (success signature update)
            }
        }
        $time_si = time - $time_si;
        $run_times{"sig"} = $time_si;
    }

##################################################################
    # Migrated contacts, calendar and name.
##################################################################

#Delete the address book if previously migrated

    if ($CONTACTS && $DELETEUSER) {
            $status = $mos->getUserBase($email);

            unless ( $mos->is_success() ) {
                $log->error( $email . '|' . $puid . ':Error reading user data from mOS: ' . $mos->{ERROR} );
                $status       = $com::owm::migBellCanada::STATUS_FAIL_MOS_CONNECT;
                $finalEventId = $com::owm::migBellCanada::STATUS_ABORT_ERROR;
                $finalEventMsg .= 'Error while reading user metadata at start of migration. ';
                goto END;
            }

            #Delete the PAB entries
            my $storageLocation = $mos->getValue('addressBookStorageLocation');
            unless ( defined($storageLocation) && ( length($storageLocation) > 0 ) ) {
                $log->error( $email . '|' . $puid . ':Error reading user data from mOS: no PAB storage location found' );
                $status       = $com::owm::migBellCanada::STATUS_FAIL_MOS_CONNECT;
                $finalEventId = $com::owm::migBellCanada::STATUS_ABORT_ERROR;
                $finalEventMsg .= 'Error while reading user PAB location at start of migration. ';
                goto END;
            }
            my ( $port, $error ) = com::owm::migBellCanada::getPortNumber( 'telnet', 'pab', $userpabstorehost, $log, $cfg );
            $status = resetPabUser( $userpabstorehost, $port, $email, $puid, $log, $defaultCos, $storageLocation, $cfg )
              if ( defined $userpabstorehost and $error == 0 );

            #TODO: Handle the case if getPortNumber errors
            if ( $status == $com::owm::migBellCanada::STATUS_GENERAL_ERROR ) {
                $log->error( $email . '|' . $puid . ':Error clearing address book for user' );
                $finalEventId = $com::owm::migBellCanada::STATUS_ABORT_ERROR;
                $finalEventMsg .= 'Error while clearing PAB at start of migration. ';
                goto END;
            } else {
                $log->info( $email . '|' . $puid . ':Cleared user from PAB server' );
            }

    }
    
    if ( $MODE eq "WL" ) {

        # For calendar, contacts and name, there is comment processing related to consent token. For
        # this reason, there is one subroutine that processes all 3 data types.
        my $do_cal = 0;    # 0=no, 1=yes
        my $do_con = 0;    # 0=no, 1=yes
        my $do_nam = 0;    # 0=no, 1=yes

        if ($CALENDAR) { $do_cal = 1; }
        if ($CONTACTS) { $do_con = 1; }
        if ($NAME)     { $do_nam = 1; }

        if ( ($CALENDAR) || ($CONTACTS) || ($NAME) ) {

            # Before the access token can be retrieved via Microsoft API, first need to get the refresh token.
            my $refreshToken = com::owm::migBellCanada::getRefreshToken( $puid, $email, $cfg, $log );

            #check temporary folder exists for storing calendar and address books vfiles
            if ( !-e "tmp/" ) {
                my $err = mkdir("tmp/");
                if ( $err != 1 ) {
                    $log->error("$LOGSTRING: couldn't create temporary dir for contact and calendar migration. Stopping migration. \n");
                    $finalEventId  = $com::owm::migBellCanada::STATUS_ABORT_ERROR;
                    $finalEventMsg = "Couldn't create temporary dir for contact and calendar migration.";
                    goto END;
                }
            }
            if ( !defined $refreshToken ) {

                # Failed to retrieve the token.
                $log->warn("$LOGSTRING:Fail to get refresh token. Skip to next user.");
                $status        = $com::owm::migBellCanada::STATUS_FAIL_NO_CONSENT;
                $finalEventId  = $com::owm::migBellCanada::STATUS_ABORT_ERROR;
                $finalEventMsg = "Failed to get refresh token.";
                $log->error("$LOGSTRING:$status:Fail to get refresh token.");

                # Update migstatus.
                if ($CALENDAR) { com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'c' ); }
                if ($CONTACTS) { com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'a' ); }
                if ($NAME) {
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'n' );
                }
                goto END;

            } elsif ( $refreshToken eq '' ) {

                # No refresh token for this account.
                $log->info("$LOGSTRING:No consent given.");

                # Update migstatus.
                if ($CALENDAR) {
                    $log->info( "$LOGSTRING:Calendar migration not attempted and migstatus for calendar NOT set, as consent was not given." );
                }
                if ($CONTACTS) {
                    $log->info( "$LOGSTRING:Contacts migration not attempted and migstatus for contacts NOT set, as consent was not given." );
                }
                if ($NAME) {
                    $log->info("$LOGSTRING:Name migration not attempted and migstatus for name NOT set, as consent was not given.");
                }

            } else {

                # Retrieve refresh token for this account, now get access token.
                my $access_token;
                if ( $access_token = com::owm::migBellCanada::getAccessToken( $puid, $email, $cfg, $log, $refreshToken ) ) {
                    $log->info("$LOGSTRING:Got access_token.");
                } else {
                    $log->warn("$LOGSTRING:Fail to get access_token. Can't continue to process this user. Skip to next user.");
                    $status        = $com::owm::migBellCanada::STATUS_FAIL_ACCESS_TOKEN;
                    $finalEventId  = $com::owm::migBellCanada::STATUS_ABORT_ERROR;
                    $finalEventMsg = "Fail to get access_token. ";
                    $log->error("$LOGSTRING:$status:Fail to get access_token.");

                    # Update migstatus.
                    if ($CALENDAR) {
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'c' );
                    }
                    if ($CONTACTS) {
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'a' );
                    }
                    if ($NAME) {
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'n' );
                    }
                    goto END;
                }

                if ($CALENDAR) {
                    my $preloadStateDbh;

                    #check if a host is defined if not skip
                    if ( !$usercalstorehost ) {
                        $log->error("$LOGSTRING: No calstorehost defined in LDAP. Cannot migrate calendar.");
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'c' );    # Set migstatus to 'c' (failed calendar update)
                        $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                        $finalEventMsg .= "Calendar Failed - No calstorehost defined in LDAP.";
                        if ( $exitOnCalendarFailure eq "true" ) {
                            goto END;
                        } else {
                            $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                            goto CALCONT;
                        }
                    }
                    $time_ca = time;

                    my $preloadItem = 'calendar';
                    my ( $startDate, $endDate );
                    ( $status, $startDate, $endDate, $preloadStateDbh ) = get_cal_start_end_dates( $preloadStateDbh, $cfg, $log, $email, $puid, $preloadItem, $PRELOADMODE );

                    unless ( $status == $com::owm::migBellCanada::STATUS_SUCCESS ) {
                        $preloadStateDbh->disconnect;
                        undef($preloadStateDbh);
                        $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'c' );    # Set migstatus to 'c' (failed calendar update)
                        $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                        $finalEventMsg .= 'Calendar migration failed. ';
                        if ( $exitOnCalendarFailure eq 'true' ) {
                            goto END;
                        } else {
                            $log->warn( $email . '|' . $puid . ':Migration continuing after failure as per configuration' );
                            goto CALCONT;
                        }
                    }

                    $log->info( $email . '|' . $puid . ':Migrating calendar from start date "' . $startDate . '" to end date "' . $endDate . '"' );
                    ( $status, $email, %calCounters ) =
                      com::owm::migBellCanada::processCalendarContacts( $puid, $email, $SDEBUG, $log, $conflictLog, $mos, $cfg, 1, 0, 0, $pwd, $access_token, $usercalstorehost, $userpabstorehost, $usertz, $startDate, $endDate, $PRELOADMODE );
                    $finalProcessList .= ",CALENDAR";
                    if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                        $log->error("$LOGSTRING:$status:Failed migrated calendar.");
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'c' );    # Set migstatus to 'c' (failed calendar update)
                        $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                        $finalEventMsg .= "Calendar migration failed. ";
                        if ( $exitOnCalendarFailure eq "true" ) {
                            goto END;
                        } else {
                            $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                        }
                    } else {
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'C' );    # Set migstatus to 'C' (success calendar update)
                        $log->debug( $email . '|' . $puid . ': about to update preload state' );
                        update_preload_state( $preloadStateDbh, $log, $email, $puid, $preloadItem, $time_ca );
                    }
                    $preloadStateDbh->disconnect;
                    undef($preloadStateDbh);
                    $time_ca = time - $time_ca;
                    $run_times{"cal"} = $time_ca;
                }
              CALCONT:
                if ($CONTACTS) {
                    if ( !$userpabstorehost ) {
                        $log->error("$LOGSTRING: No pabstorehost defined in LDAP. Cannot migrate contacts or name.");
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'a' );    # Set migstatus to 'a' (failed contacts update)
                        $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                        $finalEventMsg .= "AB Failed. No pabstorehost defined in LDAP. ";
                        if ( $exitOnContactsFailure eq "true" ) {
                            goto END;
                        } else {
                            $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                            goto PABCONT;
                        }
                    }
                    $time_ab = time;
                    ( $status, $email, %pabCounters ) = com::owm::migBellCanada::processCalendarContacts( $puid, $email, $SDEBUG, $log, $conflictLog, $mos, $cfg, 0, 1, 0, $pwd, $access_token, $usercalstorehost, $userpabstorehost );
                    $finalProcessList .= ",CONTACTS";

                    if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                        $log->error("$LOGSTRING:$status:Failed migrated contacts.");
                        $migstatus = $finalMigStatus =
                          com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'a' );    # Set migstatus to 'a' (failed contacts update)
                        $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                        $finalEventMsg .= "AB Failed. ";
                        if ( $exitOnContactsFailure eq "true" ) {
                            goto END;
                        } else {
                            $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                        }
                    } else {
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'A' );    # Set migstatus to 'A' (success contacts update)
                    }
                    $time_ab = time - $time_ab;
                    $run_times{"ab"} = $time_ab;
                }

                if ($NAME) {
                    if ( !$userpabstorehost ) {
                        $log->error("$LOGSTRING: No pabstorehost defined in LDAP. Cannot migrate name.");
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'a' );    # Set migstatus to 'a' (failed contacts update)
                        $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                        $finalEventMsg .= "Name Failed. No pabstorehost defined in LDAP. ";
                        goto END;
                    }
                    $time_na = time;
                    ( $status, $email, %nameCounters ) = com::owm::migBellCanada::processCalendarContacts( $puid, $email, $SDEBUG, $log, $conflictLog, $mos, $cfg, 0, 0, 1, $pwd, $access_token, $usercalstorehost, $userpabstorehost );
                    $finalProcessList .= ",NAME";

                    if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                        $log->error("$LOGSTRING:$status:Failed migrated names");
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'n' );    # Set migstatus to 'n' (failed name update)
                        $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                        $finalEventMsg .= "Name Failed. ";
                        if ( $exitOnNameFailure eq "true" ) {
                            goto END;
                        } else {
                            $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                        }
                    } else {
                        $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'N' );    # Set migstatus to 'N' (success name update)
                    }
                    $time_na = time - $time_na;
                    $run_times{"name"} = $time_na;
                }
              PABCONT:
            }
        }
    }

    if ( $MODE eq "MX8" ) {
        if ($NAME) {
            $time_na = time;
            my $status = com::owm::migBellCanada::setMx8Name( $email, $mailfrom, $cfg, $log );
            $time_na = time - $time_na;
            $run_times{"name"} = $time_na;

        }
        if ( $autoReplydata{'99'} ne "none" && defined $autoReplydata{'7'} ) {    #only process contacts if we have an autoreplyfile

            if ($CONTACTS) {
                if ( !$userpabstorehost ) {
                    $log->error("$LOGSTRING: No pabstorehost defined in LDAP. Cannot migrate contacts.");
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'a' );    # Set migstatus to 'a' (failed contacts update)
                    $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                    $finalEventMsg .= "AB Failed. No pabstorehost defined in LDAP. ";
                    goto END;
                }
                $time_ab = time;
                ( $status, $email, %pabCounters ) = com::owm::Mx8toMx9::processContacts( $email, $pwd, $autoReplydata{'7'}, $usercalstorehost, $userpabstorehost, $cfg, $log );
                $finalProcessList .= ",CONTACTS";
                if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                    $log->error("$LOGSTRING:$status:Failed to migrate addressbook");
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'a' );    # Set migstatus to 'a' (failed contacts update)
                    $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                    $finalEventMsg .= "AB Failed. ";
                    if ( $exitOnContactsFailure eq "true" ) {
                        goto END;
                    } else {
                        $log->warn("$email: Migration continuing after failure as per configuration");
                    }
                } else {
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'A' );    # Set migstatus to 'N' (success name update)
                }
                $time_ab = time - $time_ab;
                $run_times{"ab"} = $time_ab;
            }
        }
    }
    if ( $MODE eq 'MYI' ) {
        if ($CONTACTS) {

            #check if a host is defined if not skip
            if ( !$userpabstorehost ) {
                $log->error( $email . '|' . $puid . ':No pabstorehost defined in LDAP. Cannot migrate contacts.' );
                $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'a' );    # Set migstatus to 'a' (failed contact update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                $finalEventMsg .= 'AB Failed. No pabstorehost defined in LDAP.';
                if ( $cfg->param('exitOnContactsFailure') eq 'true' ) {
                    goto END;
                } else {
                    $log->warn( $email . '|' . $puid . ':Migration continuing after failure as per configuration' );
                    goto CALCONT;
                }
            }
            $time_ab = time;
            ( $status, $email, %pabCounters ) = $myInboxObj->migrateContacts( $log, $mos, $cfg, $email, $pwd, $userpabstorehost, $SDEBUG );
            $finalProcessList .= ",CONTACTS";
            if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                $log->error( $email . '|' . $puid . ':' . $status . ':Failed to migrate addressbook.' );
                $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'a' );    # Set migstatus to 'a' (failed contact update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                $finalEventMsg .= 'Addressbook migration failed. ';
                if ( $exitOnContactsFailure eq "true" ) {
                    goto END;
                } else {
                    $log->warn( $email . '|' . $puid . ':Migration continuing after failure as per configuration' );
                }
            } else {
                $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'A' );    # Set migstatus to 'A' (successful contact update)
            }
            $time_ab = time - $time_ab;
            $run_times{'ab'} = $time_ab;
        }

        if ($CALENDAR) {
            my $preloadStateDbh;

            #check if a host is defined if not skip
            if ( !$usercalstorehost ) {
                $log->error("$LOGSTRING: No calstorehost defined in LDAP. Cannot migrate calendar.");
                $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'c' );    # Set migstatus to 'c' (failed calendar update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                $finalEventMsg .= "Calendar Failed - No calstorehost defined in LDAP.";
                if ( $exitOnCalendarFailure eq "true" ) {
                    goto END;
                } else {
                    $log->warn("$LOGSTRING: Migration continuing after failure as per configuration");
                    goto CALCONT;
                }
            }
            $time_ca = time;
            $finalProcessList .= ",CALENDAR";
            my $preloadItem = 'calendar';

            my ( $startDate, $endDate );
            ( $status, $startDate, $endDate, $preloadStateDbh ) = get_cal_start_end_dates( $preloadStateDbh, $cfg, $log, $email, $puid, $preloadItem, $PRELOADMODE );

            unless ( $status == $com::owm::migBellCanada::STATUS_SUCCESS ) {
                $preloadStateDbh->disconnect;
                undef($preloadStateDbh);
                $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'c' );    # Set migstatus to 'c' (failed calendar update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                $finalEventMsg .= 'Calendar migration failed. ';
                if ( $exitOnCalendarFailure eq "true" ) {
                    goto END;
                } else {
                    $log->warn( $email . '|' . $puid . ':Migration continuing after failure as per configuration' );
                    goto CALCONT;
                }
            }

            $log->info( $email . '|' . $puid . ':Migrating calendar from start date "' . $startDate . '" to end date "' . $endDate . '"' );
            ( $status, $email, %calCounters ) = $myInboxObj->migrateCalendar( $log, $mos, $cfg, $email, $pwd, $usercalstorehost, $startDate, $endDate, $SDEBUG );

            if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                $log->error( $email . '|' . $puid . ':' . $status . ':Failed migrated calendar.' );
                $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'c' );    # Set migstatus to 'c' (failed calendar update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                $finalEventMsg .= 'Calendar migration failed. ';
                if ( $cfg->param('exitOnCalendarFailure') eq 'true' ) {
                    goto END;
                } else {
                    $log->warn( $email . '|' . $puid . ': Migration continuing after failure as per configuration' );
                }
            } else {
                $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'C' );    # Set migstatus to 'C' (successful calendar update)
                                                                                                                          # onload update preload table on successful migration
                update_preload_state( $preloadStateDbh, $log, $email, $puid, $preloadItem, $time_ca );
            }
            $preloadStateDbh->disconnect;
            undef($preloadStateDbh);
            $time_ca = time - $time_ca;
            $run_times{'cal'} = $time_ca;
        }

        if ($NAME) {
            $status = $myInboxObj->migrateName( $email, $cfg, $log, $mos );
            if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'n' );    # Set migstatus to 'n' (failed name update)
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                $finalEventMsg .= 'First/last name migration failed. ';
                if ( $cfg->param('exitOnNameFailure') eq 'true' ) {
                    goto END;
                } else {
                    $log->warn( $email . '|' . $puid . ': Migration continuing after failure as per configuration' );
                }
            } else {
                $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'N' );    # Set migstatus to 'N' (success name update)
            }

        }

      CALCONT:
    }

    #Set migration event to migration run completed.
    if ( $finalEventId == $com::owm::migBellCanada::STATUS_GENERAL_ERROR ) {

        #then errors have occurred but the migration had continued to run as per configuration
    } else {
        $finalEventId = $com::owm::migBellCanada::EVENT_MIGRATION_RUN_END;
    }
##################################################################
    # Migrate mailbox
##################################################################

    if ($MAILBOX) {
        unless ( $preloadMailBatchInParallel && ( $PRELOADMODE eq "true" ) ) {
            if ( $mailboxstatus eq 'P' ) {

                $log->info("$LOGSTRING:In proxy mode. mailboxstatus = $mailboxstatus");

                # Set account's 'maillogin' LDAP attribute to email address
                if ( $cfg->param('resetMaillogin') eq "true" ) {
                    $status = com::owm::migBellCanada::setMaillogin( $email, $puid, $cfg, $log, $email );
                    if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                        $log->warn("$LOGSTRING:Was not able to set maillogin to '$email' before mailbox migration");
                        $log->error("$LOGSTRING:$status:Failed migrated mailbox");
                        $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                        $finalEventMsg .= "Mailbox Failed. Was not able to set maillogin. ";
                        goto END;
                    }
                }
                $time_mb = time;

                # Read sum of messages in all folders and log for processing later
                $folders_json = $mos->getMailFolders($email);
                $folders_decoded = decode_json($folders_json);
                # Hash means folders could not be read
                if (ref($folders_decoded) eq "HASH") {
                    $log->error($LOGSTRING.': Folders could not be read. Migration counter not available!');
                } else {
                    my $counter_before = 0;
                    foreach my $folder (@$folders_decoded) {
                        $counter_before = $counter_before + $folder->{'numMessages'};
                    }
                    $log->info($LOGSTRING.': Number of Messages on target before mail migration:'.$counter_before);

                    my $query = $dbh->prepare("INSERT INTO counters (job_id, email, counter_name, total, size) VALUES (?, ?, ?, ?, ?)");
                    $query->execute($JOBID, $email, 'messagesOnTargetBefore', $counter_before, undef) or $log->error($LOGSTRING.': Cannot insert messagesOnTargetBefore counter: '.$query->err_str);
                    $log->info("$LOGSTRING: Updated messagesOnTargetBefore counter in database");
                }

                # Call Migration Tool
                # Preload has three runs
                # 1. preload or delta migration
                # 2. final migration - previous deltas have been run, but this time on success set the user to ACTIVE
                # 3. single one time migration - no previous deltas have or will be run

                com::owm::migBellCanada::disconnectDB( $dbh, $cfg, $log );
                undef($dbh);
                my $mailmigReturn = com::owm::migBellCanada::callMigrationTool( $MODE, $email, $puid, $SDEBUG, $log, $cfg, $pwd, "true", $mssvip);
                my $oldMx8MailboxStatus; # to capture the old value of mailboxstatus on MX8 before any changes are made
                if ( $mailmigReturn eq $com::owm::migBellCanada::STATUS_SUCCESS ) {    #only continue to run final if the sync migration succeeded
                    if ( $PRELOADMODE eq "false" ) {
                        #if this is not preload which means it is either single or final run, run the final sync mail run.
                        my $time_mb_lock = time;
                        # set this to success so that another level of nesting
                        # is not required below, just test that mx8maintResult
                        # is SUCCESS so the second sync can run
                        my $mx8maintResult = $com::owm::migBellCanada::STATUS_SUCCESS; 
                        my $mx8PauseSec = $cfg->param('mx8AllowForReplication');
                        if ($cfg->param("fakeFinalMode") eq "true") {
                            # if we're in fake final mode, set the MX8 pause
                            # duration to 0 so there is no attempt to change
                            # mailboxstatus on MX8
                            $mx8PauseSec = 0;
                        }
                        # if mx8PauseSec is set and is a positive integer then
                        # put MX8 into maint mode before the final sync
                        if (defined($mx8PauseSec) && (int($mx8PauseSec) > 0)) {
                            # put MX8 account into maintenance mode.  Only 
                            # do this for WL accounts as MX8/MYI have problems
                            # during the final sync otherwise
                            if ($MODE eq 'WL') {
                                my $retryCount = 0;
                                do {
                                    ($mx8maintResult, $oldMx8MailboxStatus) = setMx8MaintMode($email, $LOGSTRING, $cfg, $log, 'M');
                                    if ($mx8maintResult == $com::owm::migBellCanada::STATUS_SUCCESS) {
                                        $log->debug($LOGSTRING.':Pausing for MX8 directory replication');
                                        sleep($mx8PauseSec);
                                    } else {
                                        $retryCount ++;
                                        if ($retryCount > 5) {
                                            last;
                                        }
                                        # give whatever fault prevented the
                                        # change a chance to fix itself.
                                        sleep(1); 
                                    }
                                } while ($mx8maintResult != $com::owm::migBellCanada::STATUS_SUCCESS);
                            }
                        }
                        # only continue if the maint mode setting succeeded, or didn't happen
                        if ($mx8maintResult != $com::owm::migBellCanada::STATUS_SUCCESS) {
                            $log->error($LOGSTRING.':Unable to put MX8 mailbox into maintenance mode, skipping final migration');
                            $mailmigReturn = $com::owm::migBellCanada::STATUS_FAIL_MAILBOX_MIGRATION;
                        } else {
                            #if this is not preload which means it is either single or final run, run the final sync mail run.
                            #If this is a fakeFinal run, set the preloadMode to true
                            my $finalPreloadState = "false";
                            if ($cfg->param("fakeFinalMode") eq "true") {
                                $finalPreloadState = "true";
                            }
                            $mailmigReturn = com::owm::migBellCanada::callMigrationTool( $MODE, $email, $puid, $SDEBUG, $log, $cfg, $pwd, $finalPreloadState, $mssvip);
                            if ($cfg->param("fakeFinalMode") eq "true") {
                                #In fake final mode the mail migration will appear to have failed.
                                $log->info($LOGSTRING.': Fake Final Mode is TRUE original return from mail migration '.$mailmigReturn);
                                $mailmigReturn = $com::owm::migBellCanada::STATUS_FAIL_MAILBOX_MIGRATION;
                            }
                            $time_mb_lock = time - $time_mb_lock;
                            # only disable MX8 maintenance mode if the 
                            # migration failed, or if the user isn't going
                            # to be put into proxy mode on mx8
                            my $enable_mx8_proxy_user = $cfg->param('enable_mx8_proxy_user');
                            # if fail or (mx8 proxy not true)
                            if (($mailmigReturn != $com::owm::migBellCanada::STATUS_SUCCESS) ||
                                (defined($enable_mx8_proxy_user) && (lc($enable_mx8_proxy_user) ne 'true'))) {
                                if (defined($mx8PauseSec) && (int($mx8PauseSec) > 0)) {
                                    # only do this for WL accounts.  MX8/MYI
                                    # didn't get set to M above
                                    if ($MODE eq 'WL') {
                                        my $retryCount = 0;
                                        do {
                                            # don't care what the previous 
                                            # setting was as it should be M
                                            # and checking that here will just
                                            # add to the possible error cases
                                            # to be handled.
                                            my $dontCare;
                                            ($mx8maintResult, $dontCare) = setMx8MaintMode($email, $LOGSTRING, $cfg, $log, $oldMx8MailboxStatus);
                                            if ($mx8maintResult != $com::owm::migBellCanada::STATUS_SUCCESS) {
                                                $retryCount ++;
                                                if ($retryCount > 10) {
                                                    last;
                                                }
                                                sleep(1);
                                            }
                                        } while ($mx8maintResult != $com::owm::migBellCanada::STATUS_SUCCESS);
                                        # not sure if bailing out here is the
                                        # right thing to do, but it is the
                                        # best way to highlight that there was
                                        # a problem with this user
                                        if ($mx8maintResult != $com::owm::migBellCanada::STATUS_SUCCESS) {
                                            $log->error($LOGSTRING.':Unable to take MX8 mailbox out of maintenance mode, failing final migration');
                                            $mailmigReturn = $com::owm::migBellCanada::STATUS_FAIL_MAILBOX_MIGRATION;
                                        }
                                    }
                                }
                            }
                            $run_times{"mboxLock"} = $time_mb_lock;
                        }
                    }
                }
                $time_mb = time - $time_mb;
                $run_times{"mbox"} = $time_mb;
                $finalProcessList .= ",MAILBOX";

                $dbh = com::owm::migBellCanada::connectEventDB( $eventLog, $cfg, $log );
                if ( $dbh eq $com::owm::migBellCanada::STATUS_GENERAL_ERROR ) {
                    $log->error($LOGSTRING.': Unabled to reconnect to migration database after mail migration');
                } else {
                    $log->debug($LOGSTRING.': Reconnected to migration database');
                }

                # The dircaches need a bit of time for there data to be
                # update for the account that was migrated.
                my $pauseSec = $cfg->param('allowForReplication');

                #sleep($pauseSec);

                # Lookup account's new mailboxstatus info using PUID. - removed to support delta TODO: remove from script.
                #($status,$email,$mailboxstatus,$migstatus,$consentstatus,$maillogin) = com::owm::migBellCanada::lookupPUID($lookupKey,$cfg,$log);

                # Mailbox migration success is determined by  the return response from callMigrationTool
                # To support both delta and non-delta runs from the same configuration, migratorDryRunMode will always be set to true for the mail migration tool.
                # This script will change the mailboxstatus as follows:
                # If mail migration was successful AND if delta mode is true then mailboxstatus=P
                # If mail migration was successful AND if delta mode is false then mailboxstatus=A
                # If mail migration was unsuccessful then mailboxstatus is unchanged (should remain P)

                $log->info("$LOGSTRING: $email: migrating mailbox $mailmigReturn");

                my $error;

                #Archive the logs for each user
                my $traceLogName = $email;
                $traceLogName =~ s/@/_/;
                if ( !dirEmpty( "./log/mail/tracelogs/$traceLogName/", $error ) ) {
                    if ( defined $error ) {
                        $log->warn("$LOGSTRING: Could not archive trace logs $error");
                    } else {
                        my $result = `mv ./log/mail/tracelogs/$traceLogName $archiveDir/`;
                        $log->info("$LOGSTRING: archived to $archiveDir, parsing counters ");
                        $parseCounters_tool_cmd = $cfg->param('parseCounters_tool_cmd');    #reset cmd back to default
                        if ( defined $parseCounters_tool_cmd ) {
                            $parseCounters_tool_cmd =~ s/<file>/$archiveDir\/$traceLogName/;
                            $parseCounters_tool_cmd =~ s/<jobid>/$JOBID/;
                            $parseCounters_tool_cmd =~ s/<email>/$email/;
                            if ($SDEBUG) {
                                $parseCounters_tool_cmd =~ s/<debug>/d/;
                            } else {
                                $parseCounters_tool_cmd =~ s/<debug>//;
                            }
                            my $result = `nohup $parseCounters_tool_cmd &`;
                            $log->info( "SCRIPT|$JOBID: Invoked script to parse mail migration counters. $parseCounters_tool_cmd  $result" );
                            if ( $PRELOADMODE eq "false" ) {
                                my $compress_mail_trace_logs = $cfg->param('compress_final_mail_trace_logs');
                                if (defined($compress_mail_trace_logs) && (lc($compress_mail_trace_logs) eq 'true')) {
                                    my $compressCmd = 'gzip -1 '.$archiveDir.'/'.$traceLogName.'/trace.*';
                                    $result = `$compressCmd`;
                                    $log->info( 'SCRIPT|'.$JOBID.': Compressed final mail trace logs. Cmd: "'.$compressCmd.'" Result: "'.$result.'"' );
                                }
                            } else {
                                my $compress_mail_trace_logs = $cfg->param('compress_preload_mail_trace_logs');
                                if (defined($compress_mail_trace_logs) && (lc($compress_mail_trace_logs) eq 'true')) {
                                    my $compressCmd = 'gzip -1 '.$archiveDir.'/'.$traceLogName.'/trace.*';
                                    $result = `$compressCmd`;
                                    $log->info( 'SCRIPT|'.$JOBID.': Compressed preload mail trace logs. Cmd: "'.$compressCmd.'" Result: "'.$result.'"' );
                                }
                            }
                        }
                    }
                } else {
                    $log->warn("$LOGSTRING:No mail trace logs to archive for this batch");
                }

                # Read sum of messages in all folders and log for processing later
                $folders_json = $mos->getMailFolders($email);
                $folders_decoded = decode_json($folders_json);
                # Hash means folders could not be read
                if (ref($folders_decoded) eq "HASH") {
                    $log->error($LOGSTRING.': Folders could not be read. Migration counter not available!');
                } else {
                    my $counter_after = 0;
                    foreach my $folder (@$folders_decoded) {
                        $counter_after = $counter_after + $folder->{'numMessages'};
                    }
                    $log->info($LOGSTRING.': Number of Messages on target after mail migration:'.$counter_after);

                    my $query = $dbh->prepare("INSERT INTO counters (job_id, email, counter_name, total, size) VALUES (?, ?, ?, ?, ?)");
                    $query->execute($JOBID, $email, 'messagesOnTargetAfter', $counter_after, undef) or $log->error($LOGSTRING.': Cannot insert messagesOnTargetAfter counter: '.$query->err_str);
                    # $query->execute($JOBID, $email, 'messagesOnTargetAfter', $counter_after, undef) or die $query->err_str;
                    $log->info("$LOGSTRING: Updated messagesOnTargetAfter counter in database");
                }

                my $mailSuccess = 'false';
                ##TODO - only checking one of these migration status, so if the second one fails this looks like it succeeded *******************
                if ( $mailmigReturn eq $com::owm::migBellCanada::STATUS_SUCCESS ) {

                    $mailSuccess  = 'true';
                    $migstatus    = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'M' );    # Set migstatus to 'M' (success mail migration)
                    $finalEventId = $com::owm::migBellCanada::EVENT_MIGRATION_END;

                    #If mailbox was successful, set mailboxstatus to ACTIVE if this is not a preload run
                    if ( $PRELOADMODE eq "false" ) {
                        $log->info( "$LOGSTRING:Mailbox successfully migrated, running a final migration, updating mailboxstatus to ACTIVE." );
                        my $tryCount = 0;
                        my $mx8ProxyDone = 0;

                        do {
                            sleep($tryCount);
                            $tryCount ++;
                            $mos->userUpdateMailbox( "update", $email, "active", $cfg->param('defaultCos'), "none", $cfg, $log );

                            #Set the mx8 mail to true for all user types if configured.
                            unless ($mx8ProxyDone) {
                                my $enable_mx8_proxy_user = $cfg->param('enable_mx8_proxy_user');
                                if ( defined($enable_mx8_proxy_user)
                                    && lc($enable_mx8_proxy_user) eq 'true' )
                                {
                                    my $result = set_mx8_proxy( $email, $puid, $cfg, $log );
                                    if ( $result == $com::owm::migBellCanada::STATUS_SUCCESS ) {
                                        $log->info( $email . '|' . $puid . ':MX8 user set to proxy mode successfully' );
                                        $mx8ProxyDone = 1;
                                    } else {
                                        $log->error( $email . '|' . $puid . ':Failure setting MX8 user to proxy mode' );
                                    }
                                }
                            }
                            sleep($pauseSec);
                            ( $status, $email, $mailboxstatus, $migstatus, $consentstatus, $maillogin ) = com::owm::migBellCanada::lookupPUID( $lookupKey, $cfg, $log );
                            $log->info($LOGSTRING.':MX9 mailboxstatus now "'.$mailboxstatus.'"');
                        } while ($mailboxstatus ne 'A' && $tryCount < 6);
                    } else {
                        $log->info("$LOGSTRING:Mailbox successfully migrated, running a preload, mailboxstatus remains in PROXY");
                    }
                } else {
                    $log->error("$LOGSTRING:$status:Mailbox failed to migrate. ");
                    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'm' );    # Set migstatus to 'm' (failed mail migration)
                    $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                    $finalEventMsg .= "Mailbox Failed. ";

                    #update user status to PROXY. work around for SERVICES-3263
                    if ( ($MODE eq "MX8") || ($MODE eq "MYI")  ) {
                        $mos->userUpdateMailbox( "update", $email, "proxy", $cfg->param('defaultCos'), "none", $cfg, $log );
                        sleep($pauseSec);
                        ( $status, $email, $mailboxstatus, $migstatus, $consentstatus, $maillogin ) = com::owm::migBellCanada::lookupPUID( $lookupKey, $cfg, $log );
                    }
                    goto END;
                }

                #	if ( $mailboxstatus eq 'A' ) {
                #		$log->info("$LOGSTRING:Mailbox successfully migrated.");
                #		  if ($finalEventId == $com::owm::migBellCanada::STATUS_GENERAL_ERROR ) {
                #			  #then errors have occurred but the migration had continued to run as per configuration and mailbox migration has succeeded
                #			  $finalEventId = $com::owm::migBellCanada::STATUS_COMPLETION_ERRORS;
                #		   } else {
                #			  $finalEventId = $com::owm::migBellCanada::EVENT_MIGRATION_END;
                #		  }
                #	  } elsif ( $mailboxstatus eq 'P' ) {
                #		$log->warn("$LOGSTRING:$status:After mail migration, account in Proxy Mode.");
                #		$finalEventMsg .= "After mbox migration, mailbox in proxy mode. ";
                #		# Set account's 'maillogin' LDAP attribute back to orig
                #		if ( $cfg->param('resetMaillogin') eq "true" ) {
                #			$status = com::owm::migBellCanada::setMaillogin($email,$puid,$cfg,$log,$mailloginOrig);
                #			if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                #			  $log->warn("$LOGSTRING:Was not able to set maillogin back to original value '$mailloginOrig' after mailbox migration");
                #			  $finalEventId = $com::owm::migBellCanada::STATUS_COMPLETION_ERRORS;
                #		      $finalEventMsg .= "Mailbox Succeeded but was not able to set maillogin back. "
                #			}
                #		}
                #
                #		goto END;
                #
                #	  } elsif ( $mailboxstatus eq 'M' ) {
                #		$status = $com::owm::migBellCanada::STATUS_FAIL_MAILBOX_IN_MAINTENANCE;
                #		$log->error("$LOGSTRING:$status:Mailbox is still in maintenance mode (mailboxstatus='M').");
                #		$finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                #		$finalEventMsg .= "Mailbox Failed. (mailboxstatus='M')";
                #		# Set account's 'maillogin' LDAP attribute back to orig
                #		if ( $cfg->param('resetMaillogin') eq "true" ) {
                #			$status = com::owm::migBellCanada::setMaillogin($email,$puid,$cfg,$log,$mailloginOrig);
                #			if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                #			  $log->warn("$LOGSTRING:Was not able to set maillogin back to original value '$mailloginOrig' after mailbox migration");
                #			  $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                #		      $finalEventMsg .= "Mailbox Succeeded but was not able to set maillogin back. "
                #			}
                #		}
                #
                #		#goto END; #When in maintenance mode continue with finalising the migration. for now as workaround to SERVICES-3263
                #
                #	  } else {
                #
                #		# The script should not reach this point, but just in case...
                #		$log->warn("$LOGSTRING:mailboxstatus unexpected value (mailboxstatus=$mailboxstatus)");
                #		$status = $com::owm::migBellCanada::STATUS_FAIL_MAILBOX_MIGRATION;
                #		$log->error("$LOGSTRING:$status:Mailbox failed to migrate.");
                #		$finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                #		$finalEventMsg .= "Mailbox failed(mailboxstatus=$mailboxstatus). ";
                #
                #		goto END;
                #
                #	  }

                #
                # Append last message to MSFT inbox and welcome message to Mx inbox.
                # If the user is known to be EN, appeand EN emails.
                # If the user is known to be FR, appeand FR emails.
                # If the user is not known to be EN or FR, appeand both EN and FR emails.
                #
                if ( $PRELOADMODE eq "false" ) {
                    my $owmImapServer = $cfg->param('owm_imap_server');
                    my $welcomeEN;
                    my $lastEN;

                    # Assign Appropriate Welcome | Last Email template.
                    if ( $MODE eq "WL" ) {
                        $welcomeEN = $cfg->param('welcome_msg_EN_msft');
                        $lastEN    = $cfg->param('last_msg_EN_msft');
                    } else {
                        $welcomeEN = $cfg->param('welcome_msg_EN_myimx8');
                        $lastEN    = $cfg->param('last_msg_EN_myimx8');
                    }
                    my $sendWelcomeMsg = $cfg->param('send_welcome_msg_'.uc($MODE));
                    my $sendLastMsg = $cfg->param('send_last_msg_'.uc($MODE));
                    if (defined($sendWelcomeMsg)) {
                        if (lc($sendWelcomeMsg) eq 'true') {
                            $sendWelcomeMsg = 1;
                        } else {
                            $sendWelcomeMsg = 0;
                        }
                    } else {
                        $log->warn($LOGSTRING.':configuration key "send_welcome_msg_'.$MODE.'" not found, defaulting to true');
                        $sendWelcomeMsg = 1;
                    }
                    if (defined($sendLastMsg)) {
                        if (lc($sendLastMsg) eq 'true') {
                            $sendLastMsg = 1;
                        } else {
                            $sendLastMsg = 0;
                        }
                    } else {
                        $log->warn($LOGSTRING.':configuration key "send_last_msg_'.$MODE.'" not found, defaulting to true');
                        $sendLastMsg = 1;
                    }
                    my $languageOfMsg = 'EN';    #set default inline to English
                    if ( $MODE eq "WL" ) {
                        my $dataLine = `grep $keyCommaDelimited $userProfileAccountFile 2>/dev/null | head -1`;

                        if ( $dataLine =~ /EN/ ) {
                            $languageOfMsg = 'EN';
                        } else {
                            $log->warn("$LOGSTRING: Locale in partner file is not EN");
                            $languageOfMsg = 'EN';
                        }
                    }

                    # If default language is configured override using this
                    if ( $cfg->param('msg_locale') ) {
                        $languageOfMsg = $cfg->param('msg_locale');
                    }

                    # Append last msg to MSFT inbox.
                    if ($sendLastMsg) {
                        $log->info("$LOGSTRING:Appending EN last message into SOURCE SYSTEM");
                        my $appendImapServer = $cfg->param('msft_imap_server');
                        if ( ( $MODE eq 'MX8' ) || ( $MODE eq 'MYI' ) && $PRELOADMODE eq "false" ) {
                            $appendImapServer = $cfg->param('owm_source_imap_server');
                        }
                        print "\n\n Values: ";
                        print "$appendImapServer $email $pwd $lastEN $email \n\n";
                        my $msgRes = com::owm::imapAppend::imapAppendMessage( $appendImapServer, $email, $pwd, $lastEN, $email, $MODE );

                        if ($msgRes) {
                            my $tries = 0; # Count number of reties.
                            my $maxTries = $cfg->param('max_number_of_imap_append_tries');
                            while () {
                              $tries++;
                              $msgRes = com::owm::imapAppend::imapAppendMessage( $appendImapServer, $email, $pwd, $lastEN, $email, $MODE );

                              if( $msgRes ) {
                                if ($tries >= $maxTries) {
                                  $status = $com::owm::migBellCanada::STATUS_FAIL_LAST_MSG_APPEND;
                                  $log->error("$LOGSTRING:$status:$msgRes Fail to append last message try $tries of $maxTries)");

                                  $finalEventId = $com::owm::migBellCanada::STATUS_COMPLETION_ERRORS;
                                  $finalEventMsg .= "Failed to append last message. ";
                                  last; # Break out of loop.
                                } else {
                                  $log->info("$LOGSTRING:Fail to append last message (try $tries of $maxTries)");
                                  sleep $cfg->param('sleep_between_imap_append_tries');
                                }
                              } else {
                                $log->info("$LOGSTRING:Successfully appended last message");
                                last; # Break out of loop.
                              }
                           }
                        }
                    } else {
                        $log->info($LOGSTRING.':NOT appending EN last message into SOURCE SYSTEM as send_last_msg_'.uc($MODE).' is false');
                    }

                    #Append welcome msg to Mx inbox by IMAP Append if emailBy=imap in configuration
                    #Default behaviour is to use the migration tool and mOS. Use by IMAP if need to support multiple languages
                    if ($sendWelcomeMsg) {
                        if ( $cfg->param('emailBy') eq "imap" ) {
                            $log->info("$LOGSTRING:Appending EN welcome message into Mx");
                            my $newMode = $MODE;
                            $newMode = 'WLW' if ( $MODE eq 'WL' );
                            my $msgRes = com::owm::imapAppend::imapAppendMessage( $owmImapServer, $email, $pwd, $welcomeEN, $email, $newMode );

                            if ($msgRes) {        
                                my $tries = 0; # Count number of reties.
                                my $maxTries = $cfg->param('max_number_of_imap_append_tries');
                                while () {
                                  $tries++;
                                  $msgRes = com::owm::imapAppend::imapAppendMessage( $owmImapServer, $email, $pwd, $welcomeEN, $email, $newMode );

                                  if( $msgRes ) {
                                    if ($tries >= $maxTries) {
                                      $status = $com::owm::migBellCanada::STATUS_FAIL_WELCOME_MSG_APPEND;
                                      $log->error("$LOGSTRING:$status:$msgRes Fail to append welcome message try $tries of $maxTries)");

                                      $finalEventId = $com::owm::migBellCanada::STATUS_COMPLETION_ERRORS;
                                      $finalEventMsg .= "Failed to append welcome message. ";
                                      last; # Break out of loop.
                                    } else {
                                      $log->info("$LOGSTRING:Fail to append welcome message (try $tries of $maxTries)");
                                      sleep $cfg->param('sleep_between_imap_append_tries');
                                    }
                                  } else {
                                    $log->info("$LOGSTRING:Successfully appended welcome message");
                                    last; # Break out of loop.
                                  }                    
                              } 
                          }
                        }
                    } else {
                        $log->info($LOGSTRING.':NOT appending EN welcome message into Mx as send_welcome_msg_'.uc($MODE).' is false');
                    }

                }

                # Set account's 'maillogin' LDAP attribute back to orig
                if ( $cfg->param('resetMaillogin') eq "true" ) {
                    $mos->userUpdateMaillogin( $email, $mailloginOrig );
                    if ( !$mos->is_success ) {
                        $log->error( "$LOGSTRING:Was not able to set 'maillogin' back to original value '$mailloginOrig' after mailbox migration" );
                        $finalEventId = $com::owm::migBellCanada::STATUS_COMPLETION_ERRORS;
                        $finalEventMsg .= "Was not able to set 'maillogin' back to original. ";
                    }
                }
            } else {
                $log->error("$LOGSTRING:Not in proxy mode, so mailbox will not be migrated.");
                $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                $finalEventMsg .= "User not in proxy mode. ";
            }
        } else {
            $log->info( $LOGSTRING . ':Skipping mail migration as mode is PRELOAD and preloadMailBatchInParallel is set' );
        }
    }

    #Finalise the end of the migration for this user
  END:
    $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'z' );    # Set migstatus to 'z' (to indicate there is no active migration)

    #only set the migration status 'z' if the migration has finished. In the case we area exiting because a migration is running, exit to this ENDX instead of END
  ENDX:
    my $time_taken = time - $start;
    $finalEventName = $com::owm::migBellCanada::EVENT_NAMES{"$finalEventId"};
    $finalEventMsg  = 'Processed ' . $finalProcessList . ' in ' . $time_taken . ' seconds. ' . $finalEventMsg;
    com::owm::migBellCanada::logEventsToDatabase( $dbh, $cfg, $log, $PROCESSID, $SCRIPTNAME, __LINE__, $JOBID, $finalEventId, $finalEventName, $LOGMODE, $email, $puid, $migstatus = $finalMigStatus, $finalEventMsg );

    my $timerInsert = $dbh->prepare("INSERT INTO timers (job_id,email,timer_name,total) VALUES ('$JOBID',?,?,?)");
    $timerInsert->execute( $email, "overall", $time_taken );

    #TODO Add call here to Telstra API to update status
    $log->info("$LOGSTRING:Stop processing...");

    #Update MSSol with final status if migrating mailbox.
    if ( $MAILBOX && $PRELOADMODE eq 'false' ) {
        if ( defined $status_endpoint ) {
            my ( $statusRes, $statusError, $jobID ) = com::owm::ManageMigrationStatus::ManageMigrationStatus( $status_endpoint, $status_wsdl, $email, $finalEventId, $JOBID, $finalEventMsg, $count, $startcount, $log );
        }
        if ( $status_log eq "true" ) {
            $mssLog->info( "$LOGSTRING: UPDATING MSSOL HERE: $email, $finalEventId, $JOBID, " . $com::owm::migBellCanada::EVENT_NAMES{"$finalEventId"} . ",$finalMigStatus $finalEventMsg" );
        }
    }
    unless ( $finalEventId eq $com::owm::migBellCanada::STATUS_MIG_NOTSTARTED
        || $finalEventId eq $com::owm::migBellCanada::STATUS_MIGRATION_IN_PROGRESS
        || $finalEventId eq $com::owm::migBellCanada::STATUS_FAIL_NOT_ALLOWED )
    {
        #only print times an counters if migration was attempted
        $finalEventId   = $com::owm::migBellCanada::EVENT_FINAL_TIMES;
        $finalEventName = $com::owm::migBellCanada::EVENT_NAMES{"$finalEventId"};
        $finalEventMsg  = "Time taken in secs: ";
        foreach my $key ( keys %run_times ) {
            $finalEventMsg .= $key . " " . $run_times{$key} . " ";
            my $timerInsert = $dbh->prepare("INSERT INTO timers (job_id,email,timer_name,total) VALUES (?,?,?,?)");
            $timerInsert->execute( $JOBID, $email, $key, $run_times{$key} );
        }

        $log->info("$LOGSTRING: $finalEventMsg");
        if (
            $PRELOADMODE eq "true"
            && (   $finalEventId ne $com::owm::migBellCanada::STATUS_SUCCESS
                || $finalEventId ne $com::owm::migBellCanada::EVENT_MIGRATION_END
                || $finalEventId ne $com::owm::migBellCanada::EVENT_MIGRATION_RUN_END )
          )
        {
            $migstatus = $finalMigStatus = com::owm::migBellCanada::setMigstatus( $email, $puid, $cfg, $log, $migstatus, 'd' );    #indicates preload migration that failed
        }

        com::owm::migBellCanada::logEventsToDatabase( $dbh, $cfg, $log, $PROCESSID, $SCRIPTNAME, __LINE__, $JOBID, $finalEventId, $finalEventName, $LOGMODE, $email, $puid, $migstatus = $finalMigStatus, $finalEventMsg );

        #log counters
        $finalEventId   = $com::owm::migBellCanada::EVENT_FINAL_COUNTS;
        $finalEventName = $com::owm::migBellCanada::EVENT_NAMES{"$finalEventId"};
        $finalEventMsg  = "Process counts: ";
        if (
            ( defined( $pabCounters{'retrieved'} ) && ( $pabCounters{'retrieved'} > 0 ) )
            || ( defined( $pabCounters{'contactsRetrieved'} )
                && ( $pabCounters{'contactsRetrieved'} > 0 ) )
          )
        {
            #if there are any calendar entries processed write out the counters.
            $finalEventMsg .= "AB: ";
            foreach my $key ( keys %pabCounters ) {
                $finalEventMsg .= $key . " " . $pabCounters{$key} . " ";
                my $counterInsert = $dbh->prepare("INSERT INTO counters (job_id, email, counter_name, total, size) VALUES ('$JOBID', ?, ?, ?, NULL)");
                $counterInsert->execute( $email, "PAB-$key", $pabCounters{$key} );
            }
        } else {
            $finalEventMsg .= "AB: - ";
            $log->debug("$LOGSTRING: No address book counters to output AB Retrieved is $pabCounters{'retrieved'}");
        }
        if ( defined $calCounters{'eventsRetrieved'} && $calCounters{'eventsRetrieved'} > 0 ) {

            #if there are any calendar entries processed write out the counters.
            $finalEventMsg .= "CAL: ";

            foreach my $key ( keys %calCounters ) {
                $finalEventMsg .= $key . " " . $calCounters{$key} . " ";
                my $counterInsert = $dbh->prepare("INSERT INTO counters (job_id, email, counter_name, total, size) VALUES ('$JOBID', ?, ?, ?, NULL)");
                $counterInsert->execute( $email, "CAL-$key", $calCounters{$key} );
            }
        } else {
            $finalEventMsg .= "CAL: -";
            $log->debug("$LOGSTRING: No calendar counters to output CAL Retrieved is $calCounters{'eventsRetrieved'}");
        }

        com::owm::migBellCanada::logEventsToDatabase( $dbh, $cfg, $log, $PROCESSID, $SCRIPTNAME, __LINE__, $JOBID, $finalEventId, $finalEventName, $LOGMODE, $email, $puid, $migstatus = $finalMigStatus, $finalEventMsg );
        $log->info("$LOGSTRING: $finalEventMsg");

    }

    com::owm::migBellCanada::disconnectDB( $dbh, $cfg, $log );
    undef($dbh);
    print "";    #forces the progress bar to update. Don't know why, but without it will not update till the end.
}

######################################################################
## - Cleanup -                                                      ##
##   This routine is used to update the progress meter              ##
######################################################################
sub cleanup {
    $progress->update( ++$count );    # Update the counter
}

############################################################
## - BuildQueue -                                          #
##   Get the email addrs and populate the processing queue #
############################################################
sub buildQueue {
    my $in_file = shift;
    $log->debug("SCRIPT|Input file set to \'$in_file\'") if $SDEBUG;

    print("Main processing progress...\n");

    open my $file, "<", $in_file
      or $log->error("SCRIPT|Unable to open file $in_file: $!") && exit(1);

    while ( my $data_line = <$file> ) {
        $startcount++;
        chomp($data_line);
        my $firstLine = 0;
        if (($startcount == 1) && $firstUserStartSent) {
            $firstLine = 1;
        }
        $bw->add_work( { data_line => $data_line, first_line => $firstLine } );
    }

    close($file);

    $log->debug("SCRIPT|Processing $startcount records read from input file.") if $SDEBUG;
    print("Processing $startcount records.\n");

    return ($startcount);
}

############################################################
## - PreProcessUserlist -                                  #
##   Pre-process the user list file to group block / allow #
##   fiters                                                #
############################################################
sub PreProcessUserlist {
    my $in_file      = shift;
    my $tmp_file     = shift;
    my $allow        = "";
    my $block        = "";
    my $currentEmail = "";
    my $userCount    = 0;
    my $email;
    my $puid;
    my $listType;
    my $filterStr;

    # Delete any old tmp file.
    unlink $tmp_file;

    $log->info("SCRIPT|Pre-processing input file, $in_file and creating tmp file, $tmp_file.");
    $log->info("SCRIPT|Pre-processing may take some time deprending on the size of $in_file. See progress bar for status.");

    open( my $fh_in_file, "<", $in_file )
      or $log->logdie("SCRIPT|FATAL: Failed to open input data file: $in_file\n\n");

    open( my $fh_tmp_file, ">>", $tmp_file )
      or $log->logdie("SCRIPT|FATAL: Failed to open tmp data file for writing / appending: $tmp_file\n\n");

    # Count the number of lines in the input file.
    print("Pre-processing progress...\n");
    while (<$fh_in_file>) { }
    my $rc = $.;

    # Initialize the progess counters.
    print("Processing $rc records.\n");
    &progress_init($rc);

    # Move pointer back to start of file
    seek( $fh_in_file, 0, 0 );
    while (<$fh_in_file>) {

        $progress->update( ++$count );
        my $user_data_line = $_;
        chomp($user_data_line);
        $user_data_line =~ s/\r//g;    # Remove ^M charactors.

        ( $email, $puid, $listType, $filterStr ) = split( /,/, $user_data_line );

        # The file should be sorted so all entries are in groups of email addresses. If this is
        # NOT the case, this script will not perform as expected.
        #
        # If the email changes, perform final step on the last email allow / block string and initialize next email.
        if ( $currentEmail ne $email ) {

            # For last email allow / block string, remove the ';' delimitor at the start and write to tmp file
            if ( $block =~ m/=:/ ) {
                $block =~ s/=:/=/;
                print $fh_tmp_file ( $block . "\n" );
            }
            if ( $allow =~ m/=:/ ) {
                $allow =~ s/=:/=/;
                print $fh_tmp_file ( $allow . "\n" );
            }

            # For next email allow / block string, initialize it.
            $userCount++;
            $currentEmail = $email;
            $block        = "$email,$puid,blocked=";
            $allow        = "$email,$puid,allowed=";

        }

        if ( $listType == 0 ) {
            $block .= ":" . $filterStr;
        } elsif ( $listType == 1 ) {
            $filterStr =~ s/^\@//;    # In input domains have '@' at start. This is not needed in mOS.
            $block .= ":" . $filterStr;
        } elsif ( $listType == 2 ) {
            $allow .= ":" . $filterStr;
        } elsif ( $listType == 3 ) {
            $filterStr =~ s/^\@//;    # In input domains have '@' at start. This is not needed in mOS.
            $allow .= ":" . $filterStr;
        }

    }

    # For last email allow / block string, remove the ';' delimitor at the start and write to tmp file
    if ( $block =~ m/=:/ ) {
        $block =~ s/=:/=/;
        print $fh_tmp_file ( $block . "\n" );
    }
    if ( $allow =~ m/=:/ ) {
        $allow =~ s/=:/=/;
        print $fh_tmp_file ( $allow . "\n" );
    }

    close($fh_tmp_file);
    close($fh_in_file);

}

############################################################
## - PreProcessMailForward -                               #
############################################################
sub PreProcessMailForward {
    my $in_file  = shift;
    my $tmp_file = shift;
    my %puidAlreadyUsed;

    # Delete any old tmp file.
    unlink $tmp_file;

    $log->info("SCRIPT|Pre-processing input file, $in_file and creating tmp file, $tmp_file.");
    $log->info("SCRIPT|Pre-processing may take some time deprending on the size of $in_file. See progress bar for status.");

    open( my $fh_in_file, "<", $in_file )
      or $log->logdie("SCRIPT|FATAL: Failed to open input data file: $in_file\n\n");

    open( my $fh_tmp_file, ">>", $tmp_file )
      or $log->logdie("SCRIPT|nFATAL: Failed to open tmp data file for writing / appending: $tmp_file\n\n");

    # Count the number of lines in the input file.
    print("Pre-processing progress...\n");
    while (<$fh_in_file>) { }
    my $rc = $.;

    # Initialize the progess counters.
    print("Processing $rc records.\n");
    &progress_init($rc);

    # Move pointer back to start of file
    seek( $fh_in_file, 0, 0 );
    while (<$fh_in_file>) {

        $progress->update( ++$count );
        my $user_data_line = $_;
        chomp($user_data_line);
        $user_data_line =~ s/\r//g;    # Remove ^M charactors.

        my ( $email, $puid, @other ) = split( /,/, $user_data_line );

        if ( exists $puidAlreadyUsed{$puid} ) {
            $log->warn("$LOGSTRING:Data line already found, so skipping : $user_data_line");
        } else {
            $puidAlreadyUsed{$puid} = "found";
            print $fh_tmp_file "$user_data_line\n";
        }

    }

    close($fh_tmp_file);
    close($fh_in_file);

}

###########################################################################
# deleteAccLdapAttributes
# For PRELOAD reset delete Addressbook, signature
# For INITIAL reset delete all (not used in this work flow)
###########################################################################
sub deleteAccLdapAttributes {
    my $email = shift;
    my $cfg   = shift;
    my $state = shift;
    my $mode  = shift;
    my $log   = shift;
    my $res   = "";
    my ( $user, $domain ) = split( /@/, $email );

    my ( $result, $ldap ) = connectLDAP( $email, $cfg );

    my @entries = $result->entries;
    my $dn      = $entries[0]->dn;


    ############################
    # Remove migrated ldap entries
    #############################
    #TODO should be done just in time if we are migrating this information.

    my @deleteArray;
    my @replaceArray;
    my @changesArray;
    my @addArray;

    if ($ACCOUNT) {
        if ( $entries[0]->exists('netmaillocale') ) {
            print( "\n $email|Deleting netmaillocale " . $entries[0]->get_value("netmaillocale") ) if ($SDEBUG);
            push @deleteArray, 'netmaillocale', $entries[0]->get_value("netmaillocale");
        }

        if ( $entries[0]->exists('msglocale') ) {
            print( "\n $email|Deleting msglocale " . $entries[0]->get_value("msglocale") ) if ($SDEBUG);
            push @deleteArray, 'msglocale', $entries[0]->get_value("msglocale");
        }

        if ( $entries[0]->exists('msgtimezone') ) {
            print( "\n $email|Deleting msgtimezone " . $entries[0]->get_value("msgtimezone") ) if ($SDEBUG);
            push @deleteArray, 'msgtimezone', $entries[0]->get_value("msgtimezone");
        }
        if ($mode eq 'WL') {
            print("\n $email|Replacing cn $user") if ($SDEBUG);
            push @replaceArray, 'cn', $user;
            print("\n $email|Replacing sn $user") if ($SDEBUG);
            push @replaceArray, 'sn', $user;
        }

    }
    if ($FORWARDING) {
        if ( $entries[0]->exists('mailforwardingaddress') && $mode eq 'WL' ) {
            #only delete mail forwards for microsoft customers
            print("\n $email|Deleting mailforwarding ");
            push @deleteArray,  'mailforwardingaddress', '';
            push @deleteArray,  'maildeliveryoption',    '';
            push @replaceArray, 'mailforwarding',        '0';
        }
    }
    if ($VACATION) {
        if ( $entries[0]->exists('mailautoreplymode') && $mode eq 'MX8' ) {
            print( "\n $email|Deleting mailautoreplymode " . $entries[0]->get_value("mailautoreplymode") ) if ($SDEBUG);
            push @deleteArray, 'mailautoreplymode', $entries[0]->get_value("mailautoreplymode");
        }
    }

    if ($USERLISTS) {
        if ( $entries[0]->exists('mailBlockedSendersList') && $mode eq 'WL' ) {
            print( "\n $email|Deleting mailBlockedSendersList " . $entries[0]->get_value("mailBlockedSendersList") ) if ($SDEBUG);
            push @deleteArray, 'mailBlockedSendersList', '';
        }

        if ( $entries[0]->exists('mailApprovedSendersList') && $mode eq 'WL' ) {
            print( "\n $email|Deleting mailApprovedSendersList " . $entries[0]->get_value("mailApprovedSendersList") ) if ($SDEBUG);
            push @deleteArray, 'mailApprovedSendersList', '';
        }
    }
    
    if ($CALENDAR) {
        #Subscribed calendars are remigrated each time even for preload
        if ( $entries[0]->exists('calexternaldetails') ) {
            print( "\n $email|Deleting calexternaldetails " . $entries[0]->get_value("calexternaldetails") ) if ($SDEBUG);
            push @deleteArray, 'calexternaldetails', '';
        }
    }
   

    if ( $#deleteArray > 0 ) {
        push @changesArray, 'delete';
        push @changesArray, \@deleteArray;
    }
    if ( $#replaceArray > 0 ) {
        push @changesArray, 'replace';
        push @changesArray, \@replaceArray;
    }
    if ( $#addArray > 0 ) {
        push @changesArray, 'add';
        push @changesArray, \@addArray;
    }

    $result = $ldap->modify( $dn, changes => [@changesArray] );
    print( "\n\n $email|Updated. Result: " . $result->code . " " . $result->error . "\n" ) if ($SDEBUG);
    print("\n************************************************ \n\n");
    if ( $result->code ) {
        print( "\n $email|account not updated : ldap error code " . $result->code . " " . $result->error . "\n" );
    }

    $ldap->unbind;
    $ldap->disconnect;
}

sub connectLDAP {
    my $email            = shift;
    my $cfg              = shift;
    my $mx_dircache_host = $cfg->param('mx_dircache_host');
    my $mx_dircache_port = $cfg->param('mx_dircache_port');
    my $mx_dircache_pwd  = $cfg->param('mx_dircache_pwd');

    my $ldap = Net::LDAP->new( $mx_dircache_host, port => $mx_dircache_port );
    if ( !$ldap ) {
        die("Connect to $mx_dircache_host:$mx_dircache_port failed\n");
    }

    my $mesg = $ldap->bind( "cn=root", password => $mx_dircache_pwd );
    if ( $mesg->code ne '0' ) {
        die("Bind to $mx_dircache_host:$mx_dircache_port failed. Check LDAP password.\n");
    }

    my $searchString = "mail=$email";
    print("searchString = $searchString\n") if ($SDEBUG);

    my $result = $ldap->search( base => "", filter => "$searchString" )
      || print("search failed\n");

    if ( $result->code ) {
        die("Email lookup for $email failed. $result->error\n");
    } elsif ( $result->count == 0 ) {
        die("0 accounts had matching email ($email).\n");
    } elsif ( $result->count > 1 ) {
        die("2 or more accounts had matching email ($email).\n");
    }

    return ( $result, $ldap );

}

sub dirEmpty {
    if ( !-e $_[0] ) {
        $_[1] = 'Directory does not exist.';
        return;
    }
    if ( !-d $_[0] ) {
        $_[1] = 'Path does not reference a directory.';
        return;
    }
    if ( !opendir( DIR, $_[0] ) ) {
        $_[1] = 'Could not open directory.';
        return;
    }
    while ( $_ = readdir(DIR) ) {
        next if m/^\.\.?$/;
        return;
    }
    return 1;
}

######################################################
# ---------------------------------------------------
# Start Main
# ---------------------------------------------------
######################################################

#Connect to event database
my $dbh = com::owm::migBellCanada::connectEventDB( $eventLog, $cfg, $log );
if ( $dbh eq $com::owm::migBellCanada::STATUS_GENERAL_ERROR ) {
    $log->error("SCRIPT|Database error exiting");
    exit 0;
} else {
    $log->debug("SCRIPT|Connected to event log database");
}

$SDEBUG = $cfg->param('log_debug');

#my $logdir = $cfg->param('log_dir');
#my $logfile = $logdir . "/" . $cfg->param('log_migration_name');
my $numThreads            = $cfg->param('final_threads');
my $threadTimeout         = $cfg->param('thread_timeout');
my $eventsExport_tool_cmd = $cfg->param('eventsExport_tool_cmd');
$MODE = "WL";

GetOptions(
    '-d',             \$SDEBUG,        # Override debug in config file.
    '-INPUTFILE=s',   \$INPUTFILE,
    '-mode=s',        \$MODE,
    '-jobid=s',       \$JOBID,
    'mailbox',        \$MAILBOX,
    'contacts',       \$CONTACTS,
    'calendar',       \$CALENDAR,
    'name',           \$NAME,
    'userlists',      \$USERLISTS,
    'mailforwarding', \$FORWARDING,
    'vacation',       \$VACATION,
    'signature',      \$SIGNATURE,
    'account',        \$ACCOUNT,
    'context',        \$DELETEUSER,
    'preload=s',      \$PRELOADMODE,
    'firstUserStartSent', \$firstUserStartSent,
) || usage();

print("\n\n");
print("Migration script boss process ID: $$\n");
print("\n\n");

$LOGMODE = $MODE;
if ( $PRELOADMODE eq "true" || $cfg->param("fakeFinalMode") eq "true") {
    $LOGMODE .= "P";

    #In preload mode or fakeFinalMode the migration should not fail when individual elements fail.
    $exitOnAccountFailure    = "false";
    $exitOnSignatureFailure  = "false";
    $exitOnVacationFailure   = "false";
    $exitOnForwardingFailure = "false";
    $exitOnUserListFailure   = "false";
    $exitOnNameFailure       = "false";
    $exitOnContactsFailure   = "false";
    $exitOnCalendarFailure   = "false";
    $exitOnDeleteUserFailure = "false";
    $exitOnConnectFail       = "false";
    $numThreads              = $cfg->param('preload_threads');
}

# Initialize log file.
#logInit($logfile,$logdir);

# Debug out info
if ($SDEBUG) {
    print("Running in DEBUG mode. Preload mode = $PRELOADMODE \n");
    $log->level($DEBUG);    #Override debug in log file when -d or debug set in config file
}

if ( $MODE eq "MX8" ) {
    my $intermail = `echo \$INTERMAIL`;
    chomp($intermail);
    print "intermail check result \'$intermail\' \n" if ($SDEBUG);
    if ( !$intermail || $intermail eq "" || not defined $intermail || $intermail eq "\n" ) {
        $log->error("SCRIPT|$INPUTFILE jobid: $JOBID  cannot be started. Must be run as Mx8 IMAIL user");
        print "Script must be run as an MX8 IMAIL user \n" if ($SDEBUG);
        com::owm::migBellCanada::logEventsToDatabase( $dbh, $cfg, $log, $PROCESSID, $SCRIPTNAME, __LINE__, $JOBID, $com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,
            "MigrationNotStarted", $LOGMODE, $INPUTFILE, '', '', 'Could not start, must run as an Mx8 IMAIL user' );
        exit 1;
    }
}

# If no specific data is indicated, migrate all the data.
if (   ( !$MAILBOX )
    && ( !$CONTACTS )
    && ( !$CALENDAR )
    && ( !$NAME )
    && ( !$USERLISTS )
    && ( !$FORWARDING )
    && ( !$VACATION )
    && ( !$SIGNATURE )
    && ( !$ACCOUNT )
    && ( !$DELETEUSER ) )
{
    if ( $MODE eq "WL" ) {
        $MAILBOX    = 1;
        $CONTACTS   = 1;
        $CALENDAR   = 1;
        $NAME       = 1;
        $USERLISTS  = 1;
        $FORWARDING = 1;
        $VACATION   = 1;
        $SIGNATURE  = 1;
        $ACCOUNT    = 1;
        $DELETEUSER = 1;
    } elsif ( $MODE eq 'MYI' ) {
        $MAILBOX    = 1;
        $CONTACTS   = 1;
        $CALENDAR   = 1;
        $NAME       = 1;
        $FORWARDING = 1;
        $VACATION   = 1;
        $SIGNATURE  = 1;
        $ACCOUNT    = 1;
        $DELETEUSER = 1;
    } else {
        $ACCOUNT    = 1;
        $MAILBOX    = 1;
        $VACATION   = 1;
        $SIGNATURE  = 1;
        $CONTACTS   = 1;
        $DELETEUSER = 1;
        $NAME       = 1;
    }
}

if ($MAILBOX) {
    $dataToBeMigrated .= 'MAILBOX,';

    #Set up the archive directory for mail trace logs
    mkdir $mailtraceArchiveDir unless -d $mailtraceArchiveDir;
    unless ( $preloadMailBatchInParallel && ( $PRELOADMODE eq "true" ) ) {
        my $timestamp = `date +"%Y%m%d%H%M%S"`;
        chomp $timestamp;
        $archiveDir = "$mailtraceArchiveDir/JOB-$JOBID.$timestamp/";
        $LOGSTRING .= ":JOB-$JOBID";
        mkdir $archiveDir or $log->warn("$LOGSTRING: Could not make tracelog archive dir ($archiveDir) $! ");
    }
}
if ($CONTACTS)   { $dataToBeMigrated .= 'CONTACTS,'; }
if ($CALENDAR)   { $dataToBeMigrated .= 'CALENDAR,'; }
if ($NAME)       { $dataToBeMigrated .= 'NAME,'; }
if ($USERLISTS)  { $dataToBeMigrated .= 'USERLISTS,'; }
if ($FORWARDING) { $dataToBeMigrated .= 'FORWARDING,'; }
if ($VACATION)   { $dataToBeMigrated .= 'VACATION,'; }
if ($SIGNATURE)  { $dataToBeMigrated .= 'SIGNATURE,'; }
if ($ACCOUNT)    { $dataToBeMigrated .= 'ACCOUNT,'; }
if ($DELETEUSER) { $dataToBeMigrated .= 'DELETEUSER,'; }
chop($dataToBeMigrated);
$log->info("SCRIPT|Data to be migrated: $dataToBeMigrated");
print("DEBUG: Data to be migrated: $dataToBeMigrated\n") if ($SDEBUG);

# Check input file exists
if ( -e $INPUTFILE ) {
    $log->info("SCRIPT|Processing PUID input file: $INPUTFILE");
} else {
    $log->error("SCRIPT|input file is missing: $INPUTFILE");
    usage();
}

# Setup a boss to send out the work
$bw = Parallel::Fork::BossWorker->new(
    work_handler   => \&worker,          # Routine that does the work.
    result_handler => \&cleanup,         # Routine called after work is finish (optional).
    global_timeout => $threadTimeout,    # In seconds.
    worker_count   => $numThreads
);                                       # Number of worker threads.

# Build queue that boss will handout to worker threads.
my $rc = &buildQueue($INPUTFILE);

# Initialize the progess counters.
&progress_init($rc);

com::owm::migBellCanada::disconnectDB( $dbh, $cfg, $log );
undef($dbh);

# Set boss to start handing out work to threads.
$bw->process();

# Close log file.
#logClose();

$LOGSTRING = "SCRIPT|JOB-$JOBID";

#Move the batch file into a FINISHED directory.
my $completedDirPath = $cfg->param('batch_completed_path');
mkdir $completedDirPath unless -d $completedDirPath;
move( $INPUTFILE, $completedDirPath )
  or $log->error("$LOGSTRING: Unable to move batch file to completed directory for job id $INPUTFILE: $!");

# Exit script gracefully.
print "\n";    # This just makes things look clean

#$progress->update($rc);  #force the progress bar to update to 100%
$dbh = com::owm::migBellCanada::connectEventDB( $eventLog, $cfg, $log );
    if ( $dbh eq $com::owm::migBellCanada::STATUS_GENERAL_ERROR ) {
        $log->error('SCRIPT|Database error, exiting');
        exit 0;
    } else {
        $log->debug('SCRIPT|Reconnected to event log database');
        com::owm::migBellCanada::logEventsToDatabase( $dbh, $cfg, $log, $PROCESSID, $SCRIPTNAME, __LINE__, $JOBID, $com::owm::migBellCanada::EVENT_MIGRATION_BATCH_END,'MigrationBatchEnd', $LOGMODE, $INPUTFILE, '', '', 'Migration is complete for this batch file ' . $INPUTFILE );
        com::owm::migBellCanada::disconnectDB( $dbh, $cfg, $log );
        undef($dbh);
    }


if ( defined $eventsExport_tool_cmd ) {

    #initiate the event export report
    $log->info("SCRIPT|$JOBID: Invoking script to export all events for this job from the db.");
    $eventsExport_tool_cmd =~ s/<JOBID>/$JOBID/;
    `$eventsExport_tool_cmd`;
}

######################################################
# ---------------------------------------------------
# End Main
# ---------------------------------------------------
######################################################

######################################################
# ---------------------------------------------------
# BEGIN POD Documentation
# ---------------------------------------------------
######################################################

    __END__

    =pod

    =head1 NAME

    migrator.plx - Migration Utility

    =head1 DESCRIPTION

    =cut

    exit 0;
