#!/usr/bin/perl

###########################################################################
# 'use' modules
###########################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";


use File::Copy;
use strict;
use DBI;
use Cwd;
use Getopt::Long;
use Email::Valid;
# A package that provides an easy way to have a config file
use Config::Simple;
use Sys::Hostname;


use com::owm::migBellCanada;
use Log::Log4perl qw(get_logger :levels);
use Log::Dispatch::FileRotate;
#use IPC::System::Simple qw(system capture);
use File::Basename;

use POSIX qw(setsid);

use com::owm::ManageMigrationStatus;

###########################################################################
# Global variables
###########################################################################
$| = 1;    ## Makes STDOUT flush immediately
my $SDEBUG;
my $PROCESSID = $$;
my $DBMESSAGE;
my $JOBDESCRIPTION='';
my $DBSTATUS;
my $DBSUCCESS = 0;
my $MIGTYPE;
my $JOBID = "UNK9999";
our $configPath = 'conf/migrator.conf';
our $configLog = 'conf/log.conf';
my $MIGRATIONTYPE="";
my $PRELOAD = '';
	

# Logging object.
Log::Log4perl->init($configLog);
our $log = get_logger("WATCHER.LOG");
our $conflictLog = get_logger("PREP.CNFL");
our $eventLog = get_logger("EVENT");
our $mssLog   = get_logger("MSSLOG");

# Config object.
# And read the config file so all subs have access to params.
my $cfg  = new Config::Simple();
$cfg->read($configPath)
  or $log->logdie("SCRIPT|FATAL: ".$cfg->error()."INFO: Please fix the issue and restart the migration program. Exiting.");

our $wlBatchFileNameSuffix = $cfg->param('wlBatchFileName');
our $mx8BatchFileNameSuffix = $cfg->param('mx8BatchFileName');
our $myiBatchFileNameSuffix = $cfg->param('myiBatchFileName');
our $wlMailBatchFileNameSuffix = $cfg->param('wlMailBatchFileName');
our $mx8MailBatchFileNameSuffix = $cfg->param('mx8MailBatchFileName');
our $myiMailBatchFileNameSuffix = $cfg->param('myiMailBatchFileName');
my $wlBatchFileName = "";
my $mx8BatchFileName = "";
my $myiBatchFileName = "";
my $wlMailBatchFileName = "";
my $mx8MailBatchFileName = "";
my $myiMailBatchFileName = "";
my $SCRIPTNAME="migrationWatcher";
my $pauseSec = $cfg->param('allowForReplication');
my $pauseLevel = $cfg->param('pauseLevel');
my $mustWait = "false";
my $enforce_unique_jobid = lc($cfg->param('enforce_unique_jobid'));
our $preloadMailBatchInParallel = lc($cfg->param('preloadMailBatchInParallel')) eq 'true';

#our $platform;
our @wlBatchList;
our @mx8BatchList;
our @myiBatchList;
our @wlMailBatchList;
our @mx8MailBatchList;
our @myiMailBatchList;

my $firstUserType;

my $processDirPath=$cfg->param('processing_path');
my $microsoftDirPath= $cfg->param('microsoft_puid_file_path');
my $openwaveDirPath= $cfg->param('openwave_puid_file_path');
my $myInboxDirPath= $cfg->param('myinbox_puid_file_path');
my $errorDirPath= $cfg->param('error_path');

#Create directories if they dont exist
mkdir $processDirPath unless -d $processDirPath;

my $dbh = com::owm::migBellCanada::connectEventDB($eventLog,$cfg,$log);	
if ($dbh eq $com::owm::migBellCanada::STATUS_GENERAL_ERROR) {
	$log->error("SCRIPT|Could not connect to event log database. Exiting");
} else {
	$log->debug("SCRIPT|Connected to event log database");
}

my $start = time;
######################################################
# USAGE
######################################################
sub usage {
	print STDERR qq(
	
	Must be run as Mx8 IMAIL user.
    
	Command line to run script:
		migrationWatcher.plx 	[-ondemand | -preload | -scheduled ]
                    [-noimail]
                    [-desc "a migration batch description" ]
                    
	This script takes ONE optional mode argument. Where 
	preload (only process preload files)
	ondemand (only process ondemand files)
	scheduled (only process scheduled files)
	noimail (do not check for privileges to migrate Mx8)
    desc "a description" (a description which will be added into the migration jobs table for easier tracking of migration batches)

	

	Input file naming convention:    batchnumber.csv
	Input File Format:               email,<source>,<location>
	    where source = MYI | MX8 | WL
	    where location = VIC | NSW		
	);
	exit 0;
}

######################################################
# 
# Send a START update and then a FAIL update to the
# migration status API to record the correct status
# for the user in the Telstra migration database
#
######################################################
sub sendFailedUser($$$$$$$$$) {
    my $MIGRATIONTYPE = shift;
    my $userCount = shift;
    my $userOffset = shift;
    my $email = shift;
    my $log = shift;
    my $message = shift;
    my $jobid = shift;
    my $userStarted = shift;
    my $platform = shift;

    return if ($MIGRATIONTYPE eq "PRELOAD");

    my $status_endpoint = $cfg->param('status_endpoint');
    my $status_wsdl     = $cfg->param('status_wsdl');
    my $status_log      = $cfg->param('status_log');

    if (defined($status_endpoint) && ($status_endpoint ne '') && defined($status_wsdl)) {
        my ( $statusRes, $statusError, $jobID );
        if (!$userStarted) {
            ( $statusRes, $statusError, $jobID ) = com::owm::ManageMigrationStatus::ManageMigrationStatus( $status_endpoint, $status_wsdl, $email, $com::owm::migBellCanada::EVENT_MIGRATION_START, $jobid, 'WatcherStart',  $userOffset, $userCount,$log );
            if ( $status_log eq "true" ) {
                $mssLog->info( $email.'|'.$platform.':JOB-'.$jobid.": UPDATING MSSOL HERE: $email, $com::owm::migBellCanada::EVENT_MIGRATION_START, $jobid, WatcherStart" );
            }
        }
        ( $statusRes, $statusError, $jobID ) = com::owm::ManageMigrationStatus::ManageMigrationStatus( $status_endpoint, $status_wsdl, $email, $com::owm::migBellCanada::STATUS_FAIL_INVALID_INPUT, $jobid, $message, $userOffset, $userCount, $log );
        if ( $status_log eq "true" ) {
            $mssLog->info( $email.'|'.$platform.':JOB-'.$jobid.": UPDATING MSSOL HERE: $email, $com::owm::migBellCanada::STATUS_FAIL_INVALID_INPUT, $jobid, $message" );
        }
    } else {
        $log->error($email.':Cannot send START (code 100) and FAIL (code 150) to migration status API as API endpoint and/or WSDL are not defined');
    }
}

######################################################
# 
# Send a START update migration status API to probe
# the correct job id for the batch
#
######################################################
sub sendStartUser($$$$$$$) {
    my $MIGRATIONTYPE = shift;
    my $userCount = shift;
    my $userOffset = shift;
    my $email = shift;
    my $log = shift;
    my $jobid = shift;
    my $platform = shift;

    return if ($MIGRATIONTYPE eq "PRELOAD");

    my $status_endpoint = $cfg->param('status_endpoint');
    my $status_wsdl     = $cfg->param('status_wsdl');
    my $status_log      = $cfg->param('status_log');

    if (defined($status_endpoint) && ($status_endpoint ne '') && defined($status_wsdl)) {
        my ( $statusRes, $statusError, $jobID ) = com::owm::ManageMigrationStatus::ManageMigrationStatus( $status_endpoint, $status_wsdl, $email, $com::owm::migBellCanada::EVENT_MIGRATION_START, $jobid, 'WatcherStart', $userOffset, $userCount, $log );
        return($jobID);
    } else {
        $log->error($email.':Cannot send START (code 100) to migration status API as API endpoint and/or WSDL are not defined');
        return($jobid);
    }
}

######################################################
# read emails from batch file
# Batch files contain email addresses only
# Migration process for MSFT requires PUIDS
# This function converts the batch email file to a 
# batch puid file
#
# MX8 and MYI Batch files are of email addresses
######################################################
sub readEmailsFromBatch($){
    $log->debug("SCRIPT|readEmailsFromBatch .... ");
	print "Inside reademailsFromBatch......\n" if ($SDEBUG);
	my $file=shift;
	my ($retPUID,$retMboxID);
	
	#split file name out and add batchNumber (aka Jobid) to each output file
	$JOBID = "UNK9999";
	my @splitFile = split('-', $file);
	my $fileName = $splitFile[2];
	my @fileTS = split('/', $splitFile[0]);
	my $fileTimestamp = pop @fileTS;
	if (! $fileName ) {
		$log->error("SCRIPT|$file Input filename is in incorrect format, should already have been moved earlier in readFiles.");
		return;
    }
    my $fileRecordCount = `wc -l $file`;
	my($batchNumber, $dirs, $suffix) = fileparse($fileName, qr/\.[^.]*/);
	$JOBID = $batchNumber;
    if ($PRELOAD && $JOBID =~ /^\d+$/) {
        # TELSTRA-629 For preloads increment the JOBID by 1 billion
        # (using Milan's definition of 1 billion) to avoid conflicting with
        # job IDs from Telstra migration support solution
        if ($JOBID < 1000000000) {
            $JOBID += 1000000000;
        }
    }
	
    if ($enforce_unique_jobid eq "all" || ($enforce_unique_jobid eq "pre" && $PRELOAD eq "true")) {
        checkJobID($JOBID,$fileRecordCount);
        $batchNumber = $JOBID;
    }
    my $prefix = $fileTimestamp . "-" . $batchNumber . "-";
	$wlBatchFileName = $prefix . $wlBatchFileNameSuffix;
    $mx8BatchFileName = $prefix . $mx8BatchFileNameSuffix;
	$myiBatchFileName = $prefix . $myiBatchFileNameSuffix;
	$wlMailBatchFileName = $prefix . $wlMailBatchFileNameSuffix;
	$mx8MailBatchFileName = $prefix . $mx8MailBatchFileNameSuffix;
	$myiMailBatchFileName = $prefix . $myiMailBatchFileNameSuffix;
	$log->info("SCRIPT|$file Job ID $batchNumber. New File names $wlBatchFileName $mx8BatchFileName $myiBatchFileName");

	#Seed the random function one time
	my $random = time . $$;
	srand($random);

	$firstUserType = undef;
	my $rowCounter = 0;
	if (open(my $fh, '<', $file)) {
		my @rowList;
		while (my $row = <$fh>) {
			chomp $row;
			push(@rowList, $row);
		}
		my $totalRows = scalar(@rowList);
		USER: foreach my $row (@rowList) {
			$rowCounter++;
			my $userStarted = 0;
			my @emailArray = split(',', $row);
			my  $platform = $emailArray[1];
			$MIGTYPE = $platform;
            if ($MIGRATIONTYPE eq "PRELOAD") {
                $MIGTYPE .= 'P'; 
            }
			print "In reademailsfrombatch: $emailArray[0] $platform\n" if ($SDEBUG);
			if ($emailArray[0] =~ /^[^@]+@([-\w]+\.)+[a-z]{2,4}$/){
                # check user is in proxy mode in SUR
                my ($status, $mailboxstatus ) = ( com::owm::migBellCanada::lookupPUID( $emailArray[0], $cfg, $log ) )[0, 2];
                if ( $status eq $com::owm::migBellCanada::STATUS_SUCCESS ) {
                    #Allow migration only if the user is in PROXY mode.
                    if ( $cfg->param('onlyMigrateProxy') eq "true" && $mailboxstatus ne "P" ) {
                        $status = $com::owm::migBellCanada::STATUS_FAIL_NOT_ALLOWED;
                        $log->info($emailArray[0].':'.$status.':User mailboxstatus: '.$mailboxstatus);
                        $log->error("$emailArray[0]:$status:User cannot be migrated as user is not in proxy mode");
                        com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,$com::owm::migBellCanada::EVENT_NAMES{$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED},$MIGTYPE,$emailArray[0],$file,"Failed","User cannot be migrated as user is not in proxy mode. mailboxstatus=$mailboxstatus");
                        if ($mailboxstatus ne 'A') {
                        	sendFailedUser($MIGRATIONTYPE, $totalRows, $rowCounter, $emailArray[0], $log, "User cannot be migrated as user is not in proxy mode on MX9 mailboxstatus=$mailboxstatus", $JOBID, $userStarted, $platform);
                    	} else {
                    		$log->info($emailArray[0].':Not updating migration support solution as user has already been migrated');
                    		$mssLog->info($emailArray[0].'|'.$platform.':JOB-'.$JOBID.':Not updating migration support solution as user has already been migrated');
                    	}
                        next USER;
                    }
                }
                # can only send the START for the first user if the user is not already migrated (mailboxstats=A)
                # as we need to not send a START (100) event to the MSS in that situation, so this START call
                # needs to be immediately after the mailboxstatus check above
                if ($MIGRATIONTYPE ne "PRELOAD") {
                    if ($rowCounter == 1) {
                        my $jobId = sendStartUser($MIGRATIONTYPE, $totalRows, $rowCounter, $emailArray[0], $log, $JOBID, $platform);
                        $userStarted = 1;
                        $JOBID = $jobId;
                    }
                }
				if ($platform ne 'MYI') {
					#for WL we need to get the PUID and for mx8 we need to get the mailbox id. Do this in one step. Not required for MYI.
					($retPUID,$retMboxID) = com::owm::migBellCanada::lookupPuidFromEmail($emailArray[0],$cfg,$log);
				}
				if($platform eq 'WL'){              
                    #Check if we have a consent token for this user. If not do not put the user into the batch.
                    #The PUID is only used for logging, so a blank or incorrect PUID passed to getRefreshToken does not matter.
                    #Avoid doing the PUID lookup if the consent token is unavailable
                     my $refreshToken = com::owm::migBellCanada::getRefreshToken( $retPUID, $emailArray[0], $cfg, $log );
                    if ( !defined $refreshToken ) {
                        $log->warn("$emailArray[0]|$retPUID:Failed to get refresh token. User not included in this batch.");
                        com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,$com::owm::migBellCanada::EVENT_NAMES{$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED},$MIGTYPE,$emailArray[0],$file,"Failed","Failed to get refresh token.");
                        sendFailedUser($MIGRATIONTYPE, $totalRows, $rowCounter, $emailArray[0], $log, "Failed to get refresh token", $JOBID, $userStarted, $platform);
					} else {		
                        if($retPUID eq ''){
                            $log->error("$emailArray[0]| PUID is not found for this WL mailbox - recording exception.");
                            #force a default PUID
                            if ($cfg->param('forcePUID') eq "true" ) {
                                my $randPUID = 'FFF' . int(rand(10000000000));
                                $log->info("$emailArray[0]| A random PUID of $randPUID is being set for this user.");
                                my $status = com::owm::migBellCanada::setPUID($emailArray[0],$randPUID,$cfg,$log);
                                $mustWait = "true"; #force a wait for replication at end of processing
                                if ( $status ) {
                                        $log->error("$emailArray[0]|$randPUID: An error occurred status $status . PUID not updated.");
                                        $conflictLog->error("$emailArray[0]|$randPUID: An error occurred status $status . Force setting PUID, ldap could not updated.");
                                        com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,$com::owm::migBellCanada::EVENT_NAMES{$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED},$MIGTYPE,$emailArray[0],$file,"Failed","$randPUID: An error occurred status $status . PUID not updated.");
                                        sendFailedUser($MIGRATIONTYPE, $totalRows, $rowCounter, $emailArray[0], $log, "An error occurred status $status. Force setting random PUID $randPUID, ldap could not updated", $JOBID, $userStarted, $platform);
                                } else {
                                    push(@wlBatchList, $randPUID.', '.$emailArray[0]);
                                    $firstUserType = 'WL';
                                    if ($preloadMailBatchInParallel && ($MIGRATIONTYPE eq 'PRELOAD')) {
                                        push(@wlMailBatchList, $emailArray[0]);
                                    }
                                }
                            } else {
                                $conflictLog->info("$emailArray[0]| PUID is not found for this WL mailbox.");
                                com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,$com::owm::migBellCanada::EVENT_NAMES{"$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED"},$MIGTYPE,$emailArray[0],$file,"Failed","PUID is not found for this WL mailbox.");
                                sendFailedUser($MIGRATIONTYPE, $totalRows, $rowCounter, $emailArray[0], $log, "PUID is not found for this WL mailbox", $JOBID, $userStarted, $platform);
                            }
                        } elsif($retPUID eq 'many'){ 
                            $log->error("$emailArray[0]| Many PUIDs found for this WL mailbox - cannot start migration.");
                            com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,$com::owm::migBellCanada::EVENT_NAMES{"$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED"},$MIGTYPE,$emailArray[0],$file,"Failed","Many PUIDs found for this WL mailbox.");
                            sendFailedUser($MIGRATIONTYPE, $totalRows, $rowCounter, $emailArray[0], $log, "Many PUIDs found for this WL mailbox", $JOBID, $userStarted, $platform);
                        } else {
                            push(@wlBatchList, "$retPUID,$emailArray[0]");
                            if ($preloadMailBatchInParallel && ($MIGRATIONTYPE eq 'PRELOAD')) {
                                push(@wlMailBatchList, $emailArray[0]);
                            }
                        }
                    }
				} elsif($platform eq 'MX8'){
					# build batch file of email address, mailbox is for MX8
					push(@mx8BatchList, "$emailArray[0],$retMboxID");
					$firstUserType = 'MX8';
					if ($preloadMailBatchInParallel && ($MIGRATIONTYPE eq 'PRELOAD')) {
						push(@mx8MailBatchList, $emailArray[0]);
					}
				} elsif($platform eq 'MYI'){
					# build batch file of email address for MYI
					push(@myiBatchList, $emailArray[0]);
					$firstUserType = 'MYI';
					if ($preloadMailBatchInParallel && ($MIGRATIONTYPE eq 'PRELOAD')) {
						push(@myiMailBatchList, $emailArray[0]);
					}
				} else {
					$log->error("$emailArray[0]|\"$platform\" is not a recognised platform");
                    com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,$com::owm::migBellCanada::EVENT_NAMES{"$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED"},$MIGTYPE,$emailArray[0],$file,"Failed","Platform ($platform) in batchfile is not valid");
                    sendFailedUser($MIGRATIONTYPE, $totalRows, $rowCounter, $emailArray[0], $log, "Platform ($platform) in batchfile is not valid", $JOBID, $userStarted, $platform);
				}
			}else{
				print "$emailArray[0] email id is not valid \n" if ($SDEBUG);
				$log->error("$emailArray[0]|  email id is not valid");
				com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,$com::owm::migBellCanada::EVENT_NAMES{"$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED"},$MIGTYPE,$emailArray[0],$file,"Failed","Email address is not valid");
				sendFailedUser($MIGRATIONTYPE, $totalRows, $rowCounter, $emailArray[0], $log, "Email address is not valid", $JOBID, $userStarted, $platform);
            }
		}
	} else {
	  $log->error("SCRIPT|$fileName Could not open file  $!");
	  #Move file back to original directory
	  moveFileBack($file,$file);
	}

	#finished processing this batch file move across to completed directroy
   #Move the batch file into a FINISHED directory.
   my $completedDirPath = $cfg->param('completed_path');
   mkdir $completedDirPath unless -d $completedDirPath; 
   move($file,$completedDirPath) or $log->error("SCRIPT|$file Unable to move batch file to completed directory for job id $JOBID: $!");
   
	#write PUID into file
	if(@wlBatchList || @mx8BatchList || @myiBatchList){
		writePUIDOfEmail();
	}
}


######################################################
# write PUIDs into file
######################################################

sub writePUIDOfEmail{
    $log->debug("SCRIPT|writePUIDOfEmail .... ");
	#open file once, then write all PUIDs not open and close each time - MCB  ****
	#Create directory if DIRPATH does not exist
	mkdir -p $microsoftDirPath unless -d $microsoftDirPath; 
	mkdir -p $openwaveDirPath unless -d $openwaveDirPath; 
	mkdir -p $myInboxDirPath unless -d $myInboxDirPath; 
	
	print "writePUID: WL ".eval($#wlBatchList + 1)." \n " if ($SDEBUG && $#wlBatchList > -1);
	if (@wlBatchList) {
		open FH, ">$microsoftDirPath/$wlBatchFileName" or $log->logdie("SCRIPT|$microsoftDirPath/$wlBatchFileName can't open file  $!");
		foreach (@wlBatchList) {
			print FH $_."\n";
		}
		close FH;
		print ("invoking batch for".$wlBatchFileName."\n") if ($SDEBUG);
		invokeMigrateBatch($wlBatchFileName,$microsoftDirPath,"WL");
	} else {
		$log->info("SCRIPT|Empty Microsoft batchfile !!!");
	}

	if ($preloadMailBatchInParallel && ($MIGRATIONTYPE eq "PRELOAD")) {
		print "writePUID: WL MAIL ".eval($#wlMailBatchList +1)." \n " if ($SDEBUG && $#wlMailBatchList > -1);
		if (@wlMailBatchList) {
			open FH, '>', $microsoftDirPath.'/'.$wlMailBatchFileName or $log->logdie('SCRIPT|'.$microsoftDirPath.'/'.$wlMailBatchFileName.' can not open file '.$!);
			foreach (@wlMailBatchList) {
				print FH $_."\n";
			}
			close FH;
			print ('invoking mail batch for'.$wlMailBatchFileName."\n") if ($SDEBUG);
			invokeRunMailBatch($wlMailBatchFileName, $microsoftDirPath, 'WL');
		} else {
			$log->info('SCRIPT|Empty Microsoft Mail batchfile !!!');
		}
	}

	print "writePUID: MX8  ".eval($#mx8BatchList + 1)." \n" if ($SDEBUG && $#mx8BatchList > -1);
	if (@mx8BatchList) {
		open FH, ">$openwaveDirPath/$mx8BatchFileName" or $log->logdie("SCRIPT|$openwaveDirPath/$mx8BatchFileName  can't open file : $!");
		foreach (@mx8BatchList) {
			print FH $_."\n";
		}
		close FH;
		print ("invoking batch for".$mx8BatchFileName."\n") if ($SDEBUG);
		my $result = invokeAutoReplyExport($mx8BatchFileName,$openwaveDirPath);
		invokeMigrateBatch($mx8BatchFileName,$openwaveDirPath,"MX8");
	} else {
        $log->info("SCRIPT|Empty Openwave batchfile !!!");
    }

	if ($preloadMailBatchInParallel && ($MIGRATIONTYPE eq "PRELOAD")) {
		print "writePUID: MX8 MAIL ".eval($#mx8MailBatchList + 1)." \n " if ($SDEBUG && $#mx8MailBatchList > -1);
		if (@mx8MailBatchList) {
			open FH, '>', $openwaveDirPath.'/'.$mx8MailBatchFileName or $log->logdie('SCRIPT|'.$openwaveDirPath.'/'.$mx8MailBatchFileName.' can not open file '.$!);
			foreach (@mx8MailBatchList) {
				print FH $_."\n";
			}
			close FH;
			print ('invoking mail batch for'.$mx8MailBatchFileName."\n") if ($SDEBUG);
			invokeRunMailBatch($mx8MailBatchFileName, $openwaveDirPath, 'MX8');
		} else {
			$log->info('SCRIPT|Empty MX8 Mail batchfile !!!');
		}
	}

	print "writePUID: MYI ".eval($#myiBatchList + 1)."\n" if ($SDEBUG && $#myiBatchList > -1);	
	if (@myiBatchList) {
		open FH, ">$myInboxDirPath/$myiBatchFileName" or $log->logdie("SCRIPT|$myInboxDirPath/$myiBatchFileName  can't open : $!");
		foreach (@myiBatchList) {
			print FH $_."\n";
		}
		close FH;
		print ("invoking batch for".$myiBatchFileName."\n") if ($SDEBUG);
		invokeMigrateBatch($myiBatchFileName,$myInboxDirPath,"MYI");
	} else  {
		$log->info("SCRIPT|Empty MyInbox batchfile !!!");
	}	

	if ($preloadMailBatchInParallel && ($MIGRATIONTYPE eq "PRELOAD")) {
		print "writePUID: MYI MAIL ".eval($#myiMailBatchList + 1)." \n " if ($SDEBUG && $#myiMailBatchList > -1);
		if (@myiMailBatchList) {
			open FH, '>', $myInboxDirPath.'/'.$myiMailBatchFileName or $log->logdie('SCRIPT|'.$myInboxDirPath.'/'.$myiMailBatchFileName.' can not open file '.$!);
			foreach (@myiMailBatchList) {
				print FH $_."\n";
			}
			close FH;
			print ('invoking mail batch for'.$myiMailBatchFileName."\n") if ($SDEBUG);
			invokeRunMailBatch($myiMailBatchFileName, $myInboxDirPath, 'MYI');
		} else {
			$log->info('SCRIPT|Empty MyInbox Mail batchfile !!!');
		}
	}

}
######################################################
# call autoReplyExport script
######################################################
sub invokeAutoReplyExport($$){
	my $fileName=shift;
	my $filePath=shift;
	my $return;
	my $file =$filePath.'/'.$fileName;
	my $autoReplyToolCmd = $cfg->param('autoreply_tool_cmd');
	my $targetDir = $cfg->param('userProfileRootMx8');
	

	 $autoReplyToolCmd =~ s/<file>/$file/;
	 $autoReplyToolCmd =~ s/<targetDir>/$targetDir/;

	 if ($SDEBUG) {
		$autoReplyToolCmd =~ s/<debug>/-d/;
	} else {
		$autoReplyToolCmd =~ s/<debug>//; 
	}
	$log->info("SCRIPT|$file Executing auto reply export for batch.");
	print "Invoking auto reply extract with  ".$autoReplyToolCmd. "\n\n" if ($SDEBUG);
	$MIGTYPE = "MX8";
	$MIGTYPE .= "P" if ($MIGRATIONTYPE eq "PRELOAD");
	$log->debug("SCRIPT|$file Invoking auto reply  with   ".$autoReplyToolCmd) if $SDEBUG;
	
	$return = `$autoReplyToolCmd`;
	$log->debug("SCRIPT|$file autoReplyToolCmd res = ".$return) if $SDEBUG;
	print $return . "\n" if ($SDEBUG);
	my $successStr = 'Mailbox';
	  if ( index($return, $successStr) != -1  ) {
		return($com::owm::migBellCanada::STATUS_SUCCESS);
	  } else {
		com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_FAIL_AUTOREPLY,'AutoReplyExtractFailed',$MIGTYPE,$file,'','Fail','Error retrieving auto reply data for batch');
		return($com::owm::migBellCanada::STATUS_FAIL_AUTOREPLY);
		$log->error("SCRIPT|$file Executing auto reply export for batch failed.");
	  }
}


######################################################
# call migrateBatch script
######################################################
sub invokeMigrateBatch($$$){
	my $fileName=shift;
	my $filePath=shift;
	my $migType=shift;
	my $runInBG = $cfg->param('migrateBatchInBackground');
	my $return;
	#my $fileDir = '/opt/watcherMigration';
	my $file =$filePath.'/'.$fileName;
	my $migrToolCmd = $cfg->param('migrateBatch_tool_cmd');
	my $migrOutputFilePath = $cfg->param('debug_output_path');
	my $migrOutputFileName = $cfg->param('debug_output_filename');
	
	mkdir $migrOutputFilePath unless -d $migrOutputFilePath;  
	
	$migrOutputFileName =~ s/<processid>/$$/;
	$migrOutputFileName =~ s/<jobid>/$JOBID/;
	my $migrOutputFile =  $migrOutputFilePath . "/" . $migrOutputFileName;
	#Get number of records to process
	my $recordCount = `wc -l $file`;
	($recordCount, my $tmp) = split(' ',$recordCount);	
	#if changes have been made to the LDAP entry, force a wait for replication.
	if ($mustWait eq "true") {
        if ($recordCount < $pauseLevel) {
            print __LINE__ . " Pausing for ".$pauseSec." to allow for replication \n" if ($SDEBUG);
            sleep($pauseSec);
        } else {
            print "Skipping pause due to large batch \n" if ($SDEBUG);
        }
        $mustWait = "false";
    }

	$migrToolCmd =~ s/<file>/$file/;
	$migrToolCmd =~ s/<mode>/$migType/;
	$migrToolCmd =~ s/<jobid>/$JOBID/;
	$migrToolCmd =~ s/<outputfile>/$migrOutputFile/;
	if ($SDEBUG) {
        $migrToolCmd =~ s/<debug>/-d/;
    } else {
        $migrToolCmd =~ s/<debug>//; 
    }
    print "Migration type is $MIGRATIONTYPE \n\n" if ($SDEBUG);
    if ($MIGRATIONTYPE eq "PRELOAD") {
        $migrToolCmd =~ s/<preload>/-preload true/;
    } else {
        if ($migType eq $firstUserType) {
            $migrToolCmd =~ s/<preload>/-firstUserStartSent/;
        } else {
            $migrToolCmd =~ s/<preload>//;
        }
    }

	$migType .= 'P' if ($MIGRATIONTYPE eq "PRELOAD");
	
	$log->info("SCRIPT|$file Executing migration run. $recordCount to process");
	print "Invoking migrate batch with  ".$migrToolCmd. " $recordCount to process \n\n" if ($SDEBUG);
	
	
	com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::EVENT_BATCHFILE_START,'BatchFileStart',$migType,$file,'','','About to start migration for this batch. '.$recordCount.' to process.');

	if ( $runInBG eq "true" ) {
		$migrToolCmd = "nohup " . $migrToolCmd . " &";
		$log->debug("SCRIPT|$file Invoking migrate batch with   ".$migrToolCmd."\n") if $SDEBUG;
		`$migrToolCmd`;
	} else {
		$log->debug("SCRIPT|$file Invoking migrate batch with   ".$migrToolCmd."\n") if $SDEBUG;
		$return = `$migrToolCmd`;
		$log->debug("SCRIPT|$file migrToolCmd res = ".$return) if $SDEBUG;
	}
	
	print $return . "\n" if ($SDEBUG);
}

######################################################
# Call the batch version of the java migration tool
######################################################
sub invokeJavaTool($$$) {
	my $fileName = shift;
	my $filePath = shift;
	my $migType = shift;
	my $runInBG = $cfg->param('migrateMailInBackground');
	my ($javaToolCmd, $javaToolDir);
	my $file = $filePath.'/'.$fileName;

	if ((uc($migType) eq 'MX8') || (uc($migType) eq 'MYI')) {
		$javaToolCmd = $cfg->param('owm_migration_tool_batch_cmd');
		$javaToolCmd = $cfg->param('owm_migration_tool_batch_cmd');
		$javaToolDir = $cfg->param('owm_migration_tool_dir_preload');
	} elsif (uc($migType) eq 'WL') {
		$javaToolCmd = $cfg->param('migration_tool_batch_cmd');
		$javaToolDir = $cfg->param('migration_tool_dir_preload');
	} else {
		$log->error('SCRIPT|'.$file.' Unknown migration type "'.$migType.'"');
		return;
	}

	my $pwdOrig = cwd();
	$log->debug("SCRIPT|$file Calling Java Migration Tool ... from $javaToolDir ");
	$log->debug("SCRIPT|$file cd to ".$javaToolDir) if $SDEBUG;
	$javaToolCmd =~ s/<batchfile>/$file/;
	chdir($javaToolDir);
	if ( $runInBG eq "true" ) {
		$javaToolCmd = 'nohup ' . $javaToolCmd . ' &';
		$log->debug("SCRIPT|$file Invoking mail migration batch with   ".$javaToolCmd."\n") if $SDEBUG;
		`$javaToolCmd`;
	} else {
		$log->debug("SCRIPT|$file Invoking mail migration batch with   ".$javaToolCmd."\n") if $SDEBUG;
		my $return = `$javaToolCmd`;
		$log->debug("SCRIPT|$file javaToolCmd res = ".$return) if $SDEBUG;
	}
	$log->debug("SCRIPT|$file javaToolCmd = ".$javaToolCmd) if $SDEBUG;
	chdir($pwdOrig);
}

######################################################
# Call the batch version of the java migration tool
# via the intermediate script that also processes
# the trace logs
######################################################
sub invokeRunMailBatch($$$)
{
	my $fileName = shift;
	my $filePath = shift;
	my $migType = shift;

	my $file = $filePath.'/'.$fileName;
	my $runInBG = $cfg->param('migrateMailInBackground');

	my @command = ( './runMailBatch.plx', '--batchFile='.$file, '--JOBID='.$JOBID, '--mode='.$migType);
	push(@command, '--debug') if $SDEBUG;
	if ( $runInBG eq "true" )
	{
		# this is equivalent of using nohup <command> & , except it lets me
		# use system() instead of backticks, which prevents odd shell
		# escape issues
		my $pid = fork();
		unless (defined($pid))
		{
			$log->error('SCRIPT|'.$file.' Cannot fork child process in order to migrate mail: '.$!)
		}
		if ($pid == 0)
		{
			# child process
			$dbh->{InactiveDestroy} = 1;
			undef($dbh);
			setsid(); # generate new process control session
			open(STDIN, '<', '/dev/null'); # detatch from controlling terminal
			open(STDOUT, '>', '/dev/null'); # detatch from controlling terminal
			open(STDERR, '>', '/dev/null'); # detatch from controlling terminal
			exec(@command); # run command
			exit(0);
		}
		else
		{
			$log->debug('SCRIPT|'.$file.' Invoking mail migration batch in background with "'.join(' ', @command).'"'."\n") if $SDEBUG;
		}
	}
	else
	{
		$log->debug('SCRIPT|'.$file.' Invoking mail migration batch with "'.join(' ', @command).'"'."\n") if $SDEBUG;
		system(@command);
		$log->debug("SCRIPT|$file runMailBatch res = ".$?) if $SDEBUG;
	}
}

######################################################
# moveFile
# Moves the files specified to the processing directory
# Starts processing the batch file.
######################################################
sub moveFile($$) {
	my $filePath = shift;
	my $file = shift;
	my $finalPath = $filePath.'/'.$file;
	move($finalPath ,$processDirPath)or $log->error("SCRIPT|$file The move operation failed: $!");
	my $processingFilePath = $processDirPath.'/'.$file;
	#call readEmailsFromBatch function
	#TODO: For performance does this need to be a multithreaded process. Could have 1000 or more records in a batch file.
	#readEmailsFromBatch($processingFilePath);
}

######################################################
# moveFilesBack to Original Location
# Moves the files specified to the processing directory
# Starts processing the batch file.
######################################################
sub moveFileBack($$) {
	my $filePath = shift;
	my $file = shift;
	#my $finalPath = $filePath.'/'.$file;
	my $finalPath = $processDirPath.'/'.$file;
	print "in moveback Final Path : $finalPath\n";
	print "in moveback filePath: $filePath\n";
	
    #move($finalPath ,$processDirPath)or $log->logdie("SCRIPT|$file The move operation failed: $!");
	move($finalPath ,$filePath)or $log->error("SCRIPT|$file The move back operation failed: $!");
}
######################################################
# invalidFilename
# will write an event log for each user found in invalidFilename
# with the count not start migration status
######################################################
sub invalidFilename{
	my $file = shift;
	if (open(my $fh, '<', $file)) {
		while (my $row = <$fh>) {
			chomp $row;
			my @emailArray = split(',', $row);
			com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,"MigrationNotStarted",'',$emailArray[0],$file,'','Could not start, input file is not in the correct format');
		}
	} else {
		$log->error("SCRIPT|$file cound not open $!");
	}
}

######################################################
# readFiles
# reads each file found in the read directory
# and moves to the processing directory
# Directory paths must be fully qualified
######################################################
sub readFiles($$) {

	my $readDirPath = shift;
	my $migrationType = shift;
	my $processCount = 0;
	$log->debug("SCRIPT|In readFiles:$readDirPath\n");
	print "In readFiles:$readDirPath\n" if ($SDEBUG);
	opendir(DIR, $readDirPath) or $log->logdie("SCRIPT|$readDirPath Could not read : $!");
	
	while (my $file = readdir(DIR)) {
	    #skip if this is a system file 
		next if ($file =~ m/^\./);	
		print "In readFiles: reading $file\n" if ($SDEBUG);
		$processCount++;
		### Pass each batch file through update4Mx to set the home location ##
		#check the filename is in three parts, if not skip processing
		my @splitFile = split('-', $file);
		my $fileName = $splitFile[2];
		my $fullPathName = $readDirPath."/".$file;
		if (! $fileName ) {
			invalidFilename($fullPathName);
			if (move($fullPathName,$errorDirPath)) {
				$log->error("SCRIPT|$file Input filename is in incorrect format, moving to $errorDirPath and skipping");
			} else {
				$log->error("SCRIPT|$file Input filename is in incorrect format, unable to move to $errorDirPath. $!");
			}
			$DBMESSAGE .= $file . ' (skipped), ';
			next;
		} else {
			#only print the skipped files in the event table as processed files have individual entry already
			$DBSUCCESS++;
		}

		# call move file before calling update4Mx9.(changes for jira ticket TELSTRA-144 )
		$log->info("SCRIPT|$file moving file to processing before calling update4Mx9.");
		moveFile($readDirPath,$file);


		$log->debug("SCRIPT|$file About to process home location using update4Mx9 -realm");
		#my $fName = $readDirPath . "/" . $file;
		my $fName = $processDirPath . "/" . $file;

		#calling update4Mx9 for update Mx
		my $dbmessagerealm = "";
		my $dbstatusrealm = "";
		my $update4MxToolCmd = $cfg->param('update4Mx9_tool_cmd');
			$update4MxToolCmd  =~ s/<file>/$fName/;
			if ($SDEBUG) {
					$update4MxToolCmd  =~ s/<debug>/-d/;
			}else{
					$update4MxToolCmd  =~ s/<debug>//;
			}
					#Get number of records to process
		my $recordCount = `wc -l $fName`;
		($recordCount, my $tmp) = split(' ',$recordCount);
		$log->debug("SCRIPT|$file Calling update4MxToolCmd  ------ : $update4MxToolCmd ");
		$log->debug("SCRIPT|$file Calling update4MxToolCmd  ------ : $recordCount accounts to process");
		print (" Calling update4MxToolCmd  ------ : $update4MxToolCmd $recordCount accounts to process \n") if ($SDEBUG);

		my $return = `$update4MxToolCmd`;
		$log->debug("SCRIPT|Call to update4Mx returned $return");
		if ($return == $com::owm::migBellCanada::STATUS_GENERAL_ERROR) {
		    $log->error("SCRIPT|$file Realm update failed. Skip processing this batch ");
			$dbmessagerealm = "Realm update failed. Skip processing this batch";
			$dbstatusrealm="Failed";
			$log->error("SCRIPT|$file moving file back to original location as Realm update failed.");
			moveFileBack($readDirPath,$file);
		} elsif ($return == $com::owm::migBellCanada::STATUS_LOUD_SUCCESS) {
			$dbmessagerealm = "Realm update has completed";
			$dbstatusrealm="Success";
			#moveFile($readDirPath,$file);
			# instead of moveFile calling readEmailsFromBatch after update4Mx got success
			my $processingFilePath = $processDirPath.'/'.$file;
            # The dircaches need a bit of time to ensure ldap is updated. For small or single batches this is essential.
            print __LINE__ . " Pausing for ".$pauseSec." to allow for replication \n" if ($SDEBUG);
            sleep($pauseSec);
			readEmailsFromBatch($processingFilePath);
		} else {
		    $log->error("SCRIPT|$file Realm update failed. Skip processing this batch ");
			$dbmessagerealm="Realm update has finished without returning a result code. Likely failed.";
			$dbstatusrealm="Suspect Fail";
			moveFileBack($readDirPath,$file);
		}    
		com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::EVENT_REALM_PROCESS,'UpdateRealm','',$fName,'',$dbstatusrealm,$dbmessagerealm); 
		######### end home location processing ################
		
	}
	
	if ($processCount == 0 ) {
		$log->info("SCRIPT|No batch files found to process for $MIGRATIONTYPE");			
	}
	$log->debug("SCRIPT|Leaving readFiles:$readDirPath\n");
	closedir(DIR);
}

sub isJobIDUnique {
    my $id = shift;

    my $query = $dbh->prepare("SELECT * FROM jobs WHERE job_id=?");
    my $result = $query->execute($id) or die $query->errstr;

    if ($result == "0E0") { # 0E0 is empty result
        return 0;
    } else {
        return 1;
    }
}

sub checkJobID {
    my $job = shift;
    my $fileRecordCount = shift;
    my $hostname = hostname;

    while (isJobIDUnique($job) == 1) {
        $job++;
    }

    my $createTime = localtime();
    if ($job != $JOBID) {
        $log->info("SCRIPT|JOB-$JOBID Job ID already in use. New ID: $job");
        my $query = $dbh->prepare("INSERT INTO jobs (job_id, old_job_id, run_time, migtype, record_count,migration_host,description) VALUES (?, ?, now(), ?, ?, ?, ?)");
        my $result = $query->execute($job, $JOBID, $MIGRATIONTYPE, $fileRecordCount, $hostname, $JOBDESCRIPTION) or $log->logdie("SCRIPT|JOB-$JOBID ".$DBI::errstr); 

        $JOBID = $job;
    } else {
        #Record job id to database
        #TODO: if enforce unique job id is false still need to record it to the db for later
        $log->info("SCRIPT|JOB-$JOBID Unique");
        my $query = $dbh->prepare("INSERT INTO jobs (job_id, run_time, migtype, record_count, migration_host, description) VALUES (?, now(), ?, ?, ?, ?)");
        my $result = $query->execute($JOBID, $MIGRATIONTYPE, $fileRecordCount, $hostname, $JOBDESCRIPTION) or $log->logdie("SCRIPT|JOB-$JOBID ".$DBI::errstr);
    }
}
######################################################
# ---------------------------------------------------
# Start Main
# ---------------------------------------------------
######################################################
$SDEBUG = $cfg->param('log_debug');

$log->info("SCRIPT|******* Migration Watcher process begins.");

my $ONDEMAND = '';
my $SCHEDULED = '';
$PRELOAD = '';
my $mydir = '';
my $dbmigtype;
my $checkImail;
$DBSTATUS = "";
$DBMESSAGE = "";
GetOptions('-d', \$SDEBUG,
           '-ondemand', \$ONDEMAND,
           '-preload', \$PRELOAD,
           '-scheduled', \$SCHEDULED,
           '-noimail', \$checkImail,
           '-desc=s', \$JOBDESCRIPTION)|| usage(); 

# Debug out info
if ($SDEBUG) {
  $log->level($DEBUG); #Override debug in log file when -d or debug set in config file
  print("Running in debug mode. Process id = $PROCESSID \n");
}

unless ( defined $checkImail ) {
	my $intermail = `echo \$INTERMAIL`;
	chomp($intermail);
	print "intermail check result \'$intermail\' \n" if ($SDEBUG);
		if ( ! $intermail || $intermail eq "" || not defined $intermail || $intermail eq "\n") {
				$log->error("SCRIPT| Cannot be started. Must be run as Mx8 IMAIL user");
				print "Script must be run as an MX8 IMAIL user \n" if ($SDEBUG);
				com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,"MigrationNotStarted",'','','','','Could not start, must run as an Mx8 IMAIL user');
				usage();
		}
}

# Check if dumpSetup is running if true then log error and exit.
my $dumpProcessStatus = qx(ps -ef | grep -i dumpSetup | grep -v grep);

#Read value from config file of dump_pid 
#check if that dump file is exist if true then exit.
#TODO: May need to change to a DB lookup instead
my $dump=$cfg->param('dump_pid');

if(-e $dump){
	$log->error("SCRIPT|The file dumpsetup.pid exists ... exiting from migration.");
	$DBSTATUS = 'failed';
	$DBMESSAGE = "Dump setup is currently running. Exiting";
} elsif($dumpProcessStatus  != ''){
	$log->error("SCRIPT|DumpSetup process is running ... exiting from migration.");
	$DBSTATUS = 'failed';
	$DBMESSAGE = "Dump setup is currently running. Exiting";
} else{
	#Create any infile directories that do not exist
	if ( (!$ONDEMAND) && (!$PRELOAD) && (!$SCHEDULED) ){
		#no option was passed so process all directories except PRELOAD
		$ONDEMAND=1;
		$SCHEDULED=1;
		$log->info("SCRIPT|Processing all directories");
	}
	if($ONDEMAND){
		$DBMESSAGE .= 'ONDEMAND - ';
		$MIGRATIONTYPE='ONDEMAND';
		$log->debug("SCRIPT|Copying files from ondemand dir to processing...");
		$mydir = $cfg->param('ondemand_path');
		chomp($mydir);
		mkdir $mydir unless -d $mydir;
		$log->debug("SCRIPT|mydir = $mydir");
		print "mydir = $mydir\n" if ($SDEBUG);
		if (! -d $mydir) {
			$log->error("SCRIPT|Unable to make $mydir $!");
			$DBMESSAGE .= "Unable to make ". $mydir . " " . $!. " Could not complete processing";
			$DBSUCCESS = 1;
			$dbmigtype .= 'ondemand ';
		} else {
			readFiles($mydir,"ONDEMAND");
			$DBMESSAGE .= 'Ondemand Files Processed: '.$DBSUCCESS;
			$DBSUCCESS = 0;
			$dbmigtype .= 'ondemand ';	
		}
	} 
	
	if($PRELOAD){
		$DBMESSAGE .= ' PRELOAD - ';
		$MIGRATIONTYPE='PRELOAD';
		$log->debug("SCRIPT|Copying files from preload dir to processing...");
		$mydir = $cfg->param('preload_path');
		chomp($mydir);
		$log->debug("SCRIPT|mydir = $mydir");
		print "mydir = $mydir\n" if ($SDEBUG);
		mkdir $mydir unless -d $mydir;
		if (! -d $mydir) {
			$log->error("SCRIPT|Unable to make $mydir $!");
			$DBMESSAGE .= "Unable to make ". $mydir . " " . $!. " Could not complete processing";
			$DBSUCCESS = 1;
			$dbmigtype .= 'preload ';
		} else {
			readFiles($mydir,"PRELOAD");
			$DBMESSAGE .= 'Preload Files Processed: '.$DBSUCCESS;
			$DBSUCCESS = 0;
			$dbmigtype .= 'preload ';	
		}
	} 
	
	if($SCHEDULED){
	#check SCHEDULED dir and move files if any.
		$DBMESSAGE .= ' SCHEDULED - ';
		$MIGRATIONTYPE='SCHEDULED';
		$log->debug("SCRIPT|Copying files from scheduled dir to processing ...");
		#$mydir = "/opt/watcherMigration/scheduled";
		$mydir = $cfg->param('scheduled_path');
		chomp($mydir);
		$log->debug("SCRIPT|mydir = $mydir");
		print "mydir = $mydir\n" if ($SDEBUG);
		mkdir $mydir unless -d $mydir;
		if (! -d $mydir) {
			$log->error("SCRIPT|Unable to make $mydir $!");
			$DBMESSAGE .= "Unable to make ". $mydir . " " . $!. " Could not complete processing";
			$DBSUCCESS = 0;
			$dbmigtype .= 'scheduled ';
		} else {
			readFiles($mydir,"SCHEDULED");
			$DBMESSAGE .= 'Scheduled Files Processed: '.$DBSUCCESS;
			$DBSUCCESS = 0;
			$dbmigtype .= 'scheduled ';
		}
	} 
#	else{
#
#	#check all the directories and move files if any.
#	#If no option passed, process all files in the directories
#		$mydir = $cfg->param('ondemand_path');
#		chomp($mydir);
#		mkdir $mydir unless -d $mydir;
#		$log->debug("SCRIPT|mydir = $mydir");
#		print "mydir = $mydir\n" if ($SDEBUG);
#		$DBMESSAGE .= ' ONDEMAND - ';
#		readFiles($mydir,"ONDEMAND");
#		$DBMESSAGE .= 'Processed: '.$DBSUCCESS;
#		$DBSUCCESS = 0;
#		$dbmigtype .= 'ALL ';	
#
#		#$mydir = "/opt/watcherMigration/preload";
#		$mydir = $cfg->param('preload_path');
#		chomp($mydir);
#		mkdir $mydir unless -d $mydir;
#		$log->debug("SCRIPT|mydir = $mydir");
#		print "mydir = $mydir\n" if ($SDEBUG);
#		$DBMESSAGE .= ' PRELOAD - ';
#		readFiles($mydir,"PRELOAD");
#		$DBMESSAGE .= 'Processed: '.$DBSUCCESS;
#		$DBSUCCESS = 0;
#
#		#$mydir = "/opt/watcherMigration/scheduled";
#		$mydir = $cfg->param('scheduled_path');
#		chomp($mydir);
#		mkdir $mydir unless -d $mydir;
#		$log->debug("SCRIPT|mydir = $mydir");
#		print "mydir = $mydir\n" if ($SDEBUG);
#		$DBMESSAGE .= ' SCHEDULED - ';
#		readFiles($mydir,"SCHEDULED");
#		$DBMESSAGE .= 'Processed: '.$DBSUCCESS;
#		$DBSUCCESS = 0;
#	}

}
  my $time_taken = time - $start;
com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::EVENT_WATCHER_START,'Watcher',$dbmigtype,'','','Finished','Took '.$time_taken.' seconds: '.$DBMESSAGE);

# CLOSE DATABASE CONNECTION
$log->info("SCRIPT|closing database connection.");
com::owm::migBellCanada::disconnectDB($dbh,$cfg,$log);

   
   
$log->info("SCRIPT|******* Watcher migration done, took $time_taken seconds to complete.");
print "\n";
exit 0;	
