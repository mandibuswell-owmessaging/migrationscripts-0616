#!/usr/bin/env python

#############################################################################
#
#  Copyright 2015 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 myinbox-addressbook-export.py
#  Description:             Export MyInbox contact data to ICS file
#  Author:                  Gary Palmer - Openwave Messaging
#  Date:                    Nov, 2015
#  Customer:                Telstra
#  Version:                 1.0.0 - Nov 2015
#
#############################################################################
#
#  Version Control
#
#  Version: 1.0.0
#  Changes:
#
#############################################################################

# pylint: disable=C0103
# pylint: disable=W0703

"""
Contact the Telstra MyInbox SOAP service and extract a users address book
to write to an vcard format file
"""

from base64 import urlsafe_b64decode
import codecs
from collections import defaultdict
from httplib import BadStatusLine
from optparse import OptionParser
import pytz
from suds.client import Client
import sys
import traceback
import vobject

from pprint import pprint

def log_trace(data):
    """
    Format trace events.  Normally multi-line so need a tag on each line

    :param data: Tracing data
    """
    print u'TRACE:{0}'.format('-' * 40)
    print u'TRACE:{0}'.format(data.replace("\r\n", "\n").
                              replace("\n", "\nTRACE:"))
    sys.stdout.flush()

def log_statistic(stat, value):
    """
    Send a statistic to the calling process

    :param event: Data to log
    """
    print u'STAT:{0}:{1}'.format(stat, value)
    sys.stdout.flush()

def log_debug(event):
    """
    Format an debug level log event

    :param event: Data to log
    """
    print u'DEBUG:{0}'.format(event)
    sys.stdout.flush()

def log_info(event):
    """
    Format an info level log event

    :param event: Data to log
    """
    print u'INFO:{0}'.format(event)
    sys.stdout.flush()

def log_warn(event):
    """
    Format an warning level log event

    :param event: Data to log
    """
    print u'WARN:{0}'.format(event)
    sys.stdout.flush()

def log_error(event):
    """
    Format an error level log event

    :param event: Data to log
    """
    print u'ERROR:{0}'.format(event)
    sys.stdout.flush()

def log_fatal(event):
    """
    Format an fatal level log event

    :param event: Data to log
    """
    print u'FATAL:{0}'.format(event)
    sys.stdout.flush()

def write_contacts_to_vcf(vcards, file_path):
    """
    Takes an array of vobject vCard files and writes them to a single file
    :param vcards:
    :return:
    """
    exported = 0
    with codecs.open(file_path, 'w') as contacts_vcf:
        for vcard in vcards:
            vcard.serialize(buf=contacts_vcf)
            exported += 1
    log_statistic(u'contactsExported', exported)

def fetchAll(client, options):
    """
    Call the getAll service and return a list of contact IDs (cids)

    :param client: A SUDS client object for the calendar WSDL
    :param options: The command line options return value
    :returns: A list of lists
    """
    query = {
        'userIdentifierVO':
        {
            'email': options.email,
            'login': {
                # this call needs the username to be unqualified
                'username': options.email.split('@')[0],
                'password': options.password
            }
        },
    }

    if options.userid:
        query['userProfileVO'] = {
            'uid': long(options.userid),
        }

    groups = dict()
    groupMembers = defaultdict(list)
    categories = dict()
    contacts = dict()
    qn = u'getAll'
    stats = defaultdict(int)
    try:
        log_info('About to query SOAP GlobalManagementService:getAll')
        result = client.service.getAll(query)
        if options.tracelogs:
            req = u'{0}: {1} SOAP request: {2}'
            resp = u'{0}: {1} SOAP response: {2}'
            log_trace(req.format(options.email, qn, client.last_sent()))
            log_trace(resp.format(options.email, qn,
                                  client.last_received()))
        if result['categories'] and isinstance(result['categories'], list):
            for category in result['categories']:
                categories[category['kid']] = category['display']
                categories[category['name']] = category['display']
                stats['contactCategoriesFound'] += 1
        if result['groups'] and isinstance(result['groups'], list):
            for group in result['groups']:
                groups[group['gid']] = group['groupName']
                stats['contactGroupsFound'] += 1
        if result['contacts'] and isinstance(result['contacts'], list):
            for contact in result['contacts']:
                if 'cid' in contact:
                    contacts[contact['cid']] = contact
                    stats['contactsRetrieved'] += 1
                    if contact['groupsContactBelongsTo'] and \
                      isinstance(contact['groupsContactBelongsTo'], list):
                        for group in contact['groupsContactBelongsTo']:
                            groupMembers[group].append(contact['cid'])
                else:
                    log_error(u'Received contact without cid attribute')
    except BadStatusLine, e:
        fatal = u'HTTP error querying {1} from SOAP server: {0}'
        log_fatal(fatal.format(str(e), qn))
        exit(1)
    except Exception, e:
        msg = u'Exception while querying {1}: {0}'.format(str(e), qn)
        log_fatal(msg)
        log_trace(msg)
        log_trace(traceback.format_exc())
        exit(1)
    for stat in stats.keys():
        log_statistic(stat.encode('utf8'), stats[stat])
    return (contacts, groups, groupMembers, categories)

def getContactSettings(client, options):
    """
    :param client: A SUDS client object for the calendar WSDL
    :param options: The command line options return value
    :returns: the value of the nameFormat setting
    """
    query = {
        'userIdentifierVO':
        {
            'email': options.email,
            'login': {
                'username': options.email,
                'password': options.password
            }
        },
    }

    if options.userid:
        query['userProfileVO'] = {
            'uid': long(options.userid),
        }

    try:
        result = client.service.getContactSettings(query)
        if options.tracelogs:
            req = u'{0}: getContactSettings SOAP request: {1}'
            resp = u'{0}: getContactSettings SOAP response: {1}'
            log_trace(req.format(options.email, client.last_sent()))
            log_trace(resp.format(options.email, client.last_received()))
        if 'nameFormat' in result:
            if result['nameFormat']:
                return result['nameFormat']
            else:
                # seems to be the default when no value is set
                return 'firstnamefirst'
    except BadStatusLine, e:
        log_fatal(u'HTTP error fetching contact settings from SOAP server: {0}'.format(str(e)))
        exit(1)
    except Exception, e:
        log_fatal(u'Exception fetching contact settings from SOAP server: {0}'.format(str(e)))
        exit(1)

def groupToVcard(group, groups, groupMembers):
    """
    :param contact:Group ID
    :param groups:Dict of group id -> group name
    :param groupMembers:Group members dict
    :param options:Command line options
    :returns: a vcard object for the group
    """
    card = vobject.vCard()
    card.add('x-addressbookserver-kind').value = u'group'
    card.add('fn').value = groups[group]
    card.add('n').value = vobject.vcard.Name(
        family=groups[group].encode('utf-8'),
        given=u'',
        prefix=u'',
        suffix=u'',
        additional=u'',
    )
    card.add('uid').value = str(group).encode('utf-8')
    if group in groupMembers:
        for member in groupMembers[group]:
            card.add('x-addressbookserver-member').value = u'urn:uuid:{0}'.format(member)
    return card

def contactToVcard(contact, nameFormat, categories, options):
    """
    :param contact:Individual contact XML from SOAP
    :param nameFormat:Name format from settings
    :returns: a vcard object for the contact
    """
    card = vobject.vCard()
    card.add('n')
    notes = list()
    relatedVcardWorks = False
    email = contact['homeEmail'] or contact['jobEmail'] or ''
    card.n.value = vobject.vcard.Name(
        given=(contact['firstName'] or contact['nickName'] or ''),
        family=(contact['lastName'] or ''),
        prefix=(contact['title'] or ''),
        suffix=(contact['suffix'] or ''),
        additional=(contact['middleName'] or ''),
    )
    card.add('fn')
    if contact['firstName'] and contact['lastName']:
        if nameFormat == 'firstnamefirst':
            card.fn.value = ' '.join([contact['firstName'],
                                      contact['lastName']])
        elif nameFormat == 'lastnamefirst':
            card.fn.value = ', '.join([contact['lastName'],
                                       contact['firstName']])
        else:
            log_error(u'Unknown nameFormat found when converting contact to vCard')
    elif contact['firstName']:
        card.fn.value = contact['firstName']
    elif contact['lastName']:
        card.fn.value = contact['lastName']
    else:
        card.fn.value = email
    if contact['title']:
        notes.append(u'Title: {0}'.format(contact['title']))
    if contact['suffix']:
        notes.append(u'Suffix: {0}'.format(contact['suffix']))
    if contact['cid']:
        card.add('uid').value = str(contact['cid'])
    if contact['nickName']:
        card.add('nickname').value = contact['nickName']
    if contact['jobTitle']:
        card.add(u'title').value = contact['jobTitle']
    if contact['website']:
        card.add(u'url').value = contact['website']
    if contact['company']:
        card.add(u'org').value = [contact['company'],]
    if contact['spouseName']:
        if relatedVcardWorks:
            related = card.add(u'related')
            related.type_param = u'spouse'
            related.value_param = u'text'
            related.value = contact['spouseName']
        else:
            value = contact['spouseName']
            notes.append(u'Spouse: {0}'.format(value))
    for child in [u'childName1', u'childName2', u'childName3', u'childName4']:
        if contact[child]:
            if relatedVcardWorks:
                related = card.add(u'related')
                related.type_param = u'child'
                related.value_param = u'text'
                related.value = contact[child]
            else:
                value = contact[child]
                notes.append(u'Child: {0}'.format(value))
    if contact['categoriesContactBelongsTo'] and \
      isinstance(contact['categoriesContactBelongsTo'], list):
        cats = list()
        for cat in contact['categoriesContactBelongsTo']:
            if cat in categories:
                cats.append(categories[cat])
            else:
                warn = u'No category name found for category ID {0} for ' + \
                       'contact ID {1}'
                log_warn(warn.format(cat, contact['cid']))
        if len(cats) == 1:
            notes.append(u'Category: {0}'.format(', '.join(cats)))
        elif len(cats) > 1:
            notes.append(u'Categories: {0}'.format(', '.join(cats)))
    imaddresses = {
        u'IMAddressICQ': {u'attr': u'impp', u'value': u'icq:{0}'},
        u'IMAddressMSN': {u'attr': u'x-cp-msn', 'value': u'{0}'},
        u'IMAddressAMSN': {u'attr': u'impp', u'value': u'amsn:{0}'},
        u'IMAddressSkype': {u'attr': u'impp', u'value': u'skype:{0}'},
    }
    for imaddr in imaddresses.keys():
        if contact[imaddr]:
            value = contact[imaddr]
            if imaddresses[imaddr]['attr'] != u'impp':
                card.add(imaddresses[imaddr]['attr']).value = \
                    imaddresses[imaddr]['value'].format(value)
            else:
                notes.append(imaddresses[imaddr]['value'].format(value))
    phones = {
        u'businessFax': [u'WORK', u'FAX'],
        u'businessPhone': [u'WORK', ],
        u'homePhone': [u'HOME', ],
        u'homeFax': [u'HOME', u'FAX'],
        u'mobilePhone': [u'CELL', ],
    }
    for phone in phones.keys():
        if contact[phone]:
            tel = card.add(u'tel')
            tel.type_param = phones[phone]
            tel.value = contact[phone]
    emails = {
        u'jobEmail': [u'WORK'],
        u'homeEmail': [u'HOME'],
    }
    for email in emails.keys():
        if contact[email]:
            mail = card.add(u'email')
            mail.type_param = emails[email]
            mail.value = contact[email]
    addressMap = {
        u'streetLine1': u'street',
        u'streetLine2': u'box',
        u'poBox': u'extended',
        u'city': u'city',
        u'state': u'region',
        u'country': u'country',
        u'postCode': u'code',
    }
    addressTypes = {
        u'homeAddress': [u'HOME'],
        u'officeAddress': [u'WORK'],
        u'address': None,
    }
    for address in addressTypes.keys():
        if contact[address]:
            params = dict()
            populated = False
            for param in addressMap:
                params[addressMap[param]] = contact[address][param] or ''
                if contact[address][param]:
                    populated = True
            if populated:
                adr = card.add(u'adr')
                adr.value = vobject.vcard.Address(**params)
                if addressTypes[address]:
                    adr.type_param = addressTypes[address]
    if 'anniversaries' in contact and contact['anniversaries']:
        local_tz = pytz.timezone(options.timezone)
        for anniv in contact['anniversaries']:
            date = anniv['anniversaryStartTime']
            anivtype = anniv['anniversaryType']
            date = date.replace(tzinfo=pytz.UTC).astimezone(local_tz)

            # write to the Notes field or to a real vCard field?
            publishToNotes = True

            # If publishing to a real field, OWM pabd has some limits on the
            # dates it will accept, so fix up broken dates.  If publishing
            # to notes, we don't do any fixups.
            if not publishToNotes:
                if date.year < 1900:
                    warn = u'Replaced invalid {2} year {0} with 1900 ' + \
                           'for contact {1}'
                    log_warn(warn.format(date.year, contact['cid'], anivtype))
                    date = date.replace(1900)
                elif date.year > 2900 and date.year <= 3015:
                    warn = u'Replaced invalid {2} year {0} with {1} ' + \
                           'for contact {2}'
                    log_warn(warn.format(date.year, date.year - 1000,
                                         contact['cid'], anivtype))
                    date = date.replace(date.year - 1000)
                elif date.year > 3015:
                    warn = u'Replaced invalid {2} year {0} with 1901 ' + \
                           'for contact {1}'
                    log_warn(warn.format(date.year, contact['cid'], anivtype))
                    date = date.replace(1901)

            dateStr = date.strftime('%Y-%m-%d')
            if anivtype == 'birthday':
                card.add('BDAY').value = dateStr
                # notes.append('Birthday: {0}'.format(dateStr))
            elif anivtype == 'weddingDay':
                notes.append(u'Wedding day: {0}'.format(dateStr))
            elif anivtype == 'anniversary1':
                notes.append(u'Anniversary 1: {0}'.format(dateStr))
            elif anivtype == 'anniversary2':
                notes.append(u'Anniversary 2: {0}'.format(dateStr))
            else:
                error = u'Unexpected anniversary type of "{0}" for cid {1}'
                log_error(error.format(
                    anniv[u'anniversaryType'],
                    contact[u'cid']))
    if notes:
        card.add('note').value = "\n".join(notes)
    return card

def handleOptions():
    """
    Define / check command line options
    """
    usage = 'usage: %prog --email=EMAIL --password=PASSWORD ' + \
            '[--userid=USERID] --contactServiceURL=URL --globalServiceURL=URL'

    usage = usage + ' --output=FILENAME [--enforceend]'
    parser = OptionParser(usage=usage)
    parser.add_option('--email', action='store', type='string', dest='email',
                      help='fully qualified email address for the user')
    parser.add_option('--password', action='store', type='string', dest='password',
                      help='the users password encoded in Base64 for URL applications')
    parser.add_option('--userid', action='store', type='string', dest='userid',
                      help='numeric user identifier for the user')
    parser.add_option('--contactServiceURL', action='store', type='string',
                      dest='contactServiceURL',
                      help='URL to fetch the WSDL for the contact service from')
    parser.add_option('--globalServiceURL', action='store', type='string',
                      dest='globalServiceURL',
                      help='URL to fetch the WSDL for the global service from')
    parser.add_option('--output', action='store', dest='output',
                      help='Path to write exported icalendar data to')
    parser.add_option('--tracelogs', action='store_true', dest='tracelogs',
                      help='Enable trace logging of SOAP calls to STDOUT',
                      default=False)
    parser.add_option('--timezone', action='store', dest='timezone',
                      help='Timezone to export data in')

    try:
        (options, args) = parser.parse_args()
    except Exception, e:
        log_fatal(u'Internal error processing options: {0}'.format(str(e)))
        exit(1)
    requiredOptions = [
        'email',
        'password',
        'contactServiceURL',
        'globalServiceURL',
        'output',
        'timezone',
    ]

    for option in requiredOptions:
        if not getattr(options, option):
            log_fatal(u'--{0} argument must be supplied'.format(option))
            exit(1)
    if '@' not in options.email:
        log_fatal(u'--email argument must be supplied and must have an @ sign in it')
        parser.print_help()
        exit(1)
    try:
        password = urlsafe_b64decode(options.password)
    except TypeError, e:
        fatal = u'Cannot decode base64 argument to --password: {0}: {1}'
        log_fatal(fatal.format(str(e), options.password))
        exit(1)
    except Exception, e:
        fatal = u'Cannot decode argument to --password: {0}: {1}'
        log_fatal(fatal.format(str(e), options.password))
        exit(1)
    options.password = password
    if args:
        log_fatal(u'Unexpected command line arguments.  Aborting')
        parser.print_help()
        exit(1)
    return options

def main():
    """
    The main function.
    """
    options = handleOptions()

    try:
        contactClient = Client(options.contactServiceURL)
        contactClient.set_options(cache=None)
    except BadStatusLine, e:
        msg = u'HTTP error fetching contact WSDL from SOAP server: ' + str(e)
        log_fatal(msg)
        log_trace(msg)
        log_trace(traceback.format_exc())
        exit(1)
    except Exception, e:
        msg = u'Cannot instantiate contact SOAP client: ' + str(e)
        log_fatal(msg)
        log_trace(msg)
        log_trace(traceback.format_exc())
        exit(1)

    try:
        globalClient = Client(options.globalServiceURL)
        globalClient.set_options(cache=None)
    except BadStatusLine, e:
        msg = u'HTTP error fetching global WSDL from SOAP server: ' + str(e)
        log_fatal(msg)
        log_trace(msg)
        log_trace(traceback.format_exc())
        exit(1)
    except Exception, e:
        msg = u'Cannot instantiate global SOAP client: ' + str(e)
        log_fatal(msg)
        log_trace(msg)
        log_trace(traceback.format_exc())
        exit(1)

    contacts, groups, groupMembers, categories = fetchAll(globalClient, options)

    nameFormat = getContactSettings(contactClient, options)

    vCards = list()
    for contact in contacts:
        try:
            vCard = contactToVcard(contacts[contact], nameFormat,
                                   categories, options)
            vCards.append(vCard)
            # print vCard.serialize()
        except Exception, e:
            msg = u'Error turning XML contact into vcard: {0}'.format(str(e))
            log_fatal(msg)
            log_trace(msg)
            log_trace(traceback.format_exc())

    emptyCount = 0
    for group in groups:
        if group not in groupMembers:
            emptyCount += 1
        try:
            vCard = groupToVcard(group, groups, groupMembers)
            vCards.append(vCard)
            # print vCard.serialize()
        except Exception, e:
            msg = u'Error turning XML group into vcard: {0}'.format(str(e))
            log_fatal(msg)
            log_trace(msg)
            log_trace(traceback.format_exc())

    log_statistic(u'emptyContactGroups', emptyCount)

    try:
        write_contacts_to_vcf(vCards, options.output)
    except IOError, e:
        msg = u'Cannot open vcf file for writing: {0}'.format(str(e))
        log_fatal(msg)
        log_trace(msg)
        log_trace(traceback.format_exc())
        exit(1)
    except Exception, e:
        msg = u'Error writing vcf file: {0}'.format(str(e))
        log_fatal(msg)
        log_trace(msg)
        log_trace(traceback.format_exc())
        exit(1)

if __name__ == '__main__':
    main()
