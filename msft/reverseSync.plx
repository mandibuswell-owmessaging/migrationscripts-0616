#! /usr/bin/perl
#
# Kent.Dang@synchronoss.com
# 20 April, 2016 AEST 
#
############################################################################
### 'use' modules
#############################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl";
use lib "$FindBin::Bin/migperl/lib/perl5";
use strict;
use Net::LDAP qw(LDAP_SUCCESS);
use Getopt::Long;
use Config::Simple;
use Log::Log4perl qw(get_logger :levels);
use File::Copy;

###########################################################################
### Global variables
#############################################################################
my $DEEPDEBUG = 0;
$| = 1;    ## Makes STDOUT flush immediately
my $configPath = 'conf/migrator.conf';
my $logPath    = 'conf/log.conf';

# Parameters to get from the script options.
my ($SRCLCN, $RUNONCE, $applyldif, $processldif);

# Logging object.
Log::Log4perl->init($logPath);
my $logger = get_logger("REVERSESYNC");

#Initialise the configuration file
my $cfg = new Config::Simple();
$cfg->read($configPath)
or $logger->logdie( "\nFATAL: " . $cfg->error() . "\nINFO: Please fix the issue and restart the migration program. Exiting.\n\n" );

#reverse sync syncs from Mx9 - Mx8 so swap the keys around
my $lcnFile = $cfg->param('r_lastChangeNumberFile');

my $destMasterHost = $cfg->param('mx_dirserv_host_src');
my $destMasterPort = $cfg->param('mx_dirserv_port_src');
my $destMasterPass = $cfg->param('mx_nonroot_dirserv_pwd_src'); 
my $destMasterBind = $cfg->param('mx_nonroot_dirserv_bind_src'); 
my $srcMasterHost = $cfg->param('mx_dirserv_host');
my $srcMasterPort = $cfg->param('mx_dirserv_port');
my $srcMasterPass = $cfg->param('r_destMasterPass');
my $srcMasterBind = $cfg->param('r_destMasterBind');

my $chglogDataFile = $cfg->param("chglogDataFile");
my $runOnce     = $cfg->param("r_runOnce");
my $stopFile    = $cfg->param("r_stopFile");
my $syncDir     = $cfg->param("r_syncDir");
my $popProxyHost  = $cfg->param("r_popProxyHost");
my $imapProxyHost = $cfg->param("r_imapProxyHost");
my $smtpRelayHost = $cfg->param("r_smtpProxyHost");
my $storeHost   = $cfg->param("r_defaultMsgStoreHost");
my $replyHost   = $cfg->param("r_defaultAutoReplyHost");
my $mx8Realm  = $cfg->param("r_mx8Realm");
my $ldapsearch  = $cfg->param("r_owmLdapsearch");
my $ldapmodify  = $cfg->param("r_owmLdapmodify");
my $r_useDefaultCos = $cfg->param("r_useDefaultCos");
my $r_defaultCos = $cfg->param("r_defaultCos");
my $r_reverseAddOnly = $cfg->param("r_reverseAddOnly"); 
#=============================================================

my $SDEBUG = $cfg->param('log_debug');
$logger->debug("SCRIPT|Begin Reverse Sync ... ");
my $sleepWait = defined $cfg->param('sleepWait') ? $cfg->param('sleepWait') : 5;
my $mailsetblock = $cfg->param('blockautoreply');
my $newproxyflag = $cfg->param('setnewproxy');
my $getDestDN    = $cfg->param('getDestDN');
my $ignoreLdap34    = $cfg->param('ignoreLdap34');
defined $cfg->param('sleepWait') ? $cfg->param('sleepWait') : 5;
my $switch_negative    = defined $cfg->param('switch_negative') ? $cfg->param('switch_negative') : "false";
my $ignoreProbe    = defined $cfg->param('ignoreProbe') ? $cfg->param('ignoreProbe') : "false";
my $useDefaultCos    = defined $cfg->param('useDefaultCos') ? $cfg->param('useDefaultCos') : "false";
my $defaultCos = $cfg->param('defaultCos');
my $defaultMsgStoreHost    = $cfg->param('defaultMsgStoreHost');
my $probeDN    = $cfg->param('probeDN');

my $HOUR = `date +"%H"`;   
my $lastHour = $HOUR;
my $dir      = `pwd`;

######################################################
### USAGE
########################################################
sub usage {
        print STDERR qq(
        Utility to update attributes in SUR
        Command line to run script:
        reverseSync.pl  [-n <lastchangenumber>] [-d] [-runonce <true|false]
where:
        <lastchangenumber>     provide a last change number
        -d              turn debug on (overrides any setting config).
        -runonce            true=run once only, false=run continuously like a daemon (overrides any setting config)
        To stop when in continuous mode, either kill the script or touch the file $stopFile
        );

        exit(0);
}

GetOptions( '-d', \$SDEBUG,    # Override debug in config file.
        '-n=s', \$SRCLCN,        # Provide last change number
        '-runonce=s', \$RUNONCE
) || usage();

print("\n\n");

if ($SDEBUG) {
        print("Running in DEBUG mode\n");
        $logger->info("SCRIPT|Running in DEBUG mode\n");
        $logger->level($DEBUG); #Override debug in log file when -d or debug set in config file
}

my $tempDir = "$syncDir/temp";
my $redoDir = "$syncDir/redo";
my $goodDir = "$syncDir/good";
my $skipDir = "$syncDir/skip";
#make all the directories needed if they do not exists
mkdir $syncDir   unless -d $syncDir;
mkdir "$syncDir-archive"   unless -d "$syncDir-archive";
mkdir "$tempDir" unless -d "$tempDir";
mkdir "$redoDir" unless -d "$redoDir";
mkdir "$goodDir" unless -d "$goodDir";
mkdir "$skipDir" unless -d "$skipDir";

my $lcnFileFQFN = $syncDir . "/" . $lcnFile;
my $lcnFileFQFN_old = $lcnFileFQFN . ".old";

###########################################################################
### sub newdir
### rollover and archive logs and curlogs
#############################################################################

sub newdir {
        my ($tdir) = @_;
    my $timestamp=`date +"%Y%m%d%H%M%S"`;
        my $newdir = "$tdir-archive/$tdir.$timestamp";
    $HOUR = `date +"%H"`;
        if ( -e $tdir ) {
                if ( $HOUR ne $lastHour ) {
                        $logger->info("SCRIPT|Archiving $tdir Current hour $HOUR Last hour $lastHour");
                        $logger->debug("SCRIPT|newdir TDIR $tdir NEWDIR $newdir");
                        #archive first execution or every hour when running continuous
            $lastHour = $HOUR;        
            chomp $newdir;
            mkdir $newdir;            
            $newdir .= "\/"; # append a forward slash at the end of the directory name
            `mv $tempDir $redoDir $goodDir $skipDir $newdir`;
            `cp $tdir/lastchangenumber.* $newdir`;
                        mkdir $tempDir;
            mkdir $redoDir;
            mkdir $goodDir;
            mkdir $skipDir;
                } else {
                        $logger->debug("SCRIPT|Archiving skipped");
                }
        } else {
                $logger->debug("SCRIPT|newdir creating directory $tdir");
                `mkdir $tdir`;
        }
        return;
}

sub parseChangeNum { # changenumber=27535738,cn=changelog
    my ($firstLineInChangeRecord) = @_; 
    my ($changenumberFull,$changelogFull) = split( ",", $_ );
    my ($text,$changenumber) = split( "=", $changenumberFull );
    return $changenumber;
}

sub parseTargetDn { # targetdn=mail=fn1_poc_10022@uat.bigpond.com,dc=uat,dc=bigpond,dc=com
    my ($mydn) = @_;
    $mydn =~ s/^/dn: /;
    my @a     = split( " ", $mydn );
    my @b     = split( ",", $a[1] );
    my $nc    = scalar(@b);
    my $First = $b[0];
    my @email = split( "=", $First );
    my $base  = $b[1];

    for ( my $i = 2 ; $i < $nc ; $i++ ) {
        $base = $base . "," . $b[$i];
    }    
    return ($mydn, @email[1], $base);
}

sub parseChangeType { # changetype=modify
    my ($changetypeLDIF) = @_;
    my ($text, $changetype) = split( "=", $changetypeLDIF );
    $changetypeLDIF =~ s/changetype=/changetype: /;
    return ($changetype, $changetypeLDIF);
}

sub checkRunForever {
    if ( $RUNONCE =~ /true/i ) {
        return 0;
        # log here
    } elsif ( -e $stopFile ) {
                # Break the program
                $logger->info("SCRIPT|Reverse Directory synchronisation stop file found, exiting ...");
                print("Reverse Directory sync script now stopping!\n") if ($SDEBUG);    
        return 0;
        # log here    
    }
    return 1;    
}

# Create a new Last Change Number file IF there is no existing LCN.
# Otherwise this key will be ignored.
sub createLcn {
    my ($SRCLCN,$cnFileFQFN) = @_;
 
    if ( -e $lcnFileFQFN ) {
        my $lcnFromFile = `cat $lcnFileFQFN`;
        `echo $SRCLCN > $lcnFileFQFN`;
        $logger->info("SCRIPT|The previous '$lcnFromFile' has been written over with '$SRCLCN' in '$lcnFileFQFN' file.");
    } else {
        `echo $SRCLCN > $lcnFileFQFN`;
        if ( -e $lcnFileFQFN ) {
            $logger->info("SCRIPT|Successfully create '$lcnFileFQFN' file with '$SRCLCN'");
            return 1;
        } else {
            $logger->info("SCRIPT|Failed to create '$lcnFileFQFN'");
            return 0;
        }
    }
}

sub getLcn {
    if ( -e $lcnFileFQFN ) {
        # log here
        $_ = `cat $lcnFileFQFN`;
        chomp;
        return ($_);
    } else {
        return 0;
    }
}
 
sub ldapBind {
    my ($host,$port,$bindID,$bindPass) = @_;
        my $ldap = Net::LDAP->new ( $host, port => $port );
    if (! $ldap) {
        $logger->warn("Connect to host = '$host' on port '$port' failed.");
        return 0;
    }

    my $mesg = $ldap->bind ( $bindID, password => $bindPass );

    if ( $mesg->code ne '0' ) {
        my $code = $mesg->code;    
        #$logger->warn("Connect to host = '$host' on port '$port' with bindID = '$bindID' password = '$bindPass' failed.");
        $logger->warn("Connect to host = '$host' on port '$port' with bindID = '$bindID' failed.");  # Do not include the password in the log file.      
        return 0;
    } else {
        return $ldap;
    }
}
    
sub getSrcLcn {
    my ($host,$port,$bindID,$bindPass) = @_;
    my $ldap = ldapBind($host,$port,$bindID,$bindPass);
    if ($ldap) {
        my $mesg = $ldap->search( # perform a search
                        base   => "",
                        scope   => "base",                        
                        filter => "objectclass=top",
                        attrs => ['lastchangenumber']
                      );

        if ($mesg->code) {
            my $errMsg = $mesg->error;
            $logger->info("SCRIPT|Error '$errMsg' was received while getting the lastchangenumber");
            $_ = $ldap->unbind;    
            return 0;
        } else {       
            foreach my $entry ($mesg->all_entries) {      
                my ($lastchangenumber) = $entry->get('lastchangenumber');
                $_ = $ldap->unbind;
                return $lastchangenumber;
            }
        }    
    }
}

sub createObjectClass {
    my ($objectClass,$value) = @_;
    if ($objectClass) {
        $objectClass = $objectClass . ",'" . $value . "'";
        return $objectClass;
    } else {
        return "'$value'";
    }
}

sub fileAppend {
    my ($changeLogLine,$changeLogFileLocation) = @_;
    open(my $changeLogFile, ">>$changeLogFileLocation");
    print $changeLogFile "$changeLogLine\n";
    close $changeLogFile;
}

sub fileCreate {
    my ($changeLogLine,$changeLogFileLocation) = @_;
    open(my $changeLogFile, ">$changeLogFileLocation");
    print $changeLogFile "$changeLogLine\n";
    close $changeLogFile;
}

sub printAddLdifArray {
    my ($newChangeLogArray_ref, $processChangeLog,$fullDn) = @_;
    my @newChangeLogArray = @{$newChangeLogArray_ref};
    my $ldifString = $fullDn . "\n";
    
    while (@newChangeLogArray) {
        my $newChangeLogArrayElement = shift (@newChangeLogArray);
        if ($newChangeLogArrayElement =~ /senderscontrol/) { # ignore
        } elsif ($newChangeLogArrayElement =~ /objectclass/) {
            $_ = shift (@newChangeLogArray);
            my @objectClass = @{$_};
            foreach (@objectClass) {
                #print "objectclass: $_\n";
                $ldifString .= "objectclass: $_\n";
            }
        } else {
        my $next = shift (@newChangeLogArray);
        # print ("$newChangeLogArrayElement: $next\n");
        $ldifString .= "$newChangeLogArrayElement: $next\n";
        }
    }   

    fileCreate($ldifString,"$redoDir/$processChangeLog.ldif");
}
 
 
sub printModLdifArray {
    my ($newChangeLogArray_ref, $processChangeLog,$fullDn) = @_;
    my @newChangeLogArray = @{$newChangeLogArray_ref};
    shift (@newChangeLogArray); # remove the "original "replace"
    
    my $ldifString = $fullDn . "\n";
    $ldifString .= "changetype: modify\n";

    $_ = shift (@newChangeLogArray);
    my @modChangeArray = @{$_};
    
    my $multipleAttributes = 0;
    while (@modChangeArray) {
        my $ldapAttribute = shift (@modChangeArray);
        my $ldapValue = shift (@modChangeArray);
        if ( $multipleAttributes ) {
            $ldifString .= "-\n";
        } else {
            $multipleAttributes = 1;        
        }
        $ldifString .= "replace: $ldapAttribute\n";
        $ldifString .= "$ldapAttribute: $ldapValue\n";

    }
    fileCreate($ldifString,"$redoDir/$processChangeLog.ldif");
}

#
# return:   0 - successfully created a new record on Mx8
#           1 - the record was created by Mx8 -> Mx9 sync and there ignore this record.
#
sub addLDAP2Dest {
    my ($flattenedChangeLogArray,$targetdn,$changeLogLine,$ldapDest,$processChangeLog) = @_;
    
    my ($fullDn, $email, $baseDn) = parseTargetDn($targetdn); # Get the proper DN value
    my @changeLogArray = split( "\n", $changeLogLine );
    my @newChangeLogArray;
    #my $objectClass = "";
    my @objectClass;
    my ($attr,$value);
    
    my ($passwordTypeSave, $passwordSave);
    
    foreach my $line (@changeLogArray) {
        ($attr,$value) = split( ": ", $line );
        if ( $line =~ /objectclass: /i ) {
            # Use all objectclasses except the followings
            if ( $value =~ /pabuser/ ) { 
            } elsif ( $value =~ /pabuserprefs/ ) {
            } elsif ( $value =~ /caluser/ ) {
            } elsif ( $value =~ /caluserprefs/ ) {
            } elsif ( $value =~ /smsuserprefs/ ) {
            } else {
                push (@objectClass, $value);
            }
        } elsif ( $line =~ /^adminpolicydn:/i ) {
            push (@newChangeLogArray, $attr);
            push (@newChangeLogArray, $r_defaultCos);
        } elsif ( $line =~ /^mailpasswordtype:/i ) {
            $passwordTypeSave = $value;          
        } elsif ( $line =~ /^mailpassword:/i ) {
            $passwordSave = $value;
        } elsif ( $line =~ /^mail:/i ) {
            # use $email variable that extracted from the DN instead
        } elsif ( $line =~ /^mailboxstatus:/i ) {
            push (@newChangeLogArray, $attr);
            push (@newChangeLogArray, "P");             
        } elsif ( $line =~ /^creatorsname:/i ) {
            if ( $value =~ /$srcMasterBind/ ) { # ie, cn=mx829sync
                $logger->info("SCRIPT|SKIP ADD due to Mx 8 sync detetected for '$targetdn'");
                fileAppend($fullDn . "\n" . $changeLogLine,"$skipDir/chglogs.dat");
                return 1;
            } else {
                $logger->info("SCRIPT|Provisioning from non-Mx8 sync $targetdn\n");
            }
        } elsif ( $line =~ /^mailmessagestore:/i ) {
            push (@newChangeLogArray, $attr);
            push (@newChangeLogArray, $storeHost);        
        } elsif ( $line =~ /^mailautoreplyhost:/i ) {
            push (@newChangeLogArray, $attr);
            push (@newChangeLogArray, $replyHost);                 
        } elsif ( $line =~ /^cn:/i ) {
            push (@newChangeLogArray, $attr);
            push (@newChangeLogArray, $value); 
            push (@newChangeLogArray, "mailfrom");
            push (@newChangeLogArray, $value); 
        } elsif (( $line =~ /^sn:/i ) || ( $line =~ /^maillogin:/i ) || ( $line =~ /^mailboxid:/i ) || ( $line =~ /^mailalternateaddress:/i ) || ( $line =~ /^mailautoreplymode:/i ) || ( $line =~ /^maildeliveryoption:/i )) {
            push (@newChangeLogArray, $attr);
            push (@newChangeLogArray, $value); 
        } 
        
    }
    push (@newChangeLogArray, "mailpopproxyhost");
    push (@newChangeLogArray, $popProxyHost);   
    push (@newChangeLogArray, "mailimapproxyhost");
    push (@newChangeLogArray, $imapProxyHost);   
    push (@newChangeLogArray, "mailsmtprelayhost");
    push (@newChangeLogArray, $smtpRelayHost);  
    push (@newChangeLogArray, "netmailcurrentver");
    push (@newChangeLogArray, "4.0.5");  
    push (@newChangeLogArray, "description");
    push (@newChangeLogArray, "created on Mx9");
    
    if ( $passwordTypeSave =~ /b/i ) { # If the password type is "bcrypt" type
        my $clearTextPassword = `/opt/owm/migration/migration_tool/getAuth.plx $email`;
        if ($clearTextPassword) {
            my $ssha1Password = `/opt/owm/imail8/Mx8.1/bin/impwdhash -a ssha1 $clearTextPassword`;
            $logger->info("SCRIPT|Successfully found and convert a text password for '$email'.");
            push (@newChangeLogArray, "mailpasswordtype");
            push (@newChangeLogArray, "2");
            push (@newChangeLogArray, "mailpassword");
            push (@newChangeLogArray, $ssha1Password);            
        } else {
            $logger->info("SCRIPT|Error cannot find a text password for '$email'.");
            push (@newChangeLogArray, "mailpasswordtype");
            push (@newChangeLogArray, "2");
            push (@newChangeLogArray, "mailpassword");
            push (@newChangeLogArray, "abcdefghijklmnopqrstuvwxyz1234567890123456789012");  # a known invalid password        
        }
    } else {
            $logger->info("SCRIPT|Password type of '$passwordTypeSave' has been detected.  Use the same password for '$email'.");
            push (@newChangeLogArray, "mailpasswordtype");
            push (@newChangeLogArray, $passwordTypeSave);
            push (@newChangeLogArray, "mailpassword"); 
            push (@newChangeLogArray, $passwordSave);        
    }

    unshift (@newChangeLogArray, \@objectClass);
    unshift (@newChangeLogArray, "objectclass");
    
    my $mesg = $ldapDest->add ($targetdn,
            attrs => \@newChangeLogArray
            );
    if ($mesg->code) {
        my $errMsg = $mesg->error;
        fileCreate($fullDn . "\n" . $changeLogLine,"$redoDir/$processChangeLog.ldif.orig");
        printAddLdifArray(\@newChangeLogArray,$processChangeLog,$fullDn);      
        $logger->info("SCRIPT|Error Message '$errMsg' received while trying to create $targetdn\n");
        return 1
    } else {
        fileAppend($fullDn . "\n" . $changeLogLine,"$goodDir/chglogs.dat");
        $logger->info("SCRIPT|Successfully created '$targetdn' on the destination server\n");
        return 0;
    }

}

sub findPasswordType {
    my ($ldapSource, $email) = @_;
    
    #my $filter = "(mail=email\@bigpond.com)";
    
    my $mesg = $ldapSource->search( # perform a search
                    base   => "",
                    scope   => "sub",                        
                    filter => "(mail=$email)",
                    attrs => ['mailpasswordtype']
                  );

    if ($mesg->code) {
        my $errMsg = $mesg->error;
        print "Error = '$errMsg' \n";
        $logger->info("SCRIPT|Error = '$errMsg' received while trying to retrieve the mailpasswordtype for '$email'");
        return "";
    } else {         
        foreach my $entry ($mesg->all_entries) {      
            my ($mailpasswordtype) = $entry->get('mailpasswordtype');
            return $mailpasswordtype;
        }
    }    
}

#
# return:   0 - successfully modify a record on Mx 8
#           1 - the record was created by Mx8 -> Mx9 sync and there ignore this record.
#
sub modLDAP2Dest {
    my ($flattenedChangeLogArray,$targetdn,$changeLogLine,$ldapSource,$ldapDest,$processChangeLog) = @_;
    
    my ($fullDn, $email, $baseDn) = parseTargetDn($targetdn); # Get the proper DN value
    $logger->info("SCRIPT|DN: '$targetdn', '$fullDn', '$email', '$baseDn'");


    my @changeLogArray = split( "\n", $changeLogLine );
    my @ldapModArray;
    my @ldapArray;
    my ($attr,$value,$newPassword);
    
    
    my $changePasswordFlag = 0;
    my $passwordSave;
    
    foreach my $line (@changeLogArray) {
        ($attr,$value) = split( ": ", $line );
        if ( $line =~ /replace: mailpassword/i ) {
            $changePasswordFlag = 1; 
        } elsif ( $line =~ /mailpassword: /i ) {
            $passwordSave = $value;         
        } elsif ( $line =~ /^modifiersname:/i ) {
            if ( $value =~ /$srcMasterBind/ ) { # ie, cn=mx829sync
                $logger->info("SCRIPT|SKIP MOD due to Mx 8 sync detetected for '$targetdn'");
                fileAppend($fullDn . "\n" . $changeLogLine,"$skipDir/chglogs.dat");
                return 1;
            } else {
                $logger->info("SCRIPT|Modifying from non-Mx8 sync $targetdn\n");
            }
        } 
    } # end of the loop parsing through the change log
    
    if ($changePasswordFlag) {
        my $passwordType = findPasswordType($ldapSource, $email);
        
        if ( $passwordType =~ /b/i ) { # If the password type is "bcrypt" type
            my $clearTextPassword = `./getAuth.plx $email`;
            if ($clearTextPassword) {
                # need to have a copy of impwdhash from an $INTERMAIL/bin to be in ~migration/migration_tool
                $newPassword = `./impwdhash -a ssha1 $clearTextPassword`;
                $logger->info("SCRIPT|Successfully found and convert a text password for '$email'.");  
            }  else {
            $logger->info("SCRIPT|Error cannot find a text password for '$email'.");
            return 1;
            }
        } else {
            $newPassword = $passwordSave; # Use the the same new password on Mx9 as is for Mx8 if the password type is not "bcrypt" type.
        }
         
        push (@ldapModArray, "mailpassword");
        push (@ldapModArray, $newPassword);   
        
        push (@ldapArray, "replace");     
        push (@ldapArray, \@ldapModArray); 
    
        my $mesg = $ldapDest->modify ( $fullDn,
                                changes => \@ldapArray
                              );  
             
        $fullDn = "dn: " . $fullDn; 
        if ($mesg->code) {
            my $errMsg = $mesg->error;
            fileCreate($fullDn . "\n" . $changeLogLine,"$redoDir/$processChangeLog.ldif.orig");
            printModLdifArray(\@ldapArray,$processChangeLog,$fullDn);      
            $logger->info("SCRIPT|Error Message '$errMsg' received while trying to modify '$targetdn'");
            return 1
        } else {
            fileAppend($fullDn . "\n" . $changeLogLine,"$goodDir/chglogs.dat");
            $logger->info("SCRIPT|Successfully modified '$targetdn' on the destination server\n");
            return 0;
        }
       
    } # end of $changePasswordFlag condition
}
   


sub processChangeLog {
    my ($lcnFromFile,$lcnFromQuery) = @_;
    my $ldapSource = ldapBind($srcMasterHost,$srcMasterPort,$srcMasterBind,$srcMasterPass);
    my $ldapDest = ldapBind($destMasterHost,$destMasterPort,$destMasterBind,$destMasterPass);
   
    if ($ldapSource) {
        my $targetdn;
        my $processAdd = 0;
        my $processModify = 0;
        my $changeLogLine;
        my @changeLogArray;
        for (my $processChangeLog = $lcnFromFile+1; $processChangeLog <= $lcnFromQuery ; $processChangeLog++) { 
            $logger->info("SCRIPT|Info processing change Log = $processChangeLog");
            $processAdd = 0;
            @changeLogArray=(); # clear the elements in the array
            my $mesg = $ldapSource->search( # perform a search
                base   => "changenumber=$processChangeLog,cn=changelog",
                scope  => 'base',
                filter => '(objectclass=*)',
                async  => 0
            );
            
            if ($mesg->code != LDAP_SUCCESS) {
                my $errMesg = $mesg->error;
                $logger->info("SCRIPT|Error code '$errMesg' received while searching for change log # $processChangeLog\n");
                $_ = $ldapSource->unbind;    
                return 0;
            } else {       
                my $max = $mesg->count;     
$logger->info("SCRIPT|Info $processChangeLog has '$max' LDAP change log records.") if ($DEEPDEBUG);                
                for ( my $row = 0 ; $row < $max ; $row++ ) {
                    my $entry = $mesg->entry ( $row );
                    foreach my $attr ( $entry->attributes ) {
                        my $value = $entry->get_value( $attr );                  
                
                        if ($attr =~ /^targetdn$/i ) {
                            $targetdn=$value;
                        } elsif (( $attr =~ /^changetype/ ) && ($value =~ /add/)) {      
$logger->info("SCRIPT|Info $processChangeLog has detected ADD change log records.") if ($DEEPDEBUG);                           
                            $processAdd = 1 ;
                        } elsif (( $attr =~ /^changetype/ ) && ($value =~ /modify/)) {
$logger->info("SCRIPT|Info $processChangeLog has detected MOD change log records.") if ($DEEPDEBUG);                           
                            $processModify = 1 ;                            
                        } elsif ( $attr =~ /^changes$/i) {
                            $changeLogLine = $value; 
$logger->info("SCRIPT|Info $processChangeLog found the change log records.") if ($DEEPDEBUG);  
fileAppend($targetdn . "\n" . $changeLogLine,"$tempDir/chglogs.dat") if ($DEEPDEBUG) ;                             
                        }
                        
                        $_ = "$attr" . ": " . $value;                           
                        $_ =~ s/^changes: //;      
                        push(@changeLogArray, $_ . "\n");                      
                    }
                }
                local $" = ''; # This is the $LIST_SEPARATOR.  Default is space but don't want any space between the records.
                addLDAP2Dest ("@changeLogArray",$targetdn,$changeLogLine,$ldapDest,$processChangeLog) if ($processAdd);

                # AMShared updates passwords on Mx9, Mx9 and outlook.com, so no need to push password changes to Mx8
                #modLDAP2Dest ("@changeLogArray",$targetdn,$changeLogLine,$ldapSource,$ldapDest,$processChangeLog) if ($processModify);
                `cat $lcnFileFQFN > $lcnFileFQFN_old`;
                `echo $processChangeLog > $lcnFileFQFN`;
            }
        }    
    }
    $ldapSource->unbind if $ldapSource; 
    $ldapDest->unbind if $ldapDest;    
}

# Remove the "Stop File" if one exists
`rm -rf $stopFile` if -e $stopFile; 
    
createLcn($SRCLCN,$lcnFileFQFN) if ($SRCLCN);

my $infinity = 1;

# ================ Begin Main Loop ================
while ($infinity) {
    my $lcnFromFile = getLcn();
    my $lcnFromQuery = getSrcLcn($srcMasterHost,$srcMasterPort,$srcMasterBind,$srcMasterPass);
    $logger->info("SCRIPT|Info LCN from file = $lcnFromFile and LCN from source LDAP server = $lcnFromQuery");
    if ($lcnFromQuery > $lcnFromFile) {
        processChangeLog($lcnFromFile,$lcnFromQuery);
    } else {
        `touch $lcnFileFQFN`;
    }
    $infinity = checkRunForever();
    newdir($syncDir) if ($infinity);
    sleep ($sleepWait) if ($infinity);
}
exit 0;
# ================ End Main Loop ================