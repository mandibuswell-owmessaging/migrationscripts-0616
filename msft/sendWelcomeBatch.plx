#!/usr/bin/perl

###########################################################################
#
#  Copyright 2014 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Gary Palmer <gary.palmer@owmessaging.com>
#  Date:        March 2016
#  Version:     1.0.0 - 10 March 2016
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use warnings;
use strict;

use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;
use Log::Dispatch::FileRotate;

use com::owm::RetrievePassword;
use com::owm::imapAppend;

unless(scalar(@ARGV) == 1)
{
    die 'Pass batch filename as only command line argument';
}

my $batchFile = $ARGV[0];
unless(-f $batchFile)
{
    die 'Batch file not found';
}

unless(-r $batchFile)
{
    die 'Batch file not readable';
}

# A package that provides an easy way to have a config file
use Config::Simple;

our $configPath = 'conf/migrator.conf';
our $configLog = 'conf/log.conf';

Log::Log4perl->init($configLog);
our $log = get_logger("MIGRATE");

# Config object.
# And read the config file so all subs have access to params.
my $cfg  = new Config::Simple();
$cfg->read($configPath)
    or $log->logdie("SCRIPT|FATAL: ".$cfg->error().". Please fix the issue and restart the migration program. Exiting.");

open(BATCH, '<', $batchFile) || die 'Cannot open batchfile: '.$?;

my $owmImapServer = $cfg->param('owm_imap_server');
my $welcomeEN = $cfg->param('welcome_msg_EN_myimx8');
my $MODE = 'WL';
my $password_endpoint = $cfg->param('password_endpoint');
my $password_file = $cfg->param('password_file');
my $password_retrieval_by_file = $cfg->param('password_retrieval_by_file');
my $keyFile = $cfg->param('key_file');
my $http_username = $cfg->param('http_username');
my $http_password = $cfg->param('http_password');

while(my $email = <BATCH>)
{
    chomp($email);
    my ($pwd,$pwdError);

    my $LOGSTRING = $email;

    if (defined $password_endpoint || defined $password_file){
        if ($password_retrieval_by_file eq "true") {
            ($pwd,$pwdError) = com::owm::RetrievePassword::retrievePasswordFromFile($password_file,$email,$log); #clear text stub version
            $log->debug("$email: Getting password via flat file");
        } else {
            ($pwd,$pwdError) = com::owm::RetrievePassword::retrievePassword('UNK999',$password_endpoint, $email,$email,"OWMP", $log, $keyFile);
            $log->debug("$email: Getting password via api");
            if ($pwdError eq 1) {
                #could not retrieve the password via the Telstra interface try the flat file
                if (defined $password_file) {
                    ($pwd, $pwdError) = com::owm::RetrievePassword::retrievePasswordFromFile($password_file,$email,$log); 
                    $log->debug("$email: API Failed, trying password via flat file");
                }
            }
        }
    }

    my $msgRes = com::owm::imapAppend::imapAppendMessage($owmImapServer, $email, $pwd, $welcomeEN, $email, 'WLW');

    if( $msgRes )
    {
        $log->error("$LOGSTRING:Cannot append welcome e-mail message to MX9:$msgRes");
    }
}
