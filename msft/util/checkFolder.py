#!/usr/bin/env python

#############################################################################
#
#  Copyright 2016 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 checkFolder.py
#  Description:             Check for a message in a folder via mOS
#  Author:                  Gary Palmer - Openwave Messaging
#  Date:                    March, 2016
#  Customer:                Telstra
#  Version:                 1.0.0 - Nov 2016
#
#############################################################################
#
#  Version Control
#
#  Version: 1.0.0
#  Changes:
#
#############################################################################

# pylint: disable=C0103
# pylint: disable=W0703

import signal

from pprint import pprint

from optparse import OptionParser
# requests objects to running with a non-default signal handler for SIGCHLD
# temporarily reset it and then put it back
oldSigChld = signal.getsignal(signal.SIGCHLD)
signal.signal(signal.SIGCHLD, signal.SIG_DFL)
import requests
signal.signal(signal.SIGCHLD, oldSigChld)

parser = OptionParser()
parser.add_option('--email', action='store', type='string', dest='email',
                  help='fully qualified email address for the user')
parser.add_option('--folder', action='store', type='string', dest='folder',
                  help='full folder path')
parser.add_option('--uid', action='store', type='int', dest='uid',
                  help='UID of the message to look for')
parser.add_option('--msgid', action='store', type='string', dest='msgid',
                  help='Message-ID of the message to look for')
parser.add_option('--mosHost', action='store', dest='mosServer',
                  help='IP address or hostname of the mOS server used for migration')
parser.add_option('--mosPort', action='store', dest='mosPort',
                  help='TCP port number for the mOS server used for migration')
try:
    (options, args) = parser.parse_args()
except Exception, e:
    print u'Internal error processing options: {0}'.format(str(e))
    exit(1)

mosUrl = 'http://{0}:{1}/mxos/mailbox/v2/{2}/folders/{3}/messages/metadata/list'
mosUrl = mosUrl.format(options.mosServer, options.mosPort, options.email,
                       options.folder)
result = requests.get(mosUrl)
if result.status_code == 200:
    data = result.json()
    maxUid = 0
    uidFound = False
    msgIdFound = False
    msgIdUid = 0
    for key in data:
        value = data[key]
        if value['uid'] > maxUid:
            maxUid = value['uid']
        if value['uid'] == options.uid:
            uidFound = True
        if value['oldMsgId'] == options.msgid:
            msgIdFound = True
            msgIdUid = value['uid']

    if uidFound:
        msg = 'UID "{0}" found in target folder "{1}" for user "{2}"'
        print msg.format(options.uid, options.folder, options.email)
    else:
        if maxUid < options.uid:
            msg = 'Last UID in target folder "{0}" for user "{1}" according '
            msg = msg + 'to mxos is {2}, which is less than the requested '
            msg = msg + 'UID of {3}'
            print msg.format(options.folder, options.email, maxUid, options.uid)
        else:
            msg = 'Last UID in target folder "{0}" for user "{1}" according '
            msg = msg + 'to mxos is {2}, which is greater than the requested '
            msg = msg + 'UID of {3}, however the requested UID was not found'
            print msg.format(options.folder, options.email, maxUid, options.uid)

    if msgIdFound:
        msg = 'Requested Message-ID was found with a UID of {0} in folder {1} '
        msg = msg + 'for user "{2}"'
        print msg.format(msgIdUid, options.folder, options.email)
    else:
        msg = 'Requested Message-ID was NOT found in folder "{1}" '
        msg = msg + 'for user "{2}"'
        print msg.format(msgIdUid, options.folder, options.email)
