#!/usr/bin/perl
#############################################################################
#
#  Copyright 2015 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 checkMsftTz.pl
#  Description:             Check timezones provided by MSFT
#  Author:                  Gary Palmer - Openwave Messaging
#  Date:                    Dec, 2015
#  Customer:                Telstra
#  Version:                 1.0.0 - Dec 2015
#
#############################################################################
#
#  Version Control
#
#  Version: 1.0.0
#  Changes:
#
#############################################################################

use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration

use strict;
use warnings;

# CPAN modules
use Config::Simple;
use DateTime::TimeZone;

# custom modules
use com::owm::OlsonToVTimezone;

###########################################################################
# Global variables
###########################################################################

$| = 1;    ## Makes STDOUT flush immediately

our $configPath = 'conf/migrator.conf';
our $configLog = 'conf/log.conf';

# Logging object.
# Log::Log4perl->init($configLog);
# our $log = get_logger('MSFT.SETUP');

# Config object.
my $cfg  = new Config::Simple();
unless($cfg->read($configPath))
{
    print 'FATAL: '.$cfg->error().'. Please fix the issue '.
          'and restart the program. Exiting.'."\n";
    exit(1);
}

my $accountFile = $cfg->param('in_account');
unless (defined($accountFile))
{
    print 'FATAL: "in_account" configuration option not found in '.
          'migrator.conf.  Exiting.'."\n";
    exit(1);
}

unless (-f $accountFile)
{
    print 'FATAL: "'.$accountFile.'" (from "in_account" configuration '.
          'option) is not a file.  Exiting'."\n";
    exit(1);
}

unless (-r $accountFile)
{
    print 'FATAL: "'.$accountFile.'" (from "in_account" configuration '.
          'option) is not readable.  Exiting'."\n";
    exit(1);
}

my $input;
unless(open($input, '<', $accountFile))
{
    print 'FATAL: Cannot open "'.$accountFile.'" (from "in_account" '.
          'configuration option): '.$!."\n";
    print 'Exiting.'."\n";
    exit(1);
}

my %tzHash;

while (my $line = <$input>)
{
    my @fields = split(/,/, $line);
    if (defined($fields[3]) && (length($fields[3]) > 0))
    {
        my $tz = $fields[3];
        $tzHash{$tz} += 1;
    }
}
close($input);

foreach my $tz (sort(keys(%tzHash)))
{
    my $vtimezone = com::owm::OlsonToVTimezone::getVtimezone($tz, $cfg);
    unless(defined($vtimezone))
    {
        print 'Timezone "'.$tz.'" which was found in "in_account" '.
            'file '.$tzHash{$tz}.' times, was not found when doing '.
            'getVtimezone() lookup'."\n";
    }
}
