#!/bin/awk -f
#
# Telstra, d363756, 31.7.2015
#
# Use:
#  getPUIDs.awk [INPUT FILES]
#
# Result:
#  listOfPUIDs.csv
#
# Expected input files:
#      Partner_Account.csv
#      Partner_MailForwarding.csv
#      Partner_Signatures.csv
#      Partner_UserLists.csv
#      Partner_VacationReplies.csv
#
# Output:
#  Result file
#  Console messages (errors and results)
#
# Example:
#   cat Partner_*.csv | getPUIDs.awk
#
BEGIN {
  FS=",";
  print "Usage: getPUIDs.awk [INPUT FILES]";
  print "";
  print "EmailAddress,PUID,AppearanceCount" > "listOfPUIDs.csv";
}

{
  # Do we have a conflicting PUID?
  if (aEmailPUID[$1] == "" || length(aEmailPUID[$1]) == 0) {
    aEmailPUID[$1] = $2;
    aPUIDAppearanceCount[$1] = 1;
  }
  else {
    if (aEmailPUID[$1] != $2) {
      print "ERROR: Assigned PIUD " aEmailPUID[$1] " for " $1 " is now presented as " $2;
    }
    else {
      aPUIDAppearanceCount[$1]++;
    }
  }
}

END {
  for (sIndex in aEmailPUID) {
    print sIndex "," aEmailPUID[sIndex] "," aPUIDAppearanceCount[sIndex] >> "listOfPUIDs.csv";
  }
}


