#!/usr/bin/perl

# --------------------------------------------------------------------------- #
# Purpose:  Monitor the migration log for failed password attempts and
#           if user has accessed their account withing 90 days check if
#           the user can access via imap
#
# Required: migration.log
#           ms.dat
#
# Options:  n/a
#
# Example:  imap-check.plx
# --------------------------------------------------------------------------- #

use lib "/opt/owm/migration/migration_tool/lib";
use lib "/opt/owm/migration/migration_tool/migperl";
use lib "/opt/owm/migration/migration_tool/migperl/lib/perl5";

use strict;
use warnings;
use Time::Local;
use Mail::IMAPClient;
use Digest::MD5;

my $log_file = '/var/tmp/SMT/imap-check.log';
my $migration_log = '/opt/owm/migration/migration_tool/log/migration.log';
my $migration_err = '400 - Bad Request';
my $imap_fail_log = '/var/tmp/SMT/imap-check.fail';
my $imap_pass_log = '/var/tmp/SMT/imap-check.pass';
my $imap_skip_log = '/var/tmp/SMT/imap-check.skip';
my $imap_secret_log = '/var/tmp/SMT/imap-check.secret';
my $access_log = '/var/tmp/SMT/20160401000000-MX8Export.dat';

# --------------------------------------------------------------------------- #
# Main routine
# --------------------------------------------------------------------------- #
if (! -e $access_log){die "Error: Data file $access_log not found.\n"}

write_to_log ("IMAP password check process started");
write_to_log ("IMAP password failures detected...");

# Open and tail migration.log
open (MIGRATION_LOG, "/usr/bin/tail -n 0 -F $migration_log|") or die "Error: Can't open $migration_log: $!";

while (<MIGRATION_LOG>)
{
  # If a password failure is detected check if user has accessed account within 90 days and
  # attempt to access user's account via imap
  # only check those logs errors related to processConsent

  next if ($_ !~ /processConsent/);
  if ($_ =~ m/$migration_err/)
  {
    chomp;
    my @line = split /\|/, $_;
    my $email = $line[8];
    write_to_log ($email);
    my $action = check_last_access ($email);
    
    if ($action eq 'P')
    {
        my $password = get_password ($email);
        my $hashPWD = `echo "$password" | openssl md5`;
        chomp($hashPWD);
        my ($imap_result, $imap_error) = check_imap_access ($email, $password);
my $pwdEnc = `echo -n "\\\"$password\\\"" | openssl enc -base64 -e -aes-256-cbc -nosalt -pass pass:My_5ecret`;
chomp($pwdEnc);
        
        if ($imap_result eq 'F')
        {
          write_to_log (" FAIL: IMAP login failed: ".$imap_error);
          write_to_secret ($email." ". $hashPWD." FAIL " . $pwdEnc);
          write_to_fail ($email.': '.$imap_error);
        }
        else
        {
          write_to_log (" PASS: IMAP login passed");
          write_to_secret ($email." ". $hashPWD." PASS " . $pwdEnc);
          write_to_pass ($email);
        }
    }
    else
    {
      write_to_log (" SKIP: Last account access over 90 days or user data not found");
      write_to_skip ($email);
    }
    
   }
}

# --------------------------------------------------------------------------- #
# Log script events
# --------------------------------------------------------------------------- #
sub write_to_log
{
    open (LOG,">> $log_file") or die "Error: Can't open $log_file\n";
    
    my ($sec,$min,$hour,$mday,$mon,$year) = localtime(time);
    my $timestamp = sprintf('%u.%02u.%02u %02u:%02u:%02u',$year+1900, $mon+1, $mday, $hour, $min, $sec);
    
    print LOG "[$timestamp] @_\n";
    close LOG;
}

# --------------------------------------------------------------------------- #
# Log failed IMAP logins
# --------------------------------------------------------------------------- #
sub write_to_fail
{
    open (LOG,">> $imap_fail_log") or die "Error: Can't open $imap_fail_log: $!\n";
    
    print LOG "@_\n";
    close LOG;
}

# --------------------------------------------------------------------------- #
# Log failed IMAP logins
# --------------------------------------------------------------------------- #
sub write_to_secret
{
    open (LOG,">> $imap_secret_log") or die "Error: Can't open $imap_secret_log $!\n";
    my ($sec,$min,$hour,$mday,$mon,$year) = localtime(time);
    my $timestamp = sprintf('%u.%02u.%02u %02u:%02u:%02u',$year+1900, $mon+1, $mday, $hour, $min, $sec);

   print LOG "@_ [$timestamp]\n";
    close LOG;
}


# --------------------------------------------------------------------------- #
#
# Log passed IMAP logins
# --------------------------------------------------------------------------- #
sub write_to_pass
{
    open (LOG,">> $imap_pass_log") or die "Error: Can't open $imap_pass_log: $!\n";
    
    print LOG "@_\n";
    close LOG;
}

# --------------------------------------------------------------------------- #
# Log skipped IMAP logins
# --------------------------------------------------------------------------- #
sub write_to_skip
{
    open (LOG,">> $imap_skip_log") or die "Error: Can't open $imap_skip_log: $!\n";

    print LOG "@_\n";
    close LOG;
}

# --------------------------------------------------------------------------- #
# Convert date to epoch
# --------------------------------------------------------------------------- #
sub to_epoch
{
  my ($t) = @_; 
  my ($y, $m, $d) = ($t =~ /(\d{4})(\d{2})(\d{2})/);
  
  return timelocal (0, 0, 0, $d+0, $m-1, $y-1900);
}

# --------------------------------------------------------------------------- #
# Get the number of days difference between two dates
# --------------------------------------------------------------------------- #
sub days_diff
{
  my ($t1, $t2) = @_;
  
  return (abs (to_epoch($t2) - to_epoch($t1))) / 86400;
}

# --------------------------------------------------------------------------- #
# Get the users password
# --------------------------------------------------------------------------- #
sub get_password
{
  my ($user) = @_;
  
  chdir ('/opt/owm/migration/migration_tool');  
  my $password = `./getAuth.plx $user`;
  chdir ('/var/tmp');  
  
  return $password;
}

# --------------------------------------------------------------------------- #
# Check if the last access date for the user is less than 90 days
# --------------------------------------------------------------------------- #
sub check_last_access
{
  my $action = 'P'; # Proceed
  
  my $grep = `grep -Fw  @_ $access_log`;
  
  if ($grep ne "")
  {
     my @line = split /\|/, $grep;
     my $last_access_datestamp = $line[4];
     
     if ($last_access_datestamp ne "")
     { 
        # get the current datestamp
        my @today = (localtime)[5,4,3]; $today[0] += 1900; $today[1]++;
        my $current_datestamp = $today[0] . sprintf ("%02d",$today[1]) . sprintf ("%02d",$today[2]);
  
        # check the last_access datestamp vs the current datestamp to get difference in days
        my $num_days_diff = days_diff ($last_access_datestamp, $current_datestamp);
        if ($num_days_diff > 90) {$action = 'S';} # Skip
     }
     else
     {
        $action = 'S';
     }
  }
  else
  {
     $action = 'S';
  }

  return $action;
}

# --------------------------------------------------------------------------- #
# Check imap access for the user
# --------------------------------------------------------------------------- #
sub check_imap_access
{
  my $imap_server = '207.46.11.202';
  my $imap_user = $_[0];
  my $imap_password = $_[1];
  my $connect = 'F';
  my $login = 'T';
  my $retryCount = 0;
  my $imap;
  my $imap_error;
  
  while($connect eq 'F' && $retryCount < 5)
  {
    $connect = 'T';
    $imap = Mail::IMAPClient->new (Server => $imap_server, Ssl => 1) or $connect = 'F'; 
  }

  $imap->Showcredentials(1);
  if ($connect eq 'T')
  {
    $imap->User($imap_user);
    $imap->Password($imap_password);
    $imap->login() or $login = 'F';
    if ($login eq 'F') { $imap_error = $imap->LastError; }
#    my $err = $imap->LastError;
#    print "err=>$err\n";   
    if ($login eq 'T') {$imap->logout or die "Logout error: ", $imap->LastError, "\n";}
  }
  else 
  {
    write_to_log('Cannot connect to 207.46.11.202 IMAP server');
  }

  return ($login, $imap_error);
}