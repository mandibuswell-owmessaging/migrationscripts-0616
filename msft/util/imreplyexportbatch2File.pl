eval 'exec ${PERLHOME}/bin/perl -S $0 ${1+"$@"}' # -*- perl -*-
    if 0;
# $Id: imfolderlist
# -------------------------------------------------------------------------
#   File:       imreplyexportbatch2File
#   Author:     Huanwen Jiao, Openwave (this is a unsupported tool)
#   Version:    1.0
#               
#   This scripted is based on Andrew Perkins imreplyexportbatch v 1.0
#
#   Description:
#       The script takes an input file of mailbox no. (one per line)
#       and exports all Auto-reply messages to files. Output is
#       similar to imreplyexport apart from each mailbox is
#
# 
# -------------------------------------------------------------------------
use lib '/opt/owm/migration/Mx8.1/perl/lib';




use Getopt::Long;
use SwCom::Mail::All;
                                                                               
sub usage{ 
                                                                                
    print STDERR qq(                                                            
    Usage:                                                                      
        $commandName -inputFile inputFile.txt [-targetDir target]
    where:
		inputFile = a list of users to extract data for in the format email address, mailbox id
		targetDir = the directory to put the export data into.

	);                                                                              
                                                                                
    exit -1;                                                                   
} 

# -------------------------------------------------------------------------
# dumpAutoReplyMessages
#
# $account   - A reference to a account object which has already been read
# Dumps all auto-reply messages from mailbox into text file
# -------------------------------------------------------------------------
sub dumpAutoReplyMessages($$){
  $account = shift;
  $target = shift;

  my $hostname = `hostname`;

  # Open destination file
  $accountId = $account->emailIntID;
  $emailaddress = $account->smtpAddress;
  $fileName = "$target/$emailaddress.txt";
 open ( OUTPUTFILE, ">$fileName");

  # For each auto-prely type, check if there is a message associated
  for($i=1;$i<11;$i++){
     $reply = new Reply(
                     account => $account,
                     host    => $hostname,
                     type    => $i
                 );

     $reply->Read();
     $autoReplyMsg = $reply->text();

     # IF there is a autoreply of this type on DB, export it
     if($autoReplyMsg ne ""){
       print OUTPUTFILE "\n[Type:$i]\n";
       print OUTPUTFILE "$autoReplyMsg\n";
       print $STDERR "---> auto-repy of type $i\n" if $DEBUG;
     }

  }
	 close OUTPUTFILE;
}
                                                  
# -------------------------------------------------------------------------
#
# Begin Running...
#
# -------------------------------------------------------------------------

$DEBUG=0;
$commandName = $0;
@segs=split '/', $commandName;
$commandName = $segs[$#segs];
my $accountId;
my $emailAddress;

# -------------------------------------------------------------------------
#
# Parse Command line
#
# -------------------------------------------------------------------------

GetOptions( '-d',\$SDEBUG,               # Override debug in config file.
            '-inputFile=s',\$inputFile,
            '-targetDir=s',\$targetDir,
            ) ;#|| usage();
=pod
if ( $inputFile eq ""  ){
	print "\n no inputFile\n"
}else {
	print "\n inputFile=$inputFile\n"
}
=cut
if ( !$targetDir || $targetDir eq "" ){
	print "\n Target directory is required. \n";
	usage;
}else {
	if ( ! -d $targetDir) {
	 print "\n $targetDir not exist\n";
	 exit;
	}
}

# This script must be run as the Mx8 intermail user. Abort if it is not the case.
my $intermail = `echo \$INTERMAIL`;
chomp($intermail);
print "intermail check result \'$intermail\' \n" if ($SDEBUG);
if ( ! $intermail || $intermail eq "" || not defined $intermail || $intermail eq "\n") {
	$log->error("SCRIPT|$inputFile jobid: $JOBID  cannot be started. Must be run as Mx8 IMAIL user");
	print "Script must be run as an MX8 IMAIL user \n" if ($SDEBUG);
	com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::STATUS_MIG_NOTSTARTED,"MigrationNotStarted",$MODE,$inputFile,'','','Could not start, must run as an Mx8 IMAIL user');
	exit 1;
}
	
$logPath = "log";
#if ( !-d $logPath) {
# mkdir ($logPath);
#}
#$log = "log/export.err";
open (ERRLOG, "<../log/export.err");
#$inputFile=shift;
usage() if ! open (INPUTFILE, "<$inputFile");

# -------------------------------------------------------------------------
#
# Process each line of the input file & dump the auto reply data.
#
# -------------------------------------------------------------------------

# Each line of the input file should contain email address, mailbox ID
# Email address is not used, but the input file is shared as input for this and migrateBatch which requires the emailAddress
# So for each line try to open the account and dump the auto reply data.

while (<INPUTFILE>){
   ($emailAddress, $accountId) = split(/,/,$_);
   chomp($accountId);

   # Initialize Mailbox...
   im_init();
   $account = new Account(emailIntID=>"$accountId");

   # Read Mailbox...
   if (!$account->Read()){
      # Dump out error. [[$accountId]]\n<msg>\n format is used for
      # error so if STDERR & STDOUT are directed to the same file (by mistake)
      # the import with not import the error msg into another accounts
      # auto reply data.

      print STDOUT "[[Mailbox:$accountId]]\nError reading account\n";
        $altAccountId = "-".$accountId;
        print STDOUT "\nTrying alternative mailbox id\n";
        im_init();
        $altAccount = new Account(emailIntID=>"$altAccountId");
        if (!$altAccount->Read()){
           print STDOUT "[[Mailbox:$altAccountId]]\nError reading account\n\n";
        } else {
           print STDOUT "[[Mailbox:$altAccountId]] Success\n";
           dumpAutoReplyMessages($altAccount, $targetDir);
        }

   } else {
      print STDOUT "[[Mailbox:$accountId]] Success\n";
      dumpAutoReplyMessages($account, $targetDir);

   }
}

close ERRLOG;
exit 0;
