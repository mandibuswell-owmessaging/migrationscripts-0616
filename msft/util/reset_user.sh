#!/bin/bash
User=$1
D="cn=root"

echo $1 >> files/user_email.csv

./update4Mx9.plx -puidFile  files/user_email.csv  -updatemx

rm files/user_email.csv

# Remove address book and calendar from the account

util/resetUxUsers6.plx -email $User -d

## revert mailbox status and account names
#OIFS="$IFS"
#IFS='@'
#read -a address <<< "${User}"
#IFS='.'
#read -a domain <<< "${address[1]}"
#
#for dom in "${domain[@]}"
#do
#Domain="$Domain,dc=${dom}"
#done
#
#IFS="$OIFS"
#
#echo "dn: mail=${User}${Domain}
#changetype: modify
#replace: mailboxstatus
#mailboxstatus: P
#-
#replace: cn
#cn: $User
#-
#replace: sn
#sn: $User"|ldapmodify -D $D -w secret -h mcb-host3-as5 -p 389

# delete migrated attributes

#echo "dn: mail=$User${Domain}
#changetype: modify
#delete: migstatus"|ldapmodify -D $D -w secret -h mcb-host3-as5 -p 389
#
#echo "dn: mail=$User${Domain}
#changetype: modify
#delete:mailautoreplymode"|ldapmodify -D $D -w secret -h mcb-host3-as5 -p 389
#
#echo "dn: mail=$User${Domain}
#changetype: modify
#delete:maildeliveryoption"|ldapmodify -D $D -w secret -h mcb-host3-as5 -p 389
#
#echo "dn: mail=$User${Domain}
#changetype: modify
#delete:netmaillocale"|ldapmodify -D $D -w secret -h mcb-host3-as5 -p 389
#
#echo "dn: mail=$User${Domain}
#changetype: modify
#delete:msglocale"|ldapmodify -D $D -w secret -h mcb-host3-as5 -p 389
#
#echo "dn: mail=$User${Domain}
#changetype: modify
#delete:msgtimezone"|ldapmodify -D $D -w secret -h mcb-host3-as5 -p 389
#
#echo "dn: mail=$User${Domain}
#changetype: modify
#delete:mailforwardingaddress"|ldapmodify -D $D -w secret -h mcb-host3-as5 -p 389
#
#echo "dn: mail=$User${Domain}
#changetype: modify
#delete:mailforwarding"|ldapmodify -D $D -w secret -h mcb-host3-as5 -p 389
